/* eslint-env jest */

/*
// TODO: Upgrade React 16
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });
*/

import MockAsyncStorage from 'mock-async-storage';

const mockImpl = new MockAsyncStorage();
jest.mock('AsyncStorage', () => mockImpl);


// require('react-native-mock-render/mock');

// https://github.com/gruntjs/grunt-contrib-watch/issues/354
if (process.setMaxListeners) {
  process.setMaxListeners(10000);
}

// See https://github.com/facebook/jest/issues/2208
jest.mock('Linking', () => ({
  addEventListener: jest.fn(),
  removeEventListener: jest.fn(),
  openURL: jest.fn(),
  canOpenURL: jest.fn(),
  getInitialURL: jest.fn().mockImplementation((value: string) => Promise.resolve(value)),
}));

// See https://github.com/facebook/react-native/issues/11659
jest.mock('ScrollView', () => {
  const RealComponent = require.requireActual('ScrollView');
  class ScrollView extends RealComponent {
    scrollTo = () => {}
  }
  return ScrollView;
});

jest.mock('NetInfo', () => {
  return {
    isConnected: {
      fetch: () => Promise.resolve(true),
      addEventListener: () => {}
    },
    addEventListener: () => {}
  };
});

jest.mock('ListView', () => {
  const mockComponent = require.requireActual('react-native/jest/mockComponent');
  const RealListView  = require.requireActual('ListView');
  const ListView = mockComponent('ListView');
  ListView.prototype.render = RealListView.prototype.render;
  return ListView;
});

// calculate styles
import lightTheme from '../src/themes/light';
import EStyleSheet from 'react-native-extended-stylesheet';
EStyleSheet.build(lightTheme);

jest.mock('react-native-device-info', () => {
  const pack = require('../package.json');
  const os   = require('os');

  return {
    getVersion: () => pack.version,
    getBuildNumber: () => 'node',
    getSystemName: () => os.platform(),
    getSystemVersion: () => process.version,
  };
});

jest.mock('react-native-status-bar-size', () => {
  return {
    currentHeight: 20,
  };
});

jest.mock('InteractionManager', () => {
  return {
    runAfterInteractions: (method) => method(),
  };
});

jest.mock('react-native-i18n', () => {
  const langKey = 'en';
  return {
    translate: (scope, ...args) => {
      // console.log('scope: ', scope, ' args: ', args);
      return `[missing "${langKey}.${scope}" translation]`;
    }
  };
});

jest.mock('react-native-globalize', () => {
  class Globalize {

  }

  return {
    Globalize: Globalize
  };
});

jest.mock('react-native-showdown', () => {
  return {};
});

jest.mock('react-native-view-shot', () => {
  return {
    takeSnapshot: () => ''
  };
});

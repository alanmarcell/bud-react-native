# iOS and Android Mobile Client do Budbuds.us API

This repository contains the iOS and Android mobile client for the BudBuds.us API. We're using react native for our UI implementation and Redux for our mais architecture.

### Requirements

The docs will be aimed at OS X development machine, since its mandatory for iOS app building.

You will need for iOS:
- OS X El Captain
- X Code > 7
- Node > 4

You will need for Android:
- unknown

### Installation

First we need to clone the project:
> git clone

Then install its dependencies:
> npm install

Its nice to have React Native CLI interface:
> npm install -g react-native-cli

## Running
#### iOS

Open the .xcodeproj in the ios/ folder in the app folder. Hit run and wait for the Native React app compile. If u change the code you can just press cmd+R and it will be refreshed.

## Development Workflow

We are using a gitlab flow based workflow: https://about.gitlab.com/2014/09/29/gitlab-flow/
Commit as frequent as possible

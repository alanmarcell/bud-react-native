#!/usr/bin/env bash

cd $BUDDYBUILD_WORKSPACE

# Set configuration
case "${BUDDYBUILD_BRANCH}" in
  stable )
    echo "Setting production configuration"
    ln -s envs/production.env .env.production
    ln -s envs/production.env .env.development
    ;;
  * )
    echo "Setting staging configuration"
    ln -s envs/staging.env .env.production
    ln -s envs/staging.env .env.development
esac

import React from 'react';
import { View, Text } from 'react-native';
import { readJson } from 'utils/files';

export default class App extends React.Component {
  render() {
    readJson('strains_objs').then((data) => {
      console.log(data);
    });

    return(
      <View>
        <Text>Lendo arquivos</Text>
      </View>
    );
  }
}

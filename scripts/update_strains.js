#!/usr/bin/env yarn run babel-node

import { data as strains } from './dumps/strains';
import R from 'ramda';
import lunr from 'lunr';
import fs from 'fs';
import path from 'path';

console.log('Create index...');
const index = lunr(function () {
  this.field('name', { boost: 10 });
  this.field('breeder');
  this.field('type');
  this.ref('_id');
});

console.log('Adding itens to index and map');
const objs = R.fromPairs(R.map((item) => {
  index.add(item);
  const { _id, ...strain } = item;
  return [_id, strain];
}, strains.slice(0, 1000)));

const assets = path.resolve(__dirname, '..', 'android/app/src/main/assets/');

console.log('Save objects');
const objs_path = path.join(assets, 'strains_objs.json');
fs.writeFileSync(objs_path, JSON.stringify(objs));

// console.log('Save index');
// const index_path = path.join(assets, 'strains_idx.json');
// fs.writeFileSync(index_path, JSON.stringify(index));

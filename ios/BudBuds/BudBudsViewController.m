//
//  @interface BudBudsViewController : UIViewController @end BudBudsViewController.m
//  BudBuds
//
//  Created by Everton Ribeiro on 02/02/17.
//  Copyright © 2017 Budbuds. All rights reserved.
//

#import "BudBudsViewController.h"

@implementation BudBudsViewController

- (BOOL)shouldAutorotate {
  return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
  return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
}

@end

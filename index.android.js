import { AppRegistry } from 'react-native';
export { default as SimpleSchema } from '@clayne/simple-schema';

global.__IOS__     = false;
global.__ANDROID__ = true;

import App from './src/main';
// import App from './scripts/lab/fs';
AppRegistry.registerComponent('BudBuds', () => App);

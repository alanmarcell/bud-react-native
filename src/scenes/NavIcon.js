import React from 'react';
import { Text } from 'react-native';

import Icon from 'components/Icon';
import { Touchable } from 'utils/components';

import { default as ScenesStyles } from './styles.js';

export default class NavIcon extends React.Component {
  static propTypes = {
    onPress : React.PropTypes.func,
    iconName: React.PropTypes.string,
    text: React.PropTypes.string,
  }

  render() {
    const { onPress, text, iconName } = this.props;
    let content = null;
    if (text) {
      content = <Text style={ScenesStyles.rightText}>{text}</Text>;
    } else {
      content = <Icon style={ScenesStyles.rightIcon} name={iconName} />;
    }

    return (
      <Touchable style={ScenesStyles.iconContainer} onPress={onPress}>
        {content}
      </Touchable>
    );
  }
}

import React from 'react';

import { Actions, Scene, Modal } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { I18n } from 'utils/I18n';
import Icon from 'components/Icon';

// Geral
import LoadingModal from 'controllers/LoadingModal';

// UnLogged area
import SignInController from 'controllers/SignInController';
import PreRegisterController from 'controllers/PreRegisterController';
import UserFeaturesController from '../controllers/UserFeaturesController';
import RegisterController from 'controllers/RegisterController';
import TermsController from 'controllers/TermsController';
import PrivacyController from 'controllers/PrivacyController';
import RecoveryTokenController from 'controllers/RecoveryTokenController';
import RecoveryController from 'controllers/RecoveryController';

// Logged area
import DrawerController from 'controllers/DrawerController';
import TourController from 'controllers/TourController';

// Plants
import {
  PlantsController,
  PlantProfileController,
  PlantEditController
} from 'controllers/Plants';

// Tasks
import {
  TasksController,
  TaskProfileController,
  TaskEditController
} from 'controllers/Tasks';

// Mixes
import {
  MixesController,
  MixProfileController,
  MixEditController
} from 'controllers/Mixes';

// Buds
import { BudsController, BudProfileController, BudEditController } from 'controllers/Buds';


// Buds' Review
import ReviewEditController from 'controllers/ReviewEditController';
import ReviewController from '../controllers/ReviewController';

// QRCode
import QuickIDController from 'controllers/QuickIDController';
import QuickIDShareController from 'controllers/QuickIDShareController';

// Contact
import ContactController from 'controllers/ContactController';

// Photos
import AddPhotoController from '../controllers/AddPhotoController';
import GaleryController from '../controllers/GaleryController';

// Cups
import {
  CupProfileController,
  CupsController,
  CupEditController,
} from '../controllers/Cups';

// Editions
import {
  EditionProfileController,
  EditionEditController
} from '../controllers/Editions';

// Categories
import {
  CategoryEditController,
  CategoryProfileController,
  AddBudToCategoryController
} from '../controllers/Categories';


import {
  JudgeEditController,
  JudgeProfileController,
  JudgeAssocController,
  JudgesController
} from '../controllers/Judges';

import { default as ScenesStyles } from './styles.js';
export { ScenesStyles };

import TabIcon from './TabIcon';
import BackButton from './BackButton';
import RootController from '../controllers/RootController';

function getSceneStyle(state, props) {
  const { hideNavBar, hideTabBar } = props;
  return [
    ...(!hideNavBar ? [ScenesStyles.container] : []),
    ...(!hideTabBar ? [ScenesStyles.intoTab] : [])
  ];
}

function stackScreen(component) {
  return {
    component,
    getSceneStyle,
    backButton: BackButton,
    leftButtonStyle: ScenesStyles.leftButtonStyle,
    getTitle: ({ title }) => I18n.T(title),
    ...component.navigationOptions || {},
  };
}

class Switch extends RootController {
  componentDidMount() {
    setTimeout(_ => {
      this.redirectTo('logged_' + this.props.activeUserType.toLowerCase());
    }, 100);
  }

  render() {
    return <LoadingModal />;
  }
}

const mapStateToProps = state => ({
  activeUserType: state.user.userType
});

const ConnectedSwitch = connect(mapStateToProps)(Switch);

function Scenes(app) {
  const drawerIcon = <Icon style={ScenesStyles.menuIcon} name="menu" />;

  return Actions.create(
    <Scene key="modal" component={Modal} >
      <Scene key="root">

        <Scene key="unLogged" type="replace" hideTabBar hideNavBar={false} {...app.passProps} passProps>
          <Scene key="signIn" {...stackScreen(SignInController) } />
          <Scene key="preRegister" {...stackScreen(PreRegisterController) } />
          <Scene key="userFeatures" {...stackScreen(UserFeaturesController) } />
          <Scene key="terms" {...stackScreen(TermsController) } />
          <Scene key="privacy" {...stackScreen(PrivacyController) } />
          <Scene key="register" {...stackScreen(RegisterController) } />
          <Scene key="recoveryToken" type="replace" {...stackScreen(RecoveryTokenController) } />
          <Scene key="recoveryAccount" {...stackScreen(RecoveryController) } />
          <Scene key="firstTour" {...stackScreen(TourController) } firstTour={true} hideNavBar />
        </Scene>

        <Scene key="logged" component={ConnectedSwitch} type="reset" />

        <Scene key="logged_grower" component={DrawerController} type="replace" {...app.passProps}>

          <Scene key="grower" type="replace" tabs tabBarStyle={ScenesStyles.tabBar}>
            <Scene key="grower_tasksTab" title="tasks" iconName="calendar-check-o" icon={TabIcon} drawerIcon={drawerIcon}>
              <Scene key="grower_tasksList" {...stackScreen(TasksController) } />
              <Scene key="grower_taskProfile" {...stackScreen(TaskProfileController) } />
              <Scene key="grower_taskEdit" {...stackScreen(TaskEditController) } />
            </Scene>
            <Scene key="grower_mixesTab" title="mixes" iconName="mixes" icon={TabIcon} drawerIcon={drawerIcon}>
              <Scene key="grower_mixesList" {...stackScreen(MixesController) } />
              <Scene key="grower_mixProfile" {...stackScreen(MixProfileController) } />
              <Scene key="grower_mixEdit" {...stackScreen(MixEditController) } />
              <Scene key="grower_mixAdd" {...stackScreen(MixEditController) } />
            </Scene>
            <Scene key="grower_plantsTab" title="plants" iconName="budbuds" icon={TabIcon} drawerIcon={drawerIcon} initial={true} >
              <Scene key="grower_plantsList" {...stackScreen(PlantsController) } />
              <Scene key="grower_plantProfile" {...stackScreen(PlantProfileController) } />
              <Scene key="grower_plantEdit" {...stackScreen(PlantEditController) } />
              <Scene key="grower_plantAdd" {...stackScreen(PlantEditController) } />
              <Scene key="grower_plantQrCode" {...stackScreen(QuickIDShareController) } />

              <Scene key="grower_plantTaskProfile" {...stackScreen(TaskProfileController) } />
              <Scene key="grower_plantTaskEdit" {...stackScreen(TaskEditController) } />
              <Scene key="grower_plantTaskAdd" {...stackScreen(TaskEditController) } />

              <Scene key="grower_plantBudAdd" {...stackScreen(BudEditController) } />
              <Scene key="grower_plantPhotoAdd" {...stackScreen(AddPhotoController) } />
              <Scene key="grower_plantGalery" {...stackScreen(GaleryController) } />
            </Scene>
            <Scene key="grower_budsTab" title="buds" iconName="bud" icon={TabIcon} drawerIcon={drawerIcon}>
              <Scene key="grower_budsList" {...stackScreen(BudsController) } {...app.passProps} />
              <Scene key="grower_budProfile" {...stackScreen(BudProfileController) } />
              <Scene key="grower_budEdit" {...stackScreen(BudEditController) } />
              <Scene key="grower_budAdd" {...stackScreen(BudEditController) } />
              <Scene key="grower_budPhotoAdd" {...stackScreen(AddPhotoController) } />
              <Scene key="grower_budQrCode" {...stackScreen(QuickIDShareController) } />
              <Scene key="grower_reviewAdd" {...stackScreen(ReviewEditController) } />
              <Scene key="grower_reviewDetails" {...stackScreen(ReviewController) } />
              <Scene key="grower_budGalery" {...stackScreen(GaleryController) } />
            </Scene>
            <Scene key="grower_cupsTab" title="cups" iconName="trophy" drawerIcon={drawerIcon} >
              <Scene key="grower_cups_cupsList" {...stackScreen(CupsController) } />
              <Scene key="grower_cups_cupProfile" {...stackScreen(CupProfileController) } {...app.passProps} />
              <Scene key="grower_cups_cupAdd" {...stackScreen(CupEditController) } />
              <Scene key="grower_cups_cupEdit" {...stackScreen(CupEditController) } />
              <Scene key="grower_cups_cupQrCode" {...stackScreen(QuickIDShareController) } />

              <Scene key="grower_cups_editionProfile" {...stackScreen(EditionProfileController) } />
              <Scene key="grower_cups_editionAdd" {...stackScreen(EditionEditController) } />
              <Scene key="grower_cups_editionEdit" {...stackScreen(EditionEditController) } />
              <Scene key="grower_cups_editionQrCode" {...stackScreen(QuickIDShareController) } />
              <Scene key="grower_cups_editionPhotoAdd" {...stackScreen(AddPhotoController) } />
              <Scene key="grower_cups_editionGalery" {...stackScreen(GaleryController) } />

              <Scene key="grower_cups_categoryEdit" {...stackScreen(CategoryEditController) } />
              <Scene key="grower_cups_categoryProfile" {...stackScreen(CategoryProfileController) } />
              <Scene key="grower_cups_addBudsToCategory" {...stackScreen(AddBudToCategoryController) } />

              <Scene key="grower_cups_judgesList" {...stackScreen(JudgesController) } />
              <Scene key="grower_cups_judgeEdit" {...stackScreen(JudgeEditController) } />
              <Scene key="grower_cups_judgeProfile" {...stackScreen(JudgeProfileController) } />
              <Scene key="grower_cups_judgeAssoc" {...stackScreen(JudgeAssocController) } />


              <Scene key="grower_cups_budsList" {...stackScreen(BudsController) } {...app.passProps} />
              <Scene key="grower_cups_budProfile" {...stackScreen(BudProfileController) } />
              <Scene key="grower_cups_budEdit" {...stackScreen(BudEditController) } />
              <Scene key="grower_cups_budAdd" {...stackScreen(BudEditController) } />
              <Scene key="grower_cups_budPhotoAdd" {...stackScreen(AddPhotoController) } />
              <Scene key="grower_cups_budQrCode" {...stackScreen(QuickIDShareController) } />
              <Scene key="grower_cups_reviewAdd" {...stackScreen(ReviewEditController) } />
              <Scene key="grower_cups_reviewDetails" {...stackScreen(ReviewController) } />
              <Scene key="grower_cups_budGalery" {...stackScreen(GaleryController) } />
            </Scene>
            <Scene key="grower_qrCodeTab" title="qrcode" iconName="qrcode" icon={TabIcon} drawerIcon={drawerIcon}>
              <Scene key="grower_qrCodeReader" {...stackScreen(QuickIDController) } />
            </Scene>
            <Scene key="grower_contactTab" title="contact" iconName="contact" drawerIcon={drawerIcon}>
              <Scene key="grower_contact" {...stackScreen(ContactController) } />
            </Scene>
            <Scene key="grower_tour" component={TourController} hideNavBar hideTabBar />
          </Scene>
        </Scene>

        <Scene key="logged_consumer" component={DrawerController} type="replace" {...app.passProps}>
          <Scene key="consumer" type="replace" tabs tabBarStyle={ScenesStyles.tabBar}>
            <Scene key="consumer_budsTab" title="buds" iconName="bud" icon={TabIcon} drawerIcon={drawerIcon} initial={true}>
              <Scene key="consumer_budsList" {...stackScreen(BudsController) } {...app.passProps} />
              <Scene key="consumer_budProfile" {...stackScreen(BudProfileController) } />
              <Scene key="consumer_budEdit" {...stackScreen(BudEditController) } {...app.passProps} />
              <Scene key="consumer_budAdd" {...stackScreen(BudEditController) } {...app.passProps} />
              <Scene key="consumer_budPhotoAdd" {...stackScreen(AddPhotoController) } />
              <Scene key="consumer_budQrCode" {...stackScreen(QuickIDShareController) } />
              <Scene key="consumer_cupQrCode" {...stackScreen(QuickIDShareController) } />
              <Scene key="consumer_reviewAdd" {...stackScreen(ReviewEditController) } />
              <Scene key="consumer_reviewDetails" {...stackScreen(ReviewController) } />
              <Scene key="consumer_budGalery" {...stackScreen(GaleryController) } />
            </Scene>
            <Scene key="consumer_cupsTab" title="cups" iconName="trophy" drawerIcon={drawerIcon}>
              <Scene key="consumer_cups_cupsList" {...stackScreen(CupsController) } />
              <Scene key="consumer_cups_cupProfile" {...stackScreen(CupProfileController) } {...app.passProps} />
              <Scene key="consumer_cups_cupAdd" {...stackScreen(CupEditController) } />
              <Scene key="consumer_cups_cupEdit" {...stackScreen(CupEditController) } />
              <Scene key="consumer_cups_cupQrCode" {...stackScreen(QuickIDShareController) } />

              <Scene key="consumer_cups_editionProfile" {...stackScreen(EditionProfileController) } />
              <Scene key="consumer_cups_editionAdd" {...stackScreen(EditionEditController) } />
              <Scene key="consumer_cups_editionEdit" {...stackScreen(EditionEditController) } />
              <Scene key="consumer_cups_editionQrCode" {...stackScreen(QuickIDShareController) } />
              <Scene key="consumer_cups_editionPhotoAdd" {...stackScreen(AddPhotoController) } />
              <Scene key="consumer_cups_editionGalery" {...stackScreen(GaleryController) } />

              <Scene key="consumer_cups_categoryEdit" {...stackScreen(CategoryEditController) } />
              <Scene key="consumer_cups_categoryProfile" {...stackScreen(CategoryProfileController) } />
              <Scene key="consumer_cups_addBudsToCategory" {...stackScreen(AddBudToCategoryController) } />

              <Scene key="consumer_cups_judgesList" {...stackScreen(JudgesController) } />
              <Scene key="consumer_cups_judgeEdit" {...stackScreen(JudgeEditController) } />
              <Scene key="consumer_cups_judgeProfile" {...stackScreen(JudgeProfileController) } />
              <Scene key="consumer_cups_judgeAssoc" {...stackScreen(JudgeAssocController) } />


              <Scene key="consumer_cups_budsList" {...stackScreen(BudsController) } {...app.passProps} />
              <Scene key="consumer_cups_budProfile" {...stackScreen(BudProfileController) } />
              <Scene key="consumer_cups_budEdit" {...stackScreen(BudEditController) } {...app.passProps} />
              <Scene key="consumer_cups_budAdd" {...stackScreen(BudEditController) } {...app.passProps} />
              <Scene key="consumer_cups_budPhotoAdd" {...stackScreen(AddPhotoController) } />
              <Scene key="consumer_cups_budQrCode" {...stackScreen(QuickIDShareController) } />
              <Scene key="consumer_cups_reviewAdd" {...stackScreen(ReviewEditController) } />
              <Scene key="consumer_cups_reviewDetails" {...stackScreen(ReviewController) } />
              <Scene key="consumer_cups_budGalery" {...stackScreen(GaleryController) } />
            </Scene>

            <Scene key="consumer_qrCodeTab" title="qrcode" iconName="qrcode" icon={TabIcon} drawerIcon={drawerIcon}>
              <Scene key="consumer_qrCodeReader" {...stackScreen(QuickIDController) } />
            </Scene>
            <Scene key="consumer_contactTab" title="contact" iconName="contact" drawerIcon={drawerIcon}>
              <Scene key="consumer_contact" {...stackScreen(ContactController) } />
            </Scene>
            <Scene key="consumer_photoAdd" {...stackScreen(AddPhotoController) } />
            <Scene key="consumer_tour" component={TourController} hideNavBar hideTabBar />
          </Scene>
        </Scene>
      </Scene>
      <Scene key="splash" component={LoadingModal} modal={false} />
    </Scene>
  );
}

export {
  getSceneStyle,
  stackScreen,
  ConnectedSwitch,
  Switch,
  Scenes
};

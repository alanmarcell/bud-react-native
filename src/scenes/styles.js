import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  container: {
    paddingTop: '$dimensions.navBarheight',
  },

  intoTab: {
    paddingBottom: 50,
  },

  scene: {
    backgroundColor: '$Colors.background',
  },

  navigationBar: {
    backgroundColor: '$Colors.navbarBackground',
    borderBottomWidth: 0,
  },

  navigationBarSpecial: {
    backgroundColor: '$Colors.Purple',
  },

  navbarTitle: {
    color: '$Colors.navbarTitle',
    fontWeight: 'bold',
    fontSize: 13,
    letterSpacing: 1,
    textAlign: 'center',
  },

  // left
  iconContainer: {
    marginLeft: 5,
    marginTop: -8,
    padding: 8,
  },

  rightIcon: {
    textAlign: 'center',
    color: '$Colors.navbarIconColor',
    fontSize: 18,
    width: 20,
  },

  rightText: {
    color: '$Colors.navbarIconColor',
    fontSize: 14,
  },

  leftIcon: {
    color: '$Colors.navbarIconColor',
    fontSize: 18,
  },

  menuIcon: {
    color: '$Colors.navbarIconColor',
    fontSize: 32,
  },

  leftButtonStyle: {
    paddingHorizontal: 10,
    marginLeft: 5,
  },

  // TabBar
  tabBar: {
    backgroundColor: '$Colors.tabBarBackground',
  },

  tabBarItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  tabBarNormal: {
    color: '$Colors.Disabled',
    fontSize: 24,
  },

  tabBarFocused: {
    color: '$Colors.tabBarFocused',
    fontSize: 24,
  },

  tabBarLabel: {
    marginTop: 5,
    fontSize: '0.6rem',
  },
});

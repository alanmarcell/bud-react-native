import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';

import Icon from 'components/Icon';
import { default as ScenesStyles } from './styles.js';

export default function backButton(props) {
  const { onBack, component, style } = props;

  let onPress = onBack || component.onBack;
  if (onPress) {
    onPress = onPress.bind(null, props);
  } else {
    onPress = Actions.pop;
  }

  return (
    <TouchableOpacity
      testID="backNavButton"
      style={style}
      onPress={onPress}
    >
      <Icon style={ScenesStyles.leftIcon} name="arrow-left" />
    </TouchableOpacity>
  );
}

import React from 'react';
import PropTypes from 'prop-types';

import { View, Text } from 'react-native';

import Icon from 'components/Icon';
import { I18n } from 'utils/I18n';

import styles from './styles.js';

export default class TabIcon extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    iconName: PropTypes.string,
  };

  render() {
    const { title, iconName, selected } = this.props;
    const style = styles[selected ? 'tabBarFocused' : 'tabBarNormal'];
    return (
      <View style={styles.tabBarItem}>
        <Icon style={style} name={iconName} />
        <Text style={[style, styles.tabBarLabel]}>{I18n.T(['screens', title, 'title'])}</Text>
      </View>
    );
  }
}

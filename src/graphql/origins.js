import { gql } from 'react-apollo';

export const originFragments = {
  originBase: gql`
    fragment OriginBase on Origin {
      id
      name
      code
    }
  `
};

export const ORIGIN_SEARCH = gql`
query Origin($term: String, $cursor: String) {
  origins(code: $term, after: $cursor, orderBy: { field: NAME } ) {
    edges {
      node {
        ...OriginBase
      }
    }
    pageInfo {
      endCursor,
      hasNextPage,
    }
  }
}
${originFragments.originBase}
`;

export const ORIGINS_LIST = gql`
  query Origin($cursor: String) {
    origins(first: 20, after: $cursor) {
      edges {
        node {
          ...OriginBase
        }
      }
      pageInfo {
        endCursor
        hasNextPage
      }
    }
  }
  ${originFragments.originBase}
`;

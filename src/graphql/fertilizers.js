import { gql } from 'react-apollo';

export const fertilizerFragments = {
  fertilizerBase: gql`
    fragment FertilizerBase on Fertilizer {
      id
      name
      description
      brand
    }
  `
};

export const FERTILIZER_CREATE = gql`
  mutation createFertilizer($input: CreateFertilizerInput) {
    createFertilizer(input: $input) {
      ...FertilizerBase
    }
  }
  ${fertilizerFragments.fertilizerBase}
`;

export const FERTILIZER_SEARCH = gql`
  query Fertilizer($term: String, $cursor: String) {
    fertilizers(search: $term, after: $cursor, orderBy: { field: NAME } ) {
      edges {
        node {
          ...FertilizerBase
        }
      }
      pageInfo {
        endCursor,
        hasNextPage,
      }
    }
  }
  ${fertilizerFragments.fertilizerBase}
`;

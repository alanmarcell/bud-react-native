import { gql } from 'react-apollo';

export const judgeFragments = {
  judgeBase: gql`
   fragment judgeBase on Judge {   
      id
      name
   }
 `,
  judgeProfile: gql`
    fragment judgeProfile on Judge {   
      id
      name
      inviteCode
      judgeUser{
        id
        name
      }
      edition{
        id
        name
        owner{
          id
        }
      }
      category{
        owner{
          id
        }
      }
    }
  `
};

export const JUDGE_LIST = gql`
  query JudgeList($id: ID!){
    edition(id: $id){
      id
      name
      owner{
        id        
      }
      judges{
        nodes{
          id
          name
          judgeUser{
            id
            name
          }
        }
      }
    }
  }
`;

export const JUDGE_PROFILE = gql`
  query JudgeProfile($id: ID!) {
    judge(id: $id) {
      ...judgeProfile
    }    
  }
  ${judgeFragments.judgeProfile}
`;

// Mutation.addJudgeToJudge
export const JUDGE_ADD_TO_CATEGORY = gql`
  mutation addJudgeToCategory($categoryId: ID!, $judgeUserId: ID!) {
    addJudgeToCategory(categoryId: $categoryId, judgeUserId: $judgeUserId) {
      id
    }
  }
`;

// Mutation.deleteJudgeFromJudge
export const JUDGE_DELETE_FROM_CATEGORY = gql`
  mutation deleteJudgeFromCategory($categoryId: ID!, $judgeUserId: ID!) {
    deleteJudgeFromCategory(categoryId: $categoryId, judgeUserId: $judgeUserId) {
      result
    }
  }
`;

// Mutation.addJudgeToEdition
export const JUDGE_ADD_TO_EDITION = gql`
  mutation addJudgeToEdition($editionId: ID!, $judgeUserId: ID!) {
    addJudgeToEdition(editionId: $editionId, judgeUserId: $judgeUserId) {
      id
    }    
  }
`;


// Mutation.deleteJudgeFromEdition
export const JUDGE_DELETE_FROM_EDITION = gql`
  mutation deleteJudgeFromEdition($editionId: ID!, $judgeUserId: ID!) {
    deleteJudgeFromEdition(editionId: $editionId, judgeUserId: $judgeUserId) {
      result
    }
  }
`;

// Mutation.createJudgeForEdition
export const JUDGE_CREATE_FOR_EDITION = gql`
  mutation createJudgeForEdition($input: CreateJudgeForEditionInput) {
    createJudgeForEdition(input: $input) {
      id
    }
  }
`;

// Mutation.createJudgeForCategory
export const JUDGE_CREATE_FOR_CATEGORY = gql`
  mutation createJudgeForCategory($input: CreateJudgeForCategoryInput) {
    createJudgeForJudge(input: $input) {
      id
    }
  }
`;


// Mutation.updateJudge
export const JUDGE_UPDATE = gql`
  mutation updateJudge($id: ID!, $input: JudgeInput) {
    updateJudge(id: $id, input: $input) {
      id
    }
  }
`;

// Mutation.deleteJudge
export const JUDGE_DELETE = gql`
  mutation deleteJudge($id: ID!) {
    deleteJudge(id: $id) {
      result
    }
  }
`;

// Mutation.associateJudge
export const JUDGE_ASSOCIATE = gql`
  mutation associateJudge($inviteCode: String!) {
    associateJudge(inviteCode: $inviteCode) {
      id
    }
  }
`;

import { gql } from 'react-apollo';
import Config from 'config';

export const pestFragments = {
  pestBase: gql`
    fragment PestBase on Pest {
      id
      name
      description
    }
  `,
  pestsList: gql`
    fragment PestsList on PestConnection {
      edges {
        node {
          ...PestBase
        }
      }
      pageInfo {
        endCursor
        hasNextPage
      }
    }
  `
};

export const PEST_LIST = gql`
  query UserPests($cursor: String) {
    pests(first: ${Config.ui.DEFAULT_LIST_SIZE}, before: $cursor) {
      ...PestsList
    }
  }
  ${pestFragments.pestBase}
  ${pestFragments.pestsList}
`;

export const PEST_SEARCH = gql`
  query Pests($term: String, $cursor: String) {
    pests(search: $term, after: $cursor, orderBy: { field: NAME } ) {
      ...PestsList
    }
  }
  ${pestFragments.pestBase}
  ${pestFragments.pestsList}
`;

export const PEST_CREATE = gql`
  mutation createPest($input: CreatePestInput) {
    createPest(input: $input) {
      ...PestBase
    }
  }
  ${pestFragments.pestBase}
`;


// export const PEST_PROFILE = gql`
//   query PestQuery($id: ID!) {
//     pest(id: $id) {
//       ...PestBase
//       ...PestProfile
//     }
//   }
//   ${pestFragments.pestBase}
//   ${pestFragments.pestProfile}
// `;

// export const PEST_QUERY = gql`
//   query PestQuery($id: ID!) {
//     pest(id: $id) {
//       ...PestBase
//       ...PestProfile
//     }
//   }
//   ${pestFragments.pestBase}
//   ${pestFragments.pestProfile}
// `;
//

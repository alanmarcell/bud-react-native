import { gql } from 'react-apollo';

export const strainFragments = {
  strainBase: gql`
    fragment StrainBase on Strain {
      id
      name
      breeder
    }
  `
};

export const STRAIN_CREATE = gql`
  mutation createStrain($strain: CreateStrainInput) {
    createStrain(input: $strain) {
      ...StrainBase
    }
  }
  ${strainFragments.strainBase}
`;

export const STRAIN_SEARCH = gql`
  query Strain($query: String, $cursor: String) {
    strains(search: $query, after: $cursor, orderBy: { field: NAME } ) {
      edges {
        node {
          ...StrainBase
        }
      }
      pageInfo {
        endCursor,
        hasNextPage,
      }
    }
  }
  ${strainFragments.strainBase}
`;

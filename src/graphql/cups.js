import { gql } from 'react-apollo';
import Config from 'config';
import { fileFragments } from 'graphql/files';

export const cupFragments = {
  cupBase: gql`
    fragment CupBase on Cup {
      id
      logo{
        ...FileBase
      }
      name
      city
      description
      country
      owner{
        id
      }
    }
    ${fileFragments.fileBase}
  `,
  cupProfile: gql`
    fragment CupProfile on Cup {
      id
      name
      logo{
        ...FileBase
      }
      city
      country
      description
      owner{
        id
        name
      }
      qrCode {
        id
        code
      }
      editions{
        nodes{
          id
          name
          description
          startAt
          endAt
          logo{
            ...FileBase
          }
        }
      }
    }
    ${fileFragments.fileBase}
  `,
};

export const CUP_CREATE = gql`
  mutation createCup($input: CreateCupInput) {
    createCup(input: $input) {
      ...CupProfile
    }
  }
  ${cupFragments.cupProfile}
`;


export const CUP_UPDATE = gql`
  mutation updateCup($id: ID!, $input: CupInput) {
    updateCup(id: $id, input: $input) {
      ...CupProfile
    }
  }
  ${cupFragments.cupProfile}
`;

export const CUP_DELETE = gql`
  mutation deleteCup($id: ID!) {
    deleteCup(id: $id) {
      id
    }
  }
`;

export const CUPS_LIST = gql`
  query UserCups($cursor: String) {
    cups(first: ${Config.ui.DEFAULT_LIST_SIZE}, after: $cursor) {
      edges {
        node {
         ...CupBase
        }
      }
      pageInfo {
        endCursor
        hasNextPage
      }
    }
  }
  ${cupFragments.cupBase}
`;


export const CUP_PROFILE = gql`
  query CupProfile($id: ID!) {
    cup(id: $id) {
      ...CupProfile
    }
  }
  ${cupFragments.cupProfile}
`;

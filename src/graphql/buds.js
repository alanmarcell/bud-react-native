import { gql } from 'react-apollo';
import { strainFragments } from 'graphql/strains';
import { plantFragments } from 'graphql/plants';
import { fileFragments } from 'graphql/files';

import Config from 'config';

export const budFragments = {
  budBase: gql`
    fragment BudBase on Bud {
      id
      name
      score
      strain {
        ...StrainBase
      }
      edition {
        id
        logo {
          id
          url
        }
        cup {
          id
          logo {
            id
            url
          }
        }
      }
    }
    ${strainFragments.strainBase}
  `,
  budProfile: gql`
    fragment BudProfile on Bud {
      owner {
        id
        name
      }
      description
      harvestedAt
      mother {
        ...PlantBase
      }
      qrCode {
        id
        code
      }
      files {
        ...FileBase
      }
      edition {
        id
        name
      }
      availableCategories{
        id
        name
        jury
        judges{
          id
        }
      }
    }
    ${plantFragments.plantBase}
    ${fileFragments.fileBase}
  `
};

export const BUD_LIST = gql`
  query UserBuds($cursor: String) {
    buds(first: ${Config.ui.DEFAULT_LIST_SIZE}, after: $cursor) {
      edges {
        node {
          ...BudBase
        }
      }
      pageInfo {
        endCursor
        hasNextPage
      }
    }
  }
  ${budFragments.budBase}
`;

export const BUDS_REVIEWED = gql`
  query UserBudsReviewed($cursor: String) {
    budsReviewed(first: ${Config.ui.DEFAULT_LIST_SIZE}, after: $cursor) {
      edges {
        node {
          ...BudBase
        }
      }
      pageInfo {
        endCursor
        hasNextPage
      }
    }
  }
  ${budFragments.budBase}
`;

export const BUD_QUERY = gql`
  query BudQuery($id: ID!) {
    bud(id: $id) {
      ...BudBase
      ...BudProfile
    }
  }
  ${budFragments.budBase}
  ${budFragments.budProfile}
`;

export const BUD_DELETE = gql`
  mutation deleteBud($id: ID!) {
    deleteBud(id: $id) {
      id
    }
  }
`;

export const BUD_UPDATE = gql`
  mutation updateBud($id: ID!, $input: BudInput) {
    updateBud(id: $id, input: $input) {
      ...BudBase
      ...BudProfile
    }
  }
  ${budFragments.budBase}
  ${budFragments.budProfile}
`;

export const BUD_CREATE = gql`
  mutation createBud($input: CreateBudInput) {
    createBud(input: $input) {
      ...BudBase
      ...BudProfile
    }
  }
  ${budFragments.budBase}
  ${budFragments.budProfile}
`;

export const BUD_IMAGE_UPLOAD = gql`
  mutation addImagesToBud($id: ID!, $photos: [PhotoInput!]) {
    addImagesToBud(id: $id, photos: $photos) {
      result
    }
  }
`;

import { gql } from 'react-apollo';

export const pesticideFragments = {
  pesticideBase: gql`
    fragment PesticideBase on Pesticide {
      id
      name
      description
      brand
    }
  `
};

export const PESTICIDE_CREATE = gql`
  mutation createPesticide($input: CreatePesticideInput) {
    createPesticide(input: $input) {
      ...PesticideBase
    }
  }
  ${pesticideFragments.pesticideBase}
`;

export const PESTICIDE_SEARCH = gql`
  query Pesticide($term: String, $cursor: String) {
    pesticides(search: $term, after: $cursor, orderBy: { field: NAME } ) {
      edges {
        node {
          ...PesticideBase
        }
      }
      pageInfo {
        endCursor,
        hasNextPage,
      }
    }
  }
  ${pesticideFragments.pesticideBase}
`;


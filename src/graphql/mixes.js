import { gql } from 'react-apollo';
import Config from 'config';
// import { strainFragments } from 'graphql/strains';

export const mixFragments = {
  mixBase: gql`
    fragment MixBase on Mix {
      id
      name
      description
    }
  `,
  mixProfile: gql`
     fragment MixProfile on Mix {
      items {
        compound {
          ...on Fertilizer {
            id
            name
            brand
          }
          ...on Pesticide {
            id
            name
          }
        }
        qty
        measurement
      }
    }
  `,
  mixesList: gql`
    fragment MixesList on MixConnection {
      edges {
        node {
          ...MixBase
        }
      }
      pageInfo {
        endCursor
        hasNextPage
      }
    }
  `
};

export const MIX_LIST = gql`
  query UserMixes($cursor: String) {
    mixes(first: ${Config.ui.DEFAULT_LIST_SIZE}, before: $cursor) {
      ...MixesList
    }
  }
  ${mixFragments.mixBase}
  ${mixFragments.mixesList}
`;

export const MIX_SEARCH = gql`
  query Mixes($term: String, $cursor: String) {
    mixes(search: $term, after: $cursor, orderBy: { field: NAME } ) {
      ...MixesList
    }
  }
  ${mixFragments.mixBase}
  ${mixFragments.mixesList}
`;

export const MIX_PROFILE = gql`
  query MixQuery($id: ID!) {
    mix(id: $id) {
      ...MixBase
      ...MixProfile
    }
  }
  ${mixFragments.mixBase}
  ${mixFragments.mixProfile}
`;

export const MIX_QUERY = gql`
  query MixQuery($id: ID!) {
    mix(id: $id) {
      ...MixBase
      ...MixProfile
    }
  }
  ${mixFragments.mixBase}
  ${mixFragments.mixProfile}
`;

export const MIX_DELETE = gql`
  mutation deleteMix($id: ID!) {
    deleteMix(id: $id) {
      id
    }
  }
`;

export const MIX_UPDATE = gql`
  mutation updateMix($id: ID!, $input: MixInput) {
    updateMix(id: $id, input: $input) {
      ...MixBase
      ...MixProfile
    }
  }
  ${mixFragments.mixBase}
  ${mixFragments.mixProfile}
`;

export const MIX_CREATE = gql`
  mutation createMix($input: CreateMixInput) {
    createMix(input: $input) {
      ...MixBase
      ...MixProfile
    }
  }
  ${mixFragments.mixBase}
  ${mixFragments.mixProfile}
`;

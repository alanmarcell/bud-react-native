import { gql } from 'react-apollo';
import Config from 'config';
import { fileFragments } from 'graphql/files';

export const editionFragments = {
  editionBase: gql`
   fragment EditionBase on Edition {
     id
      logo{
        ...FileBase
      }
     name
     description
     startAt
     endAt
   }
   ${fileFragments.fileBase}
 `,
  editionProfile: gql`
    fragment EditionProfile on Edition {
      id
      name
      logo{
        ...FileBase
      }
      description
      startAt
      endAt
      cup{
        id
      }
      owner{
        id
      }
      qrCode {
        id
        code
      }
      categories{
        nodes {
          id
          name
          description
        }
      }
      buds{
        nodes{
          id
          name
          score 
        }
      }
      files{
        ...FileBase
      }
    }
    ${fileFragments.fileBase}
  `
};

export const EDITION_UPDATE = gql`
  mutation updateEdition($id: ID!, $input: EditionInput) {
    updateEdition(id: $id, input: $input) {
      ...EditionBase
      ...EditionProfile
    }
  }
  ${editionFragments.editionBase}
  ${editionFragments.editionProfile}
`;

export const EDITION_DELETE = gql`
mutation deleteEdition($id: ID!) {
  deleteEdition(id: $id) {
    id
  }
}
`;

export const EDITIONS_LIST = gql`
  query UserEditions($cursor: String) {
    Editions(first: ${Config.ui.DEFAULT_LIST_SIZE}, after: $cursor) {
      edges {
        node {
          id
          name
        }
      }
      pageInfo {
        endCursor
        hasNextPage
      }
    }
  }
`;

export const EDITION_CREATE = gql`
mutation createEdition($input: CreateEditionInput) {
  createEdition(input: $input) {
    ...EditionBase
    ...EditionProfile
  }
}
  ${editionFragments.editionBase}
  ${editionFragments.editionProfile}
`;

export const EDITION_PROFILE = gql`
  query EditionProfile($id: ID!) {
    edition(id: $id) {
      ...EditionProfile
    }
  }
  ${editionFragments.editionProfile}
`;

export const EDITION_CATEGORIES = gql`
  query EditionCategories($id: ID!) {
    edition(id: $id) {
      id
      name
      categories{
        nodes {
          id
          name
        }
      }
    }
  }
`;

export const EDITION_IMAGE_UPLOAD = gql`
  mutation addImagesToEdition($id: ID!, $photos: [PhotoInput!]) {
    addImagesToEdition(id: $id, photos: $photos) {
      result
    }
  }
`;

import { gql } from 'react-apollo';

export const fileFragments = {
  fileBase: gql`
    fragment FileBase on File {
      id
      mimetype
      url
      fileName
      createdAt
    }
  `
};

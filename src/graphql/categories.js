import { gql } from 'react-apollo';
import Config from 'config';

export const categoryFragments = {
  categoryBase: gql`
   fragment CategoryBase on Category {
      id
      name
      description
   }
 `,
  categoryProfile: gql`
    fragment CategoryProfile on Category {
      id
      name
      description
      jury
      owner{
        id
      }
      buds {
        id
        name
        score
      }
      judges {
        id
        name
      }
      edition{
        id
      }
    }
  `
};

export const CATEGORY_UPDATE = gql`
  mutation updateCategory($id: ID!, $input: CategoryInput) {
    updateCategory(id: $id, input: $input) {
      ...CategoryBase
      ...CategoryProfile
    }
  }
  ${categoryFragments.categoryBase}
  ${categoryFragments.categoryProfile}
`;

export const CATEGORY_DELETE = gql`
mutation deleteCategory($id: ID!) {
  deleteCategory(id: $id) {
    id
  }
}
`;

export const CATEGORIES_LIST = gql`
  query UserCategories($cursor: String) {
    Categories(first: ${Config.ui.DEFAULT_LIST_SIZE}, after: $cursor) {
      edges {
        node {
         ...CategoryBase
        }
      }
      pageInfo {
        endCursor
        hasNextPage
      }
    }
  }
  ${categoryFragments.categoryBase}
`;

export const CATEGORY_CREATE = gql`
mutation createCategory($input: CreateCategoryInput) {
  createCategory(input: $input) {
    ...CategoryBase
  }
}
  ${categoryFragments.categoryBase}
`;


export const CATEGORY_PROFILE = gql`
  query CategoryProfile($id: ID!) {
    category(id: $id) {
      ...CategoryProfile
    }
  }
  ${categoryFragments.categoryProfile}
`;

export const ADD_BUD_TO_CATEGORY = gql`
  mutation AddBudToCategory($budId: ID!, $categoryId: ID!) {
    addBudToCategory(budId: $budId, categoryId: $categoryId) {
      result
    }
  }
`;

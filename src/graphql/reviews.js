import { gql } from 'react-apollo';

export const reviewFragments = {
  budReviews: gql`
    fragment BudReviews on Review {
      id
      reviewedBy {
        id
        name
      }
      score
      category{
        id
      }
      consumption
      ratings {
        score
        tone
        tags
        feature {
          id
          name
        }
      }
      createdAt
    }
  `,
  budReviewed: gql`
    fragment BudReviewedBy on Reviews {
      reviewedBy {
        id
        name
      }
    }
  `,
  budReviewResumed: gql`
    fragment BudReviewResumed on ReviewResumed {
      countReviews
      ratings {
        score
        tone
        tags {
          tag
          count
        }
        feature {
          name
        }
      }
    }
  `,
};

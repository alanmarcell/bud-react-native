import { ApolloClient, createNetworkInterface } from 'react-apollo';
import { IntrospectionFragmentMatcher } from 'react-apollo';
import store from '../redux/store';
import { I18n } from 'utils/I18n';
import update from 'immutability-helper';
import R from 'ramda';

export { propType } from 'graphql-anywhere';
export {
  ApolloProvider, ApolloError,
  compose, graphql
} from 'react-apollo';

export * from './tasks';
export * from './plants';
export * from './strains';
export * from './mixes';
export * from './fertilizers';
export * from './pesticides';
export * from './pests';
export * from './buds';
export * from './reviews';
export * from './bud_reviews';
export * from './user';
export * from './origins';
export * from './contact';
export * from './cups';
export * from './editions';
export * from './categories';
export * from './judges';

import Config from '../config';

const getToken = () => store.getState().user.token;

export function createClient(app) {
  const fragmentMatcher = new IntrospectionFragmentMatcher({
    introspectionQueryResultData: {
      __schema: {
        types: [
          {
            kind: 'INTERFACE',
            name: 'Node',
            possibleTypes: [
              { name: 'Plant' },
              { name: 'Comment' },
              { name: 'Bud' },
              { name: 'Strain' },
              { name: 'Review' },
              { name: 'Seed' },
              { name: 'Pest' },
              { name: 'Pesticide' },
              { name: 'Fertilizer' },
              { name: 'Mix' },
              { name: 'Task' },
              { name: 'Cup' },
              { name: 'Edition' },
              { name: 'Category' },
            ]
          },
          {
            kind: 'UNION',
            name: 'Product',
            possibleTypes: [
              { name: 'Node' },
              { name: 'Plant' },
              { name: 'Bud' }
            ]
          },
          {
            kind: 'UNION',
            name: 'Family',
            possibleTypes: [
              { name: 'Plant' },
              { name: 'Strain' }
            ]
          },
          {
            kind: 'UNION',
            name: 'Compound',
            possibleTypes: [
              { name: 'Fertilizer' },
              { name: 'Pesticide' }
            ]
          },
          {
            kind: 'INTERFACE',
            name: 'TaskMeta',
            possibleTypes: [
              { name: 'Container' },
              { name: 'Lamp' },
              { name: 'Temperature' },
              { name: 'Medium' },
              { name: 'Water' },
              { name: 'Environment' },
              { name: 'AirHumidity' },
              { name: 'Photoperiod' },
              { name: 'EnvironmentTemperature' },
              { name: 'Ph' },
              { name: 'Ec' },
              { name: 'Tds' },
              { name: 'Cf' },
              { name: 'PesticideEvent' },
              { name: 'Death' },
              { name: 'Flowering' },
              { name: 'Germination' },
              { name: 'PreFlower' },
              { name: 'Revega' },
              { name: 'Vega' },
              { name: 'Harvest' }
            ],
          }, // this is just an example, put your own INTERFACE and UNION types here!
        ],
      },
    }
  });

  const networkInterface = createNetworkInterface({
    uri: Config.api.baseURL,
    connectToDevTools: __DEV__,
    opts: {
      timeout: Config.api.timeout_max,
    }
  });

  const UnauthorizedErrors = {
    applyAfterware: async ({ response }, next) => {

      if (response.status === 401) {

        if (__DEV__) {
          console.log('UnauthorizedErrors: ', response); // eslint-disable-line no-console
        }

        await app.handleUnauthorized();
      }
      next();
    }
  };

  networkInterface.use([{
    applyMiddleware: async (req, next) => {
      if (!req.options.headers) {
        req.options.headers = {};  // Create the header object if needed.
      }

      req.options.headers['accept-language'] = I18n.locale;

      // get the authentication token from local storage if it exists
      const token = getToken();
      if (token) {
        req.options.headers.authorization = token;
      }
      next();
    }
  }]);

  networkInterface.useAfter([UnauthorizedErrors]);

  // Initialize Apollo Client with URL to our server
  return new ApolloClient({
    networkInterface,
    fragmentMatcher,
  });
}

export function makeLoadMore(data, name, query, fetchMore) {
  return {
    hasNextPage: data ? data.pageInfo.hasNextPage : false,
    loadMoreEntries: ({ variables } = {}) => {
      return fetchMore({
        query: query,
        variables: {
          ...variables,
          cursor: data.pageInfo.endCursor,
        },
        updateQuery: (previousResult, { fetchMoreResult }) => {
          const newEdges = fetchMoreResult[name].edges;
          const pageInfo = fetchMoreResult[name].pageInfo;
          return {
            // Put the new plants at the end of the list and update `pageInfo`
            // so we have the new `endCursor` and `hasNextPage` values
            [name]: {
              edges: [...previousResult[name].edges, ...newEdges],
              pageInfo,
            },
          };
        },
      });
    },
  };
}

export function makeLoadList(query, resourcesName) {
  return {
    props(props) {
      const { data } = props;
      const { error, loading, fetchMore, refetch } = data;
      const resources = data[resourcesName];
      return {
        error,
        loading,
        [`g_${resourcesName}`]: { error, loading, refetch, ...data },
        [resourcesName]: resources ? resources.edges.map((edge) => edge.node) : [],
        ...makeLoadMore(resources, [resourcesName], query, fetchMore),
      };
    },
    options: { notifyOnNetworkStatusChange: true }
  };
}

export function makeGet(keyName = 'objectId') {
  return {
    props({ data }) {
      return { ...data };
    },
    options: (props) => {
      const id = props[keyName];
      if (id) {
        return {
          variables: { id },
        };
      } else {
        return {
          skip: true,
        };
      }
    },
  };
}

export function makeDeleteMutation(name, resourcesName, updateName, configs = {}) {
  return {
    props: ({ mutate }) => ({
      [name]({ objectId }) {
        return mutate({
          variables: { id: objectId },
          updateQueries: {
            [updateName]: (prev, { _mutationResult }) => {
              const items = prev[resourcesName].edges.map((item) => item.node);
              const deleteIndex = R.findIndex(R.propEq('id', objectId), items);
              if (deleteIndex < 0) {
                return prev;
              }
              return update(prev, {
                [resourcesName]: {
                  edges: {
                    $splice: [[deleteIndex, 1]]
                  }
                }
              });
            }
          }
        });
      }
    }),
    ...configs
  };
}

export function mutationCreate(name, typeName, resourcesName, queryUpdate, refetchQueries) {
  return {
    props({ mutate }) {
      return {
        [name]({ variables: { input, ...variables } }) {
          return mutate({
            refetchQueries,
            variables: { input, ...variables },
            // optimisticResponse: {
            //   __typename: 'Mutation',
            //   [name]: {
            //     __typename: typeName,
            //     ...input,
            //   },
            // },
            updateQueries: {
              [queryUpdate]: (previousResult, { mutationResult }) => {
                const newResource = {
                  __typename: `${typeName}Edge`,
                  node: mutationResult.data[name]
                };
                return update(previousResult, {
                  [resourcesName]: {
                    edges: {
                      $unshift: [newResource],
                    },
                  },
                });
              }
            }
          });
        }
      };
    }
  };
}

import { gql } from 'react-apollo';
import { budFragments } from 'graphql/buds';
import { reviewFragments } from 'graphql/reviews';

export const BUD_REVIEW_PROFILE = gql`
  query BudProfile($id: ID!) {
    bud(id: $id) {
      ...BudBase
      ...BudProfile
      reviewResumed {
        ...BudReviewResumed
      }
      reviews {
        edges {
          node {
            ...BudReviews
          }
        }
        totalCount
      }
      edition{
        id
        name
        startAt
        endAt
      }
      availableCategories{
        id
        name
      }
    }
  }
  ${budFragments.budBase}
  ${budFragments.budProfile}
  ${reviewFragments.budReviewResumed}
  ${reviewFragments.budReviews}
`;

export const BUD_REVIEW_CREATE = gql`
  mutation createReview($product: ProductInput!, $input: ReviewInput){
    createReview(product: $product, input: $input) {
      ...BudBase
      ...BudProfile
      reviewResumed {
        ...BudReviewResumed
      }
      reviews {
        edges {
          node {
            ...BudReviews
          }
        }
      }
    }
  }
  ${budFragments.budBase}
  ${budFragments.budProfile}
  ${reviewFragments.budReviewResumed}
  ${reviewFragments.budReviews}
`;


import { gql } from 'react-apollo';
import Config from 'config';
import { plantFragments } from './plants';
import { mixFragments } from './mixes';

export const taskFragments = {
  taskBase: gql`
    fragment TaskBase on Task {
      id
      scheduledAt
      executedAt
      description
      plant {
        ...PlantBase
      }
      meta {
        type
      }
    }
    ${plantFragments.plantBase}
  `,
  taskProfile: gql`
    fragment TaskProfile on Task {
      meta {
        ... on Container {
          value
        }
        ... on Temperature {
          value
        }
        ... on Medium {
          kind
        }
        ... on Lamp {
          name
        }
        ... on Water {
          ec
          ph
          tds
          temperature
          cf
          qty
          mixBase {
            ...MixBase
          }
          compounds {
            compound {
              ...on Fertilizer {
                id
                name
                brand
              }
            }
            qty
            measurement
          }
        }
        ... on Fertilization {
          ec
          ph
          tds
          temperature
          cf
          qty
          mixBase {
            ...MixBase
          }
          compounds {
            compound {
              ...on Fertilizer {
                id
                name
                brand
              }
            }
            qty
            measurement
          }
        }
        ... on Environment {
          size
          name
          environment
        }
        ... on AirHumidity {
          value
        }
        ... on Photoperiod {
          daylight
          darkness
        }
        ... on EnvironmentTemperature {
          value
        }
        ... on Ph {
          value
        }
        ... on Ec {
          value
        }
        ... on Tds {
          value
        }
        ... on Cf {
          value
        }
        ...on PesticideEvent {
          qty
          pest {
            id
            name
          }
          compounds {
            qty
            measurement
            compound {
              ...on Pesticide {
                id
                name
              }
            }
          }
        }
        ... on Harvest {
          total
          qty
          measurement
        }
        ... on Cure {          
          qty
          endAt
        }
        ... on Dry {          
          qty
          endAt
        }
      }
    }
    ${mixFragments.mixBase}
  `,
};

export const TASK_LIST = gql`
  query UserTasks($cursor: String, $plantId: ID) {
    tasks(plantId: $plantId, first: ${Config.ui.DEFAULT_LIST_SIZE}, after: $cursor, orderBy: { field:SCHEDULED_AT, direction:ASC }) {
      edges {
        node {
          ...TaskBase
        }
      }
      pageInfo {
        endCursor
        hasNextPage
      }
    }
  }
  ${taskFragments.taskBase}
`;

export const TASK_PROFILE = gql`
  query TaskProfile($id: ID!) {
    task(id: $id) {
      ...TaskBase
      ...TaskProfile
    }
  }
  ${taskFragments.taskBase}
  ${taskFragments.taskProfile}
`;

export const TASK_DELETE = gql`
  mutation deleteTask($id: ID!) {
    deleteTask(id: $id) {
      id
    }
  }
`;

export const TASK_UPDATE = gql`
  mutation updateTask($id: ID!, $input: TaskInput) {
    updateTask(id: $id, input: $input) {
      ...TaskBase
      ...TaskProfile
    }
  }
  ${taskFragments.taskBase}
  ${taskFragments.taskProfile}
`;

export const TASK_CREATE = gql`
  mutation createTask($input: CreateTaskInput) {
    createTask(input: $input) {
      ...TaskBase
      ...TaskProfile
    }
  }
  ${taskFragments.taskBase}
  ${taskFragments.taskProfile}
`;

export const TASK_OPTIMISTIC_EXECUTED = {
  props: ({ mutate }) => ({
    updateTask(id, input) {
      return mutate({
        variables: { id, input },
        optimisticResponse: {
          __typename: 'Mutation',
          updateTask: {
            id: id,
            __typename: 'Task',
            ...input,
          },
        },
      });
    },
  }),
};

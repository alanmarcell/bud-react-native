import { gql } from 'react-apollo';

export const sessionFragments = {
  user: gql`
    fragment User on User {
      id
      name
      nickname
    }
  `,
  userSession: gql`
    fragment UserSession on Session {
      user {
        id
        name
        nickname
        recoveryToken
        userType
      }
      token
      refreshToken
    }
  `
};

export const SESSION_QUERY = gql`
  query Session {
    session {
      ...UserSession
    }
  }
  ${sessionFragments.userSession}
`;

export const LOGIN_QUERY = gql`
  mutation Login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      session {
        ...UserSession
      }
    }
  }
  ${sessionFragments.userSession}
`;


export const RECOVERY_QUERY = gql`
  mutation RecoveryUser($token: String!, $email: String!, $password: String!) {
    recoveryUserPassword(recoveryToken: $token, email: $email, password: $password) {
      session {
        ...UserSession
      }
    }
  }
  ${sessionFragments.userSession}
`;

export const REGISTER_QUERY = gql`
  mutation Register($user: CreateUserInput!) {
    createUser(input: $user) {
      session {
        ...UserSession
      }
    }
  }
  ${sessionFragments.userSession}
`;

export const UPDATE_USER_TYPE = gql`
  mutation UpdateUserType($userType: [UserTypes]!) {
    updateUserType(userType: $userType) {
      userType
    }
  }
`;

import { gql } from 'react-apollo';

export const QRCODE_SEARCH = gql`
  query QRCode($qrCode: String!) {
    qrCode(qrCode: $qrCode) {
      id
      code
      product {
        ... on Node {
          id
        }
      }
    }
  }
`;

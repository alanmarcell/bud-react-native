import { gql } from 'react-apollo';

export const SEND_CONTACT = gql`
  mutation sendContact($input: ContactInput!) {
    sendContact(input: $input) {
      id
    }
  }
`;

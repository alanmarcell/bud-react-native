import { gql } from 'react-apollo';
import { strainFragments } from 'graphql/strains';
import { fileFragments } from 'graphql/files';

import Config from 'config';

export const plantFragments = {
  plantBase: gql`
    fragment PlantBase on Plant {
      id
      name
      gender
      genetic
      source
      environment
      stage
      bornAt
      cloneAmount
      strain {
        ...StrainBase
      }
    }
    ${strainFragments.strainBase}
  `,
  plantProfile: gql`
    fragment PlantProfile on Plant {
      description
      specie
      genetic
      source
      environment
      stage
      bornAt
      changedStageAt
      startWeek
      plantOrigin {
        ...PlantBase
      }
     owner {
        id
        name
      }
      qrCode {
        id
        code
      }
    }
  `,
  plantParents: gql`
    fragment PlantParents on Plant {
      mother {
        ... Parent
      }
      father {
        ... Parent
      }
    }

    fragment Parent on Family {
      ... on Node {
        id
      }
      ... on Strain {
        name
        breeder
      }
    }
  `,
  plantFiles: gql`
    fragment PlantFiles on Plant {
      files {
        ...FileBase
      }
    }
    ${fileFragments.fileBase}
  `
};

const taskFragments = {
  taskBase: gql`
    fragment TaskBase on Task {
      id
      scheduledAt
      executedAt
      plant {
        ...PlantBase
      }
      meta {
        type
      }
    }
    ${plantFragments.plantBase}
  `
};

export const PLANT_SEARCH = gql`
  query Plant($query: String, $cursor: String) {
    plants(search: $query, after: $cursor, orderBy: { field: NAME } ) {
      edges {
        node {
          id
          name
          strain {
            id
            name
            breeder
          }
        }
      }
      pageInfo {
        endCursor,
        hasNextPage,
      }
    }
  }
`;

export const PLANT_LIST = gql`
  query UserPlants($cursor: String) {
    plants(first: ${Config.ui.DEFAULT_LIST_SIZE}, after: $cursor) {
      edges {
        node {
          ...PlantBase
        }
      }
      pageInfo {
        endCursor
        hasNextPage
      }
    }
  }
  ${plantFragments.plantBase}
`;

// TODO tasks on this query is useless
// Tasks on plant profile come from TasksListController
// We should remove it in the apropriate time
export const PLANT_PROFILE = gql`
  query PlantProfile($id: ID!, $cursor: String) {
    plant(id: $id) {
      ...PlantBase
      ...PlantProfile
      ...PlantFiles
      tasks(first: ${Config.ui.DEFAULT_LIST_SIZE}, after: $cursor, orderBy: { field:SCHEDULED_AT, direction:ASC }) {
        edges {
          node {
            ...TaskBase
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
  ${plantFragments.plantBase}
  ${plantFragments.plantProfile}
  ${taskFragments.taskBase}
  ${plantFragments.plantFiles}
`;

export const PLANT_QUERY = gql`
  query PlantQuery($id: ID!) {
    plant(id: $id) {
      ...PlantBase
      ...PlantProfile
      ...PlantParents
      ...PlantFiles
    }
  }
  ${plantFragments.plantBase}
  ${plantFragments.plantProfile}
  ${plantFragments.plantParents}
  ${plantFragments.plantFiles}
`;

export const PLANT_DELETE = gql`
  mutation deletePlant($id: ID!) {
    deletePlant(id: $id) {
      id
    }
  }
`;

export const PLANT_UPDATE = gql`
  mutation updatePlant($id: ID!, $input: PlantInput) {
    updatePlant(id: $id, input: $input) {
      ...PlantBase
      ...PlantProfile
      ...PlantParents
    }
  }
  ${plantFragments.plantBase}
  ${plantFragments.plantProfile}
  ${plantFragments.plantParents}
`;

export const PLANT_CREATE = gql`
  mutation createPlant($input: CreatePlantInput) {
    createPlant(input: $input) {
      ...PlantBase
      ...PlantProfile
      ...PlantParents
    }
  }
  ${plantFragments.plantBase}
  ${plantFragments.plantProfile}
  ${plantFragments.plantParents}
`;

export const PLANT_IMAGE_UPLOAD = gql`
  mutation addImagesToPlant($id: ID!, $photos: [PhotoInput!]) {
    addImagesToPlant(id: $id, photos: $photos) {
      result
    }
  }
`;


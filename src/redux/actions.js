const actions = {
  UPDATE_ACTIVE_USER_TYPE: 'UPDATE_ACTIVE_USER_TYPE',
  REMOVE_CREDENTIALS: 'REMOVE_CREDENTIALS',
  UPDATE_CREDENTIALS: 'UPDATE_CREDENTIALS'
};

const updateActiveUserType = userType => ({
  type: actions.UPDATE_ACTIVE_USER_TYPE,
  userType
});

const updateCredentials = credentials => ({
  type: actions.UPDATE_CREDENTIALS,
  credentials
});

const removeCredentials = () => ({
  type: actions.REMOVE_CREDENTIALS
});

export {
  actions,
  updateActiveUserType,
  updateCredentials,
  removeCredentials
};

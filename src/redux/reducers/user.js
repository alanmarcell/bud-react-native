import { isNil, merge } from 'ramda';
import featuresByProfile from 'utils/userProfilesFeatures';
import { actions } from '../actions';

const initialState = {
  loadingStore: true,
  session: null,
  token: null,
  email: null,
  userType: null,
  features: {}
};

const updateUserType = (state, userType) => merge(state, {
  userType,
  features: featuresByProfile[userType]
});

const updateCredentials = (state, credentials) => {
  if (isNil(credentials)) {
    return state;
  }

  const newState = merge(state, {
    session: credentials.session,
    token: credentials.token,
    email: credentials.email,
  });

  return updateUserType(newState, credentials.session.user.userType[0]);
};

/**
 * Remove session and token but keep E-mail.
 * @param {*} state 
 */
const removeCredentials = state => merge(state, {
  session: null,
  token: null,
  userType: null,
  features: {}
});

const user = (state = initialState, action = {}) => {

  // Debug store actions
  // console.log('action: ', action, ' state: ', state); // eslint-disable-line no-console

  switch (action.type) {

    case actions.UPDATE_CREDENTIALS:
      return updateCredentials(state, action.credentials);

    case actions.REMOVE_CREDENTIALS:
      return removeCredentials(state);

    case actions.UPDATE_ACTIVE_USER_TYPE:
      return updateUserType(state, action.userType);

    case 'REDUX_STORAGE_LOAD':
      return merge(state, { loadingStore: false });

    default:
      return state;
  }
};

export default user;

import { combineReducers } from 'redux';
import routes from './routes';
import user from './user';
// ... other reducers

export default combineReducers({
  routes,
  user
  // ... other reducers
});

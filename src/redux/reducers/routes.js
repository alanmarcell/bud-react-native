import { ActionConst } from 'react-native-router-flux';
import {merge} from 'ramda';

const initialState = {
  scene: {},
  features: {}
};

const routes = (state = initialState, action = {}) => {
  switch (action.type) {
    // focus action is dispatched when a new screen comes into focus
    case ActionConst.FOCUS:
      return merge(state, {
        scene: action.scene
      });
    
    default:
      return state;
  }
};

export default routes;

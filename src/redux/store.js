import { createStore, applyMiddleware } from 'redux';
import reducers from './reducers';
import * as storage from 'redux-storage';
import createEngine from 'redux-storage-engine-reactnativeasyncstorage';
import filter from 'redux-storage-decorator-filter';
import { actions } from './actions';

const reducer = storage.reducer(reducers);

const engine = filter(createEngine('my-save-key'), ['user']);

const middleware = storage.createMiddleware(engine, [], [
  actions.REMOVE_CREDENTIALS,
  actions.UPDATE_ACTIVE_USER_TYPE,
  actions.UPDATE_CREDENTIALS,
  'REDUX_STORAGE_LOAD'
]);

const createStoreWithMiddleware = applyMiddleware(middleware)(createStore);
const store = createStoreWithMiddleware(reducer);

const load = storage.createLoader(engine);

load(store)
  .then((newState) => console.log('Loaded state:', newState)) // eslint-disable-line no-console
  .catch(() => console.log('Failed to load previous state')); // eslint-disable-line no-console

export default store;

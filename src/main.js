// @flow
import 'whatwg-fetch';
import React, { PureComponent } from 'react';
import { View, Platform } from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';

// Global config
// https://github.com/facebook/react-native/issues/9250
declare var console: any;
console.disableYellowBox = true;

import { Actions } from 'react-native-router-flux';
import { ApolloProvider, createClient } from 'graphql';
import Config from './config';

import { Scenes, ScenesStyles } from './scenes';
import { Router } from 'react-native-router-flux';
import { connect, Provider } from 'react-redux';
import store from './redux/store';
import { removeCredentials } from './redux/actions';

import ModalController from 'controllers/LoadingModal';

// calculate styles
import lightTheme from './themes/light';
import EStyleSheet from 'react-native-extended-stylesheet';

const RouterWithRedux = connect()(Router);

// const originalFetch = fetch;
// global.fetch = (url, options) => {
//   let req = originalFetch(url, options);
//   if (options.timeout) {
//     req = (new Promise(req.then.bind(req))).timeout(options.timeout);
//   }
//   return req;
// };

EStyleSheet.build(lightTheme);


// DEBUG Store
// __DEV__ && store.subscribe(() => console.log('new state: ', store.getState()));

class Main extends PureComponent {

  componentWillMount() {
    this.client = createClient(this);
  }

  handleUnauthorized = async () => {
    await this.props.removeCredentials();
    this.client.resetStore();
    Actions['unLogged']();
  }

  render() {
    const { loadingStore, activeUserType } = this.props;

    if (loadingStore) {
      return <ModalController loadType="spinner" />;
    }

    const scenes = Scenes(this);
    const actual = scenes[activeUserType ? activeUserType.toLowerCase() : 'unLogged'];
    Object.assign(actual, {
      initial: true
    });

    return (
      <ApolloProvider client={this.client}>
        <View style={{ flex: 1 }}>
          <RouterWithRedux
            scenes={scenes}
            sceneStyle={ScenesStyles.scene}
            navigationBarStyle={ScenesStyles.navigationBar}
            titleStyle={ScenesStyles.navbarTitle}
          />
          {Platform.OS === 'ios' ? <KeyboardSpacer /> : null}
        </View>
      </ApolloProvider>
    );
  }
}

const mapStateToProps = state => ({
  loadingStore: state.user.loadingStore,
  activeUserType: state.user.userType
});

const mapDispatchToProps = dispatch => ({
  removeCredentials: () => dispatch(removeCredentials())
});

const ConnectedMain = connect(mapStateToProps, mapDispatchToProps)(Main);

const MainContainer = () => {

  // log api config
  __DEV__ && console.log(`Api config, codename: ${Config.api.codename}, url: ${Config.api.baseURL}`); // eslint-disable-line no-console

  return (
    <Provider store={store}>
      <ConnectedMain />
    </Provider>
  );
};

export default MainContainer;

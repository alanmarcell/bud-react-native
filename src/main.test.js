import React from 'react';
import { shallow } from 'enzyme';
import { Promise } from 'bluebird';

import Main from './main';
import { context } from './__mocks__/stateHelper';

it.skip('renders correctly after delay', async () => {
  // Before delay
  let wrapper = shallow(<Main />).dive();
  expect(wrapper.find('ModalLoading').length).toEqual(1);

  // After delay
  await Promise.delay(1100);
  wrapper = wrapper.dive(context);
  expect(wrapper.find('AppController').length).toEqual(1);
});

import { Platform } from 'react-native';

const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 54;
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : 0;

const pallet = {
  Purple         : '#360968',
  DarkPurple     : '#29074D',
  Green          : '#24B574',
  LightGreen     : '#7AD3AB',
  Red            : '#C23227',
  GreenLowOpacity: 'rgba(36, 181, 116, 0.3)',
  GrayLowOpacity : '#3C484A80',
  Gray           : '#3C484A',  // Arsenic
  Silver         : '#495157',  // Abbey
  LightSilver    : '#EEEEEE',  // ??

  // Stars
  yellowStar       : '#FFC943', // Sunglow
  grayStar         : '#95989A', // Aluminium
  unSelectedStar   : '#EAEAEB', // Lily white
  medicalRed       : '#ff3232',
  medicalRedOff    : '#ffdcdb',
  medicalYellow    : '#fbe301',
  medicalYellowOff : '#fff7b7',
  medicalGreen     : '#67e500',
  medicalGreenOff  : '#e0ffda',

  // Components
  Disabled         : '#95989A',
  Loading          : '#A7E1C7',
  navbarBackground : '#24B574',
  navbarTitle      : '#FFFFFF',
  navbarIconColor  : '#FFFFFF',
  tabBarBackground : '#3C484A',
  tabBarFocused    : '#FFFFFF',
  cardsBackground  : '#FFFFFF',
  background       : '#F1F1F1',
  fieldSetHeader   : '#E1E1E1',
};

const Colors = {
  ...pallet,
  primary      : pallet.Green,
  primaryInvert: '#FFFFFF',
};

const lightTheme =  {
  Colors,

  dimensions: {
    navBarheight: APPBAR_HEIGHT + STATUSBAR_HEIGHT,
  },

  forms: {
    paddingHorizontal: 20,
  },

  Thumbs: {
    borderRadius: 68,
  },

  Buttons: {
    borderRadius: 4,
  },
};

export default lightTheme;

export {
  lightTheme
};
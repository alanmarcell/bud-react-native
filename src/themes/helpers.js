// @flow
import EStyleSheet from 'react-native-extended-stylesheet';

const stylesHelpers = EStyleSheet.create({
  // 3 columns card
  leftColumn: {
    flex: 1,
    // alignItems: 'center',
    // justifyContent: 'center',
  },
  middleColumn: {
    flex: 3,
    flexDirection: 'column',
    // justifyContent: 'center',
  },
  rightColumn: {
    flex: 1,
    // alignItems: 'flex-end',
    // justifyContent: 'flex-end',
  },

  // Alignments
  alignCenter: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  alignTopCenter: {
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  alignCenterLeft: {
    justifyContent: 'center',
  },
  alignCenterRight: {
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  alignBottomRight: {
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
});

export {stylesHelpers};
export default stylesHelpers;
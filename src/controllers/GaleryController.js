// @flow
import React from 'react';
import RootController from './RootController';
import Galery from '../components/Galery';

class GaleryController extends RootController {
  static navigationOptions = {
    title: 'screens.drawer.galery'
  };

  get actions () {
    return {};
  }

  renderView(props) {
    return (
      <Galery {...props} />
    );
  }
}

export default GaleryController;


import React from 'react';
import uuid from 'uuid';

import { I18n } from 'utils/I18n';
import U from 'utils';

import WithSearchController from '../helpers/WithSearchController';

import { graphql, compose } from 'graphql';
import {
  PESTICIDE_CREATE, PESTICIDE_SEARCH,
} from 'graphql';

import { SearchSelectFactory } from 'utils/Form/Factories';

class PesticideFactory extends WithSearchController {
  static propTypes = {
    ...WithSearchController.propTypes,
    ...SearchSelectFactory.propTypes,
  };

  static defaultProps = {
    ...WithSearchController.defaultProps,
    ...SearchSelectFactory.defaultProps,
  };

  get actions() {
    return {
      notBlock: {
        onChange: async (value) => {
          const { onChange, createPesticide } = this.props;
          if (value.index === '__new__') {
            value.index = uuid.v4();
            const input = { name: value.label, id: value.index };
            await createPesticide({
              variables: { input }
            });
          }
          onChange(value);
        },

        searchFn: async (query, current) => {
          const scope = ['forms', 'searchParent'];
          query = query.trim();
          const results = await this.search(PESTICIDE_SEARCH, query, 'pesticides');

          const extras = [];
          if (!U.isBlank(current) && !U.isBlank(current.index)) {
            extras.push({
              string: I18n.t([...scope, 'remove'], current),
              original: null,
            });
          }

          if (!U.isBlank(query)) {
            extras.push({
              string: I18n.t([...scope, 'add'], { query }),
              original: {
                index: '__new__',
                label: query,
                type : 'PESTICIDE'
              }
            });
          }

          results.data = [...extras, ...results.data];
          return results;
        }
      }
    };
  }

  searchMap(node, term, _type) {
    return {
      term,
      string: node.name,
      original: {
        index: node.id,
        label: node.name,
        brand: node.brand,
        type : 'PESTICIDE',
      }
    };
  }

  _format(string, pesticide, forContent = false) {
    if (pesticide) {
      const title = forContent ? 'brand' : '**brand**';
      return `${string}${pesticide.brand ?  ` - ${title}: ${pesticide.brand}` : ''}`;
    } else {
      return string;
    }
  }

  renderView(props) {
    return (
      <SearchSelectFactory
        {...props}
        formatText={this._format}
      />
    );
  }
}

export default compose(
  graphql(PESTICIDE_CREATE, { name: 'createPesticide' })
)(PesticideFactory);


import React from 'react';
import uuid from 'uuid';

import { I18n } from 'utils/I18n';
import U from 'utils';

import WithSearchController from '../helpers/WithSearchController';

import { graphql, compose } from 'graphql';
import { PEST_CREATE, PEST_SEARCH } from 'graphql';

import { SearchSelectFactory } from 'utils/Form/Factories';

class PestFactory extends WithSearchController {
  static propTypes = {
    ...WithSearchController.propTypes,
    ...SearchSelectFactory.propTypes,
  };

  static defaultProps = {
    ...WithSearchController.defaultProps,
    ...SearchSelectFactory.defaultProps,
  };

  get actions() {
    return {
      notBlock: {
        onChange: async (value) => {
          const { onChange, createPest } = this.props;
          if (value && value.index === '__new__') {
            value.index = uuid.v4();
            const input = { name: value.label, id: value.index };
            await createPest({
              variables: { input }
            });
          }
          onChange(value);
        },

        searchFn: async (query, current) => {
          const scope = ['forms', 'searchParent'];
          query = query.trim();
          const results = await this.search(PEST_SEARCH, query, 'pests');

          const extras = [];
          if (!U.isBlank(current) && !U.isBlank(current.index)) {
            extras.push({
              string: I18n.t([...scope, 'remove'], current),
              original: null,
            });
          }

          if (!U.isBlank(query)) {
            extras.push({
              string: I18n.t([...scope, 'add'], { query }),
              original: {
                index: '__new__',
                label: query
              }
            });
          }

          results.data = [...extras, ...results.data];
          return results;
        }
      }
    };
  }

  renderView(props) {
    return (
      <SearchSelectFactory {...props} />
    );
  }
}

export default compose(
  graphql(PEST_CREATE, { name: 'createPest' })
)(PestFactory);

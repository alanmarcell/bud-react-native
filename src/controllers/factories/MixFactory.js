import React from 'react';

import { I18n } from 'utils/I18n';
import U from 'utils';

import WithSearchController from '../helpers/WithSearchController';

import { MIX_SEARCH } from 'graphql';
import { SearchSelectFactory } from 'utils/Form/Factories';

export default class MixFactory extends WithSearchController {
  static propTypes = {
    ...WithSearchController.propTypes,
    ...SearchSelectFactory.propTypes,
  };

  static defaultProps = {
    ...WithSearchController.defaultProps,
    ...SearchSelectFactory.defaultProps,
  };

  get actions() {
    return {
      notBlock: {
        searchFn: async (query, current) => {
          const scope = ['forms', 'searchParent'];
          query = query.trim();
          const results = await this.search(MIX_SEARCH, query, 'mixes');

          const extras = [];
          if (!U.isBlank(current) && !U.isBlank(current.index)) {
            extras.push({
              string: I18n.t([...scope, 'remove'], current),
              original: null,
            });
          }

          results.data = [...extras, ...results.data];
          return results;
        },
        actionEmpty: () => this.redirectTo('grower_mixAdd'),
      },

    };
  }

  renderView(props) {
    const actionEmptyText = I18n.t('actions.newMix');
    const nextProps = {
      ...props,
      actionEmptyText
    };

    return (
      <SearchSelectFactory {...nextProps } />
    );
  }
}

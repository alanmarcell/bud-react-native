import React from 'react';
import { Text, View } from 'react-native';
import { Platform } from 'react-native';
import Modal from 'react-native-animated-modal';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import StatusBarSizeIOS from 'react-native-status-bar-size';
import { I18n } from 'utils/I18n';
import U from 'utils';

import WithSearchController from '../helpers/WithSearchController';
import { ORIGIN_SEARCH } from 'graphql';
import { SearchFactory, } from 'utils/Form/Factories';
import styles from 'utils/Form/Factories/SearchSelectFactory/styles';

class SearchOriginModal extends SearchFactory {

  _handlerBack = () => {
    const { onChangeCb } = this.props.passProps;
    
    onChangeCb();
  };

  renderModal(label) {
    const { showSearch } = this.state;
    const modalProps = {
      isVisible: showSearch,
      onModalHide: this._handlerBack
    };
    return (
      <Modal
        style={styles.modal}
        {...modalProps}
      >
        <View style={[styles.modalContainer, { borderTopWidth: StatusBarSizeIOS.currentHeight }]}>
          <View style={styles.search}>
            <View style={styles.titleContainer}>
              <View style={styles.title}>
                <Text style={styles.titleText}>{label}</Text>
              </View>
              {this.renderBack()}
            </View>
            {this.renderInput()}
          </View>
          {this.renderResult()}
        </View>
        {Platform.OS === 'ios' ? <KeyboardSpacer /> : null}
      </Modal>
    );
  }
}

class SearchOriginFactory extends WithSearchController {
  static propTypes = {
    ...WithSearchController.propTypes,
    ...SearchFactory.propTypes,
  };

  static defaultProps = {
    ...WithSearchController.defaultProps,
    ...SearchFactory.defaultProps,
    showSearchOnInit: true
  };

  get actions() {
    return {
      notBlock: {

        searchFn: async (query, current) => {
          const scope = ['forms', 'searchParent'];
          query = query.trim();
          const results = await this.search(ORIGIN_SEARCH, query, 'origins');

          const extras = [];
          if (!U.isBlank(current) && !U.isBlank(current.key)) {
            extras.push({
              string: I18n.t([...scope, 'remove'], current),
              original: null,
            });
          }
          results.data = [...extras, ...results.data];
          return results;
        }
      }
    };
  }

  searchMap(node, term, _type) {
    if (node.code)
      return {
        term,
        string: node.name,
        original: {
          key: node.id,
          label: node.name,
          // code: node.code
        }
      };
  }

  _format(string, origin, forContent = false) {
    if (origin) {
      const title = forContent ? 'code' : '**code**';
      return `${string}${origin.code ? ` - ${title}: ${origin.code}` : ''}`;
    } else {
      return string;
    }
  }

  renderView(props) {
    const { onChange } = props;
    const { onChangeCb } = props.passProps;
    if (onChangeCb) {
      props.onChange = (args) => {
        if (args) {
          const { key, label } = args;
          onChange({ key, label });          
        } else {
          onChange(args);          
        }
      };
    }
    return (
      <SearchOriginModal
        {...props}
        formatText={this._format}
      />
    );
  }
}

export default SearchOriginFactory;


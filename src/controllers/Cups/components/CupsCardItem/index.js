import React, { PureComponent, PropTypes } from 'react';
import { View, Text } from 'react-native';

import SelectableCard from 'components/SelectableCard';
import Logo from 'components/Logo';

import {
  styles,
  logoStyles
} from './styles';
import stylesHelpers from '../../../../themes/helpers';

export default class CupsCardItem extends PureComponent {
  static defaultProps = {
    ...SelectableCard.defaultProps,
    subtitle: '',
  };

  static propTypes = {
    ...SelectableCard.propTypes,
    name: PropTypes.string.isRequired,
    subtitle: PropTypes.string.isRequired,
    logo: PropTypes.object,
  };

  render() {
    const { name, subtitle, logo } = this.props;
    return (
      <SelectableCard {...this.props}>
        <Logo image={logo} styles={logoStyles} />
        <View style={[stylesHelpers.middleColumn, stylesHelpers.alignCenterLeft]}>
          <Text style={styles.name}>{name}</Text>
          <Text style={styles.subtitle}>{subtitle}</Text>
        </View>
      </SelectableCard>
    );
  }
}

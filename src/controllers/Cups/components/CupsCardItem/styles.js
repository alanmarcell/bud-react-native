// @flow
import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  subtitle: {
    color: '$Colors.Gray',
    fontSize: 13
  },
  name: {
    color       : '$Colors.Gray',
    fontWeight  : 'bold',
    fontSize    : 14,
    marginBottom: 5,
  },
});

const logo = EStyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 50,
    height: 50,
    borderRadius: 25,
  }
});

const icon = EStyleSheet.create({
  container: {
    alignSelf: 'center',
    width: 50,
    height: 50,
    marginRight: 15,
    marginLeft: 20
  },

  icon: {
    fontSize: 18,
    color: '#95989A'
  },

  radius: {
    flex: 1,
    backgroundColor: '$Colors.LightSilver',
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const logoStyles = {
  logo,
  icon
};

export default styles;

export {
  styles,
  logoStyles
};

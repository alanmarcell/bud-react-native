import { SimpleSchema } from 'components/BudForms/Schemas';
import { NameSchema, ImageSchema, cleanLogoToOpen } from 'components/BudForms/Schemas';
import { pick, path, dissoc, dissocPath } from 'ramda';

const cleanDocToOpen = doc => {
  const cleanDoc = pick([
    'name',
    'description',
    'country',
    'city',
    'qrCode',
  ], doc);

  return doc.logo
    ? cleanLogoToOpen(doc.logo)(cleanDoc)
    : cleanDoc;
};

const cleanDocToSave = cup =>
  path(['logo', 'uri'], cup)
    ? dissocPath(['logo', 'uri'], cup)
    : dissoc('logo', cup);


const CupSchema = new SimpleSchema({
  name: {
    type: NameSchema,
    required: true,
  },

  description: {
    type: String,
    required: false,
  },

  country: {
    type: String,
    required: true,
  },

  city: {
    type: String,
    required: true,
  },

  logo: {
    type: ImageSchema,
    required: false
  },

  qrCode: {
    type: String,
    required: false,
  },

});

export {
  CupSchema,
  cleanDocToOpen,
  cleanDocToSave
};

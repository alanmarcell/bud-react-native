import React, { PureComponent } from 'react';
import { ActivityIndicator } from 'react-native';
import { InputFactory } from 'utils/Form/Factories';

import { I18n } from 'utils/I18n';
import { BudForms } from 'components/BudForms';
import { I18nField, DescriptionField, NameField } from 'components/BudForms/Fields';
import QRCodeFactory from 'components/BudForms/Factories/QRCodeFactory';
import LogoPicker from 'components/LogoPicker';

import {
  cleanDocToOpen,
  cleanDocToSave,
  CupSchema
} from './CupSchema';

class CupEdit extends PureComponent {
  static propTypes = {
    cup: React.PropTypes.object,
    loading: React.PropTypes.bool,
    saveCup: React.PropTypes.func.isRequired,
    submitText: React.PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    this._buildDocState = this._buildDocState.bind(this);

    this.state = {
      doc: this._buildDocState(props.cup)
    };
  }

  _handleSubmit = (cup) => {
    this.props.saveCup(cleanDocToSave(cup));
  };

  _handleChange = (doc) => {
    this.setState({ doc });
  }

  _buildDocState(cup = {}) {
    const { qrCode, ...doc } = cup;

    return doc;
  }

  componentWillReceiveProps(nextProps, _nextState) {
    const { cup } = nextProps;

    this.setState({ doc: this._buildDocState(cup) });
  }

  render() {
    const { loading, findQrCode } = this.props;

    const { doc } = this.state;

    if (loading) {
      return <ActivityIndicator />;
    }

    const formProps = {
      ref: 'form',
      schema: CupSchema,
      doc: cleanDocToOpen(doc),
      onChange: this._handleChange,
      onSubmit: this._handleSubmit,
      submitText: this.props.submitText,
    };

    return (
      <BudForms {...formProps}>
        <I18nField
          fieldName="logo"
          type={LogoPicker}
        />
        <NameField />
        <DescriptionField />
        <I18nField
          fieldName="country"
          placeholder={true}
          type={InputFactory}
        />
        <I18nField
          fieldName="city"
          placeholder={true}
          type={InputFactory}
        />
        <I18nField
          fieldName="qrCode"
          type={QRCodeFactory}
          findQrCode={findQrCode}
          newText={I18n.t('forms.qrCode.newText')}
          oldText={I18n.t('forms.qrCode.oldText')}
        />
      </BudForms>
    );
  }
}

export default CupEdit;

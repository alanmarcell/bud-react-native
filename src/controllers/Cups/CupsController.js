// @flow
import React from 'react';
import { View, ActivityIndicator } from 'react-native';

import RootController from '../RootController';
import ActionMenu from 'components/ActionMenu';

import CupsList from 'components/ProductsList';

import { graphql, makeLoadList } from 'graphql';
import { CUPS_LIST } from 'graphql/cups';

class CupsController extends RootController {
  static navigationOptions = {
    title: 'screens.cups.title',
  };
  
  get actions() {
    return {
      onProductPress: (objectId) => {
        this.redirectTo('cupProfile', { objectId });
      }
    };
  }

  onActionPress = () => {
    this.redirectTo('cupAdd');
  }

  renderView(props) {
    const {
      loading,
    } = this.props;

    const menuItems = {
      newCup: 'trophy',
    };

    if (loading) {
      return <ActivityIndicator />;
    }
    return (
      <View style={{ flex: 1 }}>
        <CupsList {...props} productsType="cups"/>
        <ActionMenu onPress={this.onActionPress} items={menuItems} />
      </View>
    );
  }
}

export default graphql(CUPS_LIST, makeLoadList(CUPS_LIST, 'cups'))(CupsController);

import CupsController from './CupsController';
import CupProfileController from './CupProfileController';
import CupEditController from './CupEditController';

export {
  CupProfileController,
  CupsController,
  CupEditController,
};

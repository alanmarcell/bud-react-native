// @flow
import React from 'react';
import PropTypes from 'prop-types';
import R from 'ramda';

import { productsList, profile, listProfile, ProductHeader, EditionCard } from 'components';
// import CupsCardItem from 'controllers/Cups/components/CupsCardItem';

import { graphql, compose, makeGet, makeDeleteMutation, } from 'graphql';
import { CUP_PROFILE, CUP_DELETE, SESSION_QUERY } from 'graphql';

const EditionsList = productsList(EditionCard);
const ProfileComponent = listProfile (ProductHeader, EditionsList);
const CupProfile = profile(ProfileComponent);

class CupProfileController extends React.PureComponent {
  static propTypes = {
    cup: PropTypes.object.isRequired,
    editions: PropTypes.array,
    loading: PropTypes.bool,
  }

  static defaultProps = {
    cup: {},
    editions: []
  }

  static navigationOptions = {
    title: 'screens.cup.title',
  };

  render() {
    const { objectId: cupId, cup, loading, deleteCup } = this.props;
    const onProductPressArgs = 'editionProfile';
    const productsType =  'editions';

    const menuItems = [{
      key: 'newEdition',
      menu: { newEdition: 'trophy' },
      redirectTo: 'editionAdd',
      args: { cupId }
    }];


    const openQuickIDProps = {
      icon: 'trophy',
      label: (name) => `Cup: ${name}`,
    };

    const products =  R.path([productsType, 'nodes'], cup);

    const details = [{
      icon: 'location',
      text: `${cup.city} - ${cup.country}`
    }];

    const profileComponentProps = {
      ...this.props,
      products,
      productsType,
      details,
      deleteProduct: deleteCup,
      editProduct: 'cupEdit',
      product: cup,
      loading,
      icon: 'trophy',
      menuItems,
      openQuickIDProps,
      onProductPressArgs,
      productType: 'cup'
    };

    return (<CupProfile {...profileComponentProps} />);
  }
}

export default compose(
  graphql(SESSION_QUERY, { props: ({ data }) => ({ ...data }) }),
  graphql(CUP_PROFILE, makeGet()),
  graphql(CUP_DELETE, makeDeleteMutation('deleteCup', 'cups', 'UserCups', {
    options: {
      refetchQueries: [
        'UserCups'
      ]
    }
  })),
)(CupProfileController);



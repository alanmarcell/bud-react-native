import React from 'react';
import uuid from 'uuid';
import SelectStrainController from '../helpers/SelectStrainController';

import { I18n } from 'utils/I18n';
import CupEdit from './components/CupEdit';
import { graphql, compose, propType } from 'graphql';
import { makeGet, mutationCreate } from 'graphql';
import {
  cupFragments,
  CUP_PROFILE, CUP_CREATE, CUP_UPDATE
} from 'graphql';

class CupEditController extends SelectStrainController {
  static propTypes = {
    cup: propType(cupFragments.cupBase),
    loading: React.PropTypes.bool,
  };

  static navigationOptions = {
    getTitle: ({ name }) => {
      const titleEdit = 'screens.cupEdit.title';
      const titleAdd  = 'screens.cupAdd.title';
      return I18n.T(/.*Add$/.test(name) ? titleAdd : titleEdit);
    }
  };

  get actions() {
    return {
      ...super.actions,
      saveCup: async ({...cup}) => {

        let { objectId, updateCup, createCup } = this.props;
        if (objectId) {
          await updateCup({ variables: { id: objectId, input: cup }});
          this.goBack();
        } else {
          objectId = uuid.v4();
          cup.id = objectId;
          await createCup({ variables: { input: cup }});

          this.redirectTo('cupProfile', { type: 'replace', objectId });          
        }
      },  
    };
  }

  renderView(props) {
    const { name } = this.props;
    const submitEdit = 'screens.cupEdit.submit';
    const submitAdd  = 'screens.cupAdd.submit';
    const submitText = I18n.t(/.*Add$/.test(name) ? submitAdd : submitEdit);

    return (
      <CupEdit {...props} submitText={submitText} />
    );
  }
}

const refetchQueries = ['UserCups'];

export default compose(
  graphql(CUP_PROFILE, makeGet()),
  graphql(CUP_UPDATE, { 
    name: 'updateCup',
    options: {
      refetchQueries
    } 
  }),
  graphql(CUP_CREATE, mutationCreate('createCup', 'Cup', 'cups', 'UserCups', refetchQueries)),
)(CupEditController);

// TODO: This file was not beeing used. Shoul we delete it? 

// import React from 'react';

// import { I18n } from 'utils/I18n';

// import RootController from './RootController';

// // Logged area
// import TourController          from 'controllers/TourController';

// // Plants
// import PlantsController        from 'controllers/PlantsController';
// import PlantProfileController  from 'controllers/PlantProfileController';
// import PlantEditController     from 'controllers/PlantEditController';

// // Tasks
// import TasksController         from 'controllers/TasksController';
// import TaskProfileController   from 'controllers/TaskProfileController';
// import TaskEditController      from 'controllers/TaskEditController';

// // Mixes
// import MixesController        from 'controllers/MixesController';
// import MixProfileController   from 'controllers/MixProfileController';
// import MixEditController      from 'controllers/MixEditController';

// // Buds
// import BudsController          from 'controllers/BudsController';
// import BudProfileController    from 'controllers/BudProfileController';
// import BudEditController       from 'controllers/BudEditController';

// // Buds' Review
// import ReviewEditController    from 'controllers/ReviewEditController';

// // QRCode
// import QuickIDController       from 'controllers/QuickIDController';
// import QuickIDShareController  from 'controllers/QuickIDShareController';

// // Contact
// import ContactController       from 'controllers/ContactController';

// import { default as ScenesStyles } from '../scenes/styles.js';

// import TabIcon from '../scenes/TabIcon';
// import BackButton from '../scenes/BackButton';

// TODO: 'TabBar' is defined but never used
// Why this was created and why is not beeing used?
//
// class TabBar extends RootController {

//   renderView(props) {
//     const { shouldFeatureVisible } = props;

//     return (
//       <Scene key="mainTab" type="replace" tabs tabBarStyle={ScenesStyles.tabBar} >
//         <Scene key="tasksTab" title="tasks" iconName="calendar-check-o" icon={shouldHideOption(shouldFeatureVisible, 'tasks')} drawerIcon={drawerIcon}>
//           <Scene key="tasksList" {...stackScreen(TasksController)} />
//           <Scene key="taskProfile" {...stackScreen(TaskProfileController)} />
//           <Scene key="taskEdit" {...stackScreen(TaskEditController)} />
//         </Scene>
//         <Scene key="mixesTab" title="mixes" iconName="mixes" icon={shouldHideOption(shouldFeatureVisible, 'mixes')} drawerIcon={drawerIcon}>
//           <Scene key="mixesList" {...stackScreen(MixesController)} />
//           <Scene key="mixProfile" {...stackScreen(MixProfileController)} />
//           <Scene key="mixEdit" {...stackScreen(MixEditController)} />
//           <Scene key="mixAdd" {...stackScreen(MixEditController)} />
//         </Scene>
//         <Scene key="plantsTab" title="plants" iconName="budbuds" icon={shouldHideOption(shouldFeatureVisible, 'plants')} drawerIcon={drawerIcon} initial={true}>
//           <Scene key="plantsList" {...stackScreen(PlantsController)} />
//           <Scene key="plantProfile" {...stackScreen(PlantProfileController)} />
//           <Scene key="plantEdit" {...stackScreen(PlantEditController)} />
//           <Scene key="plantAdd" {...stackScreen(PlantEditController)} />
//           <Scene key="plantQrCode" {...stackScreen(QuickIDShareController)} />

//           <Scene key="plantTaskProfile" {...stackScreen(TaskProfileController)} />
//           <Scene key="plantTaskEdit" {...stackScreen(TaskEditController)} />
//           <Scene key="plantTaskAdd" {...stackScreen(TaskEditController)} />

//           <Scene key="plantBudAdd" {...stackScreen(BudEditController)} />
//         </Scene>
//         <Scene key="budsTab" title="buds" iconName="bud" icon={shouldHideOption(shouldFeatureVisible, 'buds')} drawerIcon={drawerIcon}>
//           <Scene key="budsList" {...stackScreen(BudsController)} {...app.passProps} />
//           <Scene key="budProfile" {...stackScreen(BudProfileController)} />
//           <Scene key="budEdit" {...stackScreen(BudEditController)} />
//           <Scene key="budAdd" {...stackScreen(BudEditController)} />
//           <Scene key="budQrCode" {...stackScreen(QuickIDShareController)} />
//           <Scene key="reviewAdd" {...stackScreen(ReviewEditController)} />
//         </Scene>
//         <Scene key="qrCodeTab" title="qrcode" iconName="qrcode" icon={shouldHideOption(shouldFeatureVisible, 'quickId')} drawerIcon={drawerIcon}>
//           <Scene key="qrCodeReader" {...stackScreen(QuickIDController)} />
//         </Scene>
//         <Scene key="contactTab" title="contact" iconName="contact" drawerIcon={drawerIcon}>
//           <Scene key="contact" {...stackScreen(ContactController)} />
//         </Scene>
//         <Scene key="tour" component={TourController} hideNavBar hideTabBar />
//       </Scene>
//     );
//   }
// }

// function shouldHideOption(shouldFeatureVisible, feature) {
//   return shouldFeatureVisible(feature) && TabIcon;
// }

// function getSceneStyle(state, props) {
//   const { hideNavBar, hideTabBar } = props;
//   return [
//     ...(!hideNavBar ? [ScenesStyles.container] : []),
//     ...(!hideTabBar ? [ScenesStyles.intoTab] : [])
//   ];
// }

// function stackScreen(component) {
//   return {
//     component,
//     getSceneStyle,
//     backButton: BackButton,
//     leftButtonStyle: ScenesStyles.leftButtonStyle,
//     getTitle: ({ title }) => I18n.T(title),
//     ...component.navigationOptions || {},
//   };
// }

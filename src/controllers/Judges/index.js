import JudgeEditController from './JudgeEditController';
import JudgeProfileController from './JudgeProfileController';
import JudgeAssocController from './JudgeAssocController';
import JudgesController from './JudgesController';

export {
  JudgeEditController,
  JudgeProfileController,
  JudgeAssocController,
  JudgesController
};

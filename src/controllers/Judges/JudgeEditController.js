import React from 'react';
import uuid from 'uuid';

import { I18n } from 'utils/I18n';
import JudgeEdit from './components/JudgeEdit';
import RootController from '../RootController';

import { graphql, compose, propType } from 'graphql';
import { makeGet, mutationCreate } from 'graphql';
import {
  judgeFragments,
  JUDGE_PROFILE,
  JUDGE_CREATE_FOR_EDITION,
  JUDGE_CREATE_FOR_CATEGORY,
  JUDGE_UPDATE
} from 'graphql';

class JudgeEditController extends RootController {
  static propTypes = {
    judge: propType(judgeFragments.judgeBase),
    loading: React.PropTypes.bool,
  };

  static navigationOptions = {
    getTitle: () => {
      return I18n.T('screens.judges.add.title');
    }
  };

  get actions() {
    return {
      saveJudge: async ({ ...judge }) => {

        let { objectId, updateJudge, createJudgeForEdition, createJudgeForCategory } = this.props;
        if (objectId) {
          await updateJudge({ variables: { id: objectId, input: judge } });
          this.goBack();
        } else {
          objectId = uuid.v4();
          judge.id = objectId;

          const createJudge = judge.categoryId ? createJudgeForCategory : createJudgeForEdition;

          await createJudge({ variables: { input: judge } });

          this.redirectTo('judgeProfile', { type: 'replace', objectId });
        }
      }
    };
  }

  renderView(props) {

    const submitText = I18n.t('screens.judges.forms.submit');
    return (
      <JudgeEdit {...props} submitText={submitText} />
    );
  }
}

export default compose(
  graphql(JUDGE_PROFILE, makeGet()),
  graphql(JUDGE_UPDATE, { name: 'updateJudge' }),
  // TODO: fix mutationCreate
  graphql(JUDGE_CREATE_FOR_EDITION, mutationCreate('createJudgeForEdition', 'Judge', 'categories', 'UserCategories')),
  graphql(JUDGE_CREATE_FOR_CATEGORY, mutationCreate('createJudgeForCategory', 'Judge', 'categories', 'UserCategories'))
)(JudgeEditController);

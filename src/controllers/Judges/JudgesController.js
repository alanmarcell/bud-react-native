import React from 'react';
import RootController from '../RootController';
import JudgesList from './components/JudgesList';
import { View } from 'react-native';
import { graphql, compose, makeGet } from 'graphql';
import { JUDGE_LIST, SESSION_QUERY } from 'graphql';
import { isEditionOwner } from '../Editions/isEditionOwner';
import ActionMenu from 'components/ActionMenu';
import R from 'ramda';

class JudgesController extends RootController {
  static navigationOptions = {
    title: 'screens.judges.list.title',
  };

  get actions() {
    return {
      onJudgePress: (objectId) => {
        this.redirectTo('judgeProfile', { objectId });
      },
    };
  }

  onActionPress = (option) => {
    switch (option) {
      case 'newJudge': {
        const { objectId } = this.props;
        this.redirectTo('judgeEdit', { editionId: objectId });
        return;
      }
      case 'assocJudge': {
        this.redirectTo('judgeAssoc');
        return;
      }
    }
  }

  renderActionMenu(props) {

    const judges = props.edition ? props.edition.judges.nodes : [];

    const assocJudge = R.isEmpty(judges)
      ? null
      : { assocJudge: 'judge' };

    const menuItems = isEditionOwner(props)
      ? {
        newJudge: 'judge',
        ...assocJudge
      }
      : assocJudge;

    return <ActionMenu onPress={this.onActionPress} items={menuItems} />;
  }

  renderView(props) {
    const judges = props.edition ? props.edition.judges.nodes : [];
    return (
      <View style={{ flex: 1 }}>
        <JudgesList {...props} judges={judges} isProductOwner={isEditionOwner(props)} />
        {this.renderActionMenu(props)}
      </View>
    );
  }
}

export default compose(
  graphql(SESSION_QUERY, { props: ({ data }) => ({ ...data }) }),
  graphql(JUDGE_LIST, makeGet()),
)(JudgesController);

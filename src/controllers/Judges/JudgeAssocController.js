import React from 'react';
import { I18n } from 'utils/I18n';
import JudgeAssoc from './components/JudgeAssoc';
import RootController from '../RootController';
import R from 'ramda';
import { graphql, compose } from 'graphql';
import {
  JUDGE_ASSOCIATE,
} from 'graphql';

class JudgeAssocController extends RootController {
  static navigationOptions = {
    getTitle: () => {
      return I18n.T('screens.judges.assoc.title');
    }
  };

  get actions() {
    return {
      assocJudge: async ({ inviteCode }) => {

        let { assocJudge } = this.props;
        const result = await assocJudge({ variables: { inviteCode } });
        
        const judgeId = R.path(['data', 'associateJudge', 'id'], result);
        if (judgeId) {
          this.redirectTo('judgeProfile', { type: 'replace', objectId: judgeId });
        }
      }
    };
  }

  renderView(props) {
    const submitText = I18n.t('screens.judges.assoc.submit');
    return (
      <JudgeAssoc {...props} submitText={submitText} />
    );
  }
}

export default compose(
  graphql(JUDGE_ASSOCIATE, { name: 'assocJudge', options: {
    refetchQueries: ['JudgeList', 'JudgeProfile']
  } }),
)(JudgeAssocController);

import React, { Component, PropTypes } from 'react';
import { View, Text } from 'react-native';
import R from 'ramda';

import SelectableCard from 'components/SelectableCard';
import RoundedIcon from 'components/RoundedIcon';
import Icon from 'components/Icon';

import styles from './styles';
import stylesHelpers from '../../../../themes/helpers';

export default class JudgeCardItem extends Component {
  static defaultProps = {
    ...SelectableCard.defaultProps,
  };

  static propTypes = {
    ...SelectableCard.propTypes,
    name: PropTypes.string.isRequired,
  };

  render() {

    const { name, judgeUser } = this.props;

    const icon = R.isNil(judgeUser) ? 'warning' : 'user';

    return (
      <SelectableCard {...this.props}>
        <View style={[stylesHelpers.leftColumn, stylesHelpers.alignCenter]}>
          <RoundedIcon object={this.props} source="judge" />
        </View>
        <View style={[stylesHelpers.middleColumn, stylesHelpers.alignCenterLeft]}>
          <Text style={styles.judgeName}>{name}</Text>
        </View>
        <View style={[stylesHelpers.rightColumn, stylesHelpers.alignCenterRight]}>
          <Icon style={styles.score} name={icon} />
        </View>
      </SelectableCard>
    );
  }
}

// @flow
import EStyleSheet from 'react-native-extended-stylesheet';

const borderColor = '$Colors.LightSilver';
const iconCommon = {
  paddingTop: 2,
  marginRight: 5,
};

const lineCommon = {
  flexDirection : 'row',
  justifyContent: 'space-between',
  alignItems    : 'center',
};

const styles = EStyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    padding        : 15,
  },
  headerWrapper: {
    flexDirection  : 'row',
    borderColor,
    borderBottomWidth: 0.5,
    paddingBottom: 10,
  },
  header: {
    ...lineCommon,
    flex         : 1,
    paddingBottom: 10,
  },
  info: {
    flex        : 1,
    paddingLeft : 10,
    paddingRight: 10,
  },

  qrcode: {
    color: 'black',
    fontSize: 30,
  },

  footer: {
    ...lineCommon,
    borderColor,
    paddingLeft   : 10,
    paddingRight  : 10,
    paddingTop    : 10,
    borderTopWidth: 1,
  },

  shareContainer: {
    ...lineCommon,
    borderColor,
    padding       : 10,
    borderTopWidth: 1,
  },
  shareText: {
    flex        : 1,
    paddingLeft : 10,
    paddingRight: 10,
  },

  date: {
    flexDirection : 'row',
  },
  dateIcon: iconCommon,

  /* PlantFamily
   */
  plantFamily: {
    flexDirection : 'row',
  },
  plantFamilyIcon: iconCommon,

  /* PlantFlowering
   */
  plantFlowering: {
    flexDirection : 'row',
  },
  plantFloweringIcon: iconCommon,

  infoList: {
    marginTop: 15,
  },

  infoListRow: {
    borderRadius: 2,
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 10,
    alignItems: 'center',
    height: 40,
  },

  infoListRowGrayBackground: {
    backgroundColor: '$Colors.LightSilver',
  },

  infoListLeftCol: {
    flexDirection: 'row',
    flex: 2,
  },

  infoListRightCol: {
    flexDirection: 'row',
    flex: 2,
  },

  infoIcon: {
    marginRight: 10,
    width: 18,
    height: 18,
  },
  infoLabel: {
    fontSize: 12,
    color: '$Colors.Gray',
  },

});

const inviteCodeStyles = EStyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  code: {
    padding: 10,
    fontSize: 28,
    textAlign: 'center',
  },
  touchableShare: {
    padding: 10
  },
  shareIcon: {
    textAlign: 'center', 
    fontSize: 20
  },
  title: {
    textAlign: 'center'  
  }
});

export default styles;

export {
  styles,
  inviteCodeStyles
};

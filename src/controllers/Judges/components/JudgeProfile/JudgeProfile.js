import React from 'react';
import Refreshable from 'utils/components/Refreshable';
import { styles as profileStyles, inviteCodeStyles } from './styles';
import { Text, View, ActivityIndicator, ScrollView } from 'react-native';
import { Touchable } from 'utils/components';
import { Title } from 'components';
import RoundedIcon from '../../../../components/RoundedIcon';
import withShare from '../../../helpers/withShare';
import { I18n } from 'utils/I18n';
import Icon from 'components/Icon';
import R from 'ramda';

class InviteCode extends React.PureComponent {
  getOptions = async () => {
    return {
      title: 'BudBuds',
      message: this.props.judge.inviteCode
    };
  }

  render() {
    const { judge, authedUserIsOwner } = this.props;

    if (!authedUserIsOwner || !R.isNil(judge.judgeUser)) {
      return null;
    }

    const styles = inviteCodeStyles;

    return (
      <View style={styles.container}>
        <Touchable
          style={styles.touchableShare}
          onPress={this.props.handleShare(this.getOptions)}
        >
          <Text style={styles.title}>
            {I18n.t('screens.judges.shareInviteCode')}
          </Text>
          <Text style={styles.code}>
            {judge.inviteCode}
          </Text>
          <Icon name="share-alt" style={styles.shareIcon} />
        </Touchable>
      </View>
    );
  }
}

class JudgeProfile extends React.PureComponent {
  static propTypes = {
    edition: React.PropTypes.object,
    loading: React.PropTypes.bool,
  }

  render() {
    const {
      loading,
      refetch,
      judge
    } = this.props;

    if (loading) {
      return <ActivityIndicator />;
    }

    const styles = profileStyles;

    return (
      <ScrollView refreshControl={Refreshable({ loading, refetch })}>
        <View style={styles.container}>
          <RoundedIcon object={judge} source={'judge'} />
          <Title title={judge.name} />
        </View>
        {judge.judgeUser
          ? (
            <View>
              <Text>{judge.judgeUser.name}</Text>
            </View>
          )
          : (<InviteCode {...this.props} />)
        }
      </ScrollView>
    );
  }
}

export default withShare(JudgeProfile);

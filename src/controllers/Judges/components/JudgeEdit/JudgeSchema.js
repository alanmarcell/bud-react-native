import { SimpleSchema } from 'components/BudForms/Schemas';
import { NameSchema } from 'components/BudForms/Schemas';

export const JudgeSchema = new SimpleSchema({
  name: {
    type: NameSchema,
    required: true,
  },
});

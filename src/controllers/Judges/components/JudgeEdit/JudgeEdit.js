import React from 'react';
import { ActivityIndicator } from 'react-native';
import U from 'utils';
import { BudForms } from 'components/BudForms';
import { NameField } from 'components/BudForms/Fields';
import { JudgeSchema } from './JudgeSchema';

class JudgeEdit extends React.PureComponent {
  static propTypes = {
    judge: React.PropTypes.object,
    loading: React.PropTypes.bool,
    saveJudge: React.PropTypes.func.isRequired,
    submitText: React.PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    this._buildDocState = this._buildDocState.bind(this);

    this.state = {
      doc: this._buildDocState(props.judge)
    };
  }

  _handleSubmit = ({ ...values }) => {
    const { saveJudge, editionId } = this.props;
    saveJudge({
      ...values,
      editionId,
    });
  };

  _handleChange = (doc) => {
    this.setState({ doc });
  }

  _buildDocState(judge = {}) {
    const { qrCode, ...doc } = judge;

    return doc;
  }

  componentWillReceiveProps(nextProps, _nextState) {
    const { judge } = nextProps;

    this.setState({ doc: this._buildDocState(judge) });
  }

  render() {
    const { loading } = this.props;

    const { doc } = this.state;

    if (loading) {
      return <ActivityIndicator />;
    }

    const formProps = {
      ref: 'form',
      schema: JudgeSchema,
      doc: U.pick([
        'name',
      ], doc),
      onChange: this._handleChange,
      onSubmit: this._handleSubmit,
      submitText: this.props.submitText,
    };

    return (
      <BudForms {...formProps} >
        <NameField i18nScope="screens.judges.forms.judgeName" />
      </BudForms>
    );
  }
}

export default JudgeEdit;

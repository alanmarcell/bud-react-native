import React from 'react';
import { ActivityIndicator } from 'react-native';
import U from 'utils';
import { BudForms } from 'components/BudForms';
import { JudgeAssocSchema } from './JudgeAssocSchema';
import I18nField from 'components/BudForms/Fields/I18nField';
import { InputFactory } from 'utils/Form/Factories';

class JudgeAssoc extends React.PureComponent {
  static propTypes = {
    judge: React.PropTypes.object,
    loading: React.PropTypes.bool,
    assocJudge: React.PropTypes.func.isRequired,
    submitText: React.PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    this._buildDocState = this._buildDocState.bind(this);

    this.state = {
      doc: this._buildDocState(props.judge)
    };
  }

  _handleSubmit = ({ ...values }) => {
    const { assocJudge } = this.props;
    assocJudge({
      ...values
    });
  };

  _handleChange = (doc) => {
    this.setState({ doc });
  }

  _buildDocState(judge = {}) {
    const { qrCode, ...doc } = judge;

    return doc;
  }

  componentWillReceiveProps(nextProps, _nextState) {
    const { judge } = nextProps;

    this.setState({ doc: this._buildDocState(judge) });
  }

  render() {
    const { loading } = this.props;

    const { doc } = this.state;

    if (loading) {
      return <ActivityIndicator />;
    }

    const formProps = {
      ref: 'form',
      schema: JudgeAssocSchema,
      doc: U.pick([
        'inviteCode',
      ], doc),
      onChange: this._handleChange,
      onSubmit: this._handleSubmit,
      submitText: this.props.submitText,
    };

    return (
      <BudForms {...formProps} >
        <I18nField 
          fieldName="inviteCode" 
          type={InputFactory}
          placeholder={true}
        />
      </BudForms>
    );
  }
}

export default JudgeAssoc;

import { SimpleSchema } from 'components/BudForms/Schemas';

export const JudgeAssocSchema = new SimpleSchema({
  inviteCode: {
    type: String,
    required: true,
  },
});

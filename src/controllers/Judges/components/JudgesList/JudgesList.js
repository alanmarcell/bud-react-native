import React, { Component } from 'react';
import { View, FlatList, ActivityIndicator } from 'react-native';

import { I18n } from 'utils/I18n';

import Refreshable from 'utils/components/Refreshable';

import BlankList    from 'components/BlankList';
import JudgeCardItem from '../JudgeCardItem';

import styles from './styles';

export default class JudgesList extends Component {
  static defaultProps = {
    judges: [],
  };

  static propTypes = {
    judges: React.PropTypes.array,
    onJudgePress: React.PropTypes.func.isRequired,
  }

  renderItem = ({item}) => {
    return (
      <JudgeCardItem
        {...item}
        onPress={() => this.props.onJudgePress(item.id)}
      />
    );
  };

  renderFooter = () => {
    const { loading } = this.props;
    return (
      <View style={styles.footer}>
        {loading ? <ActivityIndicator /> : null}
      </View>
    );
  };

  renderHeader = () => {
    const { judges, isProductOwner } = this.props;

    const type = isProductOwner ? 'owner' : 'user';

    if (judges.length <= 0) {
      return (
        <BlankList
          icon="home"
          title={I18n.t(`judges.${type}.blank.title`)}
          message={I18n.t(`judges.${type}.blank.message`)}
        />
      );
    } else {
      return null;
    }
  };

  render() {
    const { judges, loading, refetch } = this.props;

    return (
      <FlatList
        data={judges}
        refreshing={loading}
        refreshControl={Refreshable({ loading, refetch })}
        keyExtractor={(item, index) => index}
        renderItem={this.renderItem}
        ListHeaderComponent={this.renderHeader}
        ListFooterComponent={this.renderFooter}
      />
    );
  }
}

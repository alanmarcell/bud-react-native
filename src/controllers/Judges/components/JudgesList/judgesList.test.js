import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';

import JudgesList from './';
import { tree, context } from '../../../../__mocks__/stateHelper';

describe('<JudgesList />', () => {
  let defaultProps;

  beforeAll(() => {
    const judges = tree.get('judges', 'list');
    defaultProps = {
      judges,
      onTaskPress: () => {},
      setExecuted: () => {},
    };
  });

  test('renders correctly', () => {
    const wrapper = renderer.create(<JudgesList {...defaultProps} />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });

  test('renders empty correctly', () => {
    const props = {
      ...defaultProps,
      judges: {},
    };

    const wrapper = shallow(<JudgesList {...props}/>);
    expect(wrapper.find('BlankList')).toBeTruthy();
    expect(wrapper).toMatchSnapshot();
  });

  test.skip('children struct', () => {
    const wrapper  = shallow(<JudgesList {...defaultProps} />, context).dive(context);
    const children = wrapper.find('StaticRenderer');

    expect(children.length).toEqual(20);
    expect(children.at(0).dive().find('TaskCardItem')).toBeTruthy();
  });
});

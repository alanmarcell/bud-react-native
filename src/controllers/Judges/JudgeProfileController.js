// @flow
import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';
import NavIcon from 'scenes/NavIcon';
import RootController from '../RootController';
import JudgeProfile from './components/JudgeProfile';
import { graphql, makeGet, compose, makeDeleteMutation } from 'graphql';
import { JUDGE_PROFILE, JUDGE_DELETE, SESSION_QUERY } from 'graphql';
import R from 'ramda';

const isAuthedUserOwner = (props) => {
  const authedUserId = R.path(['session', 'user', 'id'], props);
  const { judge } = props;

  if (R.path(['edition', 'owner', 'id'], judge) === authedUserId) {
    return true;
  }

  if (R.path(['edition', 'cup', 'owner', 'id'], judge) === authedUserId) {
    return true;
  }

  if (R.path(['category', 'owner', 'id'], judge) === authedUserId) {
    return true;
  }

  return false;
};

class JudgeController extends RootController {
  static propTypes = {
    judge: PropTypes.object,
    loading: PropTypes.bool,
  }

  static navigationOptions = {
    title: 'screens.judges.profile.title',
  };

  get actions() {
    return {
      deleteEdition: async (params) => {
        const destroy = await this.confirmRemove('judge');
        if (destroy) {
          await this.props.deleteJudge(params);

          this.goBack();
        }
      },
    };
  }

  componentWillUpdate(nextProps) {
    const { loading, setButtons } = nextProps;
    if (!loading && !setButtons) {
      const { objectId } = nextProps;
      const { deleteJudge } = this.actionsWithHandle();
      this.refresh({
        setButtons: true,
        renderRightButton: () => {
          return (
            <View style={{ flex: 1, flexDirection: 'row' }}>
              <NavIcon onPress={() => deleteJudge({ objectId })} iconName="trash" />
              <NavIcon onPress={() => this.redirectTo('judgeEdit', { objectId })} iconName="pencil" />
            </View>
          );
        }
      });
    }
  }

  onActionPress = (option) => {
    switch (option) {
      case 'shareInviteCode': {
        // const { objectId } = this.props;
        // this.redirectTo('categoryAdd', { judgeId: objectId });
        return;
      }
    }
  }

  renderView(props) {
    const { loading } = props;

    if (loading) {
      return <ActivityIndicator />;
    }

    return (
      <View style={{ flex: 1 }}>
        <JudgeProfile {...props} authedUserIsOwner={isAuthedUserOwner(props)} />
      </View>
    );
  }
}

export default compose(
  graphql(SESSION_QUERY, { props: ({ data }) => ({ ...data }) }),
  graphql(JUDGE_PROFILE, makeGet()),
  graphql(JUDGE_DELETE, {
    ...makeDeleteMutation('deleteJudge', 'judges'),
    options: {
      refetchQueries: [
        'Judges',
      ],
    },
  }),
)(JudgeController);

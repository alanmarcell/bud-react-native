import React from 'react';
import { Keyboard } from 'react-native';
import { DefaultRenderer } from 'react-native-router-flux';
import Drawer from 'react-native-drawer';
import { I18n } from 'utils/I18n';
import R from 'ramda';
import DrawerView from 'components/DrawerMenu';
import RootController from './RootController';
import { connect } from 'react-redux';
import { removeCredentials } from '../redux/actions';


const getProfile = props => R.path(['session', 'user'], props) || {};

class DrawerController extends RootController {
  static propTypes = {
    navigationState: React.PropTypes.object,
  };

  static contextTypes = {
    client: React.PropTypes.object.isRequired,
  };

  state = {
    drawerOpen: false,
    drawerDisabled: false,
  };

  closeDrawer = () => {
    this._drawer.close();
  };

  openDrawer = () => {
    this._drawer.open();
  };

  _onOpen = () => {
    Keyboard.dismiss();
  };

  redirectTo(...args) {
    this.closeDrawer();
    super.redirectTo(...args);
  }

  get actions() {
    const activeUserType = this.props.activeUserType.toLowerCase();
    return {
      onTasks: () => this.redirectTo(`${activeUserType}_tasksTab`),
      onPlants: () => this.redirectTo(`${activeUserType}_plantsTab`),
      onBuds: () => this.redirectTo(`${activeUserType}_budsTab`),
      onCups   : () => this.redirectTo(`${activeUserType}_cupsTab`),
      onMixes: () => this.redirectTo(`${activeUserType}_mixesTab`),
      onQuickID: () => this.redirectTo(`${activeUserType}_qrCodeTab`),
      onTour: () => this.redirectTo(`${activeUserType}_tour`),
      onContact: () => this.redirectTo(`${activeUserType}_contactTab`),
      onLogOut: async () => {
        const confirmed = await this.confirm(
          I18n.t('screens.drawer.logout'),
          I18n.t('screens.drawer.logout.message')
        );
        if (confirmed) {
          this.props.removeCredentials();
          this.context.client.resetStore();
          this.redirectTo('unLogged');
        }
      },
    };
  }

  renderView(props) {
    const { onNavigate, navigationState: { children } } = props;

    return (
      <Drawer
        ref={(ref) => { this._drawer = ref; }}
        type="displace"
        content={<DrawerView {...props} profile={getProfile(this.props)} />}
        tapToClose={true}
        onOpen={this._onOpen}
        openDrawerOffset={0.2}
        panCloseMask={0.2}
        negotiatePan={true}
        tweenHandler={(ratio) => ({
          mainOverlay: { opacity: Math.min(0.6, ratio), backgroundColor: '#FFF' },
        })}
      >
        <DefaultRenderer navigationState={children[0]} onNavigate={onNavigate} />
      </Drawer>
    );
  }
}

const mapStateToProps = state => ({
  activeUserType: state.user.userType,
  session: state.user.session
});

const mapDispatchToProps = dispatch => ({
  removeCredentials: () => dispatch(removeCredentials())
});

export default connect(mapStateToProps, mapDispatchToProps)(DrawerController);

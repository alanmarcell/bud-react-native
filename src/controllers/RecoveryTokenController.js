// @flow
import React from 'react';

import RecoveryToken from 'components/RecoveryToken';
import RootController from './RootController';

import { graphql } from 'react-apollo';
import { SESSION_QUERY } from 'graphql/user';

class RecoveryTokenController extends RootController {
  static navigationOptions = {
    title: 'screens.register.title',
  };

  get actions() {
    return {
      notBlock: {
        onNext: () => this.redirectTo('firstTour'),
      }
    };
  }

  renderView(props) {
    const { data: { loading, session }} = props;
    if (loading) { return false; }

    return <RecoveryToken {...props} session={session}/>;
  }
}

export default graphql(SESSION_QUERY)(RecoveryTokenController);

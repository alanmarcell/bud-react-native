import React from 'react';

import Tour from 'components/Tour';
import RootController from './RootController';

export default class TourController extends RootController {
  static navigationOptions = {
    header: {
      visible: false,
    },
  };

  get actions() {
    const activeUser = this.props.name.split('_').shift();

    return {
      notBlock: {
        onNext: () => {
          if (this.props.firstTour) {
            this.redirectTo('logged');
          } else {
            if (activeUser === 'consumer') {
              this.redirectTo('budsTab');
            } else {
              this.redirectTo('plantsTab');
            }
          }
        }
      }
    };
  }

  renderView(props) {
    return <Tour {...props} />;
  }
}

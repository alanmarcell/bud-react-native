import React from 'react';
import { Alert } from 'react-native';

import R from 'ramda';
import { Actions } from 'react-native-router-flux';

import { I18n } from 'utils/I18n';
import Analytics from 'utils/Analytics';

const rootScenes = ['root', 'unLogged', 'logged', 'splash', 'refresh', 'pop'];

function userTypeScene(action, parent) {
  if (rootScenes.indexOf(action) !== -1 || rootScenes.indexOf(parent) !== -1 || !parent) {
    return action;
  }

  const parentPrefix = parent.split('_').shift();
  
  const cupPrefix = R.endsWith('cupsTab', parent) ? '_cups' : '';

  return `${parentPrefix.toLowerCase()}${cupPrefix}_${action}`;
}

export default class RootController extends React.PureComponent {

  redirectTo(action, params = {}) {
    if (this.process) {
      this.process = false;
      Actions.pop();
    }

    action = userTypeScene(action, this.props.parent);

    if (__DEV__) {
      console.log('redirectTo: ', { action, params }); // eslint-disable-line no-console
    }
    const redirected = Actions[action](params);

    return redirected;
  }

  resetTo(action, params = {}) {
    this.redirectTo(action, { type: 'reset', ...params });
  }

  goBack(params = {}) {
    this.redirectTo('pop', params);
  }

  refresh(...args) {
    this.redirectTo('refresh', ...args);
  }

  // Helpers
  confirmRemove(screen) {
    return this.confirm(
      I18n.t(`screens.${screen}.delete`),
      I18n.t(`screens.${screen}.remove_confirmation`)
    );
  }

  confirm(title, text) {
    return new Promise((resolve) => {
      Alert.alert(
        title, text,
        [
          { text: 'Cancel', onPress: () => resolve(false), style: 'cancel' },
          { text: 'OK', onPress: () => resolve(true) },
        ],
        { cancelable: false }
      );
    });
  }

  ok(text) {
    return new Promise((resolve) => {
      Alert.alert(
        '',
        text,
        [
          { text: 'OK', onPress: () => resolve(true) },
        ],
        { cancelable: false }
      );
    });
  }

  get actions() {
    throw new Error('RootController.actions cannot get directly');
  }

  renderView() {
    throw new Error('Controller not render directly');
  }

  async handleAction(actionName, showLoader, action, ...args) {
    if (showLoader) {
      this.redirectTo('splash', { loadType: 'process', show: true });
    }

    this.process = true;
    try {
      const result = await action(...args);
      if (this.process && showLoader) {
        this.goBack();
      }
      return result;
    } catch (error) {
      __DEV__ && console.trace('Error handling action: ', actionName, ' error: ', error); // eslint-disable-line no-console
      if (showLoader) {
        this.goBack();
      }
      return this.logError(error, { actionName, args });
    }
  }

  actionsWithHandle() {
    if (!this._actions) {
      let { notBlock, ...normal } = this.actions;

      notBlock = R.mapObjIndexed((action, key) => {
        return (...args) => this.handleAction(key, false, action, ...args);
      }, notBlock);

      normal = R.mapObjIndexed((action, key) => {
        return (...args) => this.handleAction(key, true, action, ...args);
      }, normal);

      this._actions = { ...notBlock, ...normal };
    }
    return this._actions;
  }

  async logError(error, context) {
    try {
      await Analytics.logException(error, {
        controller: this.constructor.displayName || this.constructor.name,
        ...context,
      });
    } catch (error) {
      __DEV__ && console.log(error); // eslint-disable-line no-console
    }
  }

  get sceneKey() {
    return this.props.sceneKey.split('_').pop();
  }

  render() {
    const { error } = this.props;
    if (error) {
      this.logError(error);
    }

    return this.renderView({ ...this.props, ...this.actionsWithHandle() });
  }
}

import React from 'react';
import { View } from 'react-native';

import RootController from './RootController';
import TermsAndPrivacy from 'components/TermsAndPrivacy';

export default class DisplayFeaturesController extends RootController {
  static navigationOptions = {
    title: 'screens.privacy.title',
  };

  get actions() {
    return {
      onIAgreePress: this.goBack,
    };
  }

  renderView(props) {
    const { profile } = props;

    return (
      <View style={{ flex: 1 }}>
        <TermsAndPrivacy contentKey={`screens.register.${profile}`} {...props} />
      </View>
    );
  }
}


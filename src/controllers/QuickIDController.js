import React from 'react';
import { connect } from 'react-redux';

import { contains, isNil } from 'ramda';

import RootController from './RootController';
import QrCodeReader from 'components/QrCodeReader';
import { QRCODE_SEARCH } from 'graphql/qrcode';

@connect(({ routes: { scene: { sceneKey } } }) => {
  return { isActive: sceneKey.indexOf('qrCodeReader') !== -1 };
})
export default class QuickIDController extends RootController {
  static navigationOptions = {
    title: 'screens.drawer.quickId',
  };

  static contextTypes = {
    client: React.PropTypes.object.isRequired,
  };

  get actions() {
    return {
      notBlock: {
        findQrCode: async (qrCode) => {
          const { client } = this.context;
          const { data, error } = await client.query({
            query: QRCODE_SEARCH,
            variables: { qrCode },
            fetchPolicy: 'network-only',
          });
          return error ? { error } : data.qrCode;
        },
      },

      goToProduct: async (qrCode) => {
        const { id: objectId, __typename: type } = qrCode.product || {};

        if(isNil(qrCode.product)){
          return ['comingSoonQrCode'];
        }

        if (isNil(type)) {
          if (__DEV__) {
            console.error('QR Code model is not supported', { objectId, type });
          }
          return ['fomart', { value: qrCode }];
        }
        const allowedTypes = ['Plant', 'Bud', 'Cup', 'Edition'];
        const isAllowed = contains(type, allowedTypes);

        if (!isAllowed) {
          return ['comingSoonQrCode', {}];
        }

        const productProfile = `${type.toLowerCase()}Profile`;

        const args = [
          productProfile, {
            objectId,
            type: 'push',
            uniqueDate: new Date()
          }];

        setImmediate(() => this.redirectTo(...args));
        return true;
      }
    };
  }

  renderView(props) {
    const { isActive } = props;
    if (!isActive) return null;

    return (
      <QrCodeReader enableSearch={true} {...props} />
    );
  }
}

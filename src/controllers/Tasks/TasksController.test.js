import React from 'react';
import { shallow } from 'enzyme';

import { context } from '../../__mocks__/stateHelper';
import Controller from './TasksController';

test.skip('renders correctly', () => {
  const wrapper = shallow(<Controller />, context);
  wrapper.dive(context);
  expect(wrapper).toMatchSnapshot();
});

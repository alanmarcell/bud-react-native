// @flow
import React from 'react';
import { View } from 'react-native';

import NavIcon from 'scenes/NavIcon';
import RootController from '../RootController';
import TaskProfile from './components/TaskProfile';

import { compose, graphql } from 'react-apollo';
import { makeDeleteMutation, makeGet } from 'graphql';
import {
  TASK_PROFILE, TASK_DELETE, TASK_UPDATE,
  TASK_OPTIMISTIC_EXECUTED
} from 'graphql';

class TaskProfileController extends RootController {
  static navigationOptions = {
    title: 'screens.task.title',
  };

  static defaultProps = {
    embeded: false,
  };

  static propTypes = {
    embeded: React.PropTypes.bool,
  };

  get actions() {
    return {
      setExecuted: async (objectId, input) => {
        const { updateTask } = this.props;
        await updateTask(objectId, input);
        this.refresh();
      },
      deleteTask: async (params) => {
        const destroy = await this.confirmRemove('task');
        if (destroy) {
          await this.props.deleteTask(params);
          this.goBack();
        }
      },
    };
  }

  renderRightButton(){
    const { objectId, embeded } = this.props;
    const { deleteTask } = this.actionsWithHandle();
    const action = embeded ? 'plantTaskEdit' : 'taskEdit';
    this.refresh({
      renderRightButton: () => {
        return (
          <View style={{ flex: 1, flexDirection: 'row'}}>
            <NavIcon onPress={() => deleteTask({ objectId })} iconName="trash" />
            <NavIcon onPress={() => this.redirectTo(action, { objectId })} iconName="pencil" />
          </View>
        );
      }
    });
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.objectId !== this.props.objectId){
      this.renderRightButton();
    }
  }

  componentDidMount() {
    this.renderRightButton();
  }

  renderView(props) {
    return (
      <TaskProfile {...props} />
    );
  }
}

export default compose(
  graphql(TASK_PROFILE, makeGet()),
  graphql(TASK_DELETE, makeDeleteMutation('deleteTask', 'tasks', 'UserTasks')),
  graphql(TASK_UPDATE, TASK_OPTIMISTIC_EXECUTED)
)(TaskProfileController);

import React, { Component } from 'react';
import { View, SectionList, ActivityIndicator } from 'react-native';

import { I18n } from 'utils/I18n';

import Refreshable from 'utils/components/Refreshable';

import BlankList    from 'components/BlankList';
import DateCard     from 'components/DateCard';
import TaskCardItem from '../TaskCardItem';

import styles from './styles';

export default class TaskList extends Component {
  static defaultProps = {
    tasks: [],
    embeded: false,
  };

  static propTypes = {
    embeded: React.PropTypes.bool,
    tasks: React.PropTypes.array,
    onTaskPress: React.PropTypes.func.isRequired,
    setExecuted: React.PropTypes.func.isRequired,
    renderPlantCard: React.PropTypes.func,
  }

  renderSectionHeader = ({ section }) => {
    return <DateCard
      style={styles.date}
      key={section.key}
      date={section.date}
    />;
  };

  handleSetExecuted = (taskId, newValue) => {
    this.props.setExecuted(taskId, newValue);
  };

  onTaskPress = (taskId) => {
    if (this.props.onTaskPress) {
      this.props.onTaskPress(taskId);
    }
  };

  renderItem = ({item}) => {
    const { embeded } = this.props;
    return (
      <TaskCardItem
        {...item}
        embeded={embeded}
        onPress={() => this.onTaskPress(item.id)}
        setExecuted={(executedAt) => this.handleSetExecuted(item.id, { executedAt })}
      />
    );
  };

  renderFooter = () => {
    const { loading } = this.props;
    return (
      <View style={styles.footer}>
        {loading ? <ActivityIndicator /> : null}
      </View>
    );
  };

  renderHeader = () => {
    const { tasks, embeded, renderPlantCard } = this.props;
    if (tasks.length <= 0 && !embeded) {
      return (
        <BlankList
          icon="home"
          title={I18n.t('tasks.blank.title')}
          message={I18n.t('tasks.blank.message')}
        />
      );
    } else {
      if (embeded && renderPlantCard) {
        return renderPlantCard();
      }
      return null;
    }
  };

  loadMore = () => {
    const { loading, loadMoreEntries, hasNextPage } = this.props;
    if (!loading && hasNextPage) {
      loadMoreEntries();
    }
  };

  render() {
    const { tasks, loading, refetch } = this.props;

    return (
      <SectionList
        sections={tasks}
        refreshing={loading}
        refreshControl={Refreshable({ loading, refetch })}
        keyExtractor={(item, index) => index}
        renderSectionHeader={this.renderSectionHeader}
        renderItem={this.renderItem}
        ListHeaderComponent={this.renderHeader}
        ListFooterComponent={this.renderFooter}
        onEndReached={this.loadMore}
      />
    );
  }
}

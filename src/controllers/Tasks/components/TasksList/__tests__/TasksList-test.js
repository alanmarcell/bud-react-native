import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';

import TasksList from '../';
import { tree, context } from '../../../../../__mocks__/stateHelper';
import { mapTasks } from '../../../TasksController';

describe('<TasksList />', () => {
  let defaultProps;

  beforeAll(() => {
    const tasks = mapTasks(tree.get('tasks', 'list'));
    defaultProps = {
      tasks,
      onTaskPress: () => {},
      setExecuted: () => {},
    };
  });

  test('renders correctly', () => {
    const wrapper = renderer.create(<TasksList {...defaultProps} />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });

  test('renders empty correctly', () => {
    const props = {
      ...defaultProps,
      tasks: {},
    };

    const wrapper = shallow(<TasksList {...props}/>);
    expect(wrapper.find('BlankList')).toBeTruthy();
    expect(wrapper).toMatchSnapshot();
  });

  test.skip('children struct', () => {
    const wrapper  = shallow(<TasksList {...defaultProps} />, context).dive(context);
    const children = wrapper.find('StaticRenderer');

    expect(children.length).toEqual(20);
    expect(children.at(0).dive().find('TaskCardItem')).toBeTruthy();
  });
});

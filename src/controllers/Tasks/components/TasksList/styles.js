// @flow
import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  listView: {
    backgroundColor: '$Colors.background',
  },

  date: {
    marginBottom: -5,
  },

  footer: {
    height: 75,
    paddingTop: 5,
  }
});

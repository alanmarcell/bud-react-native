import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';
import R from 'ramda';

import { tree, context } from '../../../../../__mocks__/stateHelper';
import TaskEditHeader from '../';

describe('TaskEditHeader', () => {
  let props;

  beforeAll(() => {
    const [[, task]] = R.toPairs(tree.get('tasks', 'list'));
    props = {
      ...task,
      onToggleExecute: () => {},
    };
  });

  test.skip('renders correctly', () => {
    const override = { executed: false };
    const wrapper = shallow(<TaskEditHeader {...props} {...override} />, context);
    const touchable = wrapper.find('Touchable');

    expect(touchable.find('Icon').prop('name')).toEqual('square-o');
    expect(wrapper).toMatchSnapshot();
  });

  test.skip('renders correctly when executed is false', () => {
    const override = { executed: true, };
    const wrapper = shallow(<TaskEditHeader {...props} {...override} />, context);
    const touchable = wrapper.find('Touchable');

    expect(touchable.find('Icon').prop('name')).toEqual('check-square-o');
    expect(wrapper).toMatchSnapshot();
  });
});

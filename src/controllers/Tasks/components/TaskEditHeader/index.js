import React, { Component, PropTypes } from 'react';
import { View, Text } from 'react-native';

import { I18n } from 'utils/I18n';
import Icon from 'components/Icon';
import PlantName from 'controllers/Plants/components/PlantName';
import TaskIcon  from '../TaskIcon';

import styles from './styles';
import stylesHelpers from '../../../../themes/helpers';

export default class TaskHeader extends Component {
  static defaultProps = {
    executedAt: false,
  };

  static propTypes = {
    type      : PropTypes.string,
    plant     : PropTypes.object,
    executedAt: PropTypes.oneOfType([
      PropTypes.instanceOf(Date),
      PropTypes.string,
    ]),
    style          : PropTypes.oneOfType([
      PropTypes.shape(),
      PropTypes.number,
    ]),
  }

  renderExecutedButton() {
    const { executedAt } = this.props;
    let icon  = executedAt ? 'check-square-o' : 'square-o';
    return (
      <Icon name={icon} style={styles.executeButtonIcon} />
    );
  }

  render() {
    const { type, plant } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.headerWrapper}>
          <View style={[stylesHelpers.leftColumn]}>
            <TaskIcon type={type} style={styles.taskIcon} rounded roundedWhite />
          </View>
          <View style={[stylesHelpers.middleColumn, stylesHelpers.alignCenterLeft]}>
            <PlantName {...plant} />
            <Text>{type ? I18n.t(`tasks.types.${type}`) : ''}</Text>
          </View>
          <View style={[stylesHelpers.rightColumn, stylesHelpers.alignCenterRight]}>
            { this.renderExecutedButton() }
          </View>
        </View>
      </View>
    );
  }
}

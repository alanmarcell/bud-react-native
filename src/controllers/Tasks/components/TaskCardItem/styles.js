// @flow
import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  taskType: {
    color: '$Colors.Gray',
    fontSize: 12
  },

  checkbox: {
    color: '$Colors.Disabled',
    fontSize: 30,
  },

  taskIcon: {
    color: '$Colors.Purple',
    fontSize: 20,
  },

  checkIcon: {
    color: '$Colors.Green',
    fontSize: 30,
  },
});

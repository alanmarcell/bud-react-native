import 'react-native';
import React from 'react';

import { shallow } from 'enzyme';
import { context } from '../../../../../__mocks__/stateHelper';

import TaskCardItem from '../';

test.skip('renders correctly', () => {
  const wrapper = shallow(
    <TaskCardItem
      onPress={() => {}}
      setExecuted={() => {}}
      type="WATER"
      plant={{name: 'Moranguinho', gender: 'male'}}
    />
  , context);
  expect(wrapper).toMatchSnapshot();
});

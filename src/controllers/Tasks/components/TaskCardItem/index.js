import React, { Component, PropTypes } from 'react';
import { View, Text } from 'react-native';
import { I18n } from 'utils/I18n';

import SelectableCard from 'components/SelectableCard';
import PlantName      from 'controllers/Plants/components/PlantName';
import TaskIcon       from '../TaskIcon';
import Icon           from 'components/Icon';
import { Touchable }  from 'utils/components';

import styles from './styles';
import stylesHelpers from '../../../../themes/helpers';

export default class TaskCardItem extends Component {
  static defaultProps = {
    type: 'WATER',
    ...SelectableCard.defaultProps,
    embeded: false,
  }

  static propTypes = {
    ...SelectableCard.propTypes,
    type: PropTypes.string,
    setExecuted: PropTypes.func.isRequired,
    embeded: PropTypes.bool,
  }

  toggleExecuted = (oldValue) => {
    const { setExecuted } = this.props;
    setExecuted(!oldValue ? new Date() : null);
  }

  renderCheck(executedAt) {
    const icon = executedAt ? 'check-square-o' : 'square-o';
    return (
      <Touchable onPress={() => this.toggleExecuted(!!executedAt)}>
        <Icon name={icon} style={styles._checkIcon} />
      </Touchable>
    );
  }

  render() {
    const { plant, embeded, executedAt, meta: { type } } = this.props;

    const scope =  type === 'WATER' ? 'forms.water.type.' : 'tasks.types.';
    const label = I18n.t(`${scope}${type}`);
    const taskIcon = type === 'FERTILIZATION' ? 'WATER' : type;

    return (
      <SelectableCard {...this.props}>
        <View style={[stylesHelpers.leftColumn, stylesHelpers.alignCenter]}>
          <TaskIcon type={taskIcon} style={styles.taskIcon} />
        </View>
        <View style={[stylesHelpers.middleColumn, stylesHelpers.alignCenterLeft]}>
          { !embeded ? <PlantName {...plant} /> : null }
          <Text style={styles.taskType}>{label}</Text>
        </View>
        <View style={[stylesHelpers.rightColumn, stylesHelpers.alignCenterRight]}>
          {this.renderCheck(executedAt)}
        </View>
      </SelectableCard>
    );
  }
}

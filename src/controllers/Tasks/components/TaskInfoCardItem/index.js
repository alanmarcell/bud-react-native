import React, { Component, PropTypes } from 'react';
import { View, Text } from 'react-native';
import moment from 'moment';

import Icon from 'components/Icon';

import styles from './styles';

export default class TaskInfoCardItem extends Component {
  static defaultProps = {
    done: false,
  };

  static propTypes = {
    label: PropTypes.string.isRequired,
    sublabel: PropTypes.string,
    icon: PropTypes.string,
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.instanceOf(Date),
    ]),
    done: PropTypes.bool,
  };

  renderSubLabel = () => {
    const { sublabel } = this.props;
    if (!sublabel) return null;
    return (<Text style={[styles.label, {fontSize: 13, fontWeight: 'normal'}]}>{sublabel}</Text>);
  }

  render() {
    let { value } = this.props;
    const { done, icon, label } = this.props;
    const doneClass = done ? styles.taskDone : null;

    if (value instanceof Date) {
      value = moment(value).format('L');
    }

    return (
      <View style={styles.cardContainer}>
        <View style={styles.iconWrapper}>
          <Icon name={icon} size={20} color="#360968" />
        </View>
        <View style={styles.labelWrapper}>
          <Text  style={[styles.label, doneClass]}>{label}</Text>
          { this.renderSubLabel() }
        </View>
        <View style={styles.valueWrapper}>
          <Text style={styles.value}>{value}</Text>
        </View>
      </View>
    );
  }
}

import 'react-native';
import React from 'react';
import TaskInfoCardItem from '../';
import renderer from 'react-test-renderer';

test('renders correctly water icon', () => {
  const tree = renderer.create(
    <TaskInfoCardItem icon="tint" label="Água" value="2L" />
  ).toJSON();
  expect(tree).toMatchSnapshot();
});

test('renders correctly temperature icon', () => {
  const tree = renderer.create(
    <TaskInfoCardItem icon="thermometer-4-4" label="EC" value="1.0" />
  ).toJSON();
  expect(tree).toMatchSnapshot();
});

test('renders correctly fertilizer icon', () => {
  const tree = renderer.create(
    <TaskInfoCardItem icon="fertilizer" label="MaxiBloom" value="2.5g" />
  ).toJSON();
  expect(tree).toMatchSnapshot();
});

test('renders correctly done', () => {
  const tree = renderer.create(
    <TaskInfoCardItem icon="thermometer-4-4" label="TDS" value="1600ppm" done />
  ).toJSON();
  expect(tree).toMatchSnapshot();
});

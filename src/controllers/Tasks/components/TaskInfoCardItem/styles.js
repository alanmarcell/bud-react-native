import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  cardContainer: {
    backgroundColor: '#FFFFFF',
    marginHorizontal: 10,
    height: 60,
    marginBottom: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconWrapper: {
    alignItems: 'center',
    flex: 1,
  },
  labelWrapper: {
    flex: 5,
  },
  label: {
    color: '#3C484A',
    fontSize: 15,
  },
  taskDone: {
    color:'#95989A',
  },
  valueWrapper: {
    flex: 3,
    alignItems: 'flex-end',
    paddingRight: 10,
  },
  value: {
    color: '$Colors.Green',
    fontSize: 18,
  },
});

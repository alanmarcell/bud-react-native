import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  taskIcon: {
    color: '$Colors.Purple',
    fontSize: 20,
  },
});

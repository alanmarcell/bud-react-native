import React from 'react';
import Icon from 'components/Icon';
import RoundedIcon    from 'components/RoundedIcon';

import styles from './styles';

const tasksIcon = {
  // TODO: temporary - refactor TasksDictionary
  // to make it easy to retrieve icon from `task.type`
  'CONTAINER'               : 'cup',
  'TEMPERATURE'             : 'thermometer-4-4',
  'MEDIUM'                  : 'growing-medium',
  'LAMP'                    : 'brightness-contrast',
  'WATER'                   : 'to-water',
  'ENVIRONMENT'             : 'environment',
  'AIR_HUMIDITY'            : 'thermometer-4-4',
  'PHOTOPERIOD'             : 'brightness-contrast',
  'ENVIRONMENT_TEMPERATURE' : 'thermometer-4-4',
  'PH'                      : 'thermometer-3-4',
  'EC'                      : 'thermometer-2-4',
  'TDS'                     : 'thermometer-1-4',
  'CF'                      : 'thermometer-2-4',
  'GERMINATION'             : 'germination',
  'HARVEST'                 : 'basket-bud',

  'PESTICIDE'               : 'bug',
  'DEATH'                   : 'alert',
  'FLOWERING'               : 'flower',
  'PREFLOWER'               : 'flower',
  'REVEGA'                  : 'germination',
  'VEGA'                    : 'germination',
};


export default class TaskIcon extends RoundedIcon {
  static propTypes = {
    type : React.PropTypes.string,
    style: React.PropTypes.oneOfType([
      React.PropTypes.shape(),
      React.PropTypes.number,
    ]),
  }

  render() {
    const { type, style, roundedWhite } = this.props;
    const iconName = tasksIcon[type] || 'calendar';
    return <Icon name={iconName} style={[ styles.tasksIcon, style ]} rounded roundedWhite={roundedWhite} />;
  }
}

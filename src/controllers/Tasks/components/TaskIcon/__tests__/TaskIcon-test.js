import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';

import TaskIcon from '../';

test('renders correctly', () => {
  const wrapper = shallow(<TaskIcon type="WATER" />).dive();
  expect(wrapper).toMatchSnapshot();
});

test('renders default image when type is invalid', () => {
  const wrapper = shallow(<TaskIcon type="invalid" />).dive();
  expect(wrapper).toMatchSnapshot();
});

import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';
import R from 'ramda';

import TaskProfile from '../';
import { tree, context } from '../../../../../__mocks__/stateHelper';

describe('TaskProfile', () => {
  test.skip('renders correctly', () => {
    const [[, task]] = R.toPairs(tree.get('tasks', 'list'));
    const component = shallow(<TaskProfile setExecuted={() => {}} task={task} />).dive(context);
    expect(component).toMatchSnapshot();
  });
});

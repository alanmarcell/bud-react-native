import React, { Component } from 'react';
import { ScrollView, ActivityIndicator, View } from 'react-native';
import U from 'utils';

import { I18n } from 'utils/I18n';

import TaskHeader from '../TaskHeader';
import TaskInfoCardItem from '../TaskInfoCardItem';
import TasksDictionary from '../TaskEdit/TasksDictionary';

export default class TaskProfile extends Component {
  static propTypes = {
    task: React.PropTypes.object,
    loading: React.PropTypes.bool,
    setExecuted: React.PropTypes.func.isRequired,
  };

  listTaskItems = () => {
    const { task } = this.props;
    const { meta: { type, ...meta} } = task;
    const taskHelper = TasksDictionary[type];
    const defaultSerializer = () => [{icon: 'gear', label: type, value: 'TODO'}];
    let data = [];

    if (taskHelper) {
      if (taskHelper.serialize && taskHelper.serialize instanceof Function) {
        data = taskHelper.serialize(task);
      } else {
        /*eslint no-console: 0*/
        console.group('TaskProfile > Serialize');
        console.log(`* ${type}.serialize() must be a fuction`);
        console.log({ meta });
        console.groupEnd();
        data = defaultSerializer(meta);
      }
    } else {
      console.log('* Missing taskHelper:', type);
      data = defaultSerializer(meta);
    }

    return data.map((item, index) => this.renderTaskInfoItem(item, index, type));
  }

  renderTaskInfoItem = (info, index, taskType) => {
    const translation = info.label || I18n.t(`tasks.types.${taskType}`);
    return (
      <TaskInfoCardItem
        key={index}
        label={translation}
        {...U.omit(['label'], info)}
      />
    );
  }

  _handleToggleExecute = () => {
    const { task: { id, executedAt }} = this.props;
    this.props.setExecuted(id, {
      executedAt: executedAt ? null : new Date(),
    });
  };

  render() {
    const { loading, task } = this.props;

    if (loading) {
      return <ActivityIndicator />;
    }

    if (!task) {
      return null;
    }

    return (
      <ScrollView>
        <TaskHeader {...task} onToggleExecute={this._handleToggleExecute} />
        <View style={{marginTop: 10}}>
          { this.listTaskItems() }
        </View>
      </ScrollView>
    );
  }
}

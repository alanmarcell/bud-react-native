import React from 'react';
import { View } from 'react-native';

import { I18nField, DateField } from 'components/BudForms/Fields';
import { InputFactory } from 'utils/Form/Factories';
import {
  AutoRadioFactory,
  BudSliderFactory,
} from 'components/BudForms/Factories';
import WaterFields from './Fields/WaterFields';
import PesticideFields from './Fields/PesticideFields';

export function TaskFields({ type, ...props }) {
  switch(type) {
    case 'AIR_HUMIDITY':
      return (
        <View>
          <I18nField
            fieldName="value"
            labelI18n="airhumidity"
            type={InputFactory}
            number={true}
          />
        </View>
      );
    case 'CONTAINER':
      return (
        <View>
          <I18nField
            fieldName="value"
            labelI18n="container"
            type={InputFactory}
            number={true}
          />
        </View>
      );
    case 'EC':
      return (
        <View>
          <I18nField
            fieldName="value"
            labelI18n="ec"
            type={InputFactory}
            number={true}
          />
        </View>
      );
    case 'TEMPERATURE':
      return (
        <View>
          <I18nField
            fieldName="value"
            labelI18n="temperature"
            type={InputFactory}
            number={true}
          />
        </View>
      );
    case 'ENVIRONMENT':
      return (
        <View>
          <I18nField
            fieldName="name"
            type={InputFactory}
          />
          <I18nField
            fieldName="environment"
            i18nScope="forms.environment"
            type={AutoRadioFactory}
          />
          <I18nField
            fieldName="size"
            type={InputFactory}
            number={true}
          />
        </View>
      );
    case 'HARVEST':
      return (
        <View>
          <I18nField
            fieldName="total"
            labelI18n="harvest"
            i18nScope="forms.harvest"
            type={AutoRadioFactory}
          />
          <I18nField
            fieldName="qty"
            labelI18n="harvest_qty"
            type={InputFactory}
            number
          />
        </View>
      );
    case 'CURE':
      return (
        <View>
          <DateField
            fieldName="endAt"
            labelI18n="cure_end_at"
          />
          <I18nField
            fieldName="qty"
            labelI18n="cure_qty"
            type={InputFactory}
            number
          />
        </View>
      );
    case 'DRY':
      return (
        <View>
          <DateField
            fieldName="endAt"
            labelI18n="dry_end_at"
          />
          <I18nField
            fieldName="qty"
            labelI18n="dry_qty"
            type={InputFactory}
            number
          />
        </View>
      );
    case 'PH':
      return (
        <View>
          <I18nField
            fieldName="value"
            labelI18n="ph"
            type={InputFactory}
            number={true}
          />
        </View>
      );
    case 'TDS':
      return (
        <View>
          <I18nField
            fieldName="value"
            labelI18n="tds"
            type={InputFactory}
            number
          />
        </View>
      );
    case 'MEDIUM':
      return (
        <View>
          <I18nField
            fieldName="kind"
            labelI18n="medium"
            i18nScope="forms.medium"
            type={AutoRadioFactory}
          />
        </View>
      );
    case 'PHOTOPERIOD':
      return (
        <View>
          <I18nField
            fieldName="daylight"
            type={BudSliderFactory}
            step={1}
            renderValue={(value) => `${value} h`}
          />
        </View>
      );
    case 'ENVIRONMENT_TEMPERATURE':
      return (
        <View>
          <I18nField
            fieldName="value"
            labelI18n="temperature"
            type={InputFactory}
            number={true}
          />
        </View>
      );
    case 'WATER':
    case 'FERTILIZATION':
      return (<WaterFields {...props} />);
    case 'PESTICIDE':
      return (<PesticideFields {...props} />);
    default:
      return null;
  }
}

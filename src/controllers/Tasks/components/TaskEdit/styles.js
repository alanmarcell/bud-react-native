import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 0,
  },

  fieldSetHeader: {
    backgroundColor: '$Colors.fieldSetHeader',
    marginHorizontal: '$forms.paddingHorizontal * -1',
    paddingHorizontal: '$forms.paddingHorizontal',
    marginBottom: 15,
    paddingTop: 10,
    paddingBottom: 10,
  },

  fertilizersContainer: {
    marginBottom: 15,
  },
});

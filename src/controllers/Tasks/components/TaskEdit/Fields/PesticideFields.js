import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';

// Form components
import I18nField from 'components/BudForms/Fields/I18nField';
import { InputFactory } from 'utils/Form/Factories';
import { PesticidesFactory } from 'components/BudForms/Factories';
import PestFactory from 'controllers/factories/PestFactory';

export default class PesticideFields extends Component {
  static propTypes = {
    doc: PropTypes.object.isRequired,
  };

  render() {
    const qtyProps = {};

    qtyProps.labelI18n = 'water';

    return (
      <View>
        <I18nField
          fieldName="pest"
          i18nScope="forms.pest"
          placeholder={true}
          type={PestFactory}
        />

        <PesticidesFactory />

        <I18nField
          fieldName="qty"
          type={InputFactory}
          number={true}
          {...qtyProps}
        />
      </View>
    );
  }
}


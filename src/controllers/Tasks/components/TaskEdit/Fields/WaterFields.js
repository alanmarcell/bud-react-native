import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';

// Form components
import I18nField from 'components/BudForms/Fields/I18nField';
import { InputFactory } from 'utils/Form/Factories';
import { AutoRadioFactory } from 'components/BudForms/Factories';

import { FertilizersFactory } from 'controllers/Mixes/components/MixEdit/FertilizersFactory';
import MixFactory from 'controllers/factories/MixFactory';

export default class WaterFields extends Component {
  static propTypes = {
    doc: PropTypes.object.isRequired,
  };

  render() {
    const { mixBase, type } = this.props.doc;
    const qtyProps = {};
    if (mixBase) {
      qtyProps.scopeArgs = { baseName: mixBase.label };
      qtyProps.labelI18n = 'waterBase';
    } else {
      qtyProps.labelI18n = 'water';
    }

    return (
      <View>
          <I18nField
          fieldName="type"
          i18nScope="forms.water.type"
          labelI18n="type"
          type={AutoRadioFactory}
        />
        <I18nField
          fieldName="mixBase"
          i18nScope="forms.mixBase"
          placeholder={true}
          type={MixFactory}
        />
        <I18nField
          fieldName="qty"
          type={InputFactory}
          number={true}
          {...qtyProps}
        />
        {type === 'FERTILIZATION' && <FertilizersFactory /> }
        <I18nField
          fieldName="temperature"
          type={InputFactory}
          number={true}
        />
        <I18nField
          fieldName="ph"
          type={InputFactory}
          number={true}
        />
        <I18nField
          fieldName="ec"
          type={InputFactory}
          number={true}
        />
        <I18nField
          fieldName="tds"
          type={InputFactory}
          number
        />
      </View>
    );
  }
}

import React, { Component } from 'react';
import { View, ActivityIndicator } from 'react-native';
import U, { default as R } from 'utils';
import moment from 'moment';

import { I18n, parseNumber } from 'utils/I18n';
import { BudForms } from 'components/BudForms';
import { DateField, DescriptionField } from 'components/BudForms/Fields';
import TaskEditHeader from '../TaskEditHeader';

import I18nField from 'components/BudForms/Fields/I18nField';
import { TasksSchemas, SessionTaskSchema } from './TaskSchemas';
import { AutoSelectFactory, PesticidesFactory } from 'components/BudForms/Factories';
import { FertilizersFactory } from 'controllers/Mixes/components/MixEdit/FertilizersFactory';
import { TaskFields } from './TaskFields';
import { TasksDictionary } from './TasksDictionary';

import styles from './styles';

const numberFields = [
  'value', 'size', 'qty', 'ph', 'ec', 'tds', 'cf', 'temperature'
];

export default class TaskEdit extends Component {
  static propTypes = {
    task: React.PropTypes.object,
    onSaveTask: React.PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      doc: this._buildDocState(props.task)
    };
  }

  componentWillReceiveProps(nextProps, _nextState) {
    const { task } = nextProps;
    this.setState({ doc: this._buildDocState(task) });
  }

  _buildDocState(task) {
    let mixBase = R.path(['meta', 'mixBase'], task);
    if (mixBase) {
      mixBase = {
        index: mixBase.id,
        label: mixBase.name,
      };
    }

    let taskType = R.path(['meta', 'type'], task);
    if (taskType) {
      taskType = {
        key: taskType,
        label: I18n.t(`tasks.types.${taskType}`)
      };
    }

    return this.getDoc({
      taskType, mixBase,
      scheduledAt: new Date(R.pathOr(new Date(), ['scheduledAt'], task)),
      description: R.path(['description'], task),
      pest:R.pathOr({},['meta', 'pest'], task)
    });
  }

  handleSubmit = (values) => {
    const { onSaveTask } = this.props;
    const { doc: { taskType: stateTaskType } } = this.state;
    const {
      scheduledAt,
      description,
      taskType = stateTaskType,
      fertilizers,
      pesticides,
      mixBase,
      pest,
      ...meta
    } = values;
    meta.type = taskType.key;

    let { task: { executedAt } } = this.props;
    if (moment(scheduledAt).isSameOrBefore(new Date())) {
      executedAt = scheduledAt;
    } else if (moment(scheduledAt).isAfter(new Date())) {
      executedAt = null;
    }

    // Pre format data by task type
    switch(taskType.key) {
      case('PHOTOPERIOD'):
        meta.darkness = 24 - meta.daylight;
        break;
      case('HARVEST'):
        meta.total = meta.total === 'TOTAL';
        break;
      case('WATER'): {
        if (fertilizers) {
          meta.compounds = fertilizers.map(({ compound, ...item }) => {
            let compoundId = compound.index;
            return { ...item, compoundId };
          });
        } else {
          meta.compounds = null;
        }
        meta.mixBaseId = mixBase ? mixBase.index : null;

        meta.type = values.type;
        break;
      }
      case('PESTICIDE'): {
        if (pesticides) {
          meta.compounds = pesticides.map(({ compound, ...item }) => {
            let compoundId = compound.index;
            return { ...item, compoundId };
          });
        } else {
          meta.compounds = null;
        }

        meta.pestId = pest ? pest.index : null;
        break;
      }
    }

    // Save the final document
    onSaveTask({
      task: { scheduledAt, executedAt, description, meta }
    });
  };

  handleChange = (doc) => {
    if (!R.equals(this.state.doc.taskType, doc.taskType)) {
      doc = this.getDoc(doc);
    }

    // Fix number fields
    numberFields.forEach((field) => {
      if (doc.hasOwnProperty(field) && U.isBlank(doc[field])) {
        doc[field] = null;
      }
    });

    if (!R.equals(this.state.doc, doc)) {
      this.setState({ doc });
    }
  };

  afterSubmit = ({ fertilizers, pesticides, ...values }, submit) => {
    const { refs: { form } } = this.form;

    numberFields.forEach((field) => {
      if (values.hasOwnProperty(field)) {
        let value = values[field];
        if (value) {
          form.onValueChange(field, U.isBlank(value) ? null : parseNumber(value));
        }
      }
    });

    if (fertilizers) {
      fertilizers.forEach(({ qty }, index) => {
        if (qty) {
          form.onValueChange(`fertilizers.${index}.qty`, parseNumber(qty));
        }
      });
    }

    if (pesticides) {
      pesticides.forEach(({ qty }, index) => {
        if (qty) {
          form.onValueChange(`pesticides.${index}.qty`, parseNumber(qty));
        }
      });
    }

    submit();
  };

  getDoc({ taskType, scheduledAt, description, mixBase, pest }) {
    const { key: formType = null } = taskType || {};
    const typeOptions = TasksDictionary[formType] || {};
    const { task = {} } = this.props;
    let doc = {
      ...R.pick(typeOptions.fields || [], task.meta || {}),
      scheduledAt,
      description,
      taskType,
    };

    // default values for new task
    if (!task.id) {
      doc = {
        ...doc,
        ...typeOptions.defaults || {},
      };
    }

    switch(formType) {
      case('HARVEST'):
        doc.total = doc ? 'TOTAL' : 'PARCIAL';
        break;
      case('WATER'): {
        doc.type = doc.taskType.key;
        doc.mixBase = mixBase;
        const fertilizers = R.pathOr([], ['meta', 'compounds'], task);
        doc.fertilizers = FertilizersFactory.mapCompounds(fertilizers);
        break;
      }
      case('FERTILIZATION'): {
        doc.type = doc.taskType.key;
        doc.taskType.key = 'WATER';
        doc.mixBase = mixBase;
        const fertilizers = R.pathOr([], ['meta', 'compounds'], task);
        doc.fertilizers = FertilizersFactory.mapCompounds(fertilizers);
        break;
      }
      case('PESTICIDE'): {        
        doc.pest = pest ? {
          index: pest.id,
          label: pest.name
        }: null;     
        const pesticides = R.pathOr([], ['meta', 'compounds'], task);
        doc.pesticides = PesticidesFactory.mapCompounds(pesticides);
        break;
      }
    }

    return doc;
  }

  render() {
    let { loading, task: { id, executedAt = null, plant } = {} } = this.props;

    if (loading) {
      return <ActivityIndicator />;
    }

    const { doc } = this.state;
    const formType = R.path(['taskType', 'key'], doc);

    // Executed or not?
    const scheduledAt = doc.scheduledAt;
    if (moment(scheduledAt).isSameOrBefore(new Date())) {
      executedAt = scheduledAt;
    } else if (moment(scheduledAt).isAfter(new Date())) {
      executedAt = null;
    }

    let submitText = id ? 'taskEdit' : 'taskAdd';
    if (executedAt) {
      submitText = id ? 'annotationEdit' : 'annotationAdd';
    }

    const handleSchema = () => {
      if (formType === 'CURE'||formType === 'DRY') {
        return SessionTaskSchema(scheduledAt)[formType];
      }
      return TasksSchemas[formType] || TasksSchemas['base'];
    };

    const formProps = {
      ref         : (ref) => this.form = ref,
      schema      : handleSchema(),
      doc         : doc,
      onSubmit    : this.handleSubmit,
      onChange    : this.handleChange,
      submitText  : I18n.T(`screens.${submitText}.submit`),
      renderFooter: this.renderFooter,
      afterSubmit : this.afterSubmit,
      containerStyle: styles.container,
    };

    let typeField = null;
    if (!id) {
      typeField = (
        <I18nField
          fieldName="taskType"
          initValue={I18n.t('forms.taskType.placeholder')}
          type={AutoSelectFactory}
          i18nScope="tasks.types"
        />
      );
    }

    return (
      <View style={{flex: 1}}>
        <TaskEditHeader type={formType} executedAt={executedAt} plant={plant} />
        <BudForms {...formProps} >
          <View style={styles.fieldSetHeader}>
            {typeField}
            <DateField fieldName="scheduledAt" labelI18n="date" />
            <DescriptionField />
          </View>
          { /* Adaptative form type */ }
          <TaskFields { ...this.props } mixes={{}} doc={doc} type={formType} />
        </BudForms>
      </View>
    );
  }
}

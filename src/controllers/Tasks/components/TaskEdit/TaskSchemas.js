import R from 'ramda';
import { TaskTypes } from '../TaskEdit/TasksDictionary';
import { SimpleSchema, SessionTaskDateSchema } from 'components/BudForms/Schemas';

export const TaskBaseSchema = new SimpleSchema({
  'taskType'       : {
    type: Object,
    required: true
  },
  'taskType.key'   : {
    type: String,
    allowedValues: TaskTypes,
    required: false
  },
  'taskType.label' : {
    type: String,
    required: false
  },
  'scheduledAt'    : Date,
  'executedAt'     : {
    type: Date,
    required: false,
  },
  'description' : {
    type: String,
    required: false
  }
});

const Schemas = {
  base: TaskBaseSchema,
  'AIR_HUMIDITY': {
    value: {
      type : Number,
      min  : 0,
      max  : 100,
      required: false,
    }
  },

  'CONTAINER': {
    value: {
      type : Number,
      min  : 0,
      max  : 30,
      required: false,
    }
  },

  'EC': {
    value: {
      type : Number,
      min  : 0,
      max  : 3,
      required: false,
    }
  },

  'TEMPERATURE': {
    value: {
      type: Number,
      min: 5,
      max: 25,
      required: false,
    }
  },

  'ENVIRONMENT': {
    name: String,
    environment: {
      type: String,
      allowedValues: ['INDOOR', 'OUTDOOR', 'GREENHOUSE'],
      required: false,
    },
    size: {
      type: Number,
      min: 0,
      max: 100,
      required: false,
    },
  },

  'GERMINATION': {
  },

  'HARVEST': {
    total: {
      type: String,
      allowedValues: ['TOTAL', 'PARCIAL'],
      required: false,
    },
    qty: {
      type: Number,
      min: 0,
      max: 1000,
      required: false,
    },
  },

  'PH': {
    value: {
      type: Number,
      min: 0,
      max: 14,
      required: false,
    },
  },

  'TDS': {
    value: {
      type: Number,
      min: 0,
      max: 3000,
      required: false,
    },
  },

  'MEDIUM': {
    kind: {
      type: String,
      allowedValues: ['HYDROPONIC', 'SOIL', 'COCONUT'],
      required: false,
    },
  },

  'PHOTOPERIOD': {
    daylight: {
      type: Number,
      min: 0,
      max: 24,
      required: false,
    },
    darkness: {
      type: Number,
      min: 0,
      max: 24,
      required: false,
    }
  },

  // TODO: Temperature can not be negative?
  'ENVIRONMENT_TEMPERATURE': {
    value: {
      type: Number,
      min: 0,
      max: 30,
      required: false,
    }
  },

  'WATER': {
    qty: {
      type: Number,
      min: 0,
      max: 5000,
      required: false,
    },
    type: {
      type: String,
      allowedValues: ['WATER', 'FERTILIZATION'],
      required: false,
    },
    'mixBase': {
      type: Object,
      required: false,
    },
    'mixBase.label': String,
    'mixBase.index': String,

    // fertilizers array of object object
    'fertilizers': {
      type: Array,
      required: false,
    },

    'fertilizers.$'    : Object,
    'fertilizers.$.qty': {
      type: Number,
    },
    'fertilizers.$.measurement': {
      type: String,
      allowedValues: ['GL', 'MLL']
    },

    // fertilizers[0].compound object
    'fertilizers.$.compound': {
      type: Object,
      required: true
    },
    'fertilizers.$.compound.index' : String,
    'fertilizers.$.compound.label' : String,
    'fertilizers.$.compound.brand' : {
      type: String,
      required: false,
    },
    'fertilizers.$.compound.type' : {
      type: String,
      allowedValues: ['PESTICIDE', 'FERTILIZER']
    },

    temperature: {
      type: Number,
      min: 0,
      max: 30,
      required: false,
    },
    ph: {
      type: Number,
      min: 0,
      max: 14,
      required: false,
    },
    ec: {
      type : Number,
      min  : 0,
      max  : 3,
      required: false,
    },
    tds: {
      type: Number,
      min: 100,
      max: 3000,
      required: false,
    },
  },

  'FERTILIZATION': {
    qty: {
      type: Number,
      min: 0,
      max: 5000,
      required: false,
    },
    type: {
      type: String,
      allowedValues: ['WATER', 'FERTILIZATION'],
      required: false,
    },
    'mixBase': {
      type: Object,
      required: false,
    },
    'mixBase.label': String,
    'mixBase.index': String,

    // fertilizers array of object object
    'fertilizers': {
      type: Array,
      required: false,
    },

    'fertilizers.$'    : Object,
    'fertilizers.$.qty': {
      type: Number,
    },
    'fertilizers.$.measurement': {
      type: String,
      allowedValues: ['GL', 'MLL']
    },

    // fertilizers[0].compound object
    'fertilizers.$.compound': {
      type: Object,
      required: true
    },
    'fertilizers.$.compound.index' : String,
    'fertilizers.$.compound.label' : String,
    'fertilizers.$.compound.brand' : {
      type: String,
      required: false,
    },
    'fertilizers.$.compound.type' : {
      type: String,
      allowedValues: ['PESTICIDE', 'FERTILIZER']
    },

    temperature: {
      type: Number,
      min: 0,
      max: 30,
      required: false,
    },
    ph: {
      type: Number,
      min: 0,
      max: 14,
      required: false,
    },
    ec: {
      type : Number,
      min  : 0,
      max  : 3,
      required: false,
    },
    tds: {
      type: Number,
      min: 100,
      max: 3000,
      required: false,
    },
  },

  'PESTICIDE': {
    qty: {
      type: Number,
      min: 0,
      max: 5000,
      required: false,
    },

    'pest': {
      type: Object,
      required: false,
    },
    'pest.label': String,
    'pest.index': String,

    'pesticides': {
      type: Array,
      required: false,
    },

    'pesticides.$'    : Object,
    'pesticides.$.qty': {
      type: Number,
    },
    'pesticides.$.measurement': {
      type: String,
      allowedValues: ['GL', 'MLL']
    },

    // pesticides[0].compound object
    'pesticides.$.compound': {
      type: Object,
      required: true
    },
    'pesticides.$.compound.index' : String,
    'pesticides.$.compound.label' : String,
    'pesticides.$.compound.brand' : {
      type: String,
      required: false,
    },
    'pesticides.$.compound.type' : {
      type: String,
      allowedValues: ['PESTICIDE', 'FERTILIZER']
    },
  },
};

const SessionSchemas = (scheduledAt) => {
  return {
    'CURE': {
      endAt: SessionTaskDateSchema(scheduledAt),
      qty: {
        type: Number,
        min: 0,
        max: 1000,
        required: false,
      },
    },
    'DRY': {
      endAt: SessionTaskDateSchema(scheduledAt),
      qty: {
        type: Number,
        min: 0,
        max: 1000,
        required: false,
      },
    },
  };
};

export const SessionTaskSchema = (scheduledAt) => {
  return R.mapObjIndexed((schema) => {
    return new SimpleSchema(schema).extend(TaskBaseSchema);
  }, SessionSchemas(scheduledAt));
};

export const TasksSchemas = R.mapObjIndexed((schema, type) => {
  if (type !== 'base') {
    schema = new SimpleSchema(schema).extend(TaskBaseSchema);
  }
  return schema;
}, Schemas);


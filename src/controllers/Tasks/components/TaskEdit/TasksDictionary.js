import U from 'utils';
import { I18n, formatNumber } from 'utils/I18n';
import moment from 'moment';

const watering = (active) => {
  return {
    active,
    fields:     ['qty', 'mixBase', 'temperature', 'ph', 'ec', 'tds'],
    defaults  : { qty: 0, temperature: 0, ph: 0, ec: 0 , type: 'WATER'},
    serialize(task) {
      const { meta: { qty, temperature, ph, ec, tds, mixBase, compounds = [] }} = task;

      const result = [
        {
          icon: 'to-water',
          label: mixBase ? mixBase.name : I18n.t(['forms', 'container','default_base']),
          value: `${qty} L`
        },
        ...compounds.map(({ compound, qty, measurement}) => ({
          icon: 'fertilizer',
          label: compound.name,
          value: `${formatNumber(qty, { minimumFractionDigits: 2 })} ${I18n.t(['forms', 'measurement', measurement])}`
        })),
      ];

      if (!U.isBlank(temperature)) {
        result.push({ icon: 'thermometer-4-4', label: I18n.t(['forms', 'temperature', 'card_label']) , value: `${temperature}º` });
      }

      if (!U.isBlank(ec)) {
        result.push({ icon: 'thermometer-2-4', label: 'EC' , value: `${formatNumber(ec)}EC` });
      }

      if (!U.isBlank(ph)) {
        result.push({ icon: 'thermometer-3-4', label: 'PH' , value: `${formatNumber(ph, { minimumFractionDigits: 2 })}pH` });
      }

      if (!U.isBlank(tds)) {
        result.push({ icon: 'thermometer-1-4', label: 'TDS', value: `${formatNumber(tds)} ppm`});
      }

      return result;
    }
  };
};

const TasksDictionary = {
  'ENVIRONMENT': {
    active    : true,
    fields    : ['name', 'environment', 'size'],
    defaults  : { size: 0 },
    serialize({ meta: { name, environment, size} }) {
      return (!size) ? [] :  [{
        icon:  'environment',
        label:  name,
        sublabel: I18n.t(['forms', 'environment', environment]),
        value:  `${size || this.defaults.size}m²`
      }];
    }
  },

  'GERMINATION': {
    active    : true,
    serialize : ({ scheduledAt = null, executedAt = null }) => {
      return [{
        icon : 'germination',
        value: moment(executedAt || scheduledAt).format('L'),
      }];
    }
  },

  'CONTAINER': {
    active    : true,
    fields    : ['value'],
    defaults  : { value: 0 },
    serialize({ meta: { value } }) {
      return (!value) ? [] : [{
        icon:  'cup',
        value:  `${value} L`
      }];
    }
  },

  'MEDIUM': {
    active: true,
    fields: ['kind'],
    serialize({ meta: { kind }}) {
      return [{
        icon:  'growing-medium',
        label:  I18n.t(['forms', 'medium', kind]),
      }];
    }
  },

  'PHOTOPERIOD': {
    active:     true,
    fields:     ['daylight', 'darkness'],
    defaults  : { daylight: 12 },
    serialize({ meta: { daylight, darkness }}) {
      return [{
        icon:   'brightness-contrast',
        value:  `${daylight} x ${darkness}`
      }];
    }
  },

  'ENVIRONMENT_TEMPERATURE': {
    active    : true,
    fields    : ['value'],
    defaults  : { value: 0 },
    serialize({ meta: { value }}) {
      return (!value) ? [] : [{
        icon:   'thermometer-4-4',
        value:  `${value}º`,
      }];
    }
  },

  'AIR_HUMIDITY': {
    active    : true,
    fields    : ['value'],
    defaults  : { value: 0 },
    serialize({ meta: { value }}) {
      return (!value) ? [] : [{
        icon:  'thermometer-4-4',
        value:  `${value}%`,
      }];
    }
  },

  'WATER': watering(true),

  'FERTILIZATION':watering(false),
  
  'PH': {
    active   : false,
    fields   : ['value'],
    defaults : { value: 0 },
    serialize({ meta: { value }}) {
      return (!value) ? [] : [{
        icon:  'thermometer-3-4',
        value:  `${formatNumber(value, { minimumFractionDigits: 2 })}pH`,
      }];
    }
  },

  'TEMPERATURE': {
    active   : false,
    fields   : ['value'],
    defaults : { value: 0 },
    serialize({ meta: { value }}) {
      return (!value) ? [] : [{
        icon:  'thermometer-4-4',
        value: `${value}º`,
      }];
    }
  },

  'EC': {
    active   : false,
    fields   : ['value'],
    defaults : { value: 0 },
    serialize({ meta: { value }}) {
      return (!value) ? [] : [{
        icon:  'thermometer-2-4',
        value:  `${formatNumber(value)}EC`,
      }];
    }
  },

  'TDS': {
    active: false,
    fields: ['value'],
    serialize({ meta: { value }}) {
      return (!value) ? [] : [{
        icon:  'thermometer-1-4',
        value:  `${formatNumber(value)} ppm`
      }];
    }
  },

  'CF': {
    active   : false,
    fields   : ['value'],
    defaults : { value: 0 },
    serialize({ meta: { value }}) {
      return (!value) ? [] : [{
        icon:  'thermometer-2-4',
        value:  value
      }];
    }
  },

  'PESTICIDE': {
    active:     true,
    fields:     ['qty', 'pest'],
    defaults  : { qty: 0 },
    serialize(task) {
      const { meta: { qty, pest, compounds = [] }} = task;

      const result = [
        {
          icon: 'to-water',
          // 
          label:  I18n.t(['forms', 'container','default_base']),
          value: `${qty} L`
        },
        {
          icon: 'bug',
          label: pest ? pest.name : 'Pest',
        },
        ...compounds.map(({ compound, qty, measurement}) => ({
          icon: 'fertilizer',
          label: compound.name,
          value: `${formatNumber(qty, { minimumFractionDigits: 2 })} ${I18n.t(['forms', 'measurement', measurement])}`
        })),
      ];

      return result;
    }
  },

  'HARVEST': {
    active    : true,
    fields    : ['total', 'qty'],
    defaults  : { total: true },
    serialize({ meta: { total, qty }}) {
      const humanized = I18n.t(['forms', 'harvest', (total) ? 'TOTAL' : 'PARCIAL']);
      return [{
        icon : 'basket-bud',
        label: `${ I18n.t(['forms', 'harvest','label'])} (${humanized})`,
        value: (qty) ? `${qty}g` : ''
      }];
    }
  },
  'DRY': {
    active    : true,
    fields    : ['endAt', 'qty'],
    // This kind of task must have at least one day of duration
    defaults  : { endAt: moment().add(1, 'day').toDate()},
    serialize({ meta: { endAt, qty }, scheduledAt}) {
      const momentEndAt =  moment(endAt);
      const sessionDays = momentEndAt.startOf('day').diff(moment(scheduledAt).startOf('day'), 'd');
      return [{
        icon : 'basket-bud',
        label: renderSessionTaskLabel('Secagem', sessionDays),
        sublabel: `Termina em ${momentEndAt.format('L')}`,
        value: (qty) ? `${qty}g` : ''
      }];
    }
  },
  'CURE': {
    active    : true,
    fields    : ['endAt', 'qty'],
    // This kind of task must have at least one day of duration
    defaults  : { endAt: moment().add(1, 'day').toDate()},
    serialize({ meta: { endAt, qty }, scheduledAt}) {
      const momentEndAt =  moment(endAt);
      const sessionDays = momentEndAt.startOf('day').diff(moment(scheduledAt).startOf('day'), 'd');
      return [{
        icon : 'basket-bud',
        label: renderSessionTaskLabel('Cura', sessionDays),
        sublabel: `Termina em ${momentEndAt.format('L')}`,
        value: (qty) ? `${qty}g` : ''
      }];
    }
  },

  'DEATH': {
    active:     false,
  },

  'FLOWERING': {
    active:     false,
  },

  'PREFLOWER': {
    active:     false,
  },

  'REVEGA': {
    active:     false,
  },

  'VEGA': {
    active:     false,
  },
};

const renderSessionTaskLabel =
  (taskName, days) => `${taskName}${(`: ${days} dia${days > 1 ? 's' : ''}`)}`;

const TaskTypes = U.pipe(
  U.toPairs,
  U.filter(([,schema]) => schema.active),
  U.map(U.head),
)(TasksDictionary);

const TaskFields = U.pipe(
  U.toPairs,
  U.filter(([,schema]) => schema.active && schema.fields),
  U.map(([,schema]) => schema.fields),
  U.flatten,
  U.uniq
)(TasksDictionary);

export default TasksDictionary;
export { TasksDictionary, TaskTypes, TaskFields };

import { TaskBaseSchema, TasksSchemas } from '../TaskSchemas';
import { TaskTypes } from '../TasksDictionary';

describe('TaskSchemas', () => {
  test('allow only tasks types for type', () => {
    const types = TaskBaseSchema.getAllowedValuesForKey('taskType.key');
    expect(types).toEqual(TaskTypes);
  });

  test('implement schema for ENVIRONMENT_TEMPERATURE', () => {
    const schema = TasksSchemas['ENVIRONMENT_TEMPERATURE'];

    // Extends base
    const defTaskType = schema.getDefinition('taskType.key');
    expect(defTaskType).not.toBeNull();

    const valueDef = schema.getDefinition('value').type[0];
    expect(valueDef).toHaveProperty('type', Number);
    expect(valueDef).toHaveProperty('min', 0);
    expect(valueDef).toHaveProperty('max', 30);
  });

  test('implement schema for AIR_HUMIDITY', () => {
    const schema = TasksSchemas['AIR_HUMIDITY'];

    // Extends base
    const defTaskType = schema.getDefinition('taskType.key');
    expect(defTaskType).not.toBeNull();

    const valueDef = schema.getDefinition('value').type[0];
    expect(valueDef).toHaveProperty('type', Number);
    expect(valueDef).toHaveProperty('min', 0);
    expect(valueDef).toHaveProperty('max', 100);
  });

  test.skip('implement schema for CONTAINER', () => {
    const schema = TasksSchemas['CONTAINER'];

    // Extends base
    const defTaskType = schema.getDefinition('taskType.key');
    expect(defTaskType).not.toBeNull();

    const volumeDef = schema.getDefinition('volume').type[0];
    expect(volumeDef).toHaveProperty('type', Number);
    expect(volumeDef).toHaveProperty('min', 0);
    expect(volumeDef).toHaveProperty('max', 30);
  });

  test('implement schema for EC', () => {
    const schema = TasksSchemas['EC'];

    // Extends base
    const defTaskType = schema.getDefinition('taskType.key');
    expect(defTaskType).not.toBeNull();

    const valueDef = schema.getDefinition('value').type[0];
    expect(valueDef).toHaveProperty('type', Number);
    expect(valueDef).toHaveProperty('min', 0);
    expect(valueDef).toHaveProperty('max', 3);
  });

  test('implement schema for TEMPERATURE', () => {
    const schema = TasksSchemas['TEMPERATURE'];

    // Extends base
    const defTaskType = schema.getDefinition('taskType.key');
    expect(defTaskType).not.toBeNull();

    const valueDef = schema.getDefinition('value').type[0];
    expect(valueDef).toHaveProperty('type', Number);
    expect(valueDef).toHaveProperty('min', 5);
    expect(valueDef).toHaveProperty('max', 25);
  });

  test.skip('implement schema for ENVIRONMENT', () => {
    const schema = TasksSchemas['ENVIRONMENT'];

    // Extends base
    const defTaskType = schema.getDefinition('taskType.key');
    expect(defTaskType).not.toBeNull();

    const sizeDef = schema.getDefinition('size').type[0];
    expect(sizeDef).toHaveProperty('type', Number);
    expect(sizeDef).toHaveProperty('min', 0);
    expect(sizeDef).toHaveProperty('max', 100);

    const types = schema.getAllowedValuesForKey('environment_type');
    expect(types).toEqual(['indoor', 'outdoor', 'greenhouse']);

    const nameDef = schema.getDefinition('name').type[0];
    expect(nameDef).toHaveProperty('type', String);
  });

  test('implement schema for GERMINATION', () => {
    const schema = TasksSchemas['GERMINATION'];

    // Extends base
    const defTaskType = schema.getDefinition('taskType.key');
    expect(defTaskType).not.toBeNull();
  });

  test('implement schema for HARVEST', () => {
    const schema = TasksSchemas['HARVEST'];

    // Extends base
    const defTaskType = schema.getDefinition('taskType.key');
    expect(defTaskType).not.toBeNull();

    const totalDef = schema.getDefinition('total').type[0];
    expect(totalDef).toHaveProperty('type', String);

    const types = schema.getAllowedValuesForKey('total');
    expect(types).toEqual(['TOTAL', 'PARCIAL']);

    const valueDef = schema.getDefinition('qty').type[0];
    expect(valueDef).toHaveProperty('type', Number);
    expect(valueDef).toHaveProperty('min', 0);
    expect(valueDef).toHaveProperty('max', 1000);
  });

  test('implement schema for PH', () => {
    const schema = TasksSchemas['PH'];

    // Extends base
    const defTaskType = schema.getDefinition('taskType.key');
    expect(defTaskType).not.toBeNull();

    const valueDef = schema.getDefinition('value').type[0];
    expect(valueDef).toHaveProperty('type', Number);
    expect(valueDef).toHaveProperty('min', 0);
    expect(valueDef).toHaveProperty('max', 14);
  });

  test('implement schema for TDS', () => {
    const schema = TasksSchemas['TDS'];

    // Extends base
    const defTaskType = schema.getDefinition('taskType.key');
    expect(defTaskType).not.toBeNull();

    const valueDef = schema.getDefinition('value').type[0];
    expect(valueDef).toHaveProperty('type', Number);
    expect(valueDef).toHaveProperty('min', 0);
    expect(valueDef).toHaveProperty('max', 3000);
  });

  test('implement schema for MEDIUM', () => {
    const schema = TasksSchemas['MEDIUM'];

    // Extends base
    const defTaskType = schema.getDefinition('taskType.key');
    expect(defTaskType).not.toBeNull();

    const kindValues = schema.getAllowedValuesForKey('kind');
    expect(kindValues).toEqual(['HYDROPONIC', 'SOIL', 'COCONUT']);
  });

  test('implement schema for PHOTOPERIOD', () => {
    const schema = TasksSchemas['PHOTOPERIOD'];

    // Extends base
    const defTaskType = schema.getDefinition('taskType.key');
    expect(defTaskType).not.toBeNull();

    const qtyDef = schema.getDefinition('daylight').type[0];
    expect(qtyDef).toHaveProperty('type', Number);
    expect(qtyDef).toHaveProperty('min', 0);
    expect(qtyDef).toHaveProperty('max', 24);
  });

  test.skip('implement schema for WATER', () => {
    const schema = TasksSchemas['WATER'];

    // Extends base
    const defTaskType = schema.getDefinition('taskType.key');
    expect(defTaskType).not.toBeNull();

    const qtyDef = schema.getDefinition('qty').type[0];
    expect(qtyDef).toHaveProperty('type', Number);
    expect(qtyDef).toHaveProperty('min', 0);
    expect(qtyDef).toHaveProperty('max', 5000);

    const temperatureDef = schema.getDefinition('temperature').type[0];
    expect(temperatureDef).toHaveProperty('type', Number);
    expect(temperatureDef).toHaveProperty('min', 0);
    expect(temperatureDef).toHaveProperty('max', 30);

    const phDef = schema.getDefinition('ph').type[0];
    expect(phDef).toHaveProperty('type', Number);
    expect(phDef).toHaveProperty('min', 0);
    expect(phDef).toHaveProperty('max', 14);

    const ecDef = schema.getDefinition('ec').type[0];
    expect(ecDef).toHaveProperty('type', Number);
    expect(ecDef).toHaveProperty('min', 0);
    expect(ecDef).toHaveProperty('max', 3);

    const tdsDef = schema.getDefinition('tds').type[0];
    expect(tdsDef).toHaveProperty('type', Number);
    expect(tdsDef).toHaveProperty('min', 100);
    expect(tdsDef).toHaveProperty('max', 3000);

    const mixDef = schema.getDefinition('mix').type[0];
    expect(mixDef).toHaveProperty('type', Array);

    const mixNameDef = schema.getDefinition('mix.$.name').type[0];
    expect(mixNameDef).toHaveProperty('type', String);

    const mixQtyDef = schema.getDefinition('mix.$.qty').type[0];
    expect(mixQtyDef).toHaveProperty('type', Number);

    const daylightValues = schema.getAllowedValuesForKey('mix.$.measurement');
    expect(daylightValues).toEqual(['gl', 'mll']);
  });
});

import React from 'react';
import { shallow } from 'enzyme';
import R from 'ramda';

import TaskEdit from '../';
import { tree } from '../../../../../__mocks__/stateHelper';

describe('TaskEdit', () => {
  let plant;
  beforeAll(() => {
    ([[, plant]] = R.toPairs(tree.get('plants', 'list')));
  });

  describe('new task', () => {
    test.skip('renders correctly new task', () => {
      const task = tree.get('tasks', 'newTask');
      const props = {
        task, plant,
        submitText: 'save',
        onSaveTask: () => {},
      };
      const wrapper = shallow(<TaskEdit {...props} />);
      expect(wrapper).toMatchSnapshot();
    });
  });

  describe('edit task', () => {
    let defaultProps;
    beforeAll(() => {
      const [[, plant]] = R.toPairs(tree.get('plants', 'list'));
      defaultProps = {
        plant,
        submitText: 'add',
        onSaveTask: () => {},
      };
    });

    test.skip('renders correctly task type of AIR_HUMIDITY', () => {
      const props = {
        ...defaultProps,
        task: {
          type: 'AIR_HUMIDITY',
          value: 10,
        }
      };
      const wrapper = shallow(<TaskEdit {...props} />);
      expect(wrapper).toMatchSnapshot();
    });

    test.skip('renders correctly task type of CONTAINER', () => {
      const props = {
        ...defaultProps,
        task: {
          type: 'CONTAINER',
          value: 10,
        }
      };
      const wrapper = shallow(<TaskEdit {...props} />);
      expect(wrapper).toMatchSnapshot();
    });

    test.skip('renders correctly task type of EC', () => {
      const props = {
        ...defaultProps,
        task: {
          type: 'EC',
          value: 3,
        }
      };
      const wrapper = shallow(<TaskEdit {...props} />);
      expect(wrapper).toMatchSnapshot();
    });

    test.skip('renders correctly task type of WATER', () => {
      const [[, task]] = R.toPairs(tree.get('tasks', 'list'));
      const props = {
        ...defaultProps,
        task, plant,
        mixes: tree.get('mixes', 'list'),
      };
      const wrapper = shallow(<TaskEdit {...props} />);
      expect(wrapper).toMatchSnapshot();
    });
  });
});

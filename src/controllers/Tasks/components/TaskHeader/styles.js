// @flow
import EStyleSheet from 'react-native-extended-stylesheet';

const iconCommon = {
  paddingTop : 2,
  marginRight: 5,
};

export default EStyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    padding        : 15,
  },
  headerWrapper: {
    flexDirection  : 'row',
  },
  date: {
    flexDirection: 'row',
  },
  dateIcon: iconCommon,

  executeButton: {
    flexDirection : 'row',
    justifyContent: 'space-between',
  },

  executeButtonIcon: {
    ...iconCommon,
    color: '$Colors.Green',
    fontSize: 30,
  },

  infoList: {
    marginTop: 15,
  },

  infoListRow: {
    borderRadius: 2,
    backgroundColor: '$Colors.LightSilver',
    flexDirection: 'row',
    paddingHorizontal: 10,
    alignItems: 'center',
    height: 40,
  },

  'infoListRow:nth-child-odd': {
    backgroundColor: '#FFFFFF',
  },

  infoListCol: {
    flexDirection : 'row',
    justifyContent: 'space-between',
    alignItems    : 'center',

    borderRadius: 2,
    backgroundColor: '$Colors.LightSilver',
    paddingHorizontal: 10,
    height: 40,
  },

  infoListLeftCol: {
    flex: 3,
    flexDirection: 'row',
  },

  infoListRightCol: {
    flexDirection: 'row',
    flex: 2,
  },

  infoIcon: {
    marginRight: 10,
    width: 12,
    height: 12,
  },

  infoLabel: {
    fontSize: 12,
    color: '$Colors.Gray',
  },

  taskStatus: {
    borderWidth: 1,
    borderRadius: 4,
    fontSize: 10,
    paddingLeft: 4,
    paddingTop: 3,
    paddingRight: 3,
    paddingBottom: 2,
    justifyContent: 'center',
    alignSelf: 'center',
  },

  taskStatusGreen: {
    color: '$Colors.Green',
    borderColor: '$Colors.Green',
  },

  taskStatusPurple: {
    color: '$Colors.Purple',
    borderColor: '$Colors.Purple',
  },

  taskIcon: {
    color: '$Colors.Purple',
    fontSize: 20,
  }

});

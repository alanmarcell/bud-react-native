import React, { Component, PropTypes } from 'react';
import { View, Text } from 'react-native';

import CommentsCount  from 'components/CommentsCount';
import PlantName      from 'controllers/Plants/components/PlantName';
import TaskIcon       from '../TaskIcon';
import InfoModal      from 'components/InfoModal';

import { Moment, Touchable } from 'utils/components';
import Icon from 'components/Icon';

import { I18n } from 'utils/I18n';

import styles from './styles';
import stylesHelpers from '../../../../themes/helpers';

export default class TaskHeader extends Component {
  static defaultProps = {
    commentsCount: 0,
  };

  static propTypes = {
    meta           : PropTypes.object,
    scheduledAt    : PropTypes.oneOfType([
      PropTypes.instanceOf(Date),
      PropTypes.string,
    ]),
    executedAt     : PropTypes.oneOfType([
      PropTypes.instanceOf(Date),
      PropTypes.string,
    ]),
    commentsCount  : PropTypes.number,
    plant          : PropTypes.object,
    onToggleExecute: PropTypes.func.isRequired,
    style          : PropTypes.oneOfType([
      PropTypes.shape(),
      PropTypes.number,
    ]),
  }

  renderHeader() {
    const { plant, meta: { type }, description } = this.props;

    const taskIcon = type === 'FERTILIZATION' ? 'WATER' : type;

    const scope =  type === 'WATER' ? 'forms.water.type.' : 'tasks.types.';

    return (
      <View style={styles.headerWrapper}>
        <View style={[stylesHelpers.leftColumn]}>
          <TaskIcon type={taskIcon} style={styles.taskIcon} />
        </View>
        <View style={[stylesHelpers.middleColumn, stylesHelpers.alignCenterLeft]}>
          <PlantName {...plant} />
          <Text>{I18n.t(`${scope}${type}`)}</Text>
        </View>
        <View style={[stylesHelpers.rightColumn, stylesHelpers.alignCenterRight]}>
          <InfoModal title={plant.name} >
            <Text>{description}</Text>
          </InfoModal>
        </View>
        <View style={[stylesHelpers.rightColumn, stylesHelpers.alignCenterRight]}>
          { this.renderExecutedButton() }
        </View>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        { this.renderHeader() }
        { this.renderInfoBar() }
      </View>
    );
  }

  renderInfoBar() {
    const { commentsCount, scheduledAt, executedAt } = this.props;

    // TODO: add i18n
    let status = 'AGENDADO';
    let statusColor = styles.taskStatusPurple;
    const taskDate = executedAt||scheduledAt;
    if (executedAt) {
      status = 'EXECUTADO';
      statusColor = styles.taskStatusGreen;
    }

    return (
      <View style={[styles.infoList, styles.infoListCol]}>
        <View style={styles.date}>
          <Icon name="calendar" style={styles.dateIcon} />
          <Moment style={styles.infoLabel} date={taskDate} action="format" args={['L']} />
        </View>
        <CommentsCount textStyle={styles.infoLabel} count={commentsCount} />
        <Text style={[styles.taskStatus, statusColor]}>{ status }</Text>
      </View>
    );
  }

  renderExecutedButton() {
    const { executedAt, onToggleExecute } = this.props;
    let icon  = (executedAt) ? 'check-square-o' : 'square-o';
    return (
      <Touchable onPress={onToggleExecute} style={styles.executeButton} >
        <Icon name={icon} style={styles.executeButtonIcon} />
      </Touchable>
    );
  }

}

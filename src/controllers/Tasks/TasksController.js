import React from 'react';
import U from 'utils';
import moment from 'moment';

import RootController from '../RootController';
import TasksList from './components/TasksList';

import { graphql, compose, makeLoadList } from 'graphql';
import { TASK_LIST, TASK_UPDATE, TASK_OPTIMISTIC_EXECUTED } from 'graphql/tasks';

export function mapTasks(tasks) {
  const map = U.pipe(
    U.sortBy(U.prop('scheduledAt')),
    U.groupBy((task) => moment(task.scheduledAt).startOf('day').format()),
    U.toPairs,
    U.map(([date, data]) => ({
      data, key: date, date: moment(date).format('LL')
    })),
  );
  return map(tasks);
}

class TasksController extends RootController {
  static navigationOptions = {
    title: 'screens.tasks.title',
  };

  static defaultProps = {
    embeded: false,
  };

  static propTypes = {
    embeded: React.PropTypes.bool,
  };

  get actions() {
    return {
      notBlock: {
        onTaskPress: ( objectId ) => {
          const { embeded } = this.props;
          this.redirectTo(embeded ? 'grower_plantTaskProfile' : 'taskProfile', { objectId, embeded });
        },
        loadMoreEntries: () => {
          const { loadMoreEntries, plantId } = this.props;
          const variables = {};
          if (plantId) {
            variables.plantId = plantId;
          }
          return loadMoreEntries({ variables });
        },
        setExecuted: (objectId, input) => {
          const { updateTask } = this.props;
          return updateTask(objectId, input);
        }
      }
    };
  }

  renderView(props) {
    let { tasks } = props;
    if (tasks) {
      tasks = mapTasks(tasks);
    }
    return <TasksList {...props} tasks={tasks} />;
  }
}

export default compose(
  graphql(TASK_LIST, {
    ...makeLoadList(TASK_LIST, 'tasks'),
    options: (props) => {
      const { plantId } = props;
      if (plantId) {
        return { variables: { plantId }};
      }
      return {};
    }
  }),
  graphql(TASK_UPDATE, TASK_OPTIMISTIC_EXECUTED)
)(TasksController);

import TasksController from './TasksController';
import TaskProfileController  from './TaskProfileController';
import TaskEditController  from './TaskEditController';

export { TaskProfileController, TasksController, TaskEditController};

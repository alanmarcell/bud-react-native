// @flow
import React from 'react';
import uuid from 'uuid';

import { I18n } from 'utils/I18n';
import TaskEdit from './components/TaskEdit';
import RootController from '../RootController';

import { graphql, compose, propType } from 'graphql';
import {
  taskFragments,
  PLANT_PROFILE,
  TASK_PROFILE, TASK_CREATE, TASK_UPDATE
} from 'graphql';
import { makeGet, mutationCreate } from 'graphql';

class TaskEditController extends RootController {
  static propTypes = {
    task: propType(taskFragments.taskBase),
    loading: React.PropTypes.bool,
  };

  static navigationOptions = {
    getTitle: ({ name }) => {
      const titleEdit = 'screens.taskEdit.title';
      const titleAdd  = 'screens.taskAdd.title';
      return I18n.T(/.*Add$/.test(name) ? titleAdd : titleEdit);
    }
  };

  get actions() {
    return {
      onSaveTask: async ({ task: input }) => {
        let { objectId, plantId, updateTask, createTask } = this.props;
        if (objectId) {
          await updateTask({ variables: { id: objectId, input }});
          this.goBack();
          this.refresh();
        } else {
          objectId = uuid.v4();
          input.id = objectId;
          input.plantId = plantId;
          await createTask({ variables: { input }});
          this.redirectTo('plantTaskProfile', { type: 'replace', objectId });
        }
      }
    };
  }

  renderView(props) {
    const { task = {}, plant = {} } = this.props;
    if (!task.plant) {
      task.plant = plant;
    }

    return (
      <TaskEdit {...props} task={task} />
    );
  }
}

export default compose(
  graphql(TASK_PROFILE, makeGet()),
  graphql(PLANT_PROFILE, makeGet('plantId')),
  graphql(TASK_UPDATE, { name: 'updateTask' }),
  graphql(TASK_CREATE, mutationCreate('createTask', 'Task', 'tasks', 'UserTasks'))
)(TaskEditController);

// @flow
import React from 'react';

import Register from 'components/Register';
import RootController from './RootController';
import { graphql, compose } from 'graphql';
import { makeLoadList } from 'graphql';
import { ORIGINS_LIST, REGISTER_QUERY } from 'graphql';
import { connect } from 'react-redux';
import { updateCredentials } from '../redux/actions';

class RegisterController extends RootController {
  static navigationOptions = {
    title: 'screens.register.title',
  };

  get actions() {
    return {
      onRegister: async(user) => {
        const { data: { createUser } } = await this.props.register({
          variables: { user }
        });

        const { session } = createUser;

        this.props.updateCredentials({
          session,
          token: session.token,
          email: user.credentials.email            
        });
        this.redirectTo('recoveryToken', {
          session
        });
      },
      notBlock: {
        onTerms   : () => this.redirectTo('terms'),
        onPollicy : () => this.redirectTo('privacy'),
      }
    };
  }

  renderView(props) {
    return <Register {...props} />;
  }
}

const mapStateToProps = undefined;

const mapDispatchToProps = dispatch => ({
  updateCredentials: credentials => dispatch(updateCredentials(credentials))
});

export default compose(
  graphql(ORIGINS_LIST, makeLoadList(ORIGINS_LIST, 'origins')),
  graphql(REGISTER_QUERY, { name: 'register' })
)(
  connect(mapStateToProps, mapDispatchToProps)(RegisterController)
);

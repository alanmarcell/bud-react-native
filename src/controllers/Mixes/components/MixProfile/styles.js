// @flow
import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  commentTitleWrapper: {
    flexDirection:'row',
    paddingHorizontal: 15,
    marginTop: 30,
    marginBottom: 15,

  },
  commentIconColor: {
    color: '$Colors.Disabled',
  },
  commentsTitle: {
    color: '$Colors.Disabled',
    fontSize: 11,
    fontWeight: 'bold',
    marginLeft: 5,
  }
});

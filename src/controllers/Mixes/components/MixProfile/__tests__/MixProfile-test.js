import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';
import R from 'ramda';

import MixProfile from '../';
import { stores } from '../../../../../__mocks__/stateHelper';

describe('MixProfile', () => {
  test.skip('renders correctly', () => {
    const mix = R.head(stores.mixes.find());
    const component = shallow(<MixProfile mix={mix} />);
    expect(component).toMatchSnapshot();
  });
});

import React, { Component } from 'react';
import { ScrollView, View, ActivityIndicator } from 'react-native';
import R from 'ramda';

import { I18n, formatNumber } from 'utils/I18n';
import MixHeader from '../MixHeader';
import TaskInfoCardItem from 'controllers/Tasks/components/TaskInfoCardItem';

export default class MixProfile extends Component {
  static propTypes = {
    mix: React.PropTypes.object,
    loading: React.PropTypes.bool,
  }

  listMixItems = () => {
    const { items } = this.props.mix || [];
    return R.addIndex(R.map)((item, index) =>
      this.renderItem(item, index)
    , items);
  }

  renderItem = (item, index) => {
    const { compound: { name }, qty, measurement } = item;
    const value = `${formatNumber(qty)} ${I18n.t(['forms', 'measurement', measurement])}`;
    return <TaskInfoCardItem key={index} icon='fertilizer' label={name} value={value} />;
  }

  render() {
    const { loading, mix } = this.props;

    if (loading) {
      return <ActivityIndicator />;
    }

    if (!mix) {
      return null;
    }

    return (
      <ScrollView>
        <MixHeader mix={mix} />
        <View style={{marginTop: 10}}>
          { this.listMixItems() }
        </View>
      </ScrollView>
    );
  }
}

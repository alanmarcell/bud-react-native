import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';
import R from 'ramda';

import MixCardItem from '../';
import { stores } from '../../../../../__mocks__/stateHelper';

test('renders correctly', () => {
  const mix = R.head(stores.mixes.find());
  const wrapper = shallow(<MixCardItem {...mix} />);
  expect(wrapper).toMatchSnapshot();
});

import React, { Component, PropTypes } from 'react';
import { View, Text } from 'react-native';

import SelectableCard     from 'components/SelectableCard';
import RoundedIcon        from 'components/RoundedIcon';
import PlantName          from 'controllers/Plants/components/PlantName';

import styles from './styles';
import stylesHelpers from '../../../../themes/helpers';

export default class MixCardItem extends Component {
  static defaultProps = {
    ...SelectableCard.defaultProps,
  };

  static propTypes = {
    ...SelectableCard.propTypes,
    name  : PropTypes.string,
    description  : PropTypes.string,
  };

  render() {
    const { name, description } = this.props;
    return (
      <SelectableCard {...this.props}>
        <View style={[stylesHelpers.leftColumn, stylesHelpers.alignCenter]}>
          <RoundedIcon object={this.props} source={require('./images/icon-mixes.png')}/>
        </View>
        <View style={[stylesHelpers.middleColumn, stylesHelpers.alignCenterLeft]}>
          <PlantName name={name} />
          <Text style={styles.plantType}>{description}</Text>
        </View>
        <View style={[stylesHelpers.rightColumn, stylesHelpers.alignCenterRight]}>
        </View>
      </SelectableCard>
    );
  }
}

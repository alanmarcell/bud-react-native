// @flow
import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  budType: {
    color: '$Colors.Gray',
    fontSize: 13
  },
  date: {
    fontSize: 11,
    color: '#95989A',
    top: 10,
  }
});

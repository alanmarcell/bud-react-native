import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';
import R from 'ramda';

import MixHeader from '../';
import { stores } from '../../../../../__mocks__/stateHelper';

describe('MixHeader', () => {
  test('renders correctly', () => {
    const mix = R.head(stores.mixes.find());
    const component = shallow(<MixHeader mix={mix} />);
    expect(component).toMatchSnapshot();
  });
});

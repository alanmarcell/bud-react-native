import React, { Component, PropTypes } from 'react';
import { View, Text } from 'react-native';
import { I18n } from 'utils/I18n';

import RoundedIcon    from 'components/RoundedIcon';
import PlantName      from 'controllers/Plants/components/PlantName';

import styles from './styles';
import stylesHelpers from '../../../../themes/helpers';

const profileThumb = require('./images/icon-mixes.png');

export default class MixHeader extends Component {
  static defaultProps = {
  };

  static propTypes = {
    mix            : PropTypes.object,
    onToggleExecute: PropTypes.func,
    style          : PropTypes.oneOfType([
      PropTypes.shape(),
      PropTypes.number,
    ]),
  }

  renderErrors(object) {
    const { synced_errors } = object;
    if (synced_errors && synced_errors.length > 0) {
      const msgs = synced_errors.map((error, index) => (
        <Text key={index} >- {I18n.t(error.msg, { defaultValue: error.msg, object })}</Text>
      ));
      return (
        <View>{msgs}</View>
      );
    }
  }

  renderHeader = () => {
    const { mix } = this.props;
    const { name, description } = mix;

    return (
      <View style={{ flex: 1 }}>
        <View style={styles.headerWrapper}>
          <View style={[stylesHelpers.leftColumn]}>
            <RoundedIcon object={mix} source={profileThumb} />
          </View>
          <View style={[stylesHelpers.middleColumn, stylesHelpers.alignCenterLeft]}>
            <PlantName name={name} />
            <Text>{description}</Text>
          </View>
          <View style={[stylesHelpers.rightColumn, stylesHelpers.alignCenterRight]}>
          </View>
        </View>
        {this.renderErrors(mix)}
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        { this.renderHeader() }
      </View>
    );
  }

}

// @flow
import EStyleSheet from 'react-native-extended-stylesheet';

const borderColor = '$Colors.LightSilver';
const iconCommon = {
  paddingTop: 2,
  marginRight: 5,
};

const lineCommon = {
  flexDirection : 'row',
  justifyContent: 'space-between',
  alignItems    : 'center',
};

export default EStyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    padding        : 15,
  },
  headerWrapper: {
    flexDirection  : 'row',
    borderColor,
    borderBottomWidth: 0.5,
    paddingBottom: 10,
  },
  header: {
    ...lineCommon,
    flex         : 1,
    paddingBottom: 10,
  },
  info: {
    flex        : 1,
    paddingLeft : 10,
    paddingRight: 10,
  },
  taskIcon: {
    alignSelf: 'flex-end',
  },

  footer: {
    ...lineCommon,
    borderColor,
    paddingLeft   : 10,
    paddingRight  : 10,
    paddingTop    : 10,
    borderTopWidth: 1,
  },

  shareContainer: {
    ...lineCommon,
    borderColor,
    padding       : 10,
    borderTopWidth: 1,
  },
  shareText: {
    flex        : 1,
    paddingLeft : 10,
    paddingRight: 10,
  },

  date: {
    flexDirection : 'row',
  },
  dateIcon: iconCommon,

  /* PlantFamily
   */
  plantFamily: {
    flexDirection : 'row',
  },
  plantFamilyIcon: iconCommon,

  /* PlantFlowering
   */
  plantFlowering: {
    flexDirection : 'row',
  },
  plantFloweringIcon: iconCommon,

  infoList: {
    marginTop: 15,
  },

  infoListRow: {
    borderRadius: 2,
    backgroundColor: '$Colors.LightSilver',
    flexDirection: 'row',
    paddingHorizontal: 10,
    alignItems: 'center',
    height: 40,
  },

  'infoListRow:nth-child-odd': {
    backgroundColor: '#FFFFFF',
  },

  infoListLeftCol: {
    flexDirection: 'row',
    flex: 3,
  },

  infoListRightCol: {
    flexDirection: 'row',
    flex: 2,
  },

  infoIcon: {
    marginRight: 10,
    width: 12,
    height: 12,
  },
  infoLabel: {
    fontSize: 12,
    color: '$Colors.Gray',
  },

});

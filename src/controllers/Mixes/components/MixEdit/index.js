import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ActivityIndicator } from 'react-native';

import { parseNumber } from 'utils/I18n';
import U from 'utils';

import { BudForms } from 'components/BudForms';
import { NameField, DescriptionField  } from 'components/BudForms/Fields';

import { FertilizersFactory } from './FertilizersFactory';
import { MixSchema } from './MixSchema';

export default class MixEdit extends Component {
  static propTypes = {
    mix       : PropTypes.object,
    loading   : PropTypes.bool,
    submitText: PropTypes.string.isRequired,
    saveMix   : PropTypes.func.isRequired,
  }

  _handleSubmit = ({ fertilizers, ...mix}) => {
    const items = fertilizers.map(({ compound, ...item }) => {
      let compoundId = compound.index;
      return { ...item, compoundId };
    });

    this.props.saveMix({
      mix: {...mix, items, kind: 'CUSTOM' },
    });
  };

  afterSubmit = ({ fertilizers }, submit) => {
    const { refs: { form } } = this.form;
    fertilizers.map(({ qty }, index) => {
      if (qty) {
        form.onValueChange(`fertilizers.${index}.qty`, parseNumber(qty));
      }
    });
    submit();
  };

  getDoc() {
    const { mix: { items = [], ...mix} = {}} = this.props;
    return {
      ...U.pick(['name', 'description'], mix),
      fertilizers: FertilizersFactory.mapCompounds(items)
    };
  }

  render() {
    const { loading } = this.props;
    if (loading) {
      return <ActivityIndicator />;
    }

    const formProps = {
      ref        : (ref) => this.form = ref,
      schema     : MixSchema,
      doc        : this.getDoc(),
      submitText : this.props.submitText,
      onSubmit   : this._handleSubmit,
      afterSubmit: this.afterSubmit,
    };

    return (
      <BudForms ref="form" {...formProps} >
        <NameField i18nScope="forms.mixName" />
        <DescriptionField />
        <FertilizersFactory />
      </BudForms>
    );
  }
}

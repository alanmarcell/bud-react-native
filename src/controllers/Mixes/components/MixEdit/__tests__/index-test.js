import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';
import R from 'ramda';

import MixEdit from '../';
import { stores } from '../../../../../__mocks__/stateHelper';

describe('MixEdit', () => {
  describe('editing mix', () => {
    const defaultProps = {
      mix: null,
      onSaveMix: () => {},
      submitText: 'save',
    };

    beforeAll(() => {
      const mix = R.head(stores.mixes.find());
      defaultProps.mix = mix;
    });

    test('renders correctly for edit mix', () => {
      const wrapper = shallow(<MixEdit {...defaultProps} />).dive();
      expect(wrapper).toMatchSnapshot();
    });
  });

  describe('adding mixes', () => {
    const defaultProps = {
      mix: null,
      onSaveMix: () => {},
      submitText: 'save',
    };

    beforeAll(() => {
      defaultProps.mix = R.head(stores.mixes.find());
    });

    test('renders correctly for new mix', () => {
      const wrapper = shallow(<MixEdit {...defaultProps} />);
      expect(wrapper).toMatchSnapshot();
    });

    test.skip('form submit calls to save mixes', () => {
      const onSaveMix = jest.fn();
      const props = {
        ...defaultProps,
        onSaveMix,
      };
      const wrapper = shallow(<MixEdit {...props} />).dive();
      const doc  = { name: 'mix1', description: 'hahah', fertilizers: [], synced_errors: [] };

      wrapper.find('Form').simulate('submit', doc);
      expect(onSaveMix).toHaveBeenCalledWith(doc);
    });
  });
});

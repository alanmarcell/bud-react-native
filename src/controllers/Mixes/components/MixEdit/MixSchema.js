import { SimpleSchema } from 'components/BudForms/Schemas';
import { NameSchema } from 'components/BudForms/Schemas';

// make `name` schema required
const RequiredNameSchema = Object.assign({}, NameSchema);
RequiredNameSchema.required = true;

export const MixSchema = new SimpleSchema({

  name: RequiredNameSchema,

  description: {
    type: String,
    required: false,
  },

  // fertilizers array of object object
  'fertilizers': {
    type: Array,
    required: true,
  },

  'fertilizers.$'    : Object,
  'fertilizers.$.qty': {
    type: Number,
  },
  'fertilizers.$.measurement': {
    type: String,
    allowedValues: ['GL', 'MLL']
  },

  // fertilizers[0].compound object
  'fertilizers.$.compound': {
    type: Object,
    required: true
  },
  'fertilizers.$.compound.index' : String,
  'fertilizers.$.compound.label' : String,
  'fertilizers.$.compound.brand' : {
    type: String,
    required: false,
  },
  'fertilizers.$.compound.type' : {
    type: String,
    allowedValues: ['PESTICIDE', 'FERTILIZER']
  }

});

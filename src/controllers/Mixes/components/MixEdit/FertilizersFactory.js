import { CompoundFactory } from 'components/BudForms/Factories';
import FertilizerFactory from 'controllers/factories/FertilizerFactory';

export class FertilizersFactory extends CompoundFactory {
  static defaultProps = {
    fieldName: 'fertilizers',
    factory: FertilizerFactory
  }
}


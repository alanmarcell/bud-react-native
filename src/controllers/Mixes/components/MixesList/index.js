import React, { Component } from 'react';
import { View, FlatList, ActivityIndicator } from 'react-native';

import { I18n } from 'utils/I18n';
import Refreshable from 'utils/components/Refreshable';

import BlankList    from 'components/BlankList';
import MixCardItem  from '../MixCardItem';

import styles from './styles';

export default class MixesList extends Component {
  static defaultProps = {
    mixes: [],
  };

  static propTypes = {
    mixes: React.PropTypes.array,
  }

  onMixPress = (mixId) => {
    if (this.props.onMixPress) {
      this.props.onMixPress(mixId);
    }
  };

  renderItem = ({ item, index }) => {
    let listLength = this.props.mixes.length;
    return (
      <MixCardItem
        {...item}
        onPress={() => this.onMixPress(item.id)}
        listLength={listLength}
        listIndex={parseInt(index)}
      />
    );
  };

  renderFooter = () => {
    const { loading } = this.props;
    return (
      <View style={styles.footer}>
        {loading ? <ActivityIndicator /> : null}
      </View>
    );
  };

  renderHeader = () => {
    const { mixes } = this.props;
    if (mixes.length <= 0) {
      return (
        <BlankList
          icon="mixes"
          title={I18n.t('mixes.blank.title')}
          message={I18n.t('mixes.blank.message')}
        />
      );
    } else {
      return null;
    }
  };

  loadMore = () => {
    const { loading, loadMoreEntries, hasNextPage } = this.props;
    if (!loading && hasNextPage) {
      loadMoreEntries();
    }
  }

  render() {
    const { mixes, loading, refetch } = this.props;
    return (
      <FlatList
        data={mixes}
        refreshing={loading}
        refreshControl={Refreshable({ loading, refetch })}
        keyExtractor={(item, index) => index}
        renderItem={this.renderItem}
        ListHeaderComponent={this.renderHeader}
        ListFooterComponent={this.renderFooter}
        onEndReached={this.loadMore}
      />
    );
  }
}

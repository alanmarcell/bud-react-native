// @flow
import React from 'react';
import { View } from 'react-native';

import RootController from '../RootController';
import ActionMenu from 'components/ActionMenu';
import MixesList from './components/MixesList';

import { graphql, makeLoadList } from 'graphql';
import { MIX_LIST } from 'graphql';

class MixesController extends RootController {
  static navigationOptions = {
    title: 'screens.mixes.title',
  };

  get actions() {
    return {
      onMixPress: (objectId) => {
        this.redirectTo('mixProfile', { objectId });
      }
    };
  }

  onActionPress = (_option) => {
    this.redirectTo('mixAdd');
  };

  renderView(props) {
    const menuItems = {
      newMix: 'mixes',
    };

    return (
      <View style={{ flex: 1 }}>
        <MixesList {...props} />
        <ActionMenu onPress={this.onActionPress} items={menuItems} />
      </View>
    );
  }
}

export default graphql(MIX_LIST, makeLoadList(MIX_LIST, 'mixes'))(MixesController);

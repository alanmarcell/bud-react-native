import React from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid';

import { I18n } from 'utils/I18n';
import MixEdit from './components/MixEdit';
import RootController from '../RootController';

import { graphql, compose, propType } from 'graphql';
import { makeGet, mutationCreate } from 'graphql';
import {
  mixFragments,
  MIX_QUERY, MIX_CREATE, MIX_UPDATE
} from 'graphql';

class MixEditController extends RootController {
  static propTypes = {
    mix       : propType(mixFragments.mixBase),
    loading   : PropTypes.bool,
    updateMix : PropTypes.func.isRequired,
    createMix : PropTypes.func.isRequired,
  };

  static navigationOptions = {
    getTitle: ({ name }) => {
      const titleEdit = 'screens.mixEdit.title';
      const titleAdd  = 'screens.mixAdd.title';
      return I18n.T(/.*Add$/.test(name) ? titleAdd : titleEdit);
    }
  };

  get actions() {
    return {
      saveMix: async ({ mix: input }) => {
        let { objectId, updateMix, createMix } = this.props;

        if (objectId) {
          await updateMix({ variables: { id: objectId, input }});
          this.redirectTo('mixProfile', { type: 'back' });
        } else {
          objectId = uuid.v4();
          input.id = objectId;
          await createMix({ variables: { input }});
          this.redirectTo('mixProfile', { type: 'replace', objectId });
        }
      }
    };
  }

  renderView(props) {
    const submitText = I18n.t(`screens.${this.sceneKey}.submit`);

    return (
      <MixEdit {...props} submitText={submitText} />
    );
  }
}

export default compose(
  graphql(MIX_QUERY, makeGet()),
  graphql(MIX_UPDATE, { name: 'updateMix' }),
  graphql(MIX_CREATE, mutationCreate('createMix', 'Mix', 'mixes', 'UserMixes')),
)(MixEditController);

// @flow
import React from 'react';
import { View } from 'react-native';

import NavIcon from 'scenes/NavIcon';
import MixProfile from './components/MixProfile';
import RootController from '../RootController';

import { propType, compose, graphql } from 'graphql';
import { makeDeleteMutation, makeGet } from 'graphql';
import { mixFragments, MIX_PROFILE, MIX_DELETE } from 'graphql';

class MixProfileController extends RootController {
  static propTypes = {
    mix: propType(mixFragments.mixBase),
  }

  static navigationOptions = {
    title: 'screens.mix.title',
    // right: makeMenu(
    //   {
    //     MixEdit    : 'screens.mix.menu.edit',
    //     onMixRemove: 'screens.mix.menu.remove',
    //   },
    //   (option, { state, navigate }) => {
    //     const { params: { objectId, onDestroyMix } } = (state || {});

    //     if (option === 'MixEdit') {
    //       navigate(option, { objectId });
    //     } else if (option === 'onMixRemove') {
    //       onDestroyMix({ objectId });
    //     }
    //   }
    // )
  };

  get actions() {
    return {
      deleteMix: async (params) => {
        const destroy = await this.confirmRemove('mix');
        if (destroy) {
          await this.props.deleteMix(params);
          this.redirectTo('mixesList', { type: 'reset' });
        }
      },
    };
  }

  componentDidMount() {
    const { objectId } = this.props;
    const { deleteMix } = this.actionsWithHandle();
    this.refresh({
      renderRightButton: () => {
        return (
          <View style={{ flex: 1, flexDirection: 'row'}}>
            <NavIcon onPress={() => deleteMix({ objectId })} iconName="trash" />
            <NavIcon onPress={() => this.redirectTo('mixEdit', { objectId })} iconName="pencil" />
          </View>
        );
      }
    });
  }

  renderView(props) {
    return (
      <MixProfile {...props} />
    );
  }
}

export default compose(
  graphql(MIX_PROFILE, makeGet()),
  graphql(MIX_DELETE, makeDeleteMutation('deleteMix', 'mixes', 'UserMixes'))
)(MixProfileController);

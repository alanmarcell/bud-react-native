import React from 'react';
import { shallow } from 'enzyme';
import R from 'ramda';

import { gaga, stores } from '../../__mocks__/stateHelper';
import Controller from './MixProfileController';

test.skip('renders correctly', () => {
  const { key: objectKey } = R.head(stores.mixes.find());
  const props = {
    navigation: { state: { params: { objectKey }}}
  };
  const wrapper = shallow(<Controller {...props} />, { context: { gaga }}).dive().dive();
  expect(wrapper).toMatchSnapshot();
});

import MixesController from './MixesController';
import MixProfileController  from './MixProfileController';
import MixEditController  from './MixEditController';

export { MixProfileController, MixesController, MixEditController };

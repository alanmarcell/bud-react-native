import React from 'react';
import { shallow } from 'enzyme';
import R from 'ramda';

import { gaga, stores } from '../../__mocks__/stateHelper';
import Controller from './MixEditController';

describe('MixEditController', function () {
  test.skip('renders form to new resource', () => {
    const props = {
      navigation: { state: {} }
    };
    const context = { context: { gaga }};
    const wrapper = shallow(<Controller {...props} />, context).dive().dive();
    expect(wrapper).toMatchSnapshot();
  });

  test.skip('renders form to edit resource', () => {
    const { key: objectKey } = R.head(stores.mixes.find());
    const props = {
      navigation: { state: { params: { objectKey }}}
    };
    const context = { context: { gaga }};
    const wrapper = shallow(<Controller {...props} />, context).dive().dive();
    expect(wrapper).toMatchSnapshot();
  });
});

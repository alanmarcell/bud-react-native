import React from 'react';
import { Alert } from 'react-native';

import Contact from 'components/Contact';
import RootController from './RootController';
import { I18n } from 'utils/I18n';

import { graphql } from 'graphql';
import { mutationCreate } from 'graphql';
import { SEND_CONTACT } from 'graphql/contact';

class ContactController extends RootController {
  static navigationOptions = {
    title: 'screens.contact.title',
  };

  get actions() {
    return {
      onSendContact: async (input) => {
        const { sendContact } = this.props;
        const { data: { error } } = await sendContact({ variables: { input } });
        const activeUser = this.props.name.split('_').shift();

        const I18nKey = !error ? 'success' : 'error';
        Alert.alert(
          I18n.t(`screens.contact.feedback.${I18nKey}.title`),
          I18n.t(`screens.contact.feedback.${I18nKey}.message`),
          [
            { text: 'OK', onPress: () => {} },
          ],
          { cancelable: false }
        );

        if (activeUser === 'consumer') {
          this.redirectTo('budsTab');
        } else {
          this.redirectTo('plantsTab');
        }
      }
    };
  }

  renderView(props) {
    return <Contact {...props} />;
  }
}

export default graphql(SEND_CONTACT, mutationCreate('sendContact', 'Contact', 'contact', 'UserContact'))(ContactController);


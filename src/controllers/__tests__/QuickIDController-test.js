import React from 'react';
import { shallow } from 'enzyme';

import {tree, context } from '../../__mocks__/stateHelper';
import Controller from '../QuickIDController';

test.skip('renders correctly', () => {
  tree.set(['navigationState'], {
    key: 'Home',
    index: 0,
    routes: [
      { routeName: 'QuickIDTab' }
    ]
  });
  const wrapper = shallow(<Controller />, context).dive();
  expect(wrapper).toMatchSnapshot();
});

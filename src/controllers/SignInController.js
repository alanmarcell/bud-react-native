import React from 'react';
import {isNil} from 'ramda';
import SignIn from 'components/SignIn';
import RootController from './RootController';

import { graphql } from 'react-apollo';
import { LOGIN_QUERY } from 'graphql/user';
import { connect } from 'react-redux';
import { updateCredentials } from '../redux/actions';

class SignInController extends RootController {
  static navigationOptions = {
    title: 'screens.sign-in.title',
  };

  get actions() {
    return {
      onSignIn: async (credentials) => {
        const { data: { login } } = await this.props.signin({ variables: credentials });
        const { session } = login;

        if (__DEV__) {
          /* eslint-disable no-console */
          console.log(`token: ${login.session.token}`);
          console.log('session', login.session);
        }

        this.props.updateCredentials({
          session,
          token: session.token,
          email: credentials.email
        });
        this.redirectTo('logged', { type: 'reset' });
      },
      notBlock: {
        onPasswordRecovery: () => this.redirectTo('recoveryAccount'),
        onRegister: () => this.redirectTo('preRegister'),
      }
    };
  }

  componentWillMount(){
    if(!isNil(this.props.token)){
      this.redirectTo('logged', { type: 'reset' });
    }
  }

  renderView(props) {
    return <SignIn {...props} />;
  }
}

const mapStateToProps = state => ({
  email: state.user.email,
  token: state.user.token
});

const mapDispatchToProps = dispatch => ({
  updateCredentials: credentials => dispatch(updateCredentials(credentials))
});

export default graphql(LOGIN_QUERY, { name: 'signin' })(
  connect(mapStateToProps, mapDispatchToProps)(SignInController)
);

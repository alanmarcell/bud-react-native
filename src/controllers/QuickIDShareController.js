import React from 'react';

import RootController from './RootController';
import QuickIDShare from 'components/QuickID/Share';

export default class QuickIDShareController extends RootController {
  static navigationOptions = {
    title: 'screens.drawer.quickId',
  };

  get actions() {
    return {};
  }

  renderView(props) {
    return (
      <QuickIDShare {...props} />
    );
  }
}

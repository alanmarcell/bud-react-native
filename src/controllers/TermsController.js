import React from 'react';
import { View } from 'react-native';

import RootController from './RootController';
import TermsAndPrivacy from 'components/TermsAndPrivacy';

export default class TermsController extends RootController {
  static navigationOptions = {
    title: 'screens.terms.title',
  };

  get actions() {
    return {
      onIAgreePress: this.goBack,
    };
  }

  renderView(props) {
    return (
      <View style={{ flex: 1 }}>
        <TermsAndPrivacy contentKey="screens.terms.text" {...props} />
      </View>
    );
  }
}

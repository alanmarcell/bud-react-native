import React, { Component } from 'react';
import ModalLoading from './ModalLoading';

export default class ModalController extends Component {
  render() {
    return (
      <ModalLoading {...this.props} />
    );
  }
}

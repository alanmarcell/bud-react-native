import React from 'react';
import uuid from 'uuid';
import R from 'ramda';
import { I18n } from 'utils/I18n';
import BudEdit from './components/BudEdit';
import SelectStrainController from '../helpers/SelectStrainController';

import { graphql, compose, propType } from 'graphql';
import { makeGet, mutationCreate } from 'graphql';
import {
  budFragments,
  STRAIN_CREATE,
  PLANT_PROFILE,
  BUD_QUERY, BUD_CREATE, BUD_UPDATE,
  EDITION_CATEGORIES
} from 'graphql';

class BudEditController extends SelectStrainController {
  static propTypes = {
    bud: propType(budFragments.budBase),
    loading: React.PropTypes.bool,
  };

  static navigationOptions = {
    getTitle: ({ name }) => {
      const titleEdit = 'screens.budEdit.title';
      const titleAdd  = 'screens.budAdd.title';
      return I18n.T(/.*Add$/.test(name) ? titleAdd : titleEdit);
    }
  };

  get actions() {
    return {
      ...super.actions,
      saveBud: async ({strain, ...bud}) => {
        // Create strain if necessary
        bud.strainId = await this.saveStrain(strain);

        let { objectId, updateBud, createBud, editionId, plantId } = this.props;

        if(R.isNil(bud.editionId) && !R.isNil(this.props.editionId)){
          bud.editionId = editionId;
        }

        if (objectId) {
          await updateBud({ variables: { id: objectId, input: bud }});
          this.goBack();
        } else {
          objectId = uuid.v4();
          bud.id = objectId;
          
          await createBud({ variables: { input: bud }});

          if (plantId || editionId) {
            this.goBack();
          } else {
            this.redirectTo('budProfile', { type: 'replace', objectId });
          }
        }
      }
    };
  }

  renderView(props) {
    const { name } = this.props;
    const submitEdit = 'screens.budEdit.submit';
    const submitAdd  = 'screens.budAdd.submit';
    const submitText = I18n.t(/.*Add$/.test(name) ? submitAdd : submitEdit);

    return (
      <BudEdit {...props}  submitText={submitText} />
    );
  }
}

const refetchQueries = [
  'UserBuds',
  'BudQuery',
  'EditionProfile',
  'CategoryProfile'
];

export default compose(
  graphql(BUD_QUERY, makeGet()),
  graphql(PLANT_PROFILE, makeGet('plantId')),
  graphql(EDITION_CATEGORIES, makeGet('editionId')),
  graphql(BUD_UPDATE, { name: 'updateBud', options: {refetchQueries} }),
  graphql(BUD_CREATE, mutationCreate('createBud', 'Bud', 'buds', 'UserBuds', refetchQueries)),
  graphql(STRAIN_CREATE, { name: 'createStrain' })
)(BudEditController);

// @flow
import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  reviews: {
    backgroundColor: '#E1E1E1',
    marginTop: 15,
    paddingTop: 30,
  },

  reviewRateTitleWrapper: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },

  reviewRateTitle: {
    color: '$Colors.Gray',
    fontWeight: 'bold',
    fontSize: 11,
  },

  reviewTitleIcon: {
    marginRight: 5,
    color: '$Colors.Gray',
  },

  reviewRateWrapper: {
    padding: 5,
  }

});

import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';
import R from 'ramda';

import BudProfile from '../';
import { tree, context } from '../../../../../__mocks__/stateHelper';

describe('BudProfile', () => {
  test('renders correctly', () => {
    const [[, bud]] = R.toPairs(tree.get('buds', 'list'));
    const component = shallow(<BudProfile bud={bud} />).dive(context);
    expect(component).toMatchSnapshot();
  });
});

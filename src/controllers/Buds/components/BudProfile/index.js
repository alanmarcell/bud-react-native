import React, { Component } from 'react';
import {
  ActivityIndicator,
  ScrollView,
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import R from 'ramda';
import moment from 'moment';
import { I18n } from 'utils/I18n';

import Button from 'components/Button';
import { BudHeader } from 'components';
import BlankList from 'components/BlankList';
import FeatureCard from 'components/Reviews/FeatureCard';
import UserReviewCard from 'components/Reviews/ReviewCard';
import Icon from 'components/Icon';

import Refreshable from 'utils/components/Refreshable';

import ReviewDictionary from 'components/Reviews/ReviewEdit/ReviewDictionary';

import styles from './styles';

const ReviewCountDown = props => {
  const startAt = R.path(['bud', 'edition', 'startAt'], props);
  const endAt = R.path(['bud', 'edition', 'endAt'], props);

  if (R.isNil(startAt)) {
    return null;
  }

  const containerStyle = {
    flex: 1
  };

  const msgStyle = {
    textAlign: 'center'
  };

  const dateStyle = {
    textAlign: 'center'
  };

  if (moment().isBefore(startAt)) {
    return (
      <View style={containerStyle}>
        <Text style={msgStyle}>{I18n.t('reviews.startAt')}</Text>
        <Text style={dateStyle}>{moment(startAt).format('lll')}</Text>
      </View>
    );
  }

  if (moment().isBefore(endAt)) {
    return (
      <View style={containerStyle}>
        <Text style={msgStyle}>{I18n.t('reviews.endAt')}</Text>
        <Text style={dateStyle}>{moment(endAt).format('lll')}</Text>
      </View>
    );
  }

  return (
    <View style={containerStyle}>
      <Text style={msgStyle}>{I18n.t('reviews.endedAt')}</Text>
      <Text style={dateStyle}>{moment(endAt).format('lll')}</Text>
    </View>
  );
};


class BudProfile extends Component {
  static propTypes = {
    bud: React.PropTypes.object,
    loading: React.PropTypes.bool,
    openQuickID: React.PropTypes.func.isRequired,
  }

  render() {
    const {
      authedUserId,
      loading,
      bud,
      refetch,
    } = this.props;

    if (loading) {
      return <ActivityIndicator />;
    }

    if (!bud) {
      return null;
    }
    const otherUserBud = authedUserId !== bud.owner.id;

    return (
      <ScrollView refreshControl={Refreshable({ loading, refetch })}>
        <BudHeader {...this.props} />
        {otherUserBud ? this.renderOthersBudWarning() : null}
        {this.renderReviewsAndRatings()}
      </ScrollView>
    );
  }

  renderOthersBudWarning() {
    if (this.props.addReviewEnabled) {
      return (
        <View style={{ padding: 15, alignItems: 'center' }}>
          <Text style={{ fontSize: 12 }}>{I18n.t('screens.bud.guest.welcome')}</Text>
          <Text style={{ fontSize: 12 }}>{I18n.t('screens.bud.guest.message')}</Text>
          <Button theme="green" type="small" onPress={() => this.props.addReviewButton()}>
            {I18n.t('screens.bud.guest.add_review')}
          </Button>
        </View>
      );
    }
    else {
      return (
        <View style={{ padding: 15, alignItems: 'center' }}>
          <Text style={{ fontSize: 12 }}>{I18n.t('screens.bud.guest.welcome')}</Text>
        </View>
      );
    }
  }

  renderReviewsAndRatings() {
    const { reviews: { edges: reviews } } = this.props.bud;

    if (R.isEmpty(reviews)) {
      return (
        <View>
          <BlankList
            icon="mixes"
            title={I18n.t('reviews.blank.title')}
            message={I18n.t('reviews.blank.message')}
          />
          <View style={{marginTop: -50, marginBottom: 30}}>
            <ReviewCountDown {...this.props} />
          </View>
        </View>
      );
    }

    return (
      <View>
        <View style={{ marginTop: 20 }}>
          {this.renderFeaturesCards()}
        </View>

        <View style={styles.reviews}>
          <View style={styles.reviewRateTitleWrapper}>
            <Icon name="star-o" style={styles.reviewRateTitleIcon} />
            <Text style={styles.reviewRateTitle}>AVALIAÇÕES</Text>
          </View>
          <ReviewCountDown {...this.props} />
          <View style={styles.reviewRateWrapper}>
            {this.renderReviewsCards()}
          </View>
        </View>
      </View>
    );
  }

  renderReviewsCards() {
    const { bud, openReview } = this.props;
    const { reviews: { edges: reviews } } = bud;

    // TODO: paging
    // reviews.edges[].node.id|ratings[].score|tags
    return R.map((review) => {
      const { node: { id, score, reviewedBy, ratings, consumption, createdAt } } = review;
      return (
        <TouchableOpacity onPress={() => openReview(id)} key={`review.${id}`}>
          <UserReviewCard
            key={`review.${id}`}
            type="review"
            user={reviewedBy}
            score={score}
            consumption={consumption}
            created_at={new Date(createdAt)}
            review={ratings} />
        </TouchableOpacity>
      );
    }, reviews);
  }


  renderFeaturesCards() {
    const { bud } = this.props;
    const { reviewResumed: { ratings } } = bud;

    let output = [];

    const ReviewDictionaryKeys = Object.keys(ReviewDictionary);
    R.map((rating) => {
      const index = ReviewDictionaryKeys.indexOf(rating.feature.name);
      output[index] = rating;
    }, ratings);

    // clean undefined items
    const cleanRatings = R.filter(f => !!f, output);

    return R.map(rating => {
      const { tags, tone, score, feature: { name } } = rating;
      const type = tone ? 'slider' : 'tag';
      const title = I18n.t(['reviews', 'ratings', name, 'label']);
      return (
        <FeatureCard
          key={`${name}.${score}`}
          fieldName={name}
          title={title}
          type={type}
          rating={score}
          tags={tags}
          tone={tone}
        />);
    }, cleanRatings);
  }
}

export default BudProfile;

import React, { Component, PropTypes } from 'react';
import { View, Text } from 'react-native';
import moment from 'moment';
import R from 'ramda';

import SelectableCard     from 'components/SelectableCard';
import RoundedIcon        from 'components/RoundedIcon';
import PlantName          from 'controllers/Plants/components/PlantName';
import Icon from 'components/Icon';

import { Moment } from 'utils/components';

import styles from './styles';
import stylesHelpers from '../../../../themes/helpers';

export default class BudCardItem extends Component {
  static defaultProps = {
    ...SelectableCard.defaultProps,
    strain: {},
  };

  static propTypes = {
    ...SelectableCard.propTypes,
    name  : PropTypes.string.isRequired,
    strain: PropTypes.object,
    score: PropTypes.number,
    harvestedAt: PropTypes.oneOfType([
      React.PropTypes.instanceOf(Date),
      React.PropTypes.instanceOf(moment),
    ]),
  };

  render() {
    const { name, harvestedAt, score } = this.props;
    const strain = R.pathOr('', ['strain', 'name'], this.props);
    const roundedScore = score ? score.toFixed(2) : 0;
    return (
      <SelectableCard {...this.props}>
        <View style={[stylesHelpers.leftColumn, stylesHelpers.alignCenter]}>
          <RoundedIcon object={this.props} source={require('./images/icon-bud.png')}/>
        </View>
        <View style={[stylesHelpers.middleColumn, stylesHelpers.alignCenterLeft]}>
          <PlantName name={name} />
          <Text style={styles.plantType}>{strain}</Text>
        </View>
        <View style={[stylesHelpers.rightColumn, stylesHelpers.alignCenterRight]}>
          <View style={styles.row}>
            <Icon style={styles.score} name="trophy" />
            <Text style={styles.score}>{roundedScore}</Text>
          </View>
          <Moment date={harvestedAt} action="format" args={['L']} style={styles.date} />
        </View>
      </SelectableCard>
    );
  }
}

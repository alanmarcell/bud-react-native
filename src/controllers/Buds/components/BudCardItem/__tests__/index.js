import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';
import R from 'ramda';

import BudCardItem from '../';
import { tree, context } from '../../../../../__mocks__/stateHelper';

test('renders correctly', () => {
  const [[, bud]] = R.toPairs(tree.get('buds', 'list'));
  const wrapper = shallow(<BudCardItem {...bud} />, context);
  expect(wrapper).toMatchSnapshot();
});

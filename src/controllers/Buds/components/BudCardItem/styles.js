// @flow
import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  budType: {
    color: '$Colors.Gray',
    fontSize: 13
  },
  date: {
    fontSize: 11,
    color: '#95989A',
    top: 10,
  },
  score: {
    color: '$Colors.Gray',
    fontSize: 18,
    marginLeft: 5,
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  }
});

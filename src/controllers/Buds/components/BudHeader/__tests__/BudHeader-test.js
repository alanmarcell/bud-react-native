import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';
import R from 'ramda';

import BudHeader from '../';
import { tree, context } from '../../../../../__mocks__/stateHelper';

describe('BudHeader', () => {
  test.skip('renders correctly', () => {
    const [[, bud]] = R.toPairs(tree.get('buds', 'list'));
    const component = shallow(<BudHeader {...bud} />, context);
    expect(component).toMatchSnapshot();
  });
});

import React, { PureComponent, PropTypes } from 'react';
import { View, Text } from 'react-native';
import moment from 'moment';
import { I18n } from 'utils/I18n';
import R from 'ramda';

import v4 from 'uuid/v4';
import {ProductHeader} from 'components';
import GaleryPreview from 'components/GaleryPreview';

import Icon from 'components/Icon';

import styles from './styles';

const profileMap = {
  seed: 'seed',
  clone: 'clone',
  batch: 'group',
  vegetative: 'vegetating',
  flowering: 'flowering',
};
const renderItemIcon = (item) => {
  if (item) {
    item = item.toLowerCase();
    return profileMap[item];
  }
  return 'budbuds';
};

const isEven = (n) => n % 2 === 0;

const renderInfoList = (bud) => {
  const { harvestedAt, score } = bud;
  const mother = R.pathOr(null, ['mother', 'name'], bud);
  const genetic = R.pathOr(null, ['mother', 'genetic'], bud);
  const source = R.pathOr(null, ['mother', 'source'], bud);
  const environment = R.pathOr(null, ['mother', 'environment'], bud);
  const stage = R.pathOr(null, ['mother', 'stage'], bud);

  const roundedScore = score ? score.toFixed(2) : 0;
  const date = new Date();
  let week = moment().diff(date, 'weeks') + 1;

  const { countReviews } = bud.reviewResumed;

  const rows = [{
    leftIcon: 'budbuds',
    leftText: mother,
    rightIcon: 'genetics',
    rightText: I18n.t('forms.genetic.' + genetic)
  }, {
    leftIcon: renderItemIcon(source),
    leftText: I18n.t('forms.source.' + source),
    rightIcon: 'location',
    rightText: I18n.t('forms.environment.' + environment)
  }, {
    leftIcon: renderItemIcon(stage),
    leftText: I18n.t('forms.stage.' + stage),
    rightIcon: 'calendar',
    rightText: I18n.t('screens.plant.week', { week })
  }, {
    leftIcon: 'star',
    leftText: `${countReviews} ${I18n.t('screens.reviews.my_review')}`,
    rightIcon: 'trophy',
    rightText: roundedScore
  }, {
    leftIcon: 'shopping-basket',
    leftText: moment(harvestedAt).format('L')
  }];


  const renderRows = () => rows.map((row, index) =>
    (<View key={v4()} style={[styles.infoListRow, isEven(index) && styles.infoListRowGrayBackground]}>
      <View style={styles.infoListLeftCol}>
        <Icon style={styles.infoIcon} name={row.leftIcon} />
        <Text style={styles.infoLabel}>{row.leftText}</Text>
      </View>
      {row.rightIcon &&
        <View style={styles.infoListRightCol}>
          <Icon style={styles.infoIcon} name={row.rightIcon} />
          <Text style={styles.infoLabel}>{row.rightText}</Text>
        </View>}
    </View>
    )
  );

  return (
    <View style={styles.infoList}>
      {renderRows()}
    </View>
  );
};

export default class BudHeader extends PureComponent {
  static propTypes = {
    bud: PropTypes.object.isRequired,
    openQuickID: PropTypes.func.isRequired,
    onToggleExecute: PropTypes.func,
    style: PropTypes.oneOfType([
      PropTypes.shape(),
      PropTypes.number,
    ]),
  }

  render() {
    const { bud: { files }, openGalery, bud } = this.props;

    const showGalery = openGalery && files && files.length !== 0;

    return (
      <View style={styles.container}>
        <ProductHeader {...this.props} productType="bud" />
        {renderInfoList(bud)}
        {showGalery && <GaleryPreview files={files} openGalery={openGalery} />}
      </View>
    );
  }
}

import React, { Component } from 'react';
import { View, FlatList, ActivityIndicator } from 'react-native';

import { I18n } from 'utils/I18n';
import U from 'utils';

import Refreshable from 'utils/components/Refreshable';

import BlankList    from 'components/BlankList';
import BudCardItem  from '../BudCardItem';

import TabView from 'components/TabView';

import styles from './styles';

class BudTabView extends TabView{

  _handleIndexChange = index => {
    const {toogleShowActionButton } = this.props;
    toogleShowActionButton(index);
    this.setState({ index });};
}

export default class BudsList extends Component {
  static defaultProps = {
    buds: [],
  };

  static propTypes = {
    buds: React.PropTypes.array,
  };

  onBudPress = (budId) => {
    if (this.props.onBudPress) {
      this.props.onBudPress(budId);
    }
  };

  renderItem = ({ item, index }) => {
    let listLength = this.props.buds.length;
    return (
     <BudCardItem
        {...item}
        onPress={() => this.onBudPress(item.id)}
        listLength={listLength}
        listIndex={parseInt(index)}
      />
    );
  };

  renderFooter = () => {
    const { loading } = this.props;
    return (
      <View style={styles.footer}>
        {loading ? <ActivityIndicator /> : null}
      </View>
    );
  };

  renderHeaderFavs = () => {
    const { budsReviewed } = this.props;
    if (budsReviewed.length <= 0) {
      return (
        <BlankList
          icon="bud"
          title={I18n.t('buds.reviews.blank.title')}
          message={I18n.t('buds.reviews.blank.message')}
        />
      );
    } else {
      return null;
    }
  }

  renderHeader = () => {
    const { buds } = this.props;
    if (buds.length <= 0) {

      return (
        <BlankList
          icon="bud"
          title={I18n.t('buds.blank.title')}
          message={I18n.t('buds.blank.message')}
        />
      );
    } else {
      return null;
    }
  };

  loadMore = () => {
    const { loading, loadMoreEntries, hasNextPage } = this.props.g_buds;
    if (!loading && hasNextPage) {
      loadMoreEntries();
    }
  }

  loadMoreFavs = () => {
    const { loading, loadMoreEntries, hasNextPage } = this.props.g_budsReviewed;
    if (!loading && hasNextPage) {
      loadMoreEntries();
    }
  }

  render() {
    const {
      buds,
      g_buds,
      budsReviewed,
      g_budsReviewed,
      toogleShowActionButton
    } = this.props;

    const budsRefresh = { loading: g_buds.loading, refetch: g_buds.refetch };
    const reviewedRefresh = {
      loading: g_budsReviewed.loading,
      refetch: g_budsReviewed.refetch
    };

    return (
      <BudTabView toogleShowActionButton={toogleShowActionButton}
        titles={[I18n.t('screens.buds.tabs.my_buds'), I18n.t('screens.buds.tabs.reviewed_buds')]}
      >
        <FlatList
          data={U.sortScore(buds)}
          refreshing={g_buds.loading}
          refreshControl={Refreshable(budsRefresh)}
          keyExtractor={(item, index) => index}
          renderItem={this.renderItem}
          ListHeaderComponent={this.renderHeader}
          ListFooterComponent={this.renderFooter}
          onEndReached={this.loadMore}
        />
        <FlatList
          data={U.sortScore(budsReviewed)}
          refreshing={g_budsReviewed.loading}
          refreshControl={Refreshable(reviewedRefresh)}
          keyExtractor={(item, index) => index}
          renderItem={this.renderItem}
          ListHeaderComponent={this.renderHeaderFavs}
          ListFooterComponent={this.renderFooter}
          onEndReached={this.loadMoreFavs}
        />
      </BudTabView>
    );
  }
}

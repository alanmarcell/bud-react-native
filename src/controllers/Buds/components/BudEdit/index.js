import React from 'react';
import { ActivityIndicator } from 'react-native';
import U from 'utils';
import { I18n } from 'utils/I18n';
import { BudForms } from 'components/BudForms';
import { I18nField, DescriptionField, NameField, DateField } from 'components/BudForms/Fields';
import { SearchSelectFactory } from 'utils/Form/Factories';
import QRCodeFactory from 'components/BudForms/Factories/QRCodeFactory';
import StrainSelect from 'components/StrainSelect';
import { connect } from 'react-redux';

import { AutoCheckboxFactory } from 'components/BudForms/Factories';
// import styles from './styles';
import { BudSchema } from './BudSchema';

const getCategoriesOptions = U.map(({id, name}) => ({
  label: name,
  value: id
}));

class Categories extends React.PureComponent {
  render() {
    const categories = U.path(['edition', 'categories', 'nodes'], this.props);

    if(U.isNil(categories)){
      return null;
    }

    return (
      <I18nField
        fieldName="categoryIds"
        i18nScope="forms.categories"
        type={AutoCheckboxFactory}
        options={getCategoriesOptions(categories)}
      />
    );
  }
}

class BudEdit extends StrainSelect {
  static propTypes = {
    bud: React.PropTypes.object,
    loading: React.PropTypes.bool,
    saveBud: React.PropTypes.func.isRequired,
    submitText: React.PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    this._buildDocState = this._buildDocState.bind(this);

    this.state = {
      doc: this._buildDocState(props.bud)
    };
  }

  _handleSubmit = ({ strain, mother, ...values }) => {
    const { saveBud } = this.props;
    saveBud({
      ...values,
      strain,
      motherId: mother ? mother.index : null,
    });
  };

  _handleChange = (doc) => {
    doc = this.handlePlantWithStrain(doc);
    this.setState({ doc });
  }

  handlePlantWithStrain(doc) {
    if (!doc.strain && doc.mother && doc.mother.strain) {
      doc.strain = {
        index: doc.mother.strain.id,
        label: doc.mother.strain.name,
        breeder: doc.mother.strain.breeder,
        type: 'STRAIN'
      };
    }

    doc.mother && delete doc.mother.strain;

    return doc;
  }

  _buildDocState(bud = {}) {
    const { qrCode, harvestedAt, ...doc } = bud;
    const { plant } = this.props;

    doc.qrCode = qrCode ? qrCode.code : null;
    doc.harvestedAt = harvestedAt ? new Date(harvestedAt) : new Date();

    if (plant) {
      doc.mother = {
        index: plant.id,
        label: plant.name,
        type: 'PLANT'
      };

      doc.strain = {
        index: plant.strain.id,
        label: plant.strain.name,
        breeder: plant.strain.breeder,
        type: 'STRAIN'
      };
    } else {
      doc.mother = this.serializeRelation(doc, 'mother');
      doc.strain = this.serializeRelation(doc, 'strain');
    }

    if(U.isNil(doc.categoryIds) && !U.isEmpty(doc.availableCategories || [])){
      doc.categoryIds = U.map(c => c.id, doc.availableCategories);
    }

    return doc;
  }

  componentWillReceiveProps(nextProps, _nextState) {
    const { bud } = nextProps;

    this.setState({ doc: this._buildDocState(bud) });
  }

  render() {
    const { loading, findQrCode } = this.props;

    const { doc } = this.state;

    if (loading) {
      return <ActivityIndicator />;
    }

    const formProps = {
      ref: 'form',
      schema: BudSchema,
      doc: U.pick([
        'name',
        'description',
        'mother',
        'strain',
        'qrCode',
        'harvestedAt',
        'categoryIds'
      ], doc),
      onChange: this._handleChange,
      onSubmit: this._handleSubmit,
      submitText: this.props.submitText,
    };

    return (
      <BudForms {...formProps} >
        <NameField i18nScope="forms.budName" />
        {this.props.activeUserType !== 'CONSUMER' &&
          <I18nField
            fieldName="mother"
            i18nScope="forms.plantMother"
            placeholder={true}
            formatText={this.formatStrain}
            type={SearchSelectFactory}
            searchFn={this.searchParent}
          />
        }
        <I18nField
          fieldName="strain"
          i18nScope="forms.strain"
          placeholder={true}
          formatText={this.formatStrain}
          type={SearchSelectFactory}
          searchFn={this.searchStrain}
        />
        <DescriptionField />
        {this.props.activeUserType !== 'CONSUMER' &&
          <DateField fieldName="harvestedAt" />
        }
        <I18nField
          fieldName="qrCode"
          type={QRCodeFactory}
          findQrCode={findQrCode}
          newText={I18n.t('forms.qrCode.newText')}
          oldText={I18n.t('forms.qrCode.oldText')}
        />
        <Categories {...this.props} />
      </BudForms>
    );
  }
}

const mapStateToProps = state => ({
  activeUserType: state.user.userType
});

export default connect(mapStateToProps)(BudEdit);

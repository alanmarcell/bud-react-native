import { SimpleSchema } from 'components/BudForms/Schemas';
import { NameSchema } from 'components/BudForms/Schemas';

export const BudSchema = new SimpleSchema({
  name           : {
    type: NameSchema,
    required: true,
  },

  description    : {
    type: String,
    required: false,
  },

  harvestedAt    : {
    type: Date,
    required: true,
  },

  'mother': {
    type: 'Object',
    required: false,
  },
  'mother.index': String,
  'mother.label': String,
  'mother.type' : String,

  strain: {
    type: 'Object',
    required: true,
  },
  'strain.index': {
    type: String,
    required: false
  }, 
  'strain.label': {
    type: String,
    required: false
  },
  'strain.breeder':{
    type: String,
    required: false,
  },
  'strain.type' : {
    type: String,
    required: false,
  },

  qrCode: {
    type: String,
    required: false,
  },

  categoryIds: {
    type: Array,
    required: false
  },

  'categoryIds.$': {
    type: String,
    required: true
  }

});

import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';

import U from 'utils';
import BudEdit from '../';
import { tree } from '../../../../../__mocks__/stateHelper';

describe('BudEdit', () => {
  let defaultProps, list, plants;

  beforeAll(() => {
    list   = tree.get('plants', 'list');
    plants = U.map(U.last, U.toPairs(list));

    defaultProps = {
      plants,
      submitText: 'save',
      onSaveBud: () => {},
    };
  });

  test.skip('renders correctly for edit bud', () => {
    const [[, bud]] = U.toPairs(tree.get('buds', 'list'));
    const props = {
      ...defaultProps,
      bud: bud,
    };
    const wrapper = shallow(<BudEdit {...props} />).dive();
    expect(wrapper).toMatchSnapshot();
  });

  test.skip('renders correctly for new bud', () => {
    const props = {
      ...defaultProps,
      bud: tree.get('buds', 'newBud'),
    };
    const wrapper = shallow(<BudEdit {...props} />).dive({
      form: {},
      onChange: () => {},
    });
    expect(wrapper).toMatchSnapshot();
  });
});

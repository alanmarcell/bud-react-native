// @flow
import React from 'react';
import { View } from 'react-native';

import RootController from '../RootController';
import ActionMenu from 'components/ActionMenu';
import BudsList from './components/BudsList';
import {connect} from 'react-redux';

import { graphql, makeLoadList, compose } from 'graphql';
import { BUD_LIST, BUDS_REVIEWED } from 'graphql/buds';

class BudsController extends RootController {
  static navigationOptions = {
    title: 'screens.buds.title',
  };

  constructor(props) {
    super(props);
    this.state = {
      showActionMenu: 0
    };
  }
  get actions() {
    return {
      onBudPress: (objectId) => {
        this.redirectTo('budProfile', { objectId });
      }
    };
  }

  onActionPress = () => {
    this.redirectTo('budAdd');
  }

  toogleShowActionButton = (args) => {
    if (this.props.activeUserType === 'CONSUMER')
      this.setState({ showActionMenu: args });
  }

  renderView(props) {
    const menuItems = {
      newBud: 'bud',
    };

    return (
      <View style={{ flex: 1 }}>
        <BudsList
          {...props}
          toogleShowActionButton={this.toogleShowActionButton}
          activeUserType={this.activeUserType} />
        {!this.state.showActionMenu ? <ActionMenu onPress={this.onActionPress} items={menuItems} /> : null}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  activeUserType: state.user.userType
});

export default compose(
  graphql(BUD_LIST, makeLoadList(BUD_LIST, 'buds')),
  graphql(BUDS_REVIEWED, makeLoadList(BUD_LIST, 'budsReviewed'))
)(
  connect(mapStateToProps)(BudsController)
);

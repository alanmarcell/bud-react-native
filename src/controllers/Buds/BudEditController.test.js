import React from 'react';
import { shallow } from 'enzyme';

import U from 'utils';
import { tree, context } from '../../__mocks__/stateHelper';
import Controller from './BudEditController';

describe.skip('BudEditController', function () {
  test('renders form to new resource', () => {
    const props = {
      navigation: { state: {} },
    };
    const wrapper = shallow(<Controller {...props} />, context).dive().dive();
    wrapper.dive(context);
    expect(wrapper).toMatchSnapshot();
  });

  test('renders form to edit resource', () => {
    const [[budId]] = U.toPairs(tree.get('buds', 'list'));
    const props = {
      navigation: { state: { params: { budId } } },
    };

    const wrapper = shallow(<Controller {...props} />, context).dive().dive();
    expect(wrapper).toMatchSnapshot();
  });
});

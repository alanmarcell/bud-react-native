// @flow
import { find } from 'lodash';
import React from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import R from 'ramda';
import NavIcon from 'scenes/NavIcon';
import BudProfile from './components/BudProfile';
import RootController from '../RootController';
import ActionMenu from 'components/ActionMenu';
import moment from 'moment';
import withCategoriesToVote from 'controllers/helpers/withCategoriesToVote';
import { propType, compose, graphql } from 'graphql';
import { makeDeleteMutation, makeGet } from 'graphql';
import {
  budFragments,
  SESSION_QUERY,
  BUD_REVIEW_PROFILE, BUD_DELETE, BUD_IMAGE_UPLOAD
} from 'graphql';

const getJuries = categories => R.pipe(
  R.map(c => c.jury),
  R.unnest,
  R.uniq
)(categories);

const getJudgesIds = categories => R.pipe(
  R.map(c => c.judges),
  R.unnest,
  R.map(j => j.id)
)(categories);



const isAddReviewEnable = (props) => {
  const { authedUserId, bud, categoriesToVote } = props;

  if (R.isNil(bud)) {
    return false;
  }

  if (R.isNil(bud.edition)) {
    return true;
  }

  const {startAt, endAt} = bud.edition;

  if(!moment().isBetween(startAt, endAt)){
    return false;
  }

  if(R.isEmpty(categoriesToVote)){
    return false;
  }

  const juries = getJuries(categoriesToVote);

  if (R.contains('PUBLIC', juries)) {
    return true;
  }

  const judgesIds = getJudgesIds(categoriesToVote);

  return R.contains(authedUserId, judgesIds);
};

const BudProfileContainer = withCategoriesToVote(props => {
  const addReviewEnabled = isAddReviewEnable(props);
  const menuItems = addReviewEnabled
    ? {
      newReview: 'star',
      newPhoto: 'camera',
    }
    : {
      newPhoto: 'camera'
    };

  return (
    <View style={{ flex: 1 }}>
      <BudProfile {...props} addReviewEnabled={addReviewEnabled} />
      <ActionMenu onPress={props.onActionPress} items={menuItems} />
    </View>
  );
});

class BudProfileController extends RootController {
  static propTypes = {
    bud: propType(budFragments.budBase),
    loading: React.PropTypes.bool,
  }

  static navigationOptions = {
    title: 'screens.bud.title',
  };

  state = {
    myBud: false,
  };

  get actions() {
    return {
      deleteBud: async (params) => {
        const destroy = await this.confirmRemove('bud');
        if (destroy) {
          await this.props.deleteBud(params);
          this.redirectTo('budsList', { type: 'reset' });
        }
      },
      openQuickID: () => {
        const { bud } = this.props;
        const { name, qrCode: { code: qrcode } } = bud;

        this.redirectTo('budQrCode', {
          icon: 'buds',
          label: `Bud: ${name}`,
          qrcode,
        });
      },
      openReview: (id) => {
        const { bud: { reviews: { edges: reviews } } } = this.props;
        const review = find(reviews, { node: { id } });

        this.redirectTo('reviewDetails', { review: review.node });
      },
      openGalery: (selectedId) => {
        const { bud: { files } } = this.props;

        this.redirectTo('budGalery', {
          files,
          selectedFile: files.find((image) => image.id === selectedId)
        });
      },
      notBlock: {
        uploadFiles: (id, photos) => {
          return this.props.addImagesToBud({ variables: { id, photos } });
        },

        addReviewButton: () => {
          const { objectId } = this.props;
          this.redirectTo('reviewAdd', { objectId });
        }
      }
    };
  }

  componentWillUpdate(nextProps) {
    const { loading, setButtons } = nextProps;
    if (!loading && !setButtons && nextProps.bud) {
      const { authedUserId } = nextProps;
      const { bud: { owner: { id: budOwnerId } } } = nextProps;
      if (authedUserId === budOwnerId) {
        const { objectId } = nextProps;
        const { deleteBud } = this.actionsWithHandle();
        this.refresh({
          setButtons: true,
          renderRightButton: () => {
            return (
              <View style={{ flex: 1, flexDirection: 'row' }}>
                <NavIcon onPress={() => deleteBud({ objectId })} iconName="trash" />
                <NavIcon onPress={() => {
                  this.redirectTo('budEdit', { objectId, editionId: R.path(['bud', 'edition', 'id'], nextProps) });
                }} iconName="pencil" />
              </View>
            );
          }
        });
      }
    }
  }

  onActionPress = (option) => {
    if (option === 'newReview') {
      const { objectId } = this.props;
      this.redirectTo('reviewAdd', { objectId });
    } else if (option === 'newPhoto') {
      const { uploadFiles } = this.actionsWithHandle();
      const { objectId } = this.props;
      this.redirectTo('budPhotoAdd', { id: objectId, uploadFiles });
    }
  }

  renderView(props) {
    return (
      <BudProfileContainer 
        {...props}
        onActionPress={this.onActionPress}
      />
    );
  }
}

const mapStateToProps = state => ({
  authedUserId: state.user.session.user.id,
});

export default compose(
  graphql(SESSION_QUERY, { props: ({ data }) => ({ ...data }) }), // retrieve user
  graphql(BUD_REVIEW_PROFILE, makeGet()),
  graphql(BUD_IMAGE_UPLOAD, {
    name: 'addImagesToBud',
    options: {
      refetchQueries: [
        'BudProfile'
      ]
    }
  }),
  graphql(BUD_DELETE, makeDeleteMutation('deleteBud', 'buds', 'UserBuds'))
)(connect(mapStateToProps)(BudProfileController));

import React from 'react';
import { shallow } from 'enzyme';
import R from 'ramda';

import { tree, context } from '../../__mocks__/stateHelper';
import Controller from './BudProfileController';

test.skip('renders correctly', () => {
  const [[budId]] = R.toPairs(tree.get('buds', 'list'));
  const props = {
    navigation: { state: { params: { budId }}}
  };
  const wrapper = shallow(<Controller {...props} />, context).dive().dive();
  expect(wrapper).toMatchSnapshot();
});

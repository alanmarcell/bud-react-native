import React from 'react';
import { View } from 'react-native';

import RootController from './RootController';
import UserFeatures from 'components/UserFeatures';

export default class UserFeaturesController extends RootController {
  static navigationOptions = {
    title: 'screens.register.title',
  };

  get actions() {
    return {
      notBlock: {
        onRegister: () => this.redirectTo('register')
      }
    };
  }

  renderView(props) {

    return (
      <View style={{ flex: 1 }}>
        <UserFeatures contentKey="screens.register.features" {...props} />
      </View>
    );
  }
}


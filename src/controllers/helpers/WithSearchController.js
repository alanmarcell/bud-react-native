import PropTypes from 'prop-types';
import RootController from '../RootController';
import R from 'ramda';

export default class WithSearchController extends RootController {
  static contextTypes = {
    client: PropTypes.object.isRequired,
  };

  searchMap(node, term, _type) {
    return {
      term,
      string: node.name,
      original: {
        index: node.id,
        label: node.name,
      }
    };
  }

  async search(query, term, type, cursor = null) {
    const { client } = this.context;
    const { data } = await client.query({
      query,
      fetchPolicy: 'network-only',
      variables: { term, cursor },
    });

    let results = { data: [] };
    if (data) {
      const { [type]: { edges, pageInfo }} = data;
      results = {
        hasNextPage: pageInfo.hasNextPage,
        loadMoreEntries: async () => {
          const newResults = await this.search(query, term, type, pageInfo.endCursor);
          newResults.data = [...results.data, ...newResults.data];
          return newResults;
        },
        data: R.reject(R.isNil, edges.map(({ node: node }) => this.searchMap(node, term, type))),
      };
    }

    return results;
  }
}

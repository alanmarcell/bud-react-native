import React from 'react';
import R from 'ramda';

const getCategoriesToVote = (props) => {
  const { authedUserId, bud } = props;

  if(R.isNil(bud)){
    return [];
  }

  const reviews = R.path(['reviews', 'edges'], bud) || [];

  return R.reduce((catsToVote, edge) => {
    return edge.node.reviewedBy.id === authedUserId
      ? R.filter(cat => R.path(['node', 'category', 'id'], edge) !== cat.id, catsToVote)
      : catsToVote;
  }, R.path(['availableCategories'], bud) || [], reviews);
};

const withCategoriesToVote = Comp => props => (
  <Comp 
    {...props}
    categoriesToVote={getCategoriesToVote(props)}
  />
);

export default withCategoriesToVote;

export {
  getCategoriesToVote,
  withCategoriesToVote
};

import React from 'react';
import uuid from 'uuid';

import RootController    from '../RootController';
import { QRCODE_SEARCH } from 'graphql/qrcode';
import { STRAIN_SEARCH } from 'graphql/strains';
import { PLANT_SEARCH }  from 'graphql/plants';

export default class SelectStrainController extends RootController {
  static contextTypes = {
    client: React.PropTypes.object.isRequired,
  };

  findQrCode = async (qrCode) => {
    const { client } = this.context;
    const { data, error } = await client.query({
      query: QRCODE_SEARCH, variables: { qrCode }
    });
    return error ? { error } : data.qrCode;
  };

  searchParent = async (query, type, cursor = null) => {
    const { client } = this.context;
    const { data } = await client.query({
      query: type === 'strains' ? STRAIN_SEARCH : PLANT_SEARCH,
      fetchPolicy: 'network-only',
      variables: { query, cursor },
    });

    let results = { data: [] };
    if (data) {
      const { [type]: { edges, pageInfo }} = data;
      results = {
        hasNextPage: pageInfo.hasNextPage,
        loadMoreEntries: async () => {
          const newResults = await this.searchParent(query, type, pageInfo.endCursor);
          newResults.data = [...results.data, ...newResults.data];
          return newResults;
        },
        data: edges.map(({ node: node }) => ({
          query,
          string: node.name,
          original: {
            index: node.id,
            label: node.name,
            ...(node.__typename === 'Plant'  ? { type: 'PLANT', strain: node.strain } : {}),
            ...(node.__typename === 'Strain' ? { breeder: node.breeder } : {})
          }
        })),
      };
    }

    return results;
  };

  saveStrain = async (strain) => {
    const { createStrain } = this.props;
    let strainId = null;
    if (strain) {
      if (strain.index === '__new__') {
        strainId = uuid.v4();
        await createStrain({ variables: {
          strain : {
            id: strainId,
            name: strain.label,
          }
        }});
      } else if (strain.index) {
        strainId = strain.index;
      }
    }
    return strainId;
  }

  get actions() {
    return {
      notBlock: {
        findQrCode  : this.findQrCode,
        searchParent: this.searchParent,
      }
    };
  }
}

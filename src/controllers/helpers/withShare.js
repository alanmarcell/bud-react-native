import React from 'react';
import Toast from 'react-native-easy-toast';
import { I18n } from 'utils/I18n';
import Analytics from 'utils/Analytics';
import { View, Share, Platform } from 'react-native';
import { default as RNShare } from 'react-native-share';

const withShare = Comp => {

  return class WithShare extends React.PureComponent {

    handleShare = getOptions => async () => {
      try {
        __DEV__ && console.log('Share...'); // eslint-disable-line no-console
        this.toast('messages.openingShare');
        const options = await getOptions();

        if (Platform.OS === 'ios') {
          await Share.share(options);
        } else {
          await RNShare.open(options);
        }
      } catch (err) {
        this.toast('messages.shareError');
        Analytics.logException(err);
      }
    }

    toast(msg) {
      this.refs.toast.show(I18n.t(msg));
    }

    render() {
      return (
        <View style={{ flex: 1 }}>
          <Comp 
            handleShare={this.handleShare}
            {...this.props} 
          />
          <Toast ref="toast" />
        </View>
      );
    }
  };
};

export default withShare;

import React from 'react';

import Recovery from 'components/Recovery';
import RootController from './RootController';

import { I18n } from 'utils/I18n';

import { graphql } from 'react-apollo';
import { RECOVERY_QUERY } from 'graphql/user';

class RecoveryController extends RootController {
  static navigationOptions = {
    title: 'screens.recovery.title',
  };

  get actions() {
    return {
      onRecovery: async (credentials) => {
        await this.props.recovery({ variables: credentials });

        this.ok(I18n.t('sign-in.password_changed_success'));
      },
    };
  }

  renderView(props) {
    return <Recovery {...props} />;
  }
}

export default graphql(RECOVERY_QUERY, { name: 'recovery' })(RecoveryController);


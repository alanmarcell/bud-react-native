// @flow
import React from 'react';
import Promise from 'bluebird';
import { chunk } from 'lodash';
import RootController from './RootController';
import ImagePicker from '../components/ImagePicker';

class AddPhotoController extends RootController {
  static navigationOptions = {
    title: 'screens.drawer.newPhoto'
  };

  get actions() {
    return {
      savePhotos: async ({ id, images }) => {
        const photos = images.map(({ uri, ...image}) => image);
        const chunkPhotos = chunk(photos, 2);

        await Promise.all(chunkPhotos.map((ps) => this.props.uploadFiles(id, ps)));
      },

      goBack: () => this.goBack()

    };
  }

  renderView(props) {
    return (
      <ImagePicker {...props} />
    );
  }
}

export default AddPhotoController;

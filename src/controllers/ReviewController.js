import React from 'react';
import PropTypes from 'prop-types';
import ReviewDetails from '../components/Reviews/ReviewDetails';
import RootController from './RootController';

class ReviewController extends RootController {
  static propTypes = {
    review: PropTypes.object,
  }

  static navigationOptions = {
    // TODO USE BUD's NAME
    title: 'screens.bud.title',
  };

  get actions() {
    return {};
  }

  renderView(props) {
    const { review } = props;

    return (
      <ReviewDetails review={review} />
    );
  }
}

export default ReviewController;


import React from 'react';
import { Alert } from 'react-native';
import { connect } from 'react-redux';

import { I18n } from 'utils/I18n';
import ReviewEdit from 'components/Reviews/ReviewEdit';
import { serializeReview } from 'components/Reviews/ReviewEdit/ReviewDictionary';
import RootController from 'controllers/RootController';

import { graphql, compose, propType } from 'graphql';
import { makeGet, mutationCreate } from 'graphql';
import { SESSION_QUERY } from 'graphql/user';
import {
  budFragments,
  reviewFragments,
  BUD_REVIEW_PROFILE,
  BUD_REVIEW_CREATE
} from 'graphql';


class ReviewEditController extends RootController {
  static propTypes = {
    review: propType(reviewFragments.reviewBase),
    bud: propType(budFragments.budBase),
    loading: React.PropTypes.bool,
  };

  static navigationOptions = {
    title: 'screens.review.title',
  };

  get actions() {
    return {
      saveReview: async ({ ...values }) => {
        // TODO: Validar de quem é o Bud
        const { objectId: budId, createReview } = this.props;
        const product = {
          type: 'BUD',
          id: budId
        };

        const review = serializeReview(values);

        await createReview({ variables: { product, input: review } });
        Alert.alert(
          I18n.t('screens.bud.add_success_title'),
          I18n.t('screens.bud.add_success_thanks'),
          [
            { text: 'OK', onPress: () => {
              // handleAction exec goBack to remove splash
              // this goBack should remove just the splash and not go back
              // this.goBack(); 
            }
            },
          ],
          { cancelable: false }
        );
      },
      openQuickID: () => {
        const { bud } = this.props;
        const { name, qrCode: { code: qrcode } } = bud;

        this.redirectTo('budQrCode', {
          icon: 'buds',
          label: `Bud: ${name}`,
          qrcode,
        });
      }
    };
  }

  renderView(props) {
    const submitText = I18n.t(`screens.${this.sceneKey}.submit`);
    return (
      <ReviewEdit {...props} submitText={submitText} />
    );
  }
}

const mapStateToProps = state => ({
  authedUserId: state.user.session.user.id,
});

export default compose(
  graphql(SESSION_QUERY),
  graphql(BUD_REVIEW_PROFILE, makeGet()),
  graphql(BUD_REVIEW_CREATE, mutationCreate('createReview', 'Review', 'reviews', 'UserReviews', ['UserBudsReviewed'])),
)(connect(mapStateToProps)(ReviewEditController));

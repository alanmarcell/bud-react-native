import React from 'react';
import moment from 'moment';
import { I18n } from 'utils/I18n';

import Refreshable from 'utils/components/Refreshable';
import styles from './styles';

import GaleryPreview from 'components/GaleryPreview';
import { productsList} from 'components';
import CupsCardItem from 'controllers/Cups/components/CupsCardItem';
import BudCardItem from 'controllers/Buds/components/BudCardItem';

const CategoryList = productsList(CupsCardItem);
const BudsCategoryList = productsList(BudCardItem);

import { View, ActivityIndicator, ScrollView, Text } from 'react-native';
import {ProductHeader} from 'components';

class EditionProfile extends React.PureComponent {
  static propTypes = {
    edition: React.PropTypes.object,
    loading: React.PropTypes.bool,
  }

  render() {
    if (this.props.loading) {
      return <ActivityIndicator />;
    }

    const {
      loading,
      refetch,
      edition,
      buds, 
      categories,
      openGalery,
      edition: { files }
    } = this.props;

    const details = [{
      icon: 'calendar',
      text: `${moment(edition.startAt).format('L')} - ${moment(edition.endAt).format('L')}`
    }];
    
    
    const refreshControl = { loading, refetch };

    const categoriesListProps = {
      products: categories,
      refreshControl,
      onProductPress: this.props.onCategoryPress,
      icon: 'trophy',
      productsType: 'categories',
      shouldSkipBlankList: true,
    };

    const categoryBudsListProps = {
      products: buds,
      refreshControl,
      onProductPress: this.props.onBudPress,
      icon: 'budbuds',
      productsType: 'bud',
      shouldSkipBlankList: true,
    };

    const showGalery = openGalery && files && files.length !== 0;

    return (
      <ScrollView refreshControl={Refreshable({ loading, refetch })}>

      <View style={styles.container}>
        <ProductHeader {...this.props} productType="edition" details={details} />
        {showGalery && <GaleryPreview files={files} openGalery={openGalery} />}
      </View>

      <View style={styles.categoriesTitleContainer}>
        <Text style={styles.categoriesTitle}>{I18n.t('screens.edition.categories.title')}</Text>
      </View>

      <CategoryList {...categoriesListProps} />
      
      <View style={styles.budsTitleContainer}>
        <Text style={styles.budsTitle}>{I18n.t('screens.edition.buds.title')}</Text>
      </View>
      <BudsCategoryList {...categoryBudsListProps} />      
    </ScrollView>
    );
  }
}

export default EditionProfile;

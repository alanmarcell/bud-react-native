import { SimpleSchema } from 'components/BudForms/Schemas';
import { NameSchema, ImageSchema, cleanLogoToOpen } from 'components/BudForms/Schemas';
import { path, dissoc, dissocPath, pathOr, isEmpty, startsWith, slice } from 'ramda';

const cleanInitalEmpySpaces = (name ) =>
    name && startsWith(' ', name)
    ? cleanInitalEmpySpaces(slice(1,Infinity,name))
    : name;

const cleanDocToOpen = (docToClean = {}) => {
  const cleanEmptyField = fieldName => {
    const cleanName = pathOr(null, [fieldName], docToClean);
    return  cleanInitalEmpySpaces(!isEmpty(cleanName) ? cleanName : null);
  };

  const doc = {
    name: cleanEmptyField('name'),
    description: cleanEmptyField('description'),
    startAt: new Date(pathOr(new Date(), ['startAt'], docToClean)),
    endAt: new Date(pathOr(new Date(), ['endAt'], docToClean)),
  };

  return docToClean.logo
    ? cleanLogoToOpen(docToClean.logo)(doc)
    : doc;
};

const cleanDocToSave = cup =>
  path(['logo', 'uri'], cup)
    ? dissocPath(['logo', 'uri'], cup)
    : dissoc('logo', cup);

const EditionSchema = new SimpleSchema({
  name: {
    type: NameSchema,
    required: true,
  },

  description: {
    type: String,
    required: false,
  },

  startAt: {
    type: Date,
    required: true,
  },
  endAt: {
    type: Date,
    required: true,
  },

  logo: {
    type: ImageSchema,
    required: false
  },

  // qrCode: {
  //   type: String,
  //   required: false,
  // },
});

export {
  EditionSchema,
  cleanDocToOpen,
  cleanDocToSave,
};

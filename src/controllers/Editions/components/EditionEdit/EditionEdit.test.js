// import {filterMenuItems} from './profile';
import R from 'ramda';

const cleanInitalEmpySpaces = name =>
  name && R.startsWith(' ', name)
    ? cleanInitalEmpySpaces(R.slice(1, Infinity, name))
    : name;

test('cleanInitalEmpySpaces correctly', () => {
  const withSpace = '    teste';
  const isUndefined = undefined;
  const clearString = cleanInitalEmpySpaces(withSpace);
  expect(cleanInitalEmpySpaces(withSpace)).toBe('teste');
  expect(cleanInitalEmpySpaces('withoutSpace')).toBe('withoutSpace');
  expect(cleanInitalEmpySpaces(null)).toBe(null);
  expect(cleanInitalEmpySpaces(isUndefined)).toBe(undefined);
});
import React,{PureComponent} from 'react';
import { ActivityIndicator } from 'react-native';
import R from 'ramda';
import { BudForms } from 'components/BudForms';
import { I18nField, DescriptionField, NameField, DateField } from 'components/BudForms/Fields';

import {
  cleanDocToOpen,
  cleanDocToSave,
  EditionSchema,
} from './EditionSchema';
import LogoPicker from 'components/LogoPicker';

class Edit extends PureComponent {
  static propTypes = {
    edition: React.PropTypes.object,
    loading: React.PropTypes.bool,
    saveEdition: React.PropTypes.func.isRequired,
    submitText: React.PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    const doc = this._buildDocState(props.edition);
    this.initalDoc = doc;
    this.state = {
      doc
    };
  }

  _handleSubmit = (doc) => {
    const { saveEdition, cupId, goBack } = this.props;
    
    if(R.equals(this.initalDoc, doc))
      return goBack();

    saveEdition({
      ...cleanDocToSave(doc),
      cupId,
    });
  };

  _handleChange = (doc) => {
   
    this.setState({doc: cleanDocToOpen(doc) });
  }

  _buildDocState = (edition = {}) =>  cleanDocToOpen(edition);

  componentWillReceiveProps(nextProps, _nextState) {
    const { edition } = nextProps;

    this.setState({ doc: this._buildDocState(edition) });
  }

  render() {
    const { loading } = this.props;
    const { doc } = this.state;

    if (loading) {
      return <ActivityIndicator />;
    }

    const formProps = {
      ref: 'form',
      schema: EditionSchema,
      doc: cleanDocToOpen(doc),
      onChange: this._handleChange,
      onSubmit: this._handleSubmit,
      submitText: this.props.submitText,
    };

    return (
      <BudForms {...formProps} >
        <I18nField
          fieldName="logo"
          type={LogoPicker}
        />

        <NameField i18nScope="forms.editionName" />

        <DescriptionField />
        <DateField fieldName="startAt" mode="datetime" dateFormat="lll" />
        <DateField fieldName="endAt" mode="datetime" dateFormat="lll" />

        {/* <I18nField
          fieldName="qrCode"
          type={QRCodeFactory}
          findQrCode={findQrCode}
          newText={I18n.t('forms.qrCode.newText')}
          oldText={I18n.t('forms.qrCode.oldText')}
        /> */}
      </BudForms>
    );
  }
}

export default Edit;

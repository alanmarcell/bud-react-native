import R from 'ramda';

const getLoggedInUserId = R.path(['session', 'user', 'id']);

const getEditionOwnerId = props =>
  R.path(['edition', 'owner', 'id'], props);

const isEditionOwner = props => getEditionOwnerId(props) === getLoggedInUserId(props);

export {
  getEditionOwnerId,
  isEditionOwner,
  getLoggedInUserId
};

import EditionProfileController from './EditionProfileController';
import EditionEditController from './EditionEditController';

export {
  EditionProfileController,
  EditionEditController,
};

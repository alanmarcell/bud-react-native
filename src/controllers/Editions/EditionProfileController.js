// @flow
import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';
import NavIcon from 'scenes/NavIcon';
import RootController from '../RootController';
import EditionProfile from './components/EditionProfile';
import ActionMenu from 'components/ActionMenu';
import R from 'ramda';
import { graphql, makeGet, compose, makeDeleteMutation } from 'graphql';
import { EDITION_PROFILE, EDITION_DELETE, SESSION_QUERY, EDITION_IMAGE_UPLOAD } from 'graphql';
import { isEditionOwner } from './isEditionOwner';
import orderBuds from 'utils/orderBuds';

class EditionProfileController extends RootController {
  static propTypes = {
    edition: PropTypes.object,
    loading: PropTypes.bool,
  }

  static navigationOptions = {
    title: 'screens.edition.title',
  };

  get actions() {
    return {
      onCategoryPress: (objectId) => {
        const isMine = isEditionOwner(this.props);
        this.redirectTo('categoryProfile', { objectId, isMine });
      },
      onBudPress: (objectId) => {
        this.redirectTo('budProfile', { objectId });
      },
      deleteEdition: async (params) => {
        const destroy = await this.confirmRemove('edition');
        if (destroy) {
          await this.props.deleteEdition(params);

          this.goBack('cupProfile', { type: 'reset' });
        }
      },
      openQuickID: () => {
        const { name, logo, qrCode: { code: qrcode } } = this.props.edition;

        this.redirectTo('editionQrCode', {
          icon: 'trophy',
          logo,
          label: `Edition: ${name}`,
          qrcode,
        });
      },
      openGalery: (selectedId) => {
        const { edition: { files } } = this.props;

        this.redirectTo('editionGalery', {
          files,
          selectedFile: files.find((image) => image.id === selectedId)
        });
      },
      notBlock: {
        uploadFiles: (id, photos) => {
          return this.props.addImagesToEdition({ variables: { id, photos } });
        },
      }
    };
  }

  componentWillUpdate(nextProps) {
    const { loading, setButtons } = nextProps;
    if (!loading && !setButtons) {
      if (isEditionOwner(nextProps)) {
        const { objectId } = nextProps;
        const { deleteEdition } = this.actionsWithHandle();
        this.refresh({
          setButtons: true,
          renderRightButton: () => {
            return (
              <View style={{ flex: 1, flexDirection: 'row' }}>
                <NavIcon onPress={() => deleteEdition({ objectId })} iconName="trash" />
                <NavIcon onPress={() => this.redirectTo('editionEdit', { objectId })} iconName="pencil" />
              </View>
            );
          }
        });
      }
    }
  }

  onActionPress = (option) => {
    const { objectId } = this.props;
    switch (option) {
      case 'newCategory': {
        this.redirectTo('categoryEdit', { editionId: objectId });
        return;
      }
      case 'judges': {
        this.redirectTo('judgesList', { objectId });
        return;
      }
      case 'newBud': {
        this.redirectTo('budEdit', { editionId: objectId });
        return;
      }
      case 'newPhoto': {
        const { uploadFiles } = this.actionsWithHandle();
        const { objectId } = this.props;
        this.redirectTo('editionPhotoAdd', { id: objectId, uploadFiles });
      }
    }
  }

  renderActionMenu(props) {
    const ownerItems = isEditionOwner(props)
      ? {
        newCategory: 'trophy',
        newBud: 'bud',
        newPhoto: 'camera'
      }
      : {};

    const menuItems = {
      ...ownerItems,
      judges: 'judge',
    };
    return <ActionMenu onPress={this.onActionPress} items={menuItems} />;
  }

  renderView(props) {
    const { loading, edition } = props;

    if (loading) {
      return <ActivityIndicator />;
    }

    const categories = R.path(['categories', 'nodes'], edition);
    const buds = orderBuds(R.path(['buds', 'nodes'], edition));
    return (
      <View style={{ flex: 1 }}>
        <EditionProfile
          {...props}
          categories={categories}
          buds={buds}
        />
        {this.renderActionMenu(props)}
      </View>
    );
  }
}

export default compose(
  graphql(SESSION_QUERY, { props: ({ data }) => ({ ...data }) }),
  graphql(EDITION_PROFILE, makeGet()),
  graphql(EDITION_IMAGE_UPLOAD, {
    name: 'addImagesToEdition',
    options: {
      refetchQueries: [
        'EditionProfile'
      ]
    }
  }),
  graphql(EDITION_DELETE, makeDeleteMutation('deleteEdition', 'editions', 'UserEditions',
    {
      options: {
        refetchQueries: [
          'CupProfile',
        ],
      },
    }
  ))
)(EditionProfileController);

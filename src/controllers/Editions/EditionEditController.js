import React from 'react';
import uuid from 'uuid';

import { I18n } from 'utils/I18n';
import EditionEdit from './components/EditionEdit';
import RootController from '../RootController';

import { graphql, compose, propType } from 'graphql';
import { makeGet, mutationCreate } from 'graphql';
import {
  editionFragments,
  EDITION_PROFILE, EDITION_CREATE, EDITION_UPDATE
} from 'graphql';

class EditionEditController extends RootController {
  static propTypes = {
    edition: propType(editionFragments.editionBase),
    loading: React.PropTypes.bool,
  };

  static navigationOptions = {
    getTitle: ({ name }) => {
      const titleEdit = 'screens.edition.edit.title';
      const titleAdd  = 'screens.edition.add.title';
      return I18n.T(/.*Add$/.test(name) ? titleAdd : titleEdit);
    }
  };

  get actions() {
    return {
      saveEdition: async ({...edition}) => {
        let { objectId, updateEdition, createEdition } = this.props;
        if (objectId) {
          await updateEdition({ variables: { id: objectId, input: edition }});
          this.goBack();
        } else {
          objectId = uuid.v4();
          edition.id = objectId;
          await createEdition({ variables: { input: edition }});

          this.redirectTo('editionProfile', { type: 'replace', objectId });          
        }
      }
    };
  }

  renderView(props) {
    const { name } = this.props;
    const submitEdit = 'screens.edition.edit.submit';
    const submitAdd  = 'screens.edition.add.submit';
    const submitText = I18n.t(/.*Add$/.test(name) ? submitAdd : submitEdit);

    return (
      <EditionEdit {...props} goBack={() => this.goBack()} submitText={submitText} />
    );
  }
}

export default compose(
  graphql(EDITION_PROFILE, makeGet()),
  graphql(EDITION_UPDATE, { name: 'updateEdition' }),
  graphql(EDITION_CREATE, mutationCreate('createEdition', 'Edition', 'editions','UserEditions', ['CupProfile'])),
)(EditionEditController);

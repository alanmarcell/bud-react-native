import React from 'react';
import { shallow } from 'enzyme';
import R from 'ramda';

import { tree, context } from '../../__mocks__/stateHelper';
import Controller from './PlantProfileController';

test.skip('renders correctly', () => {
  const [[plantId]] = R.toPairs(tree.get('plants', 'list'));
  const props = {
    navigation: { state: { params: { plantId }}}
  };
  const wrapper = shallow(<Controller {...props} />, context).dive().dive();
  expect(wrapper).toMatchSnapshot();
});

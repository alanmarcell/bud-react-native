// @flow
import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import R from 'ramda';

import NavIcon from 'scenes/NavIcon';
import PlantProfile from './components/PlantProfile';
import RootController from '../RootController';
import {TasksController} from '../Tasks';
import ActionMenu from 'components/ActionMenu';

import { propType, compose, graphql } from 'graphql';
import { makeDeleteMutation, makeGet } from 'graphql';
import {
  plantFragments,
  PLANT_PROFILE, PLANT_DELETE, PLANT_IMAGE_UPLOAD, SESSION_QUERY
} from 'graphql';

const getLoggedInUserId = R.path(['session', 'user', 'id']);

const getPlantOwnerId = props =>
  R.path(['plant', 'owner', 'id'], props) || R.path(['plant', 'user', 'id'], props);

const isPlantOwner = props => getPlantOwnerId(props) === getLoggedInUserId(props);

class PlantProfileController extends RootController {
  static propTypes = {
    plant: propType(plantFragments.plantBase),
    loading: React.PropTypes.bool,
  }

  static navigationOptions = {
    title: 'screens.plant.title',
  };

  state = {
    myPlant: false,
  };

  get actions() {
    return {
      notBlock: {
        uploadFiles: (id, photos) => {
          return this.props.addImagesToPlant({ variables: { id, photos } });
        },
      },
      deletePlant: async (params) => {
        const destroy = await this.confirmRemove('plant');
        if (destroy) {
          await this.props.deletePlant(params);
          this.redirectTo('plantsList', { type: 'reset' });
        }
      },
      openQuickID: () => {
        const { plant } = this.props;
        const { name, qrCode: { code: qrcode } } = plant;

        this.redirectTo('plantQrCode', {
          icon: 'plants',
          label: `Plant: ${name}`,
          qrcode,
        });
      },
      openGalery: (selectedId) => {
        const { plant: { files } } = this.props;

        this.redirectTo('plantGalery', {
          files,
          selectedFile: files.find((image) => image.id === selectedId)
        });
      },

    };
  }

  componentWillUpdate(nextProps) {
    const { loading, setButtons } = nextProps;

    if (!loading && !setButtons) {

      if (isPlantOwner(nextProps)) {
        this.setState({ myPlant: true });
        const { objectId } = nextProps;
        const { deletePlant } = this.actionsWithHandle();
        this.refresh({
          setButtons: true,
          renderRightButton: () => {
            return (
              <View style={{ flex: 1, flexDirection: 'row' }}>
                <NavIcon onPress={() => deletePlant({ objectId })} iconName="trash" />
                <NavIcon onPress={() => this.redirectTo('plantEdit', { objectId })} iconName="pencil" />
              </View>
            );
          }
        });
      }
    }
  }

  onActionPress = (option) => {
    if (option === 'newTask') {
      const { objectId } = this.props;
      this.redirectTo('plantTaskAdd', { plantId: objectId });
    } else if (option === 'newPhoto') {
      const { uploadFiles } = this.actionsWithHandle();
      const { objectId } = this.props;
      this.redirectTo('plantPhotoAdd', { id: objectId, uploadFiles });
    } else {
      const { objectId } = this.props;
      this.redirectTo('plantBudAdd', { plantId: objectId });
    }
  }

  renderPlantCard = (props) => {
    return <PlantProfile {...props} />;
  }

  renderActionMenu(myPlant) {
    const menuItems = {
      newBud: 'bud',
      newTask: 'calendar-plus-o',
      newPhoto: 'camera',
    };
    return (myPlant ? <ActionMenu onPress={this.onActionPress} items={menuItems} /> : null);
  }

  renderView(props) {
    const { objectId, loading } = props;
    const { myPlant } = this.state;

    if (loading) {
      return <ActivityIndicator />;
    }

    return (
      <View style={{ flex: 1 }}>
        <TasksController
          renderPlantCard={() => this.renderPlantCard(props)}
          plantId={objectId}
          embeded={true}
        />
        {this.renderActionMenu(myPlant)}
      </View>
    );
  }
}

export default compose(
  graphql(SESSION_QUERY, { props: ({ data }) => ({ ...data }) }),
  graphql(PLANT_PROFILE, makeGet()),
  graphql(PLANT_IMAGE_UPLOAD, {
    name: 'addImagesToPlant',
    options: {
      refetchQueries: [
        'PlantProfile'
      ]
    }
  }),
  graphql(PLANT_DELETE, {
    ...makeDeleteMutation('deletePlant', 'plants', 'UserPlants'),
    options: {
      refetchQueries: [
        'UserTasks',
        'UserBuds',
      ],
    },
  }),
)(PlantProfileController);

import React from 'react';
import { shallow } from 'enzyme';

import { context } from '../../__mocks__/stateHelper';
import Screen from './PlantsController';

test.skip('renders correctly', () => {
  const component = shallow(<Screen />, context);
  component.dive(context);
  expect(component).toMatchSnapshot();
});

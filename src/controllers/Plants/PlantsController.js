// @flow
import React from 'react';
import { View } from 'react-native';

import RootController from '../RootController';
import ActionMenu from 'components/ActionMenu';
import PlantsList from './components/PlantsList';

import { graphql, makeLoadList } from 'graphql';
import { PLANT_LIST } from 'graphql/plants';

class PlantsController extends RootController {
  static navigationOptions = {
    title: 'screens.plants.title',
  };

  get actions() {
    return {
      onPlantPress: (objectId) => {
        this.redirectTo('plantProfile', { objectId });
      }
    };
  }

  onActionPress = () => {
    this.redirectTo('plantAdd');
  }

  renderView(props) {
    const menuItems = {
      newPlant: 'budbuds',
    };

    return (
      <View style={{ flex: 1 }}>
        <PlantsList {...props} />
        <ActionMenu onPress={this.onActionPress} items={menuItems} />
      </View>
    );
  }
}

export default graphql(PLANT_LIST, makeLoadList(PLANT_LIST, 'plants'))(PlantsController);

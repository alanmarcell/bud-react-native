import React, { Component, PropTypes } from 'react';
import { View, Text } from 'react-native';
import moment from 'moment';
import v4 from 'uuid/v4';
import { I18n } from 'utils/I18n';

import GaleryPreview  from 'components/GaleryPreview';

import { ProductHeader } from 'components';
import Icon from 'components/Icon';

import U from 'utils';

import styles from './styles';

const profileMap = [
  // =================== line 01 ===================
  [{
    field: 'specie',
    icon: 'budbuds',
    render: (types) => types.map((type) => U.camelize(type)).join(', ')
  }, {
    field: 'genetic',
    icon: 'genetics'
  }, {
    field: 'environment',
    icon: 'location'
  }],

  // =================== line 02 ===================
  [{
    field: 'source',
    icons: {
      'seed' : 'seed',
      'clone': 'clone',
      'batch': 'group',
    },
    dateField: 'bornAt',
  }, {
    field: 'bornAt',
    icon: 'calendar',
    render: (date) => {
      let week = moment().diff(date, 'weeks') + 1;
      return I18n.t('screens.plant.week', { week });
    }
  }],

  // =================== line 03 ===================
  [{
    field: 'stage',
    icons: {
      'vegetative': 'vegetating',
      'flowering': 'flowering',
    },
    icon: 'flower',
    dateField: 'changedStageAt',
  },{
    field: 'changedStageAt',
    icon: 'calendar',
    render: (date) => {
      let week = moment().diff(date, 'weeks') + 1;
      return I18n.t('screens.plant.week', { week });
    }
  }]
];

export default class PlantHeader extends Component {
  static propTypes = {
    plant : PropTypes.object.isRequired,
    openQuickID: PropTypes.func.isRequired,
    style : PropTypes.oneOfType([
      PropTypes.shape(),
      PropTypes.number,
    ]),
  };

  renderPlantInfo = () => {
    const { plant } = this.props;

    let line1 = (profileMap.slice(0, 1)[0] || []).map(x => Object.assign({}, x, {
      label: plant[x.field]
    })).filter(x => !!x.label);

    let line2 = (profileMap.slice(1, 2)[0] || []).map(x => Object.assign({}, x, {
      label: plant[x.field],
      dateField: plant[x.dateField]
    })).filter(x => !!x.label);

    let line3 = (profileMap.slice(2, 3)[0] || []).map(x => Object.assign({}, x, {
      label: plant[x.field],
      dateField: plant[x.dateField]
    })).filter(x => !!x.label);

    return (
      <View style={styles.infoList}>
        { line1.length >= 1  && this.renderPlantInfoRow3Cols(line1) }
        { line2.length === 2 && this.renderPlantInfoRow2Cols(line2) }
        { line3.length === 2 && this.renderPlantInfoRow2Cols(line3, { noborder: true}) }
      </View>
    );
  }

  renderItemValue(item) {
    const { label, render } = item;
    if (render && U.is(Function, render)) {
      return render(label);

    } else if (U.is(String, label)) {
      return `${label.slice(0,1).toUpperCase()}${label.slice(1).toLowerCase()}`;

    } else {
      // Unkown case
      return JSON.stringify(item);
    }
  }

  renderItem(item, style) {
    if (item) {
      let { icon, icons, label } = item;
      if (icons) {
        icon = icons[label.toLowerCase()];
      }
      return (
        <View style={style}>
          <Icon style={styles.infoIcon} name={icon} />
          <Text style={styles.infoLabel}>{this.renderItemValue(item)}</Text>
          {
            item.dateField &&
            <Text style={styles.infoDate}>
              {moment(item.dateField).format('L')}
            </Text>
          }
        </View>
      );
    }
  }

  renderPlantInfoRow3Cols([left, center, right]) {
    const betterFitFlex = (arr) => {
      switch (arr.length) {
        case 1:
          return {flex: 1};
        case 2:
          return {flex: 1.3};
        case 3:
          return {flex: 2.1};
        default:
          return {flex: 1};
      }
    };

    return (
      <View key={v4()} style={styles.infoListRow}>
        {this.renderItem(left, [styles.infoListEqualCol, betterFitFlex(left.label)])}
        {this.renderItem(center, styles.infoListEqualCol)}
        {this.renderItem(right, styles.infoListEqualCol)}
      </View>
    );
  }

  renderPlantInfoRow2Cols([left, right], { noborder } = { noborder: false}) {
    return (
      <View key={v4()} style={[styles.infoListRow, { borderBottomWidth: noborder ? 0 : 0.5 }]}>
        {this.renderItem(left, styles.infoListLeftCol)}
        {this.renderItem(right, styles.infoListRightCol)}
      </View>
    );
  }

  render() {
    const { plant: { files }, openGalery } = this.props;

    return (
      <View style={styles.container}>
        <ProductHeader {...this.props} productType="plant" />
        { this.renderPlantInfo() }
        {(files && files.length !== 0 )&& <GaleryPreview files={files} openGalery={openGalery} />}
      </View>
    );
  }
}

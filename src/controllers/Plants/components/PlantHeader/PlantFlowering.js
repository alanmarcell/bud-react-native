import React, { Component } from 'react';
import { View, Text } from 'react-native';

import { I18n } from 'utils/I18n';
import Icon from 'components/Icon';

import styles from './styles';

export default class PlantFlowering extends Component {
  static propTypes = {
    flowering: React.PropTypes.oneOf([
      'automatic', 'regular'
    ]).isRequired,
  }

  render() {
    const { flowering } = this.props;
    return (
      <View style={styles.plantFlowering}>
        <Icon name="leaf" style={styles.plantFloweringIcon} />
        <Text>{I18n.t(`plants.flowering.${flowering}`)}</Text>
      </View>
    );
  }
}

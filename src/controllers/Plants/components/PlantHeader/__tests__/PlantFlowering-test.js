import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';

import PlantFlowering from '../PlantFlowering';

describe.skip('PlantFlowering', () => {
  test('renders correctly', () => {
    const plant = { flowering: 'automatic' };
    const component = shallow(<PlantFlowering {...plant} />);
    expect(component).toMatchSnapshot();
  });
});

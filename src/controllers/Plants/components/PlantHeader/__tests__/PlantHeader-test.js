import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';
import R from 'ramda';

import PlantHeader from '../';
import { tree, context } from '../../../../../__mocks__/stateHelper';

describe.skip('PlantHeader', () => {
  test('renders correctly', () => {
    const [[, plant]] = R.toPairs(tree.get('plants', 'list'));
    const component = shallow(<PlantHeader plant={plant} />, context);
    expect(component).toMatchSnapshot();
  });
});

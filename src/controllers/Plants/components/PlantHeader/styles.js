// // @flow
import EStyleSheet from 'react-native-extended-stylesheet';

const borderColor = '$Colors.LightSilver';

export default EStyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    padding        : 15,
  },

  infoList: {
    marginTop: 15,
  },

  infoListRow: {
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    paddingHorizontal: 10,
    alignItems: 'center',
    height: 35,
    borderColor,
    borderBottomWidth: 0.5,
  },

  infoListEqualCol: {
    flexDirection: 'row',
    flex: 1,
  },

  infoListLeftCol: {
    flexDirection: 'row',
    flex: 1.3,
  },

  infoListRightCol: {
    flexDirection: 'row',
    flex: 1,
  },

  infoIcon: {
    marginRight: 10,
    width: 15,
    height: 12,
  },
  infoLabel: {
    fontSize: 12,
    color: '$Colors.Gray',
  },
  infoDate: {
    fontSize: 12,
    marginLeft: 5,
    padding: 2,
    color: '$Colors.Gray',
    backgroundColor: '$Colors.LightSilver',
  },

});

import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import U from 'utils';
import { I18n, parseNumber } from 'utils/I18n';
import moment from 'moment';

import { BudForms } from 'components/BudForms';
import { I18nField, NameField, DescriptionField } from 'components/BudForms/Fields';
import {
  AutoCheckboxFactory,
  AutoRadioFactory,
  DateRadioFactory,
  InputRadioFactory,
  AutoRadioDateFactory,
} from 'components/BudForms/Factories';
import { SearchSelectFactory, InputFactory } from 'utils/Form/Factories';
import QRCodeFactory from 'components/BudForms/Factories/QRCodeFactory';
import StrainSelect from 'components/StrainSelect';

import styles from './styles';
import { PlantSchema } from './PlantSchema';

export default class PlantEdit extends StrainSelect {
  static propTypes = {
    plant: React.PropTypes.object,
    loading: React.PropTypes.bool,
    savePlant: React.PropTypes.func.isRequired,
    submitText: React.PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      doc: this._buildDocState(props.plant)
    };
  }

  componentWillReceiveProps(nextProps, _nextState) {
    const { plant } = nextProps;
    this.setState({ doc: this._buildDocState(plant) });
  }

  _buildDocState(plant) {
    const { qrCode, bornAt, changedStageAt, ...doc } = plant;

    this.handleSourceToShowOrigin(doc);

    doc.qrCode = qrCode ? qrCode.code : null;
    if (doc.startWeek) {
      doc.bornAt = null;
    } else {
      doc.bornAt = bornAt ? new Date(bornAt) : new Date();
    }
    doc.changedStageAt = changedStageAt ? new Date(changedStageAt) : new Date();

    doc.mother = this.serializeRelation(doc, 'mother');
    doc.father = this.serializeRelation(doc, 'father');
    doc.strain = this.serializeRelation(doc, 'strain');
    doc.plantOrigin = this.serializeRelation(doc, 'plantOrigin');

    return doc;
  }

  handleSubmit = (formData) => {
    const { plant } = this.props;
    if (formData.startWeek > 0 && plant.startWeek !== formData.startWeek) {
      formData.bornAt = this._resolveDateWeeksAgo(formData.startWeek);
    } else if (formData.startWeek > 0) {
      formData.bornAt = plant.bornAt;
    }

    formData.changedStageAt = this.state.doc.changedStageAt;

    this.props.savePlant(formData);
  };

  afterSubmit = ({ startWeek, cloneAmount, ...values }, submit) => {
    const { refs: { form } } = this.form;

    try {
      if (startWeek) {
        form.onValueChange('startWeek', parseNumber(startWeek));
      }

      if (cloneAmount) {
        form.onValueChange('cloneAmount', parseNumber(cloneAmount));
      }
    } catch (err) {
      __DEV__ && console.error(err);
    }

    submit();
  };

  handleChange = (doc) => {
    doc = this.handleBornAt(doc);
    doc = this.handleSpecieRuderalis(doc);
    doc = this.handleGeneticFeminized(doc);
    doc = this.handleSourceToShowOrigin(doc);

    if (doc.plantOrigin && doc.plantOrigin.strain) {
      delete doc.plantOrigin.strain;
      delete doc.plantOrigin.type;
    }

    this.setState({ doc });
  };

  _resolveDateWeeksAgo = (weeksNumber) => {
    return moment().subtract((weeksNumber - 1), 'weeks').toDate();
  };

  handleOutsideState = (doc) => this.setState({ doc });

  handleSourceToShowOrigin(doc) {
    this.showPlantOrigin = doc.source === 'CLONE';

    return doc;
  }

  handleBornAt(doc) {
    if (!U.equals(this.state.doc.bornAt, doc.bornAt)) {
      doc.startWeek = null;
    } else if (!U.equals(this.state.doc.startWeek, doc.startWeek)) {
      doc.bornAt = null;
    }

    return doc;
  }

  handleSpecieRuderalis(doc) {
    if (doc.specie && doc.specie.some(x => x === 'RUDERALIS')) {
      doc.genetic = 'AUTO';
    }

    return doc;
  }

  handleGeneticFeminized(doc) {
    if (doc.genetic === 'FEMINIZED') {
      doc.gender = 'FEMALE';
    }

    return doc;
  }

  render() {
    const { loading, findQrCode } = this.props;

    if (loading) {
      return <ActivityIndicator />;
    }

    const { doc } = this.state;

    const formProps = {
      ref: (ref) => this.form = ref,
      schema: PlantSchema,
      doc: U.pick([
        'name',
        'description',
        'specie',
        'genetic',
        'gender',
        'source',
        'environment',
        'stage',
        'bornAt',
        'changedStageAt',
        'cloneAmount',
        'startWeek',
        'qrCode',
        'mother',
        'father',
        'strain',
        'plantOrigin',
      ], doc),
      onSubmit: this.handleSubmit,
      onChange: this.handleChange,
      afterSubmit: this.afterSubmit,
      submitText: this.props.submitText,
      handleOutsideState: this.handleOutsideState,
      containerStyle: styles.container,
    };

    const showPlantOrigin = this.showPlantOrigin;

    return (
      <BudForms {...formProps} >
        <NameField i18nScope="forms.plantName" />
        <DescriptionField />
        <I18nField
          fieldName="strain"
          i18nScope="forms.strain"
          placeholder={true}
          formatText={this.formatStrain}
          type={SearchSelectFactory}
          searchFn={this.searchStrain}
        />
        <I18nField
          fieldName="mother"
          i18nScope="forms.mother"
          placeholder={true}
          formatText={this.formatStrain}
          type={SearchSelectFactory}
          searchFn={this.searchStrain}
        />
        <I18nField
          fieldName="father"
          i18nScope="forms.father"
          placeholder={true}
          formatText={this.formatStrain}
          type={SearchSelectFactory}
          searchFn={this.searchStrain}
        />
        <I18nField
          fieldName="specie"
          i18nScope="forms.specie"
          type={AutoCheckboxFactory}
        />
        <I18nField
          fieldName="genetic"
          i18nScope="forms.genetic"
          type={AutoRadioFactory}
        />
        <I18nField
          fieldName="gender"
          i18nScope="forms.gender"
          type={AutoRadioFactory}
        />
        <I18nField
          fieldName="source"
          i18nScope="forms.source"
          type={AutoRadioFactory}
        />
        {showPlantOrigin &&
          <I18nField
            fieldName="plantOrigin"
            i18nScope="forms.plantOrigin"
            placeholder={true}
            formatText={this.formatStrain}
            type={SearchSelectFactory}
            searchFn={this.searchParent}
          />
        }

        {showPlantOrigin &&
          <I18nField
            fieldName="cloneAmount"
            i18nScope="forms.cloneAmount"
            type={InputFactory}
            number={true}
            placeholder={true}
          />
        }
        <I18nField
          fieldName="environment"
          i18nScope="forms.environment"
          type={AutoRadioFactory}
        />
        <I18nField
          fieldName="stage"
          i18nScope="forms.stage"
          type={AutoRadioDateFactory}
        />
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 2 }}>
            <I18nField
              fieldName="bornAt"
              i18nScope="forms.bornAt"
              type={DateRadioFactory}
              selected={!doc.startWeek}
              placeholder={true}
            />
          </View>
          <View style={{ flex: 2 }}>
            <I18nField
              fieldName="startWeek"
              i18nScope="forms.startWeek"
              type={InputRadioFactory}
              number={true}
              selected={!!doc.startWeek}
              placeholder={true}
            />
          </View>
        </View>

        <I18nField
          fieldName="qrCode"
          type={QRCodeFactory}
          findQrCode={findQrCode}
          newText={I18n.t('forms.qrCode.newText')}
          oldText={I18n.t('forms.qrCode.oldText')}
        />
      </BudForms>
    );
  }
}

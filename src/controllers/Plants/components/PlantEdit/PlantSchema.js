import { SimpleSchema } from 'components/BudForms/Schemas';
import { NameSchema } from 'components/BudForms/Schemas';

export const PlantSchema = new SimpleSchema({
  name: NameSchema,

  description: {
    type: String,
    required: false
  },

  strain: {
    type: 'Object',
    required: false,
  },

  'strain.index': String,
  'strain.label': String,
  'strain.breeder':{
    type: String,
    required: false,
  },

  plantOrigin: {
    type: 'Object',
    required: false,
  },

  'plantOrigin.index': String,
  'plantOrigin.label': String,

  'mother': {
    type: 'Object',
    required: false,
  },
  'mother.index': String,
  'mother.label': String,
  'mother.breeder':{
    type: String,
    required: false,
  },

  'father': {
    type: 'Object',
    required: false,
  },
  'father.index': String,
  'father.label': String,
  'father.breeder':{
    type: String,
    required: false,
  },

  specie: {
    type: Array,
    required: false,
  },

  'specie.$': {
    type: String,
    allowedValues: ['INDICA', 'SATIVA', 'RUDERALIS'],
  },

  genetic: {
    type: String,
    allowedValues: ['AUTO', 'REGULAR', 'FEMINIZED'],
    required: false,
  },

  gender: {
    type: String,
    allowedValues: ['FEMALE', 'MALE', 'UNKNOWN'],
    required: false,
  },

  source: {
    type: String,
    allowedValues: ['CLONE', 'SEED'],
    required: false,
  },

  environment: {
    type: String,
    allowedValues: ['INDOOR', 'OUTDOOR', 'GREENHOUSE'],
    required: false,
  },

  stage: {
    type: String,
    allowedValues: ['VEGETATIVE', 'FLOWERING'],
    required: false,
  },

  bornAt: {
    type: Date,
    required: false,
  },

  startWeek: {
    type: Number,
    min  : 1,
    max  : 10000,
    required: false,
  },

  cloneAmount: {
    type: Number,
    min  : 1,
    max  : 10000,
    required: false,
  },

  changedStageAt: {
    type: Date,
    required: false,
  },

  qrCode: {
    type: String,
    required: false,
  },
});

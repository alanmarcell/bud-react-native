import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';
import R from 'ramda';

import PlantEdit from '../';
import { tree } from '../../../../../__mocks__/stateHelper';

describe.skip('PlantEdit', () => {
  test('renders correctly for edit plant', () => {
    const [[, plant]] = R.toPairs(tree.get('plants', 'list'));
    const props = {
      plant,
      submitText: 'save'
    };
    const component = shallow(<PlantEdit {...props} />).dive();
    expect(component).toMatchSnapshot();
  });
});

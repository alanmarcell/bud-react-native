import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';
import R from 'ramda';

import PlantCardItem from '../';
import { tree, context } from '../../../../../__mocks__/stateHelper';

test.skip('renders correctly', () => {
  const [[, plant]] = R.toPairs(tree.get('plants', 'list'));
  const wrapper = shallow(
    <PlantCardItem
      onPress={() => {}}
      {...plant}
    />
  , context);
  expect(wrapper).toMatchSnapshot();
});

import React, { Component, PropTypes } from 'react';
import { View, Text } from 'react-native';
import moment from 'moment';

import SelectableCard     from 'components/SelectableCard';
import RoundedIcon        from 'components/RoundedIcon';
import PlantName          from '../PlantName';

import { Moment } from 'utils/components';

import styles from './styles';
import stylesHelpers from '../../../../themes/helpers';

export default class PlantCardItem extends Component {
  static defaultProps = {
    gender: 'female',
    ...SelectableCard.defaultProps,
  };

  static propTypes = {
    ...SelectableCard.propTypes,
    name  : PropTypes.string.isRequired,
    gender: PropTypes.oneOf(['FEMALE', 'MALE']),
    strain: PropTypes.object,
    date  : PropTypes.oneOfType([
      React.PropTypes.instanceOf(Date),
      React.PropTypes.instanceOf(moment),
    ]),
  };

  render() {
    const { name, gender, date, strain } = this.props;
    return (
      <SelectableCard {...this.props}>
        <View style={[stylesHelpers.leftColumn, stylesHelpers.alignCenter]}>
          <RoundedIcon object={this.props} />
        </View>
        <View style={[stylesHelpers.middleColumn, stylesHelpers.alignCenterLeft]}>
          <PlantName name={name} gender={gender} />
          <Text style={styles.plantType}>{(strain || {}).name}</Text>
        </View>
        <View style={[stylesHelpers.rightColumn, stylesHelpers.alignCenterRight]}>
          <Moment date={date} action="format" args={['L']} style={styles.date} />
        </View>
      </SelectableCard>
    );
  }
}

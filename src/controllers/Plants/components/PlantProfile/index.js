import React, { Component } from 'react';
import { ScrollView } from 'react-native';

import PlantHeader from '../PlantHeader';

export default class PlantProfile extends Component {
  static propTypes = {
    plant: React.PropTypes.object,
    loading: React.PropTypes.bool,
    openQuickID: React.PropTypes.func.isRequired,
  }

  render() {
    const { plant } = this.props;
    if (!plant) {
      return null;
    }

    return (
      <ScrollView>
        <PlantHeader {...this.props} />
      </ScrollView>
    );
  }
}

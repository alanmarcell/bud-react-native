import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';
import R from 'ramda';

import PlantProfile from '../';
import { tree, context } from '../../../../../__mocks__/stateHelper';

describe.skip('PlantProfile', () => {
  test('renders correctly', () => {
    const [[, plant]] = R.toPairs(tree.get('plants', 'list'));
    const component = shallow(<PlantProfile plant={plant} />).dive(context);
    expect(component).toMatchSnapshot();
  });
});

import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';
import R from 'ramda';

import PlantName from '../';
import { tree } from '../../../../../__mocks__/stateHelper';

describe.skip('PlantName', () => {
  let plant;
  beforeAll(() => {
    ([[, plant]] = R.toPairs(tree.get('plants', 'list')));
  });

  test('renders correctly', () => {
    const component = shallow(<PlantName {...plant} />);
    expect(component.find('Text').children().text()).toEqual(plant.name);
    expect(component.find('Image').length).toEqual(1);
    expect(component).toMatchSnapshot();
  });

  test('renders null component when invalid gender', () => {
    const component = shallow(<PlantName {...plant} gender="invalid" />);
    expect(component.find('Image').length).toEqual(0);
    expect(component).toMatchSnapshot();
  });

  test('do not render gender when gender props is not present', () => {
    const genderlessPlant = R.omit('gender', plant);

    const component = shallow(<PlantName {...genderlessPlant} />);
    expect(component.find('Image').length).toEqual(0);
    expect(component).toMatchSnapshot();
  });
});


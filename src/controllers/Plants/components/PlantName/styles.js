import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  name: {
    color       : '$Colors.Gray',
    fontWeight  : 'bold',
    fontSize    : 14,
    marginBottom: 5,
  },
  gender: {
    marginLeft: 5,
  },
});

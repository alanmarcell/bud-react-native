import React from 'react';
import { View, Text, Image } from 'react-native';

const genderImages = {
  FEMALE: require('./images/ico-venus.png'),
  MALE  : require('./images/ico-mars.png'),
};

import styles from './styles';

export default class PlantName extends React.Component {
  static defaultProps = {
    gender: false,
  };

  static propTypes = {
    name  : React.PropTypes.string,
    gender: React.PropTypes.oneOfType([
      React.PropTypes.bool,
      React.PropTypes.string,
    ]),
    style : React.PropTypes.oneOfType([
      React.PropTypes.shape(),
      React.PropTypes.number,
    ]),
  }

  renderGender = () => {
    const { gender } = this.props;

    if (!gender || gender == 'unknown')
      return null;

    const   genderIcon = genderImages[gender];
    return !genderIcon ? null : (
      <Image
        style={styles.gender}
        source={genderIcon}
      />
    );
  }

  render() {
    const { name, style } = this.props;
    return (
      <View style={[styles.container, style]}>
        <Text style={styles.name}>{name}</Text>
        { this.renderGender() }
      </View>
    );
  }
}

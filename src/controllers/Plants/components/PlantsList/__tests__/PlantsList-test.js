import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';

import U from 'utils';
import PlantsList from '../';
import { tree, context } from '../../../../../__mocks__/stateHelper';

describe.skip('<PlantsList />', () => {
  const list   = tree.get('plants', 'list');
  const plants = U.map(U.last, U.toPairs(list));

  test('renders correctly', () => {
    const wrapper = renderer.create(<PlantsList plants={plants} />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });

  test('children struct', () => {
    const wrapper  = shallow(<PlantsList plants={plants} />, context).dive(context);
    const children = wrapper.find('StaticRenderer');

    expect(children.length).toEqual(10);
    expect(children.at(0).dive().find('PlantCardItem')).toBeTruthy();
  });
});

describe.skip('<PlantsList /> empty', () => {
  test('renders correctly', () => {
    const wrapper = shallow(<PlantsList />, context);
    expect(wrapper.find('BlankList')).toBeTruthy();
    expect(wrapper).toMatchSnapshot();
  });
});

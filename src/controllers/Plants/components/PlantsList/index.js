import React, { Component } from 'react';
import { View, FlatList, ActivityIndicator } from 'react-native';

import { I18n } from 'utils/I18n';

import Refreshable from 'utils/components/Refreshable';

import BlankList     from 'components/BlankList';
import PlantCardItem from '../PlantCardItem';

import styles from './styles';

export default class PlantList extends Component {
  static defaultProps = {
    plants: [],
  };

  static propTypes = {
    plants: React.PropTypes.array,
  }

  onPlantPress = (plantId) => {
    if (this.props.onPlantPress) {
      this.props.onPlantPress(plantId);
    }
  };

  renderItem = ({ item, index }) => {
    let listLength = this.props.plants.length;
    return (
      <PlantCardItem
        {...item}
        onPress={() => this.onPlantPress(item.id)}
        listLength={listLength}
        listIndex={parseInt(index)}
      />
    );
  };

  renderFooter = () => {
    const { loading } = this.props;
    return (
      <View style={styles.footer}>
        {loading ? <ActivityIndicator /> : null}
      </View>
    );
  };

  renderHeader = () => {
    const { plants } = this.props;
    if (plants.length <= 0) {
      return (
        <BlankList
          icon="budbuds"
          title={I18n.t('plants.blank.title')}
          message={I18n.t('plants.blank.message')}
        />
      );
    } else {
      return null;
    }
  };

  loadMore = () => {
    const { loading, loadMoreEntries, hasNextPage } = this.props;
    if (!loading && hasNextPage) {
      loadMoreEntries();
    }
  }

  render() {
    const { plants, loading, refetch } = this.props;
    return (
      <FlatList
        data={plants}
        refreshing={loading}
        refreshControl={Refreshable({ loading, refetch })}
        keyExtractor={(item, index) => index}
        renderItem={this.renderItem}
        ListHeaderComponent={this.renderHeader}
        ListFooterComponent={this.renderFooter}
        onEndReached={this.loadMore}
      />
    );
  }
}

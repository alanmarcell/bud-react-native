import React from 'react';
import uuid from 'uuid';

import { I18n } from 'utils/I18n';
import PlantEdit from './components/PlantEdit';
import SelectStrainController from '../helpers/SelectStrainController';

import { graphql, compose, propType } from 'graphql';
import { makeGet, mutationCreate } from 'graphql';
import {
  STRAIN_CREATE,
  plantFragments,
  PLANT_QUERY, PLANT_CREATE, PLANT_UPDATE
} from 'graphql';

class PlantEditController extends SelectStrainController {
  static propTypes = {
    plant: propType(plantFragments.plantBase),
    loading: React.PropTypes.bool,
  };

  static navigationOptions = {
    getTitle: ({ name }) => {
      const titleEdit = 'screens.plantEdit.title';
      const titleAdd  = 'screens.plantAdd.title';
      return I18n.T(/.*Add$/.test(name) ? titleAdd : titleEdit);
    }
  };

  get actions() {
    return {
      ...super.actions,
      savePlant: async ({ strain, mother, father, plantOrigin, ...plant}) => {
        // Create strain if necessary
        plant.strainId = await this.saveStrain(strain);
        const motherId = await this.saveStrain(mother);
        if (motherId) {
          plant.mother = { id: motherId, type: 'STRAIN' };
        }
        const fatherId = await this.saveStrain(father);
        if (fatherId) {
          plant.father = { id: fatherId, type: 'STRAIN' };
        }

        if (plantOrigin) {
          plant.plantOriginId = plantOrigin.index;
        }

        let { objectId, updatePlant, createPlant } = this.props;
        if (objectId) {
          await updatePlant({ variables: { id: objectId, input: plant }});
          this.redirectTo('plantProfile', { type: 'back' });
        } else {
          objectId = uuid.v4();
          plant.id = objectId;
          await createPlant({ variables: { input: plant }});
          this.redirectTo('plantProfile', { type: 'replace', objectId });
        }
      }
    };
  }

  renderView(props) {
    const { plant = {} } = this.props;

    const submitText = I18n.t(`screens.${this.sceneKey}.submit`);

    return (
      <PlantEdit {...props} plant={plant} submitText={submitText} />
    );
  }
}

export default compose(
  graphql(PLANT_QUERY, makeGet()),
  graphql(PLANT_UPDATE, { name: 'updatePlant' }),
  graphql(PLANT_CREATE, mutationCreate('createPlant', 'Plant', 'plants', 'UserPlants')),
  graphql(STRAIN_CREATE, { name: 'createStrain' })
)(PlantEditController);

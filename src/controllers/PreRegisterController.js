// @flow
import React from 'react';
import PreRegister from 'components/PreRegister';
import RootController from './RootController';

export default class PreRegisterController extends RootController {
  static navigationOptions = {
    title: 'screens.register.title',
  };

  get actions() {
    return {
      notBlock: {
        onContinue: () => this.redirectTo('userFeatures'),
        onTerms   : () => this.redirectTo('terms'),
        onPollicy : () => this.redirectTo('privacy'),
      }
    };
  }

  renderView(props) {
    return <PreRegister {...props} />;
  }
}

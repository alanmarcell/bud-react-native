import React from 'react';
import { I18n } from 'utils/I18n';
import AddBudToCategory from './components/AddBudToCategory';
import RootController from '../RootController';

import { graphql, propType } from 'graphql';
import { mutationCreate } from 'graphql';
import {
  categoryFragments,
  ADD_BUD_TO_CATEGORY } from 'graphql';

import { QRCODE_SEARCH } from 'graphql/qrcode';

class AddBudToCategoryController extends RootController {
  static propTypes = {
    category: propType(categoryFragments.categoryBase),
    loading: React.PropTypes.bool,
  };

  static contextTypes = {
    client: React.PropTypes.object.isRequired,
  };

  static navigationOptions = {
    getTitle: ({ name }) => {
      const titleEdit = 'screens.categoryEdit.title';
      const titleAdd  = 'screens.categoryAdd.title';
      return I18n.T(/.*Add$/.test(name) ? titleAdd : titleEdit);
    }
  };

  get actions() {
    return {
      notBlock: {
        findQrCode : async (qrCode) => {
          const { client } = this.context;
          const query = QRCODE_SEARCH;
          const { data, error } = await client.query({
            query,
            variables: { qrCode }
          });
          return error ? { error } : data.qrCode;
        }
      },
      addBudToCategory: async ({...category}) => {
        const { addBudToCategory } = this.props;
        const {budId, categoryId}  = category;  
        const result = await addBudToCategory({ variables: { budId, categoryId }});
      }
    };
  }

  renderView(props) {
    const { name } = this.props;
    const submitEdit = 'screens.categoryEdit.submit';
    const submitAdd  = 'screens.categoryAdd.submit';
    const submitText = I18n.t(/.*Add$/.test(name) ? submitAdd : submitEdit);
    return (
      <AddBudToCategory {...props} submitText={submitText} />
    );
  }
}



export default graphql(ADD_BUD_TO_CATEGORY,  mutationCreate('addBudToCategory', 
  'AddBudToCategory',
  'result',
  'UserCategories')
)(AddBudToCategoryController);

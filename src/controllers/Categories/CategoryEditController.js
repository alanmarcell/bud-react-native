import React from 'react';
import uuid from 'uuid';

import { I18n } from 'utils/I18n';
import CategoryEdit from './components/CategoryEdit';
import RootController from '../RootController';
import R from 'ramda';
import { graphql, compose, propType } from 'graphql';
import { makeGet, mutationCreate } from 'graphql';
import {
  categoryFragments,
  CATEGORY_PROFILE, CATEGORY_CREATE, CATEGORY_UPDATE
} from 'graphql';

class CategoryEditController extends RootController {
  static propTypes = {
    category: propType(categoryFragments.categoryBase),
    loading: React.PropTypes.bool,
  };

  static navigationOptions = {
    getTitle: ({ objectId }) => {
      const titleEdit = 'screens.category.edit.title';
      const titleAdd = 'screens.category.add.title';
      return I18n.T(R.isNil(objectId) ? titleAdd : titleEdit);
    }
  };

  get actions() {
    return {
      saveCategory: async ({ ...category }) => {

        let { objectId, updateCategory, createCategory } = this.props;
        if (objectId) {
          await updateCategory({ variables: { id: objectId, input: category } });
          this.goBack();
        } else {
          objectId = uuid.v4();
          category.id = objectId;
          await createCategory({ variables: { input: category } });

          this.goBack();
        }
      }
    };
  }

  renderView(props) {
    const {  objectId } = this.props;
    const submitEdit = 'screens.category.edit.submit';
    const submitAdd = 'screens.category.add.submit';
    const submitText = I18n.t(R.isNil(objectId) ? submitAdd : submitEdit);
    return (
      <CategoryEdit {...props} submitText={submitText} />
    );
  }
}

export default compose(
  graphql(CATEGORY_PROFILE, makeGet()),
  graphql(CATEGORY_UPDATE, { name: 'updateCategory' }),
  graphql(CATEGORY_CREATE, mutationCreate('createCategory', 'Category', 'categories', 'UserEditions', ['EditionProfile'])),
)(CategoryEditController);

// @flow
import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';
import NavIcon from 'scenes/NavIcon';
import RootController from '../RootController';
import {CategoryProfile} from './components/CategoryProfile';
import R from 'ramda';
import { graphql, makeGet, compose, makeDeleteMutation } from 'graphql';
import { CATEGORY_PROFILE, CATEGORY_DELETE, SESSION_QUERY } from 'graphql';
import orderBuds from 'utils/orderBuds';

const getLoggedInUserId = R.path(['session', 'user', 'id']);

const getCategoryOwnerId = props =>
  R.path(['category', 'owner', 'id'], props);

const isCategoryOwner = props =>
  getCategoryOwnerId(props) === getLoggedInUserId(props);

class CategoryProfileController extends RootController {
  static propTypes = {
    category: PropTypes.object,
    loading: PropTypes.bool,
  }

  static navigationOptions = {
    title: 'screens.category.title',
  };

  get actions() {
    return {
      deleteCategory: async (params) => {
        const destroy = await this.confirmRemove('category');
        if (destroy) {
          await this.props.deleteCategory(params);

          this.goBack('cupProfile', { type: 'reset' });
        }
      },
      onProductPress: (objectId) => {
        this.redirectTo('budProfile', { objectId });
      },
    };
  }

  componentWillUpdate(nextProps) {
    const { loading, setButtons } = nextProps;
    if (!loading && !setButtons) {
      if (isCategoryOwner(nextProps)) {
        const { objectId } = nextProps;
        const { deleteCategory } = this.actionsWithHandle();
        this.refresh({
          setButtons: true,
          renderRightButton: () => {
            return (
              <View style={{ flex: 1, flexDirection: 'row' }}>
                <NavIcon onPress={() => deleteCategory({ objectId })} iconName="trash" />
                <NavIcon onPress={() => this.redirectTo('categoryEdit', { objectId })} iconName="pencil" />
              </View>
            );
          }
        });
      }
    }
  }

  renderView(props) {
    const { loading, category } = props;

    if (loading) {
      return <ActivityIndicator />;
    }

    const buds = orderBuds(R.path(['buds'], category));

    return (
      <View style={{ flex: 1 }}>
        <CategoryProfile
          {...props}
          buds={buds}
        />
      </View>
    );
  }
}

export default compose(
  graphql(SESSION_QUERY, { props: ({ data }) => ({ ...data }) }),
  graphql(CATEGORY_PROFILE, makeGet()),
  graphql(CATEGORY_DELETE, {
    ...makeDeleteMutation('deleteCategory', 'categories', 'UserCategories'),
    options: {
      refetchQueries: [
        'UserEditions',
      ],
    },
  }),
)(CategoryProfileController);

import React,{PureComponent} from 'react';
import { ActivityIndicator } from 'react-native';
import U from 'utils';
import { I18n } from 'utils/I18n';

import { BudForms } from 'components/BudForms';
import { I18nField } from 'components/BudForms/Fields';
import QRCodeFactory from 'components/BudForms/Factories/QRCodeFactory';

import { connect } from 'react-redux';

import AddBudToCategorySchema from './AddBudToCategorySchema';

class Edit extends PureComponent {
  static propTypes = {
    categoryId: React.PropTypes.string.isRequired,
    loading: React.PropTypes.bool,
    addBudToCategory: React.PropTypes.func.isRequired,
    submitText: React.PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    const categoryId = props.categoryId;
    this.state = {
      doc: {categoryId}
    };
  }

  _handleSubmit = ({ ..._values }) => {
    const { addBudToCategory } = this.props;
    const {doc} = this.state;
    addBudToCategory({
      ...doc
    });
  };

  _handleChange = (doc) => {
    this.setState({doc});
  }

  render() {
    const { loading, findQrCode } = this.props;
    const { doc } = this.state;

    if (loading) {
      return <ActivityIndicator />;
    }

    const formProps = {
      ref: 'form',
      schema: AddBudToCategorySchema,
      doc: U.pick([
        'budId',
        'categoryId',
      ], doc),
      onChange: this._handleChange,
      onSubmit: this._handleSubmit,
      submitText: this.props.submitText,
    };

    return (
      <BudForms {...formProps} >
        <I18nField
          fieldName="budId"
          type={QRCodeFactory}
          actionType="addBudToCup"
          findQrCode={findQrCode}
          newText={I18n.t('forms.qrCode.newText')}
          oldText={I18n.t('forms.qrCode.oldText')}
        />
      </BudForms>
    );
  }
}

const mapStateToProps = state => ({
  activeUserType: state.user.userType
});

export default connect(mapStateToProps)(Edit);

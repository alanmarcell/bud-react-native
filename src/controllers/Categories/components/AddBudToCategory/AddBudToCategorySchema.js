import { SimpleSchema } from 'components/BudForms/Schemas';

const AddBudToCategorySchema = new SimpleSchema({
  categoryId: {
    type: String,
    required: true,
  },
  budId: {
    type: String,
    required: true,
  }
});

export default AddBudToCategorySchema;

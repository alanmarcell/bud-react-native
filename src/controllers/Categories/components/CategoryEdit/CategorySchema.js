import { SimpleSchema } from 'components/BudForms/Schemas';
import { NameSchema } from 'components/BudForms/Schemas';

export const CategorySchema = new SimpleSchema({
  name: {
    type: NameSchema,
    required: true,
  },

  description: {
    type: String,
    required: false,
  },

  jury: {
    type: Array,
    required: false,
  },

  'jury.$': {
    type: String,
    allowedValues: ['PUBLIC', 'PRIVATE'],
    required: false,
  },

  // qrCode: {
  //   type: String,
  //   required: false,
  // },

});

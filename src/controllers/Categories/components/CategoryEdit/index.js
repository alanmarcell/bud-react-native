import React, { PureComponent } from 'react';
import { ActivityIndicator } from 'react-native';
import U from 'utils';
import { I18n } from 'utils/I18n';
import { InputFactory } from 'utils/Form/Factories';

import { BudForms } from 'components/BudForms';
import { I18nField, DescriptionField, NameField } from 'components/BudForms/Fields';
import { AutoCheckboxFactory } from 'components/BudForms/Factories';
import QRCodeFactory from 'components/BudForms/Factories/QRCodeFactory';

import { connect } from 'react-redux';

import { CategorySchema } from './CategorySchema';

class CategoryEdit extends PureComponent {
  static propTypes = {
    category: React.PropTypes.object,
    loading: React.PropTypes.bool,
    saveCategory: React.PropTypes.func.isRequired,
    submitText: React.PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    this._buildDocState = this._buildDocState.bind(this);

    this.state = {
      doc: this._buildDocState(props.category)
    };
  }

  _handleSubmit = ({ ...values }) => {
    const { saveCategory, editionId } = this.props;
    saveCategory({
      ...values,
      editionId,
    });
  };

  _handleChange = (doc) => {
    this.setState({ doc });
  }

  _buildDocState(category = {}) {
    const { qrCode, ...doc } = category;

    return doc;
  }

  componentWillReceiveProps(nextProps, _nextState) {
    const { category } = nextProps;

    this.setState({ doc: this._buildDocState(category) });
  }

  render() {
    const { loading } = this.props;

    const { doc } = this.state;

    if (loading) {
      return <ActivityIndicator />;
    }

    const formProps = {
      ref: 'form',
      schema: CategorySchema,
      doc: U.pick([
        'name',
        'description',
        'jury',
        // 'qrCode',
      ], doc),
      onChange: this._handleChange,
      onSubmit: this._handleSubmit,
      submitText: this.props.submitText,
    };

    return (
      <BudForms {...formProps} >
        <NameField i18nScope="forms.categoryName" />

        <DescriptionField />

        <I18nField
          fieldName="jury"
          i18nScope="forms.juryType"
          type={AutoCheckboxFactory}
          options={['PUBLIC', 'PRIVATE']}
        />

        {/* <I18nField
          fieldName="qrCode"
          type={QRCodeFactory}
          findQrCode={findQrCode}
          newText={I18n.t('forms.qrCode.newText')}
          oldText={I18n.t('forms.qrCode.oldText')}
        /> */}
      </BudForms>
    );
  }
}

const mapStateToProps = state => ({
  activeUserType: state.user.userType
});

export default connect(mapStateToProps)(CategoryEdit);

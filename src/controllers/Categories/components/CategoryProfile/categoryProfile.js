import React, { PureComponent } from 'react';

import Refreshable from 'utils/components/Refreshable';
import styles from './styles';
import { View, ActivityIndicator, ScrollView, } from 'react-native';

import { ProductHeader } from 'components';
import { productsList } from 'components';
import BudCardItem from 'controllers/Buds/components/BudCardItem';
const BudsList = productsList(BudCardItem);

class CategoryProfile extends PureComponent {
  static propTypes = {
    category: React.PropTypes.object,
    loading: React.PropTypes.bool,
  }

  render() {
    const {
      loading,
      refetch,
      category,
      buds,
      icon,
      onProductPress
    } = this.props;

    if (loading) {
      return <ActivityIndicator />;
    }

    const details = [{
      icon: 'judge',
      text: `${category.jury}`,
    }];

    const listProps = {
      products: buds,
      refreshControl: { loading, refetch },
      onProductPress,
      productsType: 'buds',
      icon: 'budbuds',
      blakListProps:{
        title: 'screens.category.buds',
        message: 'screens.category.buds'
      }
    };

    return (
      <ScrollView refreshControl={Refreshable({ loading, refetch })}>
        <View style={styles.container}>
        <ProductHeader {...this.props} productType="category" details={details} />
        </View>

        <BudsList {...listProps} />
        {/* <List {...this.props} productsType="judges" /> */}
      </ScrollView>
    );
  }
}
export {CategoryProfile};
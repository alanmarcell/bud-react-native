import { ActionConst } from 'react-native-router-flux';

const initialState = {
  scene: {},
};

export default function routes(state = initialState, action = {}) {
  switch (action.type) {
    // TODO: is it really necessary? 
    // react-native-router-flux does not take care of it
    //
    // focus action is dispatched when a new screen comes into focus
    case ActionConst.FOCUS:
      return {
        ...state,
        scene: action.scene,
      };

    default:
      return state;
  }
}

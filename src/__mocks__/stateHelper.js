const context = {};

const plants = {
  list: {
    plant1: {
      _id: '57a0727f0e4dab0011b5f03c',
      name: 'Hk',
      nick: 'HK11',
      qr_code: '6d8477fe36e156f7bae6ff4a94155ccd639d2bb7',
      harvested_at: new Date('2016-08-30T21:00:00.000Z'),
      description: '',
      environment: 'outdoor',
      genetics: 'auto',
      strain: {
        '_id': '58c1b97acc558700178cab73',
        'name': 'Hindu Kush'
      },
      mother: {
        '_id': '57a060870e4dab0011b5f032',
        'name': 'HK Master # 1',
      },
      specie: ['indica'],
      'bornAt': new Date('2017-11-19T22:51:20.594Z'),
      'changedStageAt': new Date('2017-11-19T22:51:20.594Z'),
      stage: [
        {
          created_at: '2017-03-09T23:33:19.017Z',
          type: 'flowering'
        }
      ]
    }
  }
};

const buds = {
  list: {
    bud1: {
      _id: 'bud_1_id',
      name: 'bud 1 name',
      qr_code: '6d8477fe36e156f7bae6ff4a94155ccd639d2bb7',
      harvestedAt: new Date('2016-08-30T21:00:00.000Z'),
      description: '',
      strain: {
        '_id': '58c1b97acc558700178cab73',
        'name': 'Hindu Kush'
      },
      mother: {
        '_id': '57a060870e4dab0011b5f032',
        'name': 'HK Master # 1',
      },
      owner: {
        id: 'owner id'
      },
      reviews: {
        edges: []
      }
    }
  },
  newBud: {
    harvestedAt: new Date('2017-11-19T20:42:41.842Z')
  }
};

const mixes = [
  {
    _id: '57bc57b7e6fcf5001184bdb2',
    created_on: new Date('2016-08-23T14:03:35.189Z'),
    description: '25 litros',
    fertilizers: {
      '0': {
        'measurement': 'gl',
        'name': 'Maxigrow',
        'qty': 0.6000000238418579,
      },
      '1': {
        'measurement': 'mll',
        'name': 'Bioheaven',
        'qty': 3.200000047683716,
      },
      '2': {
        'measurement': 'mll',
        'name': 'Biogrow',
        'qty': 0.7200000286102295,
      }
    },
    modified_on: new Date('2016-08-23T14:03:35.189Z'),
    name: 'Vega',
    qr_code: 'a6043977e1e60ef0fae77e746cc009f30df95c66'
  }
];

const tree = {
  get: (store, type) => {
    switch (store) {
      case 'plants': return plants[type];
      case 'buds': return buds[type];
      default: return plants[type];
    }
  }
};

const stores = {
  mixes: {
    find: () => mixes
  }
};

export {
  context,
  tree,
  stores
};

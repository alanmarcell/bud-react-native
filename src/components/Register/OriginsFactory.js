import React, { Component } from 'react';
import { View } from 'react-native';
import SearchOriginFactory from 'controllers/factories/SearchOriginFactory';
import { I18nField } from 'components/BudForms/Fields';
import { I18n } from 'utils/I18n';
import R from 'ramda';
import { SelectFactory } from 'utils/Form/Factories';

export class OriginsFactory extends Component {
  static defaultProps = {
    fieldName: 'origin',
    origins: [],
  }

  static propTypes = {
    origins: React.PropTypes.array
  };

  constructor(props) {
    super(props);
    this.selectOriginCb = this.selectOriginCb.bind(this);
    this.searchOriginCb = this.searchOriginCb.bind(this);
    this.state = {showSearch: false};
  }

  selectOriginsOptions() {
    const { origins } = this.props;

    const hasCode = (origin) => origin.code;
    const noCodeOri = R.reject(hasCode, origins);

    return R.map(({ id, name: label }) => {
      return {
        key: id,
        label //I18n.t(`screens.register.origins.${label}`)
      };
    }, R.append({ id: null, name: 'Growshops' }, noCodeOri));
  }

  renderModal(showSearch) {
    if (!showSearch)
      return null;

    return (
      <I18nField
        fieldName={this.props.fieldName}
        onChangeCb={this.searchOriginCb}
        type={SearchOriginFactory}
        placeholder={I18n.t('forms.origin.select')} 
      />);
  }

  searchOriginCb() {
    this.setState({ showSearch: false });
  }

  selectOriginCb(args) {
    if (args.label === 'Growshops') {
      this.setState({ showSearch: true });
    } else {
      this.setState({ showSearch: false });
    }
  }

  render() {

    return (
      <View>
        {this.renderModal(this.state.showSearch)}
        <I18nField
          fieldName={this.props.fieldName}
          type={SelectFactory}
          options={this.selectOriginsOptions()}
          onChangeCb={ this.selectOriginCb}  
          placeholder={I18n.t('forms.origin.select')} />          
      </View>
    );
  }

}

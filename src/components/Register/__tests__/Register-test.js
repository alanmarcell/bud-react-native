import React from 'react';
import { shallow } from 'enzyme';

import Target from '../';
import { tree } from '../../../__mocks__/stateHelper';

test.skip('renders correctly', () => {
  const props = {
    session      : tree.get('session'),
    onUpdateData : () => {},
    onRegister   : () => {},
    onTerms      : () => {},
    onPollicy    : () => {},
  };
  const component = shallow(<Target {...props} />);
  expect(component).toMatchSnapshot();
});

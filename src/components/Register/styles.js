import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 30,
  },

  markdownContainer: {
    padding: 15,
  },

  paragraph: {
    textAlign: 'center',
    fontSize: 12,
  }
});

import React, { Component } from 'react';
import { View, ActivityIndicator } from 'react-native';
import { I18n } from 'utils/I18n';
import moment from 'moment';

import I18nField from 'components/BudForms/Fields/I18nField';
import { BudForms, SimpleSchema } from 'components/BudForms';
import {
  EmailField,
  PasswordField,
  NameField,
  BirthDateField
} from 'components/BudForms/Fields';
import {
  EmailSchema,
  RegisterPasswordSchema,
  NameSchema,
  BirthDateSchema
} from 'components/BudForms/Schemas';

import { OriginsFactory } from './OriginsFactory';

import { SelectFactory } from 'utils/Form/Factories';

import styles from './styles';
import MarkdownBox  from 'components/MarkdownBox';

export default class Register extends Component {
  static defaultProps = {
    origins: [],
  };
  static propTypes = {
    origins    : React.PropTypes.array,
    loading    : React.PropTypes.bool,
    onRegister : React.PropTypes.func.isRequired,
    onTerms    : React.PropTypes.func.isRequired,
    onPollicy  : React.PropTypes.func.isRequired,
  };

  _handleSubmit = (values) => {
    const { email, password, origin = {}, userType = {}, ...user } = values;
    if (origin) user.origin = { id: origin.key };
    user.userType = userType.key ? [userType.key] : ['GROWER'];
    user.credentials = { email, password };
    this.props.onRegister(user);
  };

  _handleLink = (target) => {
    if (target === 'terms') {
      this.props.onTerms();
    } else {
      this.props.onPollicy();
    }
  };

  renderFooter = () => {
    return (
      <View style={styles.markdownContainer}>
        <MarkdownBox styles={styles} onOpenLink={this._handleLink}>
          {I18n.t('screens.register.text')}
        </MarkdownBox>
      </View>
    );
  };

  buildSchema = () => {
    return {
      'name'    : NameSchema,
      'email'   : EmailSchema,
      'password': RegisterPasswordSchema,
      'bornAt'  : BirthDateSchema,
      'origin'  : {
        type: Object,
        required: false,
      },
      'userType': {
        type: Object,
        required: false
      },
      'userType.key': {
        type: String,
        allowedValues: ['GROWER', 'CONSUMER', 'GARDENER', 'CLUB'],
      },
      'userType.label': String,
      'origin.key': {
        type: String,
      },
      'origin.label': String
    };
  }

  userTypeOptions() {
    return [
      {
        key: 'GROWER',
        label: I18n.t('screens.register.user_types.GROWER')
      },
      {
        key: 'CONSUMER',
        label: I18n.t('screens.register.user_types.CONSUMER')
      },
      /*{
        key: 'GARDENER',
        label: I18n.t('screens.register.user_types.GARDENER')
      },
      {
        key: 'CLUB',
        label: I18n.t('screens.register.user_types.CLUB')
      },*/
    ];
  }

  render() {
    const { loading } = this.props;

    if (loading) {
      return <ActivityIndicator />;
    }

    const formProps = {
      ref           : 'form',
      doc           : {
        bornAt: moment().subtract(18, 'year').toDate()
      },
      schema        : new SimpleSchema(this.buildSchema()),
      onSubmit      : this._handleSubmit,
      submitText    : I18n.t('screens.register.submit'),
      renderFooter  : this.renderFooter,
    };

    return (
      <BudForms {...formProps} >
        <NameField maxLength={40} />
        <EmailField />
        <PasswordField />
        <BirthDateField />
        <I18nField
          fieldName="userType"
          i18nScope="forms.user_type"
          options={this.userTypeOptions()}
          type={SelectFactory}
          initValue={I18n.t('screens.register.user_types.GROWER')}
        />
        <OriginsFactory origins={this.props.origins} />
      </BudForms>
    );
  }
}

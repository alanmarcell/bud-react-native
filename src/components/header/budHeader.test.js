import { isNil, last, length, reduce, take } from 'ramda';

const toPairs = list => {
  return reduce((pairs, item) => {
    const lastPair = last(pairs);

    if(isNil(lastPair)){
      return [[item]];
    }

    switch(length(lastPair)){
      case 1: {
        const allExceptLast = take(length(pairs) -1, pairs);
        return allExceptLast.concat([lastPair.concat(item)]);
      }
      case 2: return pairs.concat([[item]]);
      default: return pairs;
    }

  }, [], list); 
};

it('[[1,2],[3,4]]', () => {
  const pairs = toPairs([1, 2, 3, 4]);
  expect(pairs).toEqual([[1, 2], [3, 4]]);
});

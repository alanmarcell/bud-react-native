import React, { PureComponent, PropTypes } from 'react';
import { View, Text } from 'react-native';
import moment from 'moment';
import { I18n } from 'utils/I18n';
import toPairs from 'utils/toPairs';
import R from 'ramda';
import v4 from 'uuid/v4';
import { ProductHeader } from 'components';
import GaleryPreview from 'components/GaleryPreview';

import Icon from 'components/Icon';

import styles from './styles';

const profileMap = {
  seed: 'seed',
  clone: 'clone',
  batch: 'group',
  vegetative: 'vegetating',
  flowering: 'flowering',
};

const renderItemIcon = (item) => {
  if (item) {
    item = item.toLowerCase();
    return profileMap[item];
  }
  return 'budbuds';
};

const isEven = (n) => n % 2 === 0;

const infoListCol = position =>
  class InfoListCol extends PureComponent {
    render() {
      const { icon, text } = this.props;
      const style = position === 'left'
        ? styles.infoListLeftCol
        : styles.infoListRightCol;

      return (
        <View style={style}>
          <Icon style={styles.infoIcon} name={icon} />
          <Text style={styles.infoLabel}>{text}</Text>
        </View>
      );
    }
  };

const InfoListLeftCol = infoListCol('left');
const InfoListRightCol = infoListCol('right');

const infoRow = (InfoListLeftCol, InfoListRightCol) =>
  class InfoRow extends PureComponent {
    render() {
      const { row, index } = this.props;
      const leftInfo = row[0];
      const rightInfo = row[1] || {};
      return (
        <View style={[styles.infoListRow, isEven(index) && styles.infoListRowGrayBackground]}>
          <InfoListLeftCol icon={leftInfo.icon} text={leftInfo.text} />
          {rightInfo.text &&
            <InfoListRightCol icon={rightInfo.icon} text={rightInfo.text} />
          }
        </View>
      );
    }
  };

const InfoRow = infoRow(InfoListLeftCol, InfoListRightCol);

const renderInfoList = (bud) => {
  const { harvestedAt, score } = bud;
  const mother = R.pathOr(null, ['mother', 'name'], bud);
  const genetic = R.pathOr(null, ['mother', 'genetic'], bud);
  const source = R.pathOr(null, ['mother', 'source'], bud);
  const environment = R.pathOr(null, ['mother', 'environment'], bud);
  const stage = R.pathOr(null, ['mother', 'stage'], bud);

  const roundedScore = String(score ? score.toFixed(2) : 0);
  const date = new Date();
  let week = moment().diff(date, 'weeks') + 1;

  const { countReviews } = bud.reviewResumed;

  const infos = [{
    icon: 'budbuds',
    text: mother,
  },
  {
    icon: 'genetics',
    text: genetic ? I18n.t('forms.genetic.' + genetic) : null
  }, {
    icon: renderItemIcon(source),
    text: source ? I18n.t('forms.source.' + source) : null,
  },
  {
    icon: 'location',
    text: environment ? I18n.t('forms.environment.' + environment) : null
  }, {
    icon: renderItemIcon(stage),
    text: stage ? I18n.t('forms.stage.' + stage) : null,
  },
  {
    icon: 'calendar',
    text: week ? I18n.t('screens.plant.week', { week }) : null
  }, {
    icon: 'star',
    text: `${countReviews} ${I18n.t('screens.reviews.my_review')}`,
  },
  {
    icon: 'trophy',
    text: roundedScore
  }, {
    icon: 'shopping-basket',
    text: moment(harvestedAt).format('L')
  }];

  const rows = toPairs(R.filter(({ text }) => !R.isNil(text), infos));

  const renderRows = () => rows.map((row, index) =>
    <InfoRow key={v4()} row={row} index={index} />
  );

  return (
    <View style={styles.infoList}>
      {renderRows()}
    </View>
  );
};

class BudHeader extends PureComponent {
  static propTypes = {
    bud: PropTypes.object.isRequired,
    openQuickID: PropTypes.func.isRequired,
    onToggleExecute: PropTypes.func,
    style: PropTypes.oneOfType([
      PropTypes.shape(),
      PropTypes.number,
    ]),
  }

  render() {
    const { bud: { files }, openGalery, bud } = this.props;

    const showGalery = openGalery && files && files.length !== 0;

    return (
      <View style={styles.container}>
        <ProductHeader {...this.props} productType="bud" />
        {renderInfoList(bud)}
        {showGalery && <GaleryPreview files={files} openGalery={openGalery} />}
      </View>
    );
  }
}

export default BudHeader;
export { BudHeader };
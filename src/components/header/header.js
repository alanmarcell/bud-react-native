
import React from 'react';
import styles from './styles';
import stylesHelpers from '../../themes/helpers';
import R from 'ramda';
import { View, Text } from 'react-native';
import { I18n } from 'utils/I18n';
import PropTypes from 'prop-types';

import Icon from 'components/Icon';
import PlantName from 'controllers/Plants/components/PlantName';
import InfoModal from 'components/InfoModal';
import Logo from 'components/Logo';

class Errors extends React.PureComponent {
  render() {
    const { object } = this.props;
    const { synced_errors } = object;
    if (synced_errors && synced_errors.length > 0) {
      const msgs = synced_errors.map((error, index) => (
        <Text key={index} >- {I18n.t(error.msg, { defaultValue: error.msg, object })}</Text>
      ));
      return (
        <View>{msgs}</View>
      );
    }

    return null;
  }
}

class Header extends React.PureComponent {
  render() {
    const { productType } = this.props;
    const product = this.props[productType];

    return (
      <View style={styles.header}>
        <View style={styles.headerWrapper}>
          {this.props.children}
        </View>
        <Errors object={product} />
      </View>
    );
  }
}

class Title extends React.PureComponent {
  render() {
    return (
      <View style={[stylesHelpers.middleColumn, stylesHelpers.alignCenterLeft]}>
        <PlantName name={this.props.title} />
        <Text>{this.props.subTitle}</Text>
      </View>
    );
  }
}

class Description extends React.PureComponent {
  render() {
    if (R.isEmpty(this.props.description)) {
      return null;
    }

    return (
      <View style={[stylesHelpers.rightColumn, stylesHelpers.alignCenterRight]}>
        <InfoModal title={this.props.title} >
          <Text>{this.props.description}</Text>
        </InfoModal>
      </View>
    );
  }
}

class Details extends React.PureComponent {
  render() {
    return (
      <View style={styles.infoList}>
        {
          this.props.rows.map((row, i) => (
            <View style={styles.infoListRow} key={i}>
              <Icon name={row.icon} style={styles.infoIcon} />
              <Text style={styles.infoLabel}>{row.text}</Text>
            </View>
          ))
        }
      </View>
    );
  }
}

class QrCode extends React.PureComponent {
  render() {
    const { onPress } = this.props;

    if(!R.isNil(onPress)) {
      return (
        <View style={[stylesHelpers.rightColumn, stylesHelpers.alignCenterRight]}>
          <Icon onPress={onPress} name="qrcode" color={styles._qrcode.color} size={styles._qrcode.fontSize} />
        </View>
      );
    }
    return null;
  }
}

const getLogo = product => {
  if (!R.isEmpty(product.logo)) {
    return product.logo;
  }

  if (!R.isEmpty(R.path(['edition', 'logo'], product))) {
    return product.edition.logo;
  }

  if (!R.isEmpty(R.path(['edition', 'cup', 'logo'], product))) {
    return product.edition.cup.logo;
  }

  return null;
};

class ProductHeader extends React.PureComponent {
  static propTypes = {
    productType: PropTypes.string.isRequired
  }

  render() {
    const { openQuickID, productType, details = [] } = this.props;
    const product = this.props[productType];
    const { name, description } = product;

    const strain = R.pathOr('', ['strain', 'name'], product);

    const logo = getLogo(product);

    return (
      <View>
        <Header {...this.props}>
          <Logo image={logo} />
          <Title title={name} subTitle={strain} />
          <Description title={name} description={description} />
          <QrCode onPress={openQuickID} />
        </Header>
        <Details rows={details} />
      </View>
    );
  }
}

export default ProductHeader;

export {
  Header,
  ProductHeader,
  Errors,
  Title,
  Description,
  QrCode
};

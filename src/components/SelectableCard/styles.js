// @flow
import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  container: {
  },

  'container:first-child': {
    marginTop: 10,
  },

  cardWrapper: {
    backgroundColor: '$Colors.cardsBackground',
    flexDirection: 'row',
    height: 70,
    marginHorizontal: 10,
    marginBottom: 5,
    paddingRight: 10,
    paddingVertical: 10,
  },

  selectedCard: {
    backgroundColor: '$Colors.GreenLowOpacity',
    height: 70,
    position: 'absolute',
    top: 0, bottom: 5,
    left: 0, right: 0,
  },

  executed: {
    opacity: 0.5,
  },

  icon: {
    marginLeft: 19,
    marginTop: 10,
  }

});

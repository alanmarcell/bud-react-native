import React, { Component, PropTypes } from 'react';
import { View } from 'react-native';

import { Touchable } from 'utils/components';
import RoundedIcon from 'components/RoundedIcon';

import EStyleSheet from 'react-native-extended-stylesheet';
import styles from './styles';

export default class SelectableCard extends Component {
  static defaultProps = {
    selected   : false,
    listLength : 1,
    listIndex  : 0,
    executed   : false,
  };

  static propTypes = {
    onPress   : PropTypes.func,
    selected  : PropTypes.bool,
    executed  : PropTypes.bool,
    listLength: PropTypes.number,
    listIndex : PropTypes.number,
  };

  renderSelected() {
    if (this.props.selected) {
      return (
        <View style={styles.selectedCard}>
          <RoundedIcon style={styles.icon} selected />
        </View>
      );
    }
    return null;
  }

  render() {
    const { listLength, listIndex, onPress = () => {}, executed } = this.props;
    const style = EStyleSheet.child(styles, 'container', listIndex, listLength);
    const subview = [styles.cardWrapper];
    if (executed) {
      subview.push(styles.executed);
    }

    return (
      <Touchable style={style} onPress={onPress}>
        <View style={subview}>
          {this.props.children}
        </View>
        { this.renderSelected() }
      </Touchable>
    );
  }
}

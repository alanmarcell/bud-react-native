import React, { Component } from 'react';
import { View, Image, Text } from 'react-native';
import { I18n } from 'utils/I18n';

import { BudForms, SimpleSchema } from 'components/BudForms';
import Button from 'components/Button';
import { EmailField, PasswordField } from 'components/BudForms/Fields';
import { EmailSchema, LoginPasswordSchema } from 'components/BudForms/Schemas';
import { connect } from 'react-redux';

import styles from './styles';
const logo = require('./images/logo.png');

const SessionSchema = new SimpleSchema({
  email: EmailSchema,
  password: LoginPasswordSchema,
});

class SignIn extends Component {
  static propTypes = {
    onSignIn: React.PropTypes.func.isRequired,
    onPasswordRecovery: React.PropTypes.func.isRequired,
    onRegister: React.PropTypes.func.isRequired,
  };

  state = {
    email: ''
  }

  renderFooter = () => {
    const { onRegister, onPasswordRecovery } = this.props;
    return (
      <View>
        <Button theme="purple" type="normal" onPress={onRegister}>
          {I18n.t('screens.sign-in.register')}
        </Button>
        <Text style={styles.lostButton} onPress={onPasswordRecovery}>
          {I18n.t('screens.sign-in.lost-password')}
        </Text>
      </View>
    );
  };

  handleEmailChange = (text) => {
    this.setState({ email: text });
  }

  async componentWillMount() {
    this.setState({ email: this.props.email });
  }

  render() {
    const { onSignIn } = this.props;
    const formProps = {
      ref: 'form',
      schema: SessionSchema,
      onSubmit: onSignIn,
      submitText: I18n.t('screens.sign-in.submit'),
      renderFooter: this.renderFooter,
    };

    return (
      <BudForms {...formProps} >
        <Image style={styles.image} source={logo} />
        <EmailField
          onChangeText={this.handleEmailChange}
          value={this.state.email} />
        <PasswordField />
      </BudForms>
    );
  }
}

const mapStateToProps = state => ({
  email: state.user.email
});

export default connect(mapStateToProps)(SignIn);

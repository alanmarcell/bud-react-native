import React from 'react';
import { shallow } from 'enzyme';

import Target from '../';
import { tree, context } from '../../../__mocks__/stateHelper';

test.skip('renders correctly', () => {
  const props = {
    session: tree.get('session'),
    onSignIn: () => {},
    onUpdateData: () => {},
    onPasswordRecovery: () => {},
    onRegister: () => {},
  };
  const component = shallow(<Target {...props} /> , context);
  expect(component).toMatchSnapshot();
});

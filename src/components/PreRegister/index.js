import React, { Component } from 'react';
import { ScrollView, View, Text } from 'react-native';

import Button       from 'components/Button';
import RoundedIcon  from 'components/RoundedIcon';
import Icon         from 'components/Icon';
import MarkdownBox  from 'components/MarkdownBox';
import { I18n } from 'utils/I18n';

import styles from './styles';

export default class PreRegister extends Component {
  static propTypes = {
    onPollicy: React.PropTypes.func.isRequired,
    onTerms: React.PropTypes.func.isRequired,
    onContinue: React.PropTypes.func.isRequired,
  };

  _onContinue = () => {
    this.props.onContinue();
  }

  _handleLink = (target) => {
    if (target === 'terms') {
      this.props.onTerms();
    } else {
      this.props.onPollicy();
    }
  };

  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.card}>
          <RoundedIcon source="lock"
            iconStyle={styles._lockerIcon}
            containerStyle={styles.lockerIconContainer}
            style={styles.lockerIconDropshadow}
          />
          <MarkdownBox onOpenLink={this._handleLink}>{I18n.t('screens.pre-register.text')}</MarkdownBox>
        </View>
        <Button onPress={this._onContinue} theme="purple" type="bordered" style={styles.buttonWrapperInlineChildren}>
          <Text style={styles.buttonText}>{I18n.T('screens.pre-register.continue_button')}</Text>
          <Icon name="arrow-right" style={styles.buttonIcon} />
        </Button>
      </ScrollView>
    );
  }
}

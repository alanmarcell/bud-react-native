import React from 'react';
import { SliderFactory } from 'utils/Form/Factories';

/**
 * Intermediate component for SliderFactory customization
 */
export default class BudSliderFactory extends React.Component {
  static defaultProps = {
    ...SliderFactory.defaultProps,
  };

  static propTypes = {
    ...SliderFactory.propTypes,
  };

  render() {
    const passProps = {
      ...this.props.passProps,
      // trackStyle: styles.sliderTrack,
      // thumbStyle: styles.sliderThumb,
      // debugTouchArea: true,
    };
    return <SliderFactory {...this.props} passProps={passProps} />;
  }
}

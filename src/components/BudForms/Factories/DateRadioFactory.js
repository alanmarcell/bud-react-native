import React from 'react';
import { View } from 'react-native';

import DateFactory from 'utils/Form/Factories/DateFactory';
import { RadioOption } from 'utils/Form/Factories/RadioFactory';

export default class DateRadioFactory extends DateFactory {
  static propTypes = {
    ...DateFactory.propTypes,
    selected: React.PropTypes.bool,
  }

  renderContent(onPress, content) {
    const { selected } = this.props;
    return (
      <View style={{paddingTop: 8, paddingBottom: 8}}>
        <RadioOption
          underline={false}
          checked={selected}
          label={content}
          onPress={onPress}
        />
      </View>
    );
  }
}

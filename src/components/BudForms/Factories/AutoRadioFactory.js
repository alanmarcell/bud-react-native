import React from 'react';

import { RadioFactory } from 'utils/Form/Factories';
import AutoOptionsFactory from './AutoOptionsFactory';

export default class AutoRadioFactory extends AutoOptionsFactory {
  static propTypes = {
    ...RadioFactory.propTypes,
    ...AutoOptionsFactory.propTypes,
  };

  render() {
    const props = this.makeOptions();
    return (
      <RadioFactory {...props} />
    );
  }
}

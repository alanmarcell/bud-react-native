import React from 'react';

import { SelectFactory } from 'utils/Form/Factories';
import AutoOptionsFactory from './AutoOptionsFactory';

export default class AutoSelectFactory extends AutoOptionsFactory {
  static propTypes = {
    ...SelectFactory.propTypes,
    ...AutoOptionsFactory.propTypes,
  };

  getAllowValues() {
    const { schema, fieldName } = this.props;
    return schema.getAllowedValuesForKey(`${fieldName}.key`);
  }

  render() {
    const props = this.makeOptions('key');
    return (
      <SelectFactory {...props} />
    );
  }
}

import React from 'react';

import { CheckboxFactory } from 'utils/Form/Factories';
import AutoOptionsFactory from './AutoOptionsFactory';

export default class AutoCheckboxFactory extends AutoOptionsFactory {
  static propTypes = {
    ...CheckboxFactory.propTypes,
    ...AutoOptionsFactory.propTypes,
  };

  render() {
    const props = this.makeOptions();
    return (
      <CheckboxFactory {...props} />
    );
  }
}

import React, { Component } from 'react';
import { I18n } from 'utils/I18n';
import R from 'ramda';

export default class AutoOptionsFactory extends Component {
  static propTypes = {
    i18nScope: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.array,
    ]),
  };

  getAllowValues() {
    const { schema, fieldName } = this.props;
    return schema.getAllowedValuesForKey(fieldName);
  }

  makeOptions(keyName = 'value') {
    let { options, i18nScope, ...props } = this.props;
    if (!R.isNil(i18nScope) && R.is(Function, i18nScope.join)) {
      i18nScope = i18nScope.join('.');
    }

    if (!options) {
      const allowValues = this.getAllowValues();

      if (allowValues) {
        options = R.map((value) => (
          { [keyName]: value, label: I18n.t(`${i18nScope}.${value}`)}
        ), allowValues);
      }
    }

    return { ...props, options };
  }
}

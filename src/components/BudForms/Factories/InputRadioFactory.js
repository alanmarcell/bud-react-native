import React from 'react';
import { View } from 'react-native';

import { InputFactory } from 'utils/Form/Factories';
import { RadioOption } from 'utils/Form/Factories/RadioFactory';

class Radio extends RadioOption {
  renderContainer() {
    return (
      <View style={{ paddingTop: 8 }}>
        {this.renderIcon()}
      </View>
    );
  }
}

export default class InputRadioFactory extends InputFactory {
  static propTypes = {
    ...InputFactory.propTypes,
    selected: React.PropTypes.bool,
  };

  inputProps() {
    const props = super.inputProps();
    props.ref = (f) => this.field = f;
    return props;
  }

  _handlePress = () => {
    this.field.focus();
  };

  renderContent(_onPress, content) {
    return super.renderContent(this._handlePress, content);
  }

  renderInput() {
    const { selected } = this.props;
    return (
      <View style={{flexDirection: 'row', flex: 1}}>
        <Radio underline={false} checked={selected} label={false} />
        {super.renderInput()}
      </View>
    );
  }
}

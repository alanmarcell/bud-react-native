import { CompoundFactory } from 'components/BudForms/Factories';
import Pesticides from 'controllers/factories/Pesticides';

export default class PesticideFactory extends CompoundFactory {
  static defaultProps = {
    fieldName: 'pesticides',
    factory: Pesticides
  }
}


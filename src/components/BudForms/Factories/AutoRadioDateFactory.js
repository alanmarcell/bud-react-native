import React from 'react';

import { RadioDateFactory } from 'utils/Form/Factories';
import AutoOptionsFactory from './AutoOptionsFactory';

export default class AutoRadioDateFactory extends AutoOptionsFactory {
  static propTypes = {
    ...RadioDateFactory.propTypes,
    ...AutoOptionsFactory.propTypes,
  };

  render() {
    const props = this.makeOptions();
    return (
      <RadioDateFactory {...props} />
    );
  }
}

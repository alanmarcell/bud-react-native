import React from 'react';
import { View } from 'react-native';

import BaseFactory from 'utils/Form/Factories/BaseFactory';
import EStyleSheet from 'react-native-extended-stylesheet';

import Stars from 'components/Stars';

export default class ScoreFactory extends BaseFactory {
  static defaultProps = {
    ...BaseFactory.defaultProps,
    form     : {},
    value    : 1,
    underline: false,
  };

  static propTypes = {
    ...BaseFactory.propTypes,
    value: React.PropTypes.number,
  };

  renderField() {
    const { value } = this.state;
    const { fieldName } = this.props;

    return(
      <View key={fieldName} style={styles.container}>
        <Stars
          selecteds={value}
          onPress={this.lazyChange}
          fieldName={fieldName}
          startSize={24}
        />
      </View>
    );
  }
}

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    marginVertical: 15,
  }
});


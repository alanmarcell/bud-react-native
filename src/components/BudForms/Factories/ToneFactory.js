import React from 'react';
import { View } from 'react-native';
import _ from 'lodash';

import Slider from 'utils/Slider';
import SliderFactory from 'utils/Form/Factories/SliderFactory';
import EStyleSheet from 'react-native-extended-stylesheet';
import LinearGradient from 'react-native-linear-gradient';
import chroma from 'chroma-js';

export const domain = { min: 1, max: 100 };
export const colors = chroma
  .scale(['#FF6D1E', '#8CC63F', '#009245', '#662D91', '#603813'])
  .domain([domain.min, domain.max])
  .colors(domain.max);
export const gradient = {
  locations: _.range(0, 1, 1 / domain.max),
  start: {x: 0.0, y: 0.0},
  end: {x: 1, y: 0},
};

export default class ToneFactory extends SliderFactory {
  renderLabel() {
    return null;
  }

  inputProps() {
    const { value } = this.state;
    return {
      value,
      minimumValue: domain.min,
      maximumValue: domain.max,
      step: 1,
      onValueChange: this.handleChange,
      onSlidingComplete: this.lazyChange,
      style: styles.slider,
      maximumTrackTintColor: 'transparent',
      minimumTrackTintColor: 'transparent',
      thumbTintColor: 'white',
      thumbTouchSize: {
        width: 30,
        height: 30,
      }
    };
  }

  renderValue() {
    let { value } = this.state;
    const color = colors[value - 1];

    return (
      <View style={[styles.color, { backgroundColor: color }]}></View>
    );
  }

  renderSlider() {
    return (
      <View style={styles.sliderContainer}>
        <LinearGradient
          { ...gradient }
          style={styles.colorsBar}
          colors={colors}
        >
        <Slider
          {...this.inputProps() }
          thumbStyle={styles.thumbStyle}
        />
        </LinearGradient>
      </View>
    );
  }

  renderField() {
    const { fieldName } = this.props;
    return (
      <View key={fieldName} style={styles.container}>
        {this.renderValue()}
        {this.renderSlider()}
      </View>
    );
  }
}

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },

  slider: {
    position: 'relative',
    marginTop: -15,
  },

  sliderContainer: {
    flex: 1,
    height: 25,
  },

  thumbStyle: {
    position: 'relative',
    borderWidth: 1,
    borderColor: '#E2E2E2',
    height: 25,
    width: 25,
    borderRadius: 25 / 2,
  },

  colorsBar: {
    marginTop: 5,
    height: 15,
  },

  color: {
    width: 25,
    height: 25,
    marginRight: 7,
    alignSelf: 'center',
    borderRadius: 5,
  },
});

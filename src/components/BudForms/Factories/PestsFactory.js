import { CompoundFactory } from 'components/BudForms/Factories';
import Pest from 'controllers/factories/Pest';

export default class PestFactory extends CompoundFactory {
  static defaultProps = {
    fieldName: 'pest',
    factory: Pest
  }
}


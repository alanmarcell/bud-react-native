import React from 'react';
import { Text, View } from 'react-native';
import Modal from 'react-native-animated-modal';
import { Touchable } from 'utils/components';
import U from 'utils';

import Icon from 'components/Icon';
import BaseFactory from 'utils/Form/Factories/BaseFactory';
import EStyleSheet from 'react-native-extended-stylesheet';
import QrCodeReader from 'components/QrCodeReader';
import Button from 'components/Button';

export default class QRCodeFactory extends BaseFactory {
  static defaultProps = {
    ...BaseFactory.defaultProps,
    value  : null,
    form   : {},
    newText: 'Scan one or let us generate',
    oldText: 'Scan a new one',
    cancelText: 'Cancel',
  };

  static propTypes = {
    ...BaseFactory.propTypes,
    value      : React.PropTypes.string,
    objectId   : React.PropTypes.string,
    newText    : React.PropTypes.string,
    oldText    : React.PropTypes.string,
    cancelText : React.PropTypes.string,
    actionType : React.PropTypes.string,
    findQrCode : React.PropTypes.func,
  };

  state = {
    isReaderVisible: false,
  };

  componentWillReceiveProps(nextProps) {
    const { value } = this.props;
    if (value != nextProps.value) {
      this._hideReader();
    }
  }

  _showReader = () => {
    this.setState({ isReaderVisible: true });
  };

  _hideReader = () => {
    this.setState({ isReaderVisible: false });
  };

  _goToProduct = (qrCode, rawCode, routerProcessedQrCode) => {
    const qrCodeData = qrCode || {} ;
    const { __typename: type } = qrCodeData.product || {};
    const code = qrCodeData.code || routerProcessedQrCode || rawCode;

    const {actionType} = this.props;

    const isSettingToCup = actionType === 'addBudToCup';

    if(isSettingToCup){
      const id =  qrCodeData.product.id;
      setTimeout(() => this.props.onChange(id));
    } else {
      if (type) {
        return ['already', {}];
      } else {
        setTimeout(() => this.props.onChange(code));
      }
    }

    return [];
  };

  _findQrCode = async (qrCode) => {
    const { findQrCode, value } = this.props;
    // The same qrcode skip
    if (qrCode === value) {
      setImmediate(() => this._hideReader());
      return null;
    }

    return findQrCode(qrCode);
  }

  renderModal() {
    const { isReaderVisible } = this.state;
    const { cancelText } = this.props;
    return (
      <Modal
        style={styles.modal}
        isVisible={isReaderVisible}
        onRequestClose={this._hideReader}
      >
        <View style={styles.backdrop} onStartShouldSetResponder={this._hideReader}>
          <QrCodeReader
            style={styles.reader}
            findQrCode={this._findQrCode}
            goToProduct={this._goToProduct}
            readerEnable={isReaderVisible}
            enableSearch={false}
          />
          <Button style={styles.cancelButton} type="modal" theme="purple" onPress={this._hideReader}>
            {cancelText}
          </Button>
        </View>
      </Modal>
    );
  }

  renderField() {
    const { fieldName, newText, oldText, value } = this.props;
    const content = U.isBlank(value) ? newText : oldText;
    return (
      <View key={`qrcode_${fieldName}`} style={{ flex: 1 }}>
        {this.renderLabel()}
        <Touchable style={styles.touchable} onPress={this._showReader}>
          <Icon name="qrcode" style={styles._qrcode} />
          <Text style={styles.fieldText}>{content}</Text>
        </Touchable>
        {this.renderModal()}
      </View>
    );
  }
}

const styles = EStyleSheet.create({
  modal: {
    margin: 0,
  },

  fieldText: {
    fontWeight: 'bold',
  },

  reader: {
    height: '50%',
    flex: 0,
  },

  qrcode: {
    fontSize: 30,
    color: 'black',
    marginRight: 10,
  },

  backdrop: {
    flex: 1,
    backgroundColor: 'transparent',
    justifyContent: 'center',
  },

  cancelButton: {
    marginHorizontal: '25%'
  },

  touchable: {
    marginVertical: 3,
    flexDirection: 'row',
    alignItems: 'center',
  }
});

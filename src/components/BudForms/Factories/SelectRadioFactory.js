import React from 'react';
import { View } from 'react-native';

import SelectFactory from 'utils/Form/Factories/SelectFactory';
import { RadioOption } from 'utils/Form/Factories/RadioFactory';

export default class SelectRadioFactory extends SelectFactory {
  static propTypes = {
    ...SelectFactory.propTypes,
    selected: React.PropTypes.bool,
  }

  renderContent(onPress, content) {
    const { selected } = this.props;
    return (
      <View style={{paddingTop: 8, paddingBottom: 8}}>
        <RadioOption
          underline={false}
          checked={selected}
          label={content}
          onPress={onPress}
        />
      </View>
    );
  }
}

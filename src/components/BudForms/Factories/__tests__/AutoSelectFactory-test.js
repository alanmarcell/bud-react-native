import React from 'react';
import renderer from 'react-test-renderer';

import { AutoSelectFactory } from '../';
import { BudForms } from 'components/BudForms';
import I18nField from 'components/BudForms/Fields/I18nField';
import { TasksSchemas } from '../../../../controllers/Tasks/components/TaskEdit/TaskSchemas';

describe('AutoSelectFactory', () => {
  test('renders correctly', () => {
    const tree = renderer.create(
      <BudForms schema={TasksSchemas.base}>
        <I18nField fieldName="taskType" type={AutoSelectFactory} />
      </BudForms>
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });
});

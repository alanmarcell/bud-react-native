import React from 'react';
import { View } from 'react-native';
import renderer from 'react-test-renderer';

import { AutoRadioFactory } from '../';
import { BudForms } from 'components/BudForms';
import I18nField from 'components/BudForms/Fields/I18nField';

// import { TasksSchemas } from 'components/TaskEdit/TaskSchemas';
import { ArrayFactory } from 'utils/Form/Factories';
import { SimpleSchema } from 'components/BudForms/Schemas';

describe('AutoRadioFactory', () => {
  const schema = new SimpleSchema({
    environment_type: {
      type: String,
      allowedValues: ['indoor', 'outdoor', 'greenhouse'],
    },
    mix: Array,
    'mix.$': Object,
    'mix.$.measurement' : {
      type: String,
      allowedValues: ['gl', 'mll']
    },
  });

  const doc = {
    environment_type: 'indoor',
    mix: [
      { measurement: 'gl' }
    ]
  };

  test('renders correctly', () => {
    const tree = renderer.create(
      <BudForms doc={doc} schema={schema}>
        <I18nField
          fieldName="environment_type"
          type={AutoRadioFactory}
        />
      </BudForms>
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('render correctly in array', () => {
    let options;

    class AutoRadioFactoryMocked extends AutoRadioFactory {
      makeOptions(...args) {
        const result = super.makeOptions(...args);
        ({ options } = result);
        return result;
      }
    }

    const tree = renderer.create(
      <BudForms doc={doc} schema={schema}>
        <I18nField fieldName="mix" type={ArrayFactory}>
          <View>
            <I18nField fieldName="measurement" type={AutoRadioFactoryMocked} />
          </View>
        </I18nField>
      </BudForms>
    ).toJSON();
    expect(tree).toMatchSnapshot();
    expect(options[0]).toMatchObject({value: 'gl'});
    expect(options[1]).toMatchObject({value: 'mll'});
  });
});

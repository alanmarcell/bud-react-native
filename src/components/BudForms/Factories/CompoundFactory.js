import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';

import { I18n, formatNumber } from 'utils/I18n';

import { AutoRadioFactory } from 'components/BudForms/Factories';
import { I18nField } from 'components/BudForms/Fields';
import { Field, InputFactory } from 'utils/Form/Factories';
import { ArrayFactory } from 'utils/Form/Factories';

export default class CompoundFactory extends Component {
  static propTypes = {
    factory: PropTypes.any.isRequired,
    fieldName: PropTypes.string.isRequired,
  }

  static serializeRelation(doc, option) {
    const value = doc[option] || {};
    let result = null;
    if (value.id) {
      result = {
        index: value.id,
        label: value.name,
        type : value.__typename.toUpperCase()
      };
    }
    return result;
  }

  static mapCompounds(compounds) {
    return compounds.map(({ compound, __typename, qty, ...item }) => {
      return {
        ...item,
        qty: formatNumber(qty),
        compound: this.serializeRelation({ compound }, 'compound'),
      };
    });
  }

  render() {
    return (
      <Field
        fieldName={this.props.fieldName}
        type={ArrayFactory}
        addLabel={I18n.t(`forms.${this.props.fieldName}.addButton`)}
      >
        <I18nField
          fieldName="compound"
          i18nScope={`forms.${this.props.fieldName}`}
          placeholder={true}
          type={this.props.factory}
        />

        <View style={{flexDirection: 'row'}}>
          <View style={{flex: 3}}>
            <I18nField
              fieldName="qty"
              type={InputFactory}
              number
            />
          </View>

          <View style={{flex: 2}}>
            <I18nField
              fieldName="measurement"
              i18nScope="forms.measurement"
              type={AutoRadioFactory}
            />
          </View>
        </View>
      </Field>
    );
  }
}


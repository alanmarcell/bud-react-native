import React from 'react';
import { shallow } from 'enzyme';

import FieldMeasurement from '../FieldMeasurement';

describe('FieldMeasurement', () => {
  const defaultProps = {
    fieldName: 'measurement',
    value    : { qty: 1.4, measurement: 'gl' },
    onChange : () => {},
  };

  test.skip('renders correctly field', () => {
    const props = {
      ...defaultProps,
    };

    const wrapper = shallow(<FieldMeasurement {...props} />);
    const label   = wrapper.find('Label');
    const input   = wrapper.find('InputFactory').dive();
    const radio   = wrapper.find('RadioFactory').dive();

    expect(label.length).toEqual(1);
    expect(input.length).toEqual(1);
    expect(radio.length).toEqual(1);

    expect(input.find('Label').length).toEqual(0);
    expect(radio.find('Label').length).toEqual(0);

    expect(label.children().text()).toEqual('measurement');

    expect(wrapper.state().value).toEqual(props.value);

    const radioOptions = radio.find('RadioOption');
    expect(radioOptions.length).toEqual(2);

    expect(wrapper).toMatchSnapshot();
  });

  test.skip('raise onChange if Input pressed', () => {
    const onChange = jest.fn();
    const props = {
      ...defaultProps,
      label: 'measurementCustom',
      value: { qty: 2.3, measurement: 'mll' },
      onChange,
    };

    const wrapper = shallow(<FieldMeasurement {...props} />);
    const label   = wrapper.find('Label');
    expect(label.children().text()).toEqual('measurementCustom');

    const input   = wrapper.find('InputFactory').dive();
    const radio   = wrapper.find('RadioFactory').dive();

    input.children().simulate('changeText', 1.2);
    radio.find('RadioOption').at(1).simulate('press');

    const newValue = { qty: 1.2, measurement: 'gl' };
    expect(onChange).toHaveBeenCalledWith(newValue);
    expect(wrapper.state().value).toEqual(newValue);

    const radioOptions = radio.find('RadioOption');

    expect(radioOptions.at(0)).toHaveProp('checked', false);
    expect(radioOptions.at(1)).toHaveProp('checked', true);

    expect(wrapper).toMatchSnapshot();
  });

  test.skip('show erros if have one', () => {
    const props = {
      ...defaultProps,
      errorMessage: 'Error message',
    };
    const wrapper = shallow(<FieldMeasurement {...props} />);
    const text = wrapper.find('Text').childAt(0);
    expect(text.text()).toEqual(props.errorMessage);
  });
});

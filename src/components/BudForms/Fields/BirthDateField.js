import { DateFactory } from 'utils/Form/Factories';
import I18nField from './I18nField';

export class BirthDateField extends I18nField {
  static defaultProps = {
    ...I18nField.defaultProps,
    fieldName: 'bornAt',
    type     : DateFactory,
  };
}

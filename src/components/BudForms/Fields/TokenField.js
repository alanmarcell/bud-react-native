import { InputFactory } from 'utils/Form/Factories';
import I18nField from './I18nField';

export class TokenField extends I18nField {
  static defaultProps = {
    ...I18nField.defaultProps,
    fieldName      : 'token',
    type           : InputFactory,
    autoCapitalize : 'none',
    autoCorrect    : false,
    placeholder    : true,
  };
}

import React, { Component } from 'react';
import { Field } from 'utils/Form';
import { I18n } from 'utils/I18n';
import R from 'ramda';

export default class I18nField extends Component {
  static defaultProps = {
    ...Field.defaultProps,
    i18nScope: null,
    scopeArgs: {},
    maxLength: 80,
  };

  static propTypes = {
    ...Field.propTypes,
    scopeArgs: React.PropTypes.object,
    i18nScope: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.array,
    ]),
    labelI18n: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.array,
    ]),
  };

  scope(term) {
    let { i18nScope, label, labelI18n, fieldName } = this.props;
    if (i18nScope === null) {
      i18nScope = ['forms', labelI18n || label || fieldName];
    }

    if (!R.isNil(i18nScope) && R.is(Function, i18nScope.join)) {
      i18nScope = i18nScope.join('.');
    }

    return `${i18nScope}.${term}`;
  }

  t(term, ...args) {
    return I18n.t(this.scope(term), ...args);
  }

  T(term, ...args) {
    return I18n.T(this.scope(term), ...args);
  }

  makeOptions(values) {
    return values.map((value) => {
      return { label: this.t(value), value };
    });
  }

  passProps(more = {}) {
    let { label, placeholder, scopeArgs } = this.props;

    return {
      ...this.props,
      label: label !== false ? this.T('label', scopeArgs) : label,
      placeholder: placeholder === true ? this.t('placeholder') : placeholder,
      ...more
    };
  }

  render() {
    return (<Field {...this.passProps()} />);
  }
}

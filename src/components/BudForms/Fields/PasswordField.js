import { InputPasswdFactory } from 'utils/Form/Factories';
import I18nField from './I18nField';

export class PasswordField extends I18nField {
  static defaultProps = {
    ...I18nField.defaultProps,
    fieldName       : 'password',
    type            : InputPasswdFactory,
    secureTextEntry : true,
    autoCapitalize  : 'none',
    autoCorrect     : false,
    placeholder     : true,
  };
}

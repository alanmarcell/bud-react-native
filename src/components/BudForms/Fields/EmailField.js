import { InputFactory } from 'utils/Form/Factories';
import I18nField from './I18nField';

export class EmailField extends I18nField {
  static defaultProps = {
    ...I18nField.defaultProps,
    fieldName      : 'email',
    type           : InputFactory,
    keyboardType   : 'email-address',
    autoCapitalize : 'none',
    autoCorrect    : false,
    placeholder    : true,
    maxLength      : 254,
  };
}

import { DateFactory } from 'utils/Form/Factories';

import I18nField from './I18nField';

export class DateField extends I18nField {
  static defaultProps = {
    ...I18nField.defaultProps,
    fieldName: 'date',
    type     : DateFactory,
  };
}

import { InputFactory } from 'utils/Form/Factories';
import I18nField from './I18nField';

export class NameField extends I18nField {
  static defaultProps = {
    ...I18nField.defaultProps,
    fieldName      : 'name',
    type           : InputFactory,
    autoCapitalize : 'words',
    autoCorrect    : false,
    placeholder    : true,
  };
}

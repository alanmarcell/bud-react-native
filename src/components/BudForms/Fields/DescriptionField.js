import { InputFactory } from 'utils/Form/Factories';
import I18nField from './I18nField';

export class DescriptionField extends I18nField {
  static defaultProps = {
    ...I18nField.defaultProps,
    fieldName  : 'description',
    type       : InputFactory,
    maxLength: 250,
    passProps: {
      autoCorrect: true,
      multiline: true,
      maxHeight: 100,
      autoGrow: true,
    }
  };
}

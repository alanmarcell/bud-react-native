import { View, Text } from 'react-native';
import React from 'react';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import U from 'utils';

import { Form } from 'utils/Form';
export { SimpleSchema } from './Schemas';
import Button from 'components/Button';

import styles from './styles';
export { styles as BudFormsStyles };

/**
 * General form component with a custom Budbuds requires
 */
export default class BudForms extends React.PureComponent {
  constructor (props) {
    super(props);

    this.state = { hasError: false, errorMessages: {} };
  }

  static propTypes = {
    ...Form.propTypes,
    submitText: React.PropTypes.string,
    renderFooter: React.PropTypes.func,
    renderHeader: React.PropTypes.func,
  };

  static defaultProps = {
    ...Form.defaultProps,
    renderFooter: () => null,
    renderHeader: () => null,
  }

  get doc() {
    const { state: { doc }} = this.refs.form;
    return doc;
  }

  _handlePress = () => {
    this.refs.form.submit();
    if (!U.isEmpty(this.refs.form.errorMessages)) {
      this.setState({
        hasError: true,
        errorMessages: this.refs.form.errorMessages,
      });
    }
  }

  renderSubmit(submitText) {
    if (submitText) {
      return (
        <Button style={styles.submitButton} type="normal" onPress={this._handlePress}>
          {submitText}
        </Button>
      );
    }
    return null;
  }

  renderError() {
    console.log('err', this.state.errorMessages); // eslint-disable-line no-console
    const errors = Object.keys(this.state.errorMessages);
    return (
      <View style={styles.errors} >
      {errors.map((error) => <Text style={styles.textError}>- {this.state.errorMessages[error]}</Text>)}
      </View>
    );
  }

  render() {
    const {
      submitText,
      children,
      renderFooter,
      renderHeader,
      doc,
      ...props
    } = this.props;

    const formProps = {
      ...props,
      doc: U.deepClone(doc),
      containerStyle: [styles.form, props.containerStyle],
    };

    return (
      <KeyboardAwareScrollView
        ref="scroll"
        style={styles.scrollView}
        contentContainerStyle={styles.scrollViewContent}
        keyboardShouldPersistTaps="handled"
        keyboardDismissMode="interactive"
        contentInset={{}}
      >
        <Form {...formProps} ref="form">
          {children}
          {this.state.hasError && this.renderError()}
          {this.renderSubmit(submitText)}
          {renderFooter()}
        </Form>
      </KeyboardAwareScrollView>
    );
  }
}

export { BudForms };

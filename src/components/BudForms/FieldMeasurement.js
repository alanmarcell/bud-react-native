import React from 'react';
import { View } from 'react-native';

import BaseFactory  from 'utils/Form/Factories/BaseFactory';
import InputFactory from 'utils/Form/Factories/InputFactory';
import RadioFactory from 'utils/Form/Factories/RadioFactory';
import Label        from 'utils/Form/Label';

import { I18n } from 'utils/I18n';

export default class FieldMeasurement extends BaseFactory {
  static defaultProps = {
    ...BaseFactory.defaultProps,
    form   : {},
    options: [
      { label: I18n.t('forms.measurement.mll'), value: 'mll' },
      { label: I18n.t('forms.measurement.gl'), value: 'gl' },
    ],
    value: {
      qty: null,
      measurement: 'gl',
    },
  };

  static propTypes = {
    ...BaseFactory.propTypes,
    value: React.PropTypes.shape({
      qty: React.PropTypes.number,
      measurement : React.PropTypes.string,
    }),
    options: React.PropTypes.array
  };

  state = {
    value: {},
  }

  constructor(props) {
    super(props);
    this.state.value = props.value;
  }

  handleChange = (key, value) => {
    const { onChange } = this.props;

    if (key === 'qty') {
      value = parseFloat(value);
    }

    const newValue = {
      ...this.state.value,
      [key]: value,
    };

    if (this.state.value !== newValue) {
      this.setState({ value: newValue });
    }

    if (typeof onChange === 'function') {
      onChange(newValue);
    }
  }

  componentWillReceiveProps(nextProps) {
    const { value } = nextProps;
    if (value !== this.state.value) {
      this.setState({ value });
    }
  }

  renderField() {
    const { fieldName, label, options, ...props } = this.props;
    let { value } = props;
    if (!value || typeof value !== 'object') {
      value = {};
    }

    return (
      <View key={fieldName} style={{ flex: 1 }}>
        <Label>{label || fieldName}</Label>
        <InputFactory
          fieldName={`${fieldName}-qty`}
          label={false}
          value={value.qty ? `${value.qty}` : value.qty}
          onChange={(value) => this.handleChange('qty', value)}
        />
        <RadioFactory
          fieldName={`${fieldName}-measurement`}
          label={false}
          options={options}
          value={value.measurement}
          onChange={(value) => this.handleChange('measurement', value)}
        />
      </View>
    );
  }
}

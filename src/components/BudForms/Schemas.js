import { I18n } from 'utils/I18n';
import { SimpleSchema as SS } from 'utils/Form';
import { pick, merge } from 'ramda';
import moment from 'moment';

export class SimpleSchema extends SS {
  messageForError(errorInfo) {
    const { name, dataType, type } = errorInfo;
    if (__DEV__) {
      console.log({ errorInfo }); // eslint-disable-line no-console
      console.log(`forms.${name}.error`); // eslint-disable-line no-console
    }

    let scope = ['forms', name, 'error'];
    if (type === 'maxNumber' || type === 'minNumber') {
      scope = ['forms', 'errors', type];
    } else if (dataType === 'Number' && type === 'expectedType') {
      scope.push('expectedNumber');
    }

    scope = scope.join('.');
    scope = scope.replace(/\.[\d]*\./, '.');
    scope = scope.replace(/\.\$\./, '.');

    return I18n.t(scope, errorInfo);
  }
}

export const NameSchema = {
  type: String,
  min: 1,
};

// https://regex101.com/r/LFrL7b/1
export const NickSchema = {
  type: String,
  min: 4,
  regEx: /^[a-zA-Z\d]{1,}$/,
};

export const BirthDateSchema = {
  type: Date,
  custom: function() {
    let value = moment(this.value);
    if (value.isAfter(moment().subtract(18, 'years'))) {
      return 'underAge';
    }
  }
};

export const SessionTaskDateSchema = (scheduledAt) => {
  return {
    type: Date,
    custom: function () {
      let endAt = moment(this.value);
      let scheduledAtValue = moment(scheduledAt);
      if (endAt.isSameOrBefore(scheduledAtValue)) {
        return 'End must be after start';
      }
    }
  };
};

export const LoginPasswordSchema = {
  type: String,
  min: 6,
};

export const RegisterPasswordSchema = {
  type: String,
  min: 6,
};

export const EmailSchema = {
  type: String,
  regEx: SimpleSchema.RegEx.EmailWithTLD,
};

export const ImageSchema = new SimpleSchema({
  id: {
    type: String,
    required: false
  },
  uri: {
    type: String,
    required: false
  },
  url: {
    type: String,
    required: false
  },
  data: {
    type: String,
    required: false
  },
  mimetype: {
    type: String, // error
    required: false
  },
  size: {
    type: Number, // error
    required: false
  },
  fileName: {
    type: String,
    required: false
  },
  type: {
    type: String,
    required: false
  }
});

export const cleanLogoToOpen = logo =>
  merge({
    logo: pick([
      'id',
      'uri',
      'url',
      'data',
      'mimetype',
      'size',
      'fileName',
      'type'], logo)
  });

export const OptionSchema = new SimpleSchema({
  key: {
    type: String,
    required: false
  },
  label: {
    type: String,
    required: false
  }
});

import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  scrollView: {
    backgroundColor: '$Colors.background',
  },

  scrollViewContent: {
    paddingBottom: 40,
  },

  form: {
    flex: 1,
    paddingHorizontal: '$forms.paddingHorizontal',
    paddingTop: 20,
    paddingBottom: 40,
  },
  errors: {
    flex: 1,
    backgroundColor: '$Colors.Red',
    minHeight: 50,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 15,
  },
  textError: {
    color: '$Colors.primaryInvert',
    padding: 5,
  }
});

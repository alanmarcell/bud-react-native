import React, { PureComponent } from 'react';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import EStyleSheet from 'react-native-extended-stylesheet';

export default class TabView extends PureComponent {
  state = {
    index: 0,
    routes: []
  }

  _handleIndexChange = index => this.setState({ index });

  _renderHeader = props => {
    const tabProps = {
      ...props,
      style: styles.tab,
      indicatorStyle: styles.indicator,
    };

    return (<TabBar {...tabProps} />);
  };

  componentWillMount () {
    const {titles } = this.props;
    const routes = titles.map((title, idx) =>
        ({ key: idx.toString(), title }));

    this.setState({ routes });
  }

  render() {
    const { children } = this.props;

    const scenes = children.reduce((acc, scene, index) =>
        ({ ...acc, [index]: () => scene }), {});

    return (
      <TabViewAnimated
        style={styles.container}
        tabStyle={styles.tab}
        navigationState={this.state}
        renderScene={SceneMap(scenes)}
        renderHeader={this._renderHeader}
        onIndexChange={this._handleIndexChange}
      />
    );
  }
}

const styles = EStyleSheet.create({
  container: {
    flex: 1,
  },
  tab: {
    backgroundColor: '$Colors.navbarBackground',
  },
  indicator: {
    backgroundColor: '$Colors.Purple',
    height: 5,
    width: '25%',
    marginLeft: '10%',
  }
});

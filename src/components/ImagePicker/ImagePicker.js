import React, { Component } from 'react';
import uuid from 'uuid';
import {
  View,
  ScrollView,
  TouchableOpacity,
  NativeModules,
  Image,
  ActivityIndicator
} from 'react-native';

import Icon from 'components/Icon';
import ActionMenu from 'components/ActionMenu';

const ImageCropPicker = NativeModules.ImageCropPicker;

import styles from './styles';

class MultiImagePicker extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      images: [],
      selectedPhoto: {}
    };
  }

  onSelectPhotos = (photos) => {
    photos = [photos];
    const images = photos.map((image) => ({
      uri: `data:${image.mime};base64,${image.data}`,
      data: image.data,
      mimetype: image.mime,
      size: image.size,
      fileName: image.filename || `photo_${uuid()}`,
      type: 'PHOTO',
      id: uuid(),
    }));
    this.setState({
      loading: false,
      images: [].concat(this.state.images, images),
      selectedImage: images[0] || this.state.images[0],
    });
  }

  onClickPhoto = (selectedId) => {
    this.setState({
      selectedImage: this.state.images.find((image) => image.id === selectedId)
    });
  }

  openPicker = () => {
    ImageCropPicker.openPicker({
      includeBase64: true,
      cropping: true,
      width: 640,
      height: 640,
    }).then(this.onSelectPhotos).catch(() => {
      if (this.state.images.length === 0) {
        return this.props.goBack();
      }

      this.setState({ loading: false });
    });
  }


  renderAsset = (source, style) => {
    const { selectedImage } = this.state;
    let props = { source, style };

    if (selectedImage.id === source.id) {
      props.style = [props.style, styles.selectedMiniature];
    }

    return <Image {...props} />;
  }

  handleOpenPicker = () => {
    this.setState({ loading: true });


    this.openPicker();
  }

  renderGalery = () => {
    const { images } = this.state;
    return (
      <ScrollView horizontal style={styles.galery} contentContainerStyle={{ alignItems: 'center' }}>
        {images.map((image) => (
          <TouchableOpacity onPress={() => this.onClickPhoto(image.id)} key={image.id}>
            {this.renderAsset(image, styles.miniatures)}
          </TouchableOpacity>
        ))}
        {images.length < 5 &&
          <TouchableOpacity onPress={this.handleOpenPicker} style={styles.addMoreContainer} >
            <Icon name="camera" style={styles.addMore} />
          </TouchableOpacity>
        }
      </ScrollView>
    );
  }

  renderPreviewImage = () => {
    const { selectedImage } = this.state;
    return (
      <View style={styles.previewContainer}>
        <Image style={styles.previewImage} source={selectedImage} />
      </View>
    );
  }

  render() {
    const { loading, images } = this.state;
    const { id, savePhotos } = this.props;

    return (
      <View style={styles.container}>
        {loading && <ActivityIndicator size="large" />}
        {images.length === 0 && this.openPicker()}
        {images.length !== 0 && this.renderPreviewImage()}
        {images.length !== 0 && this.renderGalery()}

        {images.length !== 0
          && <ActionMenu
                iconName="check"
                onPress={() => savePhotos({ id, images })}
             />
        }
      </View>
    );
  }
}

export default MultiImagePicker;

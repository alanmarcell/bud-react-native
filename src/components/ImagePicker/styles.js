import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  container: {
    flex: 1,
  },

  previewImage: {
    width: '100%',
    height: '80%',
  },

  galery: {
    flex: 1,
    marginLeft: 10,
  },

  miniatures: {
    width: 75,
    height: 75,
    marginRight: 5,
    marginBottom: 20
  },

  selectedMiniature: {
    borderWidth: 5,
    borderColor: '$Colors.Purple',
  },

  addMore: {
    fontSize: 24,
    color: '$Colors.primaryInvert',
    alignSelf: 'center',
  },

  addMoreContainer: {
    flex: 1,
    width: 75,
    height: 75,
    backgroundColor: '$Colors.GrayLowOpacity',
    marginRight: 5,
    marginBottom: 20,
    justifyContent: 'center'
  }
});

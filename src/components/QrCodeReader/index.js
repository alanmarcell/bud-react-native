import React from 'react';
import { Platform, View, Text, Alert } from 'react-native';

import Icon from 'components/Icon';
import { I18n } from 'utils/I18n';
import { router } from 'config';

import Camera from 'react-native-camera';

import styles from './styles';
const photo = require('./cannabis-sativa-301x450.jpg');

export default class QrCodeReader extends React.Component {
  static defaultProps = {
    readerEnable: true,
  };

  static propTypes = {
    readerEnable : React.PropTypes.bool,
    findQrCode   : React.PropTypes.func,
    goToProduct  : React.PropTypes.func,
    children     : React.PropTypes.any,
    style        : React.PropTypes.any,
  };

  state = {
    qrCode: null,
    quickId: null,
    cameraEnable: true,
  };
  processing = false;

  async componentDidUpdate() {
    try {
      const { findQrCode, goToProduct, enableSearch } = this.props;
      const { qrCode } = this.state;

      if (this.processing || !qrCode) {
        return null;
      }

      this.processing = true;

      let errorKey, options = { qrCode };
      let { name, params, error } = router.process(qrCode);

      // TODO: Remove console
      // console.log(error, name, params);

      if (error) {
        errorKey = 'format';
      } else if (name === 'qrCode' || name === 'qrCodeCustom') {
        // TODO: Remove console
        // console.log('read qr', qrCode);
        
        const qrCodeData = await findQrCode(params.qrCode);
        if ((qrCodeData && qrCodeData.id) || (!qrCodeData && !enableSearch)) {
          const redirected = await (goToProduct(qrCodeData, qrCode, params.qrCode));
          // TODO: remove console
          // console.log('redirect', redirected);
          if (redirected === true) {
            return;
          }
          [errorKey, options] = redirected;
        } else if (qrCodeData && !qrCodeData.error)  {
          errorKey = 'not_found';
        } else if (qrCodeData && qrCodeData.error) {
          errorKey = qrCodeData.error;
        } else if (enableSearch){
          errorKey = 'comingSoonQrCode';
        }
      } else {
        errorKey = 'format';
      }

      if (errorKey) {
        await this._showError(errorKey, options);
      }

      this.processing = false;
      this.setState({ qrCode: null });

    } catch (error) {
      console.error(error);
    }
  }

  _showError(errorKey, options) {
    return new Promise((resolve) => {
      const errorScope   = 'quickId.error';
      const alertTitle   = I18n.t(`${errorScope}.${errorKey}.title`);
      const alertMessage = I18n.t(`${errorScope}.${errorKey}.message`, options);
      Alert.alert(alertTitle, alertMessage, [{
        text: 'OK',
        onPress: resolve,
      }]);
    });
  }

  _handleBarCodeRead = (cameraResponse) => {
    const { findQrCode, goToProduct } = this.props;
    const { qrCode } = this.state;

    if (
      qrCode
      || typeof findQrCode !== 'function'
      || typeof goToProduct !== 'function'
    ) {
      return null;
    }

    if (cameraResponse && cameraResponse.data) {
      const { data: qrCode } = cameraResponse;
      this.setState({ qrCode });
    }
  }

  // componentDidMount() {
  //   setTimeout(() => {
  //     this._handleBarCodeRead({
  //       data: 'https://budbuds.us/q/a02e12d1690ccf1615efec6099eed3fd05ccd759'
  //       // data: 'https://budbuds.us/q/05b7e3f5eb920cc6fc2561fc28052e176ffb5b1d'
  //     });
  //   }, 1000);
  // }

  renderCamera() {
    const { children, readerEnable } = this.props;
    const { cameraEnable } = this.state;
    let Component = View;
    if (readerEnable && cameraEnable) {
      Component = Camera;
    }

    // https://github.com/lwansbrough/react-native-camera/issues/652
    const barCodeTypes = Platform.select({
      ios: ['org.iso.QRCode'],
      android: ['qr'],
    });

    return (
      <Component
        onBarCodeRead={this._handleBarCodeRead}
        aspect={Camera.constants.Aspect.fill}
        style={styles.camera}
        barCodeTypes={barCodeTypes}
        cameraType="back"
        source={photo}
        resizeMode="cover"
      >
        <View style={styles.cameraInto} >
          {children}
        </View>
      </Component>
    );
  }

  renderFramework() {
    const { qrCode, cameraEnable } = this.state;
    const rectangle = qrCode || !cameraEnable ? null : (
      <View style={styles.rectangle}>
        <View style={styles.cornerTop}>
          <Icon style={styles._cornerTopLeft} name="corner" />
          <Icon style={styles._cornerTopRight} name="corner" />
        </View>
        <View style={styles.cornerBottom}>
          <Icon style={styles._cornerBottomLeft} name="corner" />
          <Icon style={styles._cornerBottomRight} name="corner" />
        </View>
      </View>
    );
    return (
      <View style={styles.overlayer}>
        <View style={styles.framework}>
          {rectangle}
        </View>
      </View>
    );
  }

  render() {
    const { qrCode } = this.state;
    const { style } = this.props;
    return (
      <View style={[styles.container, style]} >
        {this.renderCamera()}
        {this.renderFramework()}
        <View style={styles[qrCode ? 'processing' : 'unprocessing']}>
          <Text style={styles.processingText}>Processing...</Text>
        </View>
      </View>
    );
  }
}

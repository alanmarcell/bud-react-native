import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';
import Promise from 'bluebird';

import QrCodeReader from '../';

describe('QrCodeReader', () => {
  const qrCodeData = [
    { _id: '58be4d7d295a1f0016f08d5d', nick: 'markOne', model: 'product.bud', },
    { _id: '58be4b0f295a1f0016f08d56', nick: 'markOne', model: 'product.plant.cannabis', },
  ];

  const defaultProps = {
    goToQuickId : () => {},
    quickIdCheck: (id) => qrCodeData.find((item) => item._id === id),
  };

  test('renders correctly', () => {
    const wrapper = shallow(<QrCodeReader {...defaultProps} />);
    expect(wrapper).toMatchSnapshot();
  });

  test.skip('check quickid and redirect bud', async () => {
    const quickIdCheck = jest.fn(defaultProps.quickIdCheck);
    const goToQuickId  = jest.fn();

    const props = {
      ...defaultProps,
      goToQuickId,
      quickIdCheck,
    };

    const wrapper = shallow(<QrCodeReader {...props} />);
    const camera  = wrapper.find('Camera');
    const data    = { data: 'https://budbuds.us/q/58be4d7d295a1f0016f08d5d' };
    camera.simulate('barCodeRead', data);
    expect(quickIdCheck).toHaveBeenCalledWith('58be4d7d295a1f0016f08d5d');
    await Promise.delay(10);
    expect(goToQuickId).toHaveBeenCalledWith(qrCodeData[0], data.data);
  });

  test.skip('check quickid and redirect to plant', async () => {
    const quickIdCheck = jest.fn(defaultProps.quickIdCheck);
    const goToQuickId  = jest.fn();

    const props = {
      ...defaultProps,
      goToQuickId,
      quickIdCheck,
    };

    const wrapper = shallow(<QrCodeReader {...props} />);
    const camera  = wrapper.find('Camera');
    const data    = { data: 'https://budbuds.us/q/58be4b0f295a1f0016f08d56' };
    camera.simulate('barCodeRead', data);
    expect(quickIdCheck).toHaveBeenCalledWith('58be4b0f295a1f0016f08d56');
    await Promise.delay(10);
    expect(goToQuickId).toHaveBeenCalledWith(qrCodeData[1], data.data);
  });
});

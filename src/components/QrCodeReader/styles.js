// @flow
import EStyleSheet from 'react-native-extended-stylesheet';

const corner = {
  fontSize: 18,
  color: 'white',
};

export default EStyleSheet.create({
  container: {
    flex: 1,
  },

  camera: {
    flex: 1,
    width: '100%'
  },

  cameraInto: {
    flex: 1,
    backgroundColor: 'transparent',
  },

  overlayer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },

  framework: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },

  $rWidth: '60%',

  rectangle: {
    height: '$rWidth',
    width: '$rWidth',
    backgroundColor: 'transparent',
  },

  // Corners
  cornerTop: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  cornerBottom: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
  },

  cornerTopLeft: {
    ...corner,
    alignSelf: 'flex-start',
    transform: [{ rotate: '180deg' }],
  },

  cornerTopRight: {
    ...corner,
    transform: [{ rotate: '-90deg' }],
  },

  cornerBottomLeft: {
    ...corner,
    alignSelf: 'flex-start',
    transform: [{ rotate: '90deg' }],
  },

  cornerBottomRight: {
    ...corner,
  },

  processingText: {
    color: 'black',
    fontSize: 20,
  },

  unprocessing: {
    width: 0,
    height: 0,
  },

  processing: {
    backgroundColor: 'rgba(255, 255, 255, 0.5)',
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },

  searchContainer: {
    backgroundColor: '$Colors.Gray',
    opacity: 0.8,
    padding: 30,
  },

  searchLabel: {
    color: 'white',
    marginVertical: 5,
    textAlign: 'center',
  },

  inputContainer: {
    backgroundColor: 'transparent',
    borderWidth: 3,
    borderColor: 'white',
    flexDirection: 'row',
  },

  searchIcon: {
    color: 'white',
    fontSize: 16,
    padding: 10,
  },

  searchInput: {
    flex: 1,
    backgroundColor: 'transparent',
    '@media android': {
      padding: 0,
    },
    color: 'white',
    paddingLeft: 5,
  }
});

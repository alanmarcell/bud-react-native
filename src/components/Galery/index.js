import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import Moment from 'utils/components/Moment';
import Image from 'components/Image';

import styles from './styles';

export default class Galery extends Component {
  static propTypes = {
    files: PropTypes.array.isRequired,
    selectedFile: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props);

    this.state = {
      images: props.files,
      selectedImage: props.selectedFile,
    };
  }

  onClickPhoto = (selectedId) => {
    this.setState({
      selectedImage: this.state.images.find((image) => image.id === selectedId)
    });
  }

  renderAsset = ({ url, id }, style) => {
    const { selectedImage } = this.state;
    const source = { uri: url };

    let props = { source, style };

    if (selectedImage.id === id) {
      props.style = [props.style, styles.selectedMiniature];
    }

    return <Image {...props} />;
  }

  renderGaleryItem = ({ item }) => (
    <TouchableOpacity onPress={() => this.onClickPhoto(item.id)}>
      {this.renderAsset(item, styles.miniatures)}
      <Moment
        date={item.createdAt}
        action="format"
        args={['L']}
        containerStyle={styles.dateContainer}
        style={{ fontSize: 10 }}
      />
    </TouchableOpacity>
  )

  renderGalery = () => {
    const { images } = this.state;

    return (
      <FlatList
        contentContainerStyle={styles.galery}
        data={images}
        extraData={this.state}
        keyExtractor={(item) => item.id}
        renderItem={this.renderGaleryItem}
      />
    );
  }

  renderPreviewImage = () => {
    const { selectedImage } = this.state;
    const source = { uri: selectedImage.url };
    const props = { source, style: styles.previewImage };

    return (
      <View style={styles.previewContainer}>
        <Image {...props} />
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        {this.renderPreviewImage()}
        {this.renderGalery()}
      </View>
    );
  }
}


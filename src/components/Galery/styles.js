import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  container: {
    flex: 1,
  },

  previewImage: {
    width: '100%',
    height: '80%',
  },

  galery: {
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },

  miniatures: {
    width: 75,
    height: 75,
    marginRight: 5,
    marginBottom: 5
  },

  selectedMiniature: {
    borderWidth: 5,
    borderColor: '$Colors.Purple',
  },

  dateContainer: {
    marginRight: 5,
    alignSelf: 'center',
    marginBottom: 15
  },
});


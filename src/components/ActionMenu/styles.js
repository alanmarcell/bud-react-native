import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  container: {
    backgroundColor: 'rgba(0,0,0,.5)'
  },

  button: {
    backgroundColor: 'rgba(66,0,105,1.00)',
  },

  item: {
    backgroundColor: 'white',
    width          : null,
    borderRadius   : 3,
    flexDirection  : 'row',
    paddingBottom  : 10,
    paddingTop     : 10,
    paddingLeft    : 20,
    paddingRight   : 20,
  },

  icon: {
    marginLeft: 10,
    fontSize  : 15,
  },

  iconMain: {
    fontSize  : 28,
    color: 'white',
  },
});

import React, { Component } from 'react';
import { Text } from 'react-native';
import R from 'ramda';

import Icon from 'components/Icon';
import ActionButton from 'react-native-action-button';
import { I18n } from 'utils/I18n';

import styles from './styles';

export default class ActionMenu extends Component {
  static propTypes = {
    onPress: React.PropTypes.func,
    items: React.PropTypes.object,
    iconName: React.PropTypes.string,
  };

  onPress = (item) => {
    if (this.props.onPress) {
      this.props.onPress(item);
    }
  };

  renderItens() {
    return R.toPairs(this.props.items).map(([label, icon]) => {
      return (
        <ActionButton.Item
          key={label}
          onPress={() => this.onPress(label)}
          style={styles.item}
          size={32}
        >
          <Text>{I18n.T(`actions.${label}`)}</Text>
          <Icon name={icon} style={styles.icon} />
        </ActionButton.Item>
      );
    });
  }

  renderMenuWithItems() {
    return (
      <ActionButton
        buttonColor={styles._button.backgroundColor}
        bgColor={styles._container.backgroundColor}
        spacing={10}
        offsetX={10}
        offsetY={20}
        style={styles.menu}
      >
        {this.renderItens()}
      </ActionButton>
    );
  }

  render() {
    const { iconName, onPress } = this.props;

    if (iconName) {
      return (
        <ActionButton
          buttonColor={styles._button.backgroundColor}
          bgColor={styles._container.backgroundColor}
          spacing={10}
          offsetX={10}
          offsetY={20}
          style={styles.menu}
          onPress={onPress}
          icon={<Icon name={iconName} style={styles.iconMain} />}
        />
      );
    }

    return this.renderMenuWithItems();
  }
}

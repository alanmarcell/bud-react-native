import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';

import { context } from '../../../__mocks__/stateHelper';
import ActionMenu from '../';

describe('ActionMenu', function() {
  const props = {
    items: {
      newPlant: 'cannabis'
    }
  };

  test('renders correctly', () => {
    const wrapper = shallow(<ActionMenu {...props} />, context);
    expect(wrapper).toMatchSnapshot();
  });

  test('suport onPress', () => {
    const onPress = jest.fn();
    const wrapper = shallow(<ActionMenu {...props} onPress={onPress} />, context);
    const item = wrapper.find('ActionButtonItem');
    item.simulate('press');
    expect(onPress).toHaveBeenCalledWith('newPlant');
  });
});

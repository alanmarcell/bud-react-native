import React from 'react';
import { shallow } from 'enzyme';

import Target from '../';

test('renders correctly', () => {
  const props = {
    contentKey       : 'some i18n key',
    onIAgreePress    : () => {},
  };
  const component = shallow(<Target {...props} />);
  expect(component).toMatchSnapshot();
});


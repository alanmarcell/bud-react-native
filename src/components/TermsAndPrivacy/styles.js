import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 20,
    marginBottom: 10,
  },

  markdown: {
    marginHorizontal: -20,
    backgroundColor: '#E9E9EF',
  },
});

import React, { Component } from 'react';
import { View, ActivityIndicator } from 'react-native';
import { InteractionManager } from 'react-native';
import Markdown from 'react-native-showdown';
import { I18n } from 'utils/I18n';

import styles from './styles';

class TermsAndPrivacy extends Component {
  static propTypes = {
    contentKey:    React.PropTypes.string.isRequired,
    onIAgreePress: React.PropTypes.func.isRequired,
  };

  state = {
    content: null,
  }

  _onIAgree = () => {
    this.props.onIAgreePress();
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      const { contentKey } = this.props;

      const css = `
        html {
          font-size: 14px;
        }

        body {
          background-color: #F1F1F1;
          padding-left: 20px;
          padding-right: 20px;
        }
      `;

      this.setState({
        content: <Markdown style={styles.markdown} body={I18n.t(contentKey)} pureCSS={css} />
      });
    });
  }

  render() {
    let { content } = this.state;
    if (!content) {
      content = <ActivityIndicator animating={true} />;
    }

    return (
      <View style={styles.container}>
        {content}
      </View>
    );
  }
}

export default TermsAndPrivacy;


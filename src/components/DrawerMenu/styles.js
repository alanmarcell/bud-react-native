import EStyleSheet from 'react-native-extended-stylesheet';

// Drawer stylesheet
export default EStyleSheet.create({
  container: {
    backgroundColor: '$Colors.Silver',
    marginTop: -20,
  },

  drawerContainer: {
    flex: 1,
  },

  chooser: {
    marginTop: 5,
    width: '100%',
  },

  chooserBackgrop: {
    flex: 1,
    justifyContent: 'flex-start',
    marginTop: 25,
    marginLeft: -75,
  },

  options: {
    backgroundColor: '$Colors.Silver',
  },

  textOption: {
    fontWeight: 'bold',
    color: '$Colors.LightGreen',
    paddingRight: 10,
  },

  headerContainer: {
    alignItems: 'center',
    backgroundColor: '$Colors.Gray',
    flexDirection: 'row',
    height: 172,
    paddingLeft: 20,
  },

  userInfoWrapper: {
    marginLeft: 15,
  },

  userName: {
    fontWeight: 'bold',
    color: '#FFFFFF',
    fontSize: 16,
    marginBottom: 5,
  },

  userEmail: {
    color: '$Colors.LightGreen',
    fontSize: 14,
  },

  // menu items
  menuContainer: {
    marginHorizontal: 20,
  },

  menuItem: {
    flexDirection: 'row',
    borderBottomColor: '#41484E',
    borderBottomWidth: 1,
    paddingVertical: 15,
    paddingHorizontal: 10,
  },

  menuItemText: {
    marginLeft: 10,
    color: '#CCCCCC',
    fontSize: 12,
  },
  menuFooterText: {
    marginLeft: 10,
    color: '#CCCCCC',
    fontSize: 9,
  },

});

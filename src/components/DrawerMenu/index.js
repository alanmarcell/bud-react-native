import React, { Component } from 'react';
import { ScrollView, View, Text, TouchableHighlight } from 'react-native';
import { Select, Option } from 'react-native-chooser';
import R from 'ramda';
import {connect} from 'react-redux';
import {updateActiveUserType} from '../../redux/actions';

import Icon from 'components/Icon';
import RoundedIcon from '../RoundedIcon';
import { I18n } from 'utils/I18n';
import { graphql } from 'graphql';
import styles from './styles';
import { Actions } from 'react-native-router-flux';
import { UPDATE_USER_TYPE } from 'graphql';

const items = {
  tasks: {
    icon: 'calendar-check-o',
    action: 'onTasks',
  },
  cup: {
    icon: 'trophy',
    action: 'onCups',
    skipFeatureCheck: true,
  },
  mixes: {
    icon: 'mixes',
    action: 'onMixes',
  },
  plants: {
    icon: 'budbuds',
    action: 'onPlants',
  },
  buds: {
    icon: 'bud',
    action: 'onBuds',
  },
  quickId: {
    icon: 'qrcode',
    action: 'onQuickID',
  },
  tour: {
    icon: 'budbuds-logo',
    action: 'onTour',
    skipFeatureCheck: true,
  },
  contact: {
    icon: 'comment-o',
    action: 'onContact',
    skipFeatureCheck: true,
  },
  // preferences: {
  //   icon: 'gear',
  //   action: 'onPreferences',
  // },
  logout: {
    icon: 'sign-out',
    action: 'onLogOut',
    skipFeatureCheck: true,
  }
};


class DrawerMenu extends Component {
  static propTypes = {
    profile: React.PropTypes.object.isRequired,
  };

  renderItem = ([name, item]) => {
    let action = this.props[item.action] || (() => {});

    if (item.skipFeatureCheck || this.props.features[name] === true) {
      return (
        <TouchableHighlight key={name} onPress={action}>
          <View style={styles.menuItem}>
            <Icon style={styles.menuItemText} name={item.icon} />
            <Text style={styles.menuItemText}>
              {I18n.T(`screens.drawer.${name}`)}
            </Text>
          </View>
        </TouchableHighlight>
      );
    }
  };

  onSelectUserType = (userType) => {
    this.props.updateUserTypeDb({ variables: { userType: [userType] } });
    this.props.updateActiveUserType(userType);
    Actions['logged']();
  }

  renderTypeChooser () {
    const { profile } = this.props;
    return (
        <Select
          onSelect={this.onSelectUserType}
          defaultText={this.props.activeUserType}
          indicator="down"
          transparent
          style={styles.chooser}
          backdropStyle={styles.chooserBackgrop}
          optionListStyle={styles.options}
          textStyle={styles.textOption}
        >
          {profile.userType.map((type) =>
              (
                <Option
                  key={type}
                  value={type}
                  styleText={styles.textOption}
                >
                  {type}
                </Option>)
          )}
        </Select>
    );
  }

  render() {
    const { profile } = this.props;

    profile.userType = ['GROWER', 'CONSUMER'];

    return (
      <ScrollView style={styles.container}>
        <View style={styles.drawerContainer}>
          <View style={styles.headerContainer}>
            <RoundedIcon
              source="user"
              iconStyle={{ fontSize: 35 }}
              containerStyle={{height: 64, width: 64}}
              style={{borderRadius: 64}}
            />
            <View  style={styles.userInfoWrapper}>
              <Text style={styles.userName}>{profile.name}</Text>
              {this.renderTypeChooser()}
            </View>
          </View>
          <View style={styles.menuContainer}>
            {R.map(this.renderItem, R.toPairs(items))}
          </View>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  activeUserType: state.user.userType,
  features: state.user.features
});

const mapDispatchToProps = dispatch => ({
  updateActiveUserType: type => dispatch(updateActiveUserType(type))
});


export default graphql(UPDATE_USER_TYPE, { name: 'updateUserTypeDb' })(
  connect(mapStateToProps, mapDispatchToProps)(DrawerMenu)
);

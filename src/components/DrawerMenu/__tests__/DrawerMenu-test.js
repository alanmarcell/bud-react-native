import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';

import DrawerMenu from '../';
import { context } from '../../../__mocks__/stateHelper';

test.skip('renders correctly', () => {
  const profile = {
    nick: 'usernick',
    email: 'user@example.com'
  };

  const wrapper = shallow(
    <DrawerMenu profile={profile} />, context
  );

  expect(wrapper).toMatchSnapshot();
});

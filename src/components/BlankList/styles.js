// @flow
import EStyleSheet from 'react-native-extended-stylesheet';

const textCommon = {
  color    : '$Colors.Silver',
  textAlign: 'center',
};

export default EStyleSheet.create({
  container: {
    flex         : 1,
    flexDirection: 'column',
    alignItems   : 'center',
    padding      : 40,
  },
  icon: {
    ...textCommon,
    fontSize: 30,
    padding : 10,
  },
  title: {
    padding: 10,
  },
  titleText: {
    ...textCommon,
    fontSize  : 20,
    fontWeight: 'bold',
  },
  message: {
    padding: 20,
  },
  messageText: {
    ...textCommon,
    textAlign: 'center',
  },
});

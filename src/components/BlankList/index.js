import React from 'react';
import { ScrollView, View, Text } from 'react-native';

import Icon from 'components/Icon';

import styles from './styles';

export default class BlankList extends React.Component {
  static propTypes = {
    icon    : React.PropTypes.string,
    title   : React.PropTypes.string,
    message : React.PropTypes.string,
    children: React.PropTypes.any,
  }

  renderIcon() {
    const { icon } = this.props;
    return icon && (<Icon name={icon} style={styles.icon} />);
  }

  renderTitle() {
    const { title } = this.props;
    return title && (
      <View id="title" style={styles.title}>
        <Text style={styles.titleText}>{title}</Text>
      </View>
    );
  }

  renderMessage() {
    const { message } = this.props;
    return message && (
      <View id="message" style={styles.message}>
        <Text style={styles.messageText}>{message}</Text>
      </View>
    );
  }

  render() {
    const { children } = this.props;

    return (
      <ScrollView contentContainerStyle={styles.container}>
        { this.renderIcon() }
        { this.renderTitle() }
        { this.renderMessage() }
        { children }
      </ScrollView>
    );
  }
}

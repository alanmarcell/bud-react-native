import { Text } from 'react-native';
import React from 'react';
import { shallow } from 'enzyme';

import BlankList from '../';

describe('<BlankList />', () => {
  const props = {
    icon   : 'home',
    title  : 'Empty List',
    message: 'Lorem ipsum Qui labore ex deserunt enim ullamco.',
  };

  test('renders correctly', () => {
    const children = (<Text>children</Text>);
    const wrapper = shallow(<BlankList {...props}>{children}</BlankList>);
    expect(wrapper.find('icon')).toBeTruthy();
    expect(wrapper.find('View#title')).toBeTruthy();
    expect(wrapper.find('View#message')).toBeTruthy();
    expect(wrapper.find(children)).toBeTruthy();
    expect(wrapper).toMatchSnapshot();
  });

  test('renders without attributes', () => {
    const wrapper = shallow(<BlankList />);
    expect(wrapper.find('icon').length).toBeFalsy();
    expect(wrapper.find('View#title').length).toBeFalsy();
    expect(wrapper.find('View#message').length).toBeFalsy();
    expect(wrapper).toMatchSnapshot();
  });
});

import React, { Component } from 'react';
import { ScrollView, View, Text, Image } from 'react-native';
import Swiper from 'react-native-swiper';

import Button from 'components/Button';
import Icon         from 'components/Icon';
import { I18n } from 'utils/I18n';

import styles from './styles';

export default class Tour extends Component {

  static defaultProps = {};

  static propTypes = {
    onNext: React.PropTypes.func.isRequired,
  };

  _onNext = () => {
    this.props.onNext();
  }

  render() {
    let dot = <View style={styles.dot} />;
    let activeDot = <View style={[styles.dot, styles.activeDot]} />;

    return (
      <ScrollView style={styles.container}>
        <Swiper dot={dot} activeDot={activeDot} loop={false}>
          <View style={styles.slide}>
            <View style={styles.imageWrapper}>
              <Image source={require('./images/budbuds-logo.png')} />
            </View>
            <Text style={styles.title}>{I18n.t('screens.tour.slide1.title')}</Text>
            <Text style={styles.text}>{I18n.t('screens.tour.slide1.text')}</Text>
            <Image source={require('./images/icon-right-arrow.png')} />
          </View>

          <View style={styles.slide}>
            <View style={styles.imageWrapper}>
              <Image source={require('./images/slide-2-organize.png')} />
            </View>
            <Text style={styles.title}>{I18n.t('screens.tour.slide2.title')}</Text>
            <Text style={styles.text}>{I18n.t('screens.tour.slide2.text')}</Text>
          </View>

          <View style={styles.slide}>
            <View style={styles.imageWrapper}>
              <Image source={require('./images/slide-3-tasks.png')} />
            </View>
            <Text style={styles.title}>{I18n.t('screens.tour.slide3.title')}</Text>
            <Text style={styles.text}>{I18n.t('screens.tour.slide3.text')}</Text>
          </View>

          <View style={styles.slide}>
            <View style={styles.imageWrapper}>
              <Image source={require('./images/slide-4-results.png')} />
            </View>
            <Text style={styles.title}>{I18n.t('screens.tour.slide4.title')}</Text>
            <Text style={styles.text}>{I18n.t('screens.tour.slide4.text')}</Text>
          </View>

          <View style={styles.slide}>
            <View style={styles.imageWrapper}>
              <Image source={require('./images/slide-5-privacy.png')} />
            </View>
            <Text style={styles.title}>{I18n.t('screens.tour.slide5.title')}</Text>
            <Text style={styles.text}>{I18n.t('screens.tour.slide5.text')}</Text>
            <Button onPress={this._onNext} theme="purple" type="bordered" style={styles.buttonWrapperInlineChildren}>
              <Text style={styles.buttonText}>{I18n.T('screens.pre-register.continue_button')}</Text>
              <Icon name="arrow-right" style={styles.buttonIcon} />
            </Button>
          </View>
        </Swiper>
      </ScrollView>
    );
  }
}

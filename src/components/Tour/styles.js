import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#CDEEDF',
  },

  slide: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingHorizontal: '10%',
    marginTop: 30,
    marginBottom: 50,
  },

  title: {
    fontWeight: 'bold',
    fontSize: 24,
    color: '$Colors.Gray',
  },

  text: {
    fontSize: 18,
    color: '$Colors.Gray',
    textAlign: 'center',
  },

  dot: {
    backgroundColor: '#A7E1C7',
    width: 8,
    height: 8,
    borderRadius: 4,
    marginLeft: 3,
    marginRight: 3,
    marginTop: 3,
    marginBottom: 3,
  },

  activeDot: {
    backgroundColor: '#24B574',
  },

  imageWrapper: {
    height: 260,
    justifyContent: 'center',
    marginBottom: 25,
  },

  buttonWrapperInlineChildren: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  buttonText: {
    fontWeight: 'bold',
    color: '$Colors.Purple',
    fontSize: 11,
    marginRight:5,
  },

  buttonIcon: {
    color: '$Colors.Purple',
    fontSize: 11,
  },
});

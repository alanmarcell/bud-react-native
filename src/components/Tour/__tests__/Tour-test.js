import 'react-native';
import React from 'react';
import Tour from '../';
import renderer from 'react-test-renderer';

test('renders correctly', () => {
  const tree = renderer.create(
    <Tour />
  ).toJSON();
  expect(tree).toMatchSnapshot();
});

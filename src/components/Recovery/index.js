import React, { Component } from 'react';
import { I18n } from 'utils/I18n';

import { BudForms, SimpleSchema } from 'components/BudForms';
import { EmailField, PasswordField, TokenField } from 'components/BudForms/Fields';
import { EmailSchema, LoginPasswordSchema } from 'components/BudForms/Schemas';

const SessionSchema = new SimpleSchema({
  email: EmailSchema,
  password: LoginPasswordSchema,
  token: String,
});

export default class SignIn extends Component {
  static propTypes = {
    onRecovery: React.PropTypes.func.isRequired,
  };

  render() {
    const { onRecovery } = this.props;
    const formProps = {
      ref          : 'form',
      schema       : SessionSchema,
      onSubmit     : onRecovery,
      submitText   : I18n.t('screens.recovery.submit')
    };

    return (
      <BudForms {...formProps} >
        <EmailField />
        <TokenField />
        <PasswordField label="new_password" />
      </BudForms>
    );
  }
}

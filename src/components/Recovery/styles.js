import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  container: {
    flex: 1,
  },

  image: {
    marginTop: '9%',
    marginBottom: '6%',
    alignSelf: 'center',
  },

  submitButton: {
    marginTop: 30,
  },

  lostButton: {
    alignSelf: 'center',
    marginTop: 14,
    marginBottom: 20,
    color: '$Colors.Green',
    textDecorationLine: 'underline',
  }
});

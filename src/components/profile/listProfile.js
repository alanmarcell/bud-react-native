
import React from 'react';
import { View, ActivityIndicator, ScrollView } from 'react-native';
import Refreshable from 'utils/components/Refreshable';
import { styles } from 'components/profile';

const listProfile = (Header, List) =>
class ListProfile extends React.PureComponent {
  render() {
    const props = this.props;
    const { loading, refetch, products, icon , productsType} = props;

    if (loading) {
      return <ActivityIndicator />;
    }

    const { onProductPress } = props;
    const listProps = {
      products,
      refreshControl: { loading, refetch },
      onProductPress,
      icon,
      productsType
    };
    
    return (
      // <View style={{ flex: 1 }}>
      <ScrollView refreshControl={Refreshable({ loading, refetch })}>
        <View style={styles.container}>
          <Header {...this.props}/>
        </View>
        <List {...listProps} />
      </ScrollView>
      // </View>
    );
  }
};

export {listProfile};
export default listProfile;
// import {filterMenuItems} from './profile';
import R from 'ramda';


const menuItems = [{
  key: 'newEdition' ,
  menu: { newEdition: 'trophy'},
  redirectTo: 'editionAdd',
  args:  { cupId: '1234-4321-1234-4321' } 
},{
  key: 'newEdition2' ,
  menu: { newEdition2: 'trophy2'},
  redirectTo: 'editionAdd2',
  args:  { cupId: '1234-4321-1234-4321' } 
}];

const filterMenuItems = menuItems =>  R.prop(['menu'], ...menuItems);

test('filterMenuItems correctly', () => {
  const items = filterMenuItems(menuItems);
  expect(items.newEdition).toBe('trophy');
});

const getMenuAction = ({menuItems}) =>{
  const option = 'newEdition';

  const menu = R.find(R.propEq('key', option))(menuItems);
  const getOptions = R.prop(R.__, menu );

  const action = getOptions('redirectTo');
  return action;
};

test('getMenuAction correctly', () => {
  const action = getMenuAction({menuItems});
  expect(action).toBe('editionAdd');
});


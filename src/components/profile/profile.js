// @flow
import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';
import R from 'ramda';

import NavIcon from 'scenes/NavIcon';
import RootController from 'controllers/RootController';
import ActionMenu from 'components/ActionMenu';

const getLoggedInUserId = R.path(['session', 'user', 'id']);

const getProductOwnerId = props =>
  R.path([props.productType, 'owner', 'id'], props) || R.path([props.productType, 'user', 'id'], props);

const isProductOwner = props => getProductOwnerId(props) === getLoggedInUserId(props);

const profile = Comp =>{

  return class Profile extends RootController {
    static propTypes = {
      product: PropTypes.object.isRequired,
      loading: PropTypes.bool,
      productType: PropTypes.string.isRequired,
    }

    static defaultProps = {
      product: {},
    };
  
    get actions() {
      return {
        onProductPress: (objectId) => {
          const {onProductPressArgs} = this.props;
          this.redirectTo(onProductPressArgs, { objectId });
        },
        deleteProduct: async (params) => {
          const {productType} = this.props;
          const destroy = await this.confirmRemove(productType);
          if (destroy) {
            await this.props.deleteProduct(params);
            this.goBack();
          }
        },
        openQuickID: () => {
          const { name, logo, qrCode: { code: qrcode } } = this.props.product;
          const {openQuickIDProps:{icon, label}, productType} = this.props;

          this.redirectTo(`${productType}QrCode`, {
            icon,
            logo,
            label: label(name),
            qrcode,
          });
        },
      };
    }
    componentWillUpdate(nextProps) {
      this.renderRightButton(nextProps);
    }

    renderRightButton(nextProps) {
      const { loading, setButtons, editProduct } = nextProps;

      if (!loading && !setButtons) {
        if (isProductOwner(nextProps)) {
          const { objectId } = nextProps;

          const { deleteProduct } = this.actionsWithHandle();
          this.refresh({
            setButtons: true,
            renderRightButton: () => {
              return (
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <NavIcon onPress={() => deleteProduct({ objectId })} iconName="trash" />
                  <NavIcon onPress={() => this.redirectTo( editProduct, { objectId })} iconName="pencil" />
                </View>
              );
            }
          });
        }
      }
    }

    onActionPress = (option) => {
      const {menuItems} = this.props;        
      const getMenuOptions = 
      R.prop(
        R.__, R.find(
          R.propEq('key', option))(menuItems));
      
      const redirectTo = getMenuOptions('redirectTo' );
      const args = getMenuOptions('args');

      this.redirectTo(redirectTo, args);      
    }

    renderActionMenu(props) {
      const { menuItems } = props;
      const items = filterMenuItems(menuItems);

      if (!isProductOwner(props)) return null;
      return <ActionMenu onPress={this.onActionPress} items={items} />;
    }

    renderView(props) {
      const { loading } = props;
      if (loading) {
        return <ActivityIndicator />;
      }
      // this.renderRightButton(props);
      return (        
        <View style={{ flex: 1 }}>
          <Comp {...props} /> 
          {this.renderActionMenu(props)}
        </View>);
    }
  };
};

const filterMenuItems = menuItems =>  R.prop(['menu'], ...menuItems);

export default profile;

export { profile , isProductOwner};

import React from 'react';
import { Platform, Image as NativeImage } from 'react-native';
import FastImage from 'react-native-fast-image-old';
import WebImage from 'react-native-web-image-old';

class Image extends React.PureComponent {
  render() {
    const isIOS = Platform.OS === 'ios';

    if (this.props.useNative) {
      return <NativeImage {...this.props} />;
    }

    return isIOS ? <FastImage {...this.props} /> : <WebImage {...this.props} />;
  }
}

export default Image;

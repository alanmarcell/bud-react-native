import React, { Component, PropTypes } from 'react';
import { Text, TouchableOpacity } from 'react-native';

import styles from './styles';

export default class Button extends Component {
  static defaultProps = {
    theme:      'green',
    type:       'normal',
    disabled:   false,
    isLoading:  false,
    loadingText: null,
    style:      {},
    textStyle:  {},
    onPress:    () => {},
  };

  static propTypes = {
    theme:      PropTypes.oneOf(['green', 'purple', 'red']),
    // normal =   width full, height 50
    // small  =   width full, height 30
    // modal  =   width adjusted, height 36
    // bordered =   width full, transparent background
    type:       PropTypes.oneOf(['normal', 'small', 'modal', 'bordered']),
    disabled:   PropTypes.bool,
    loadingText: PropTypes.string,
    onPress:    PropTypes.func,
  }

  onPress() {
    if (!this.props.disabled) {
      this.props.onPress();
    }
  }

  // TODO: refactor the buttons style factory
  _getButtonStyle() {
    let btnStyles = [
      styles.button,
      styles[this.props.type],
      styles[this.props.theme]
    ];

    if (this.props.type != 'modal') {
      btnStyles.push(styles.buttonFullWidth);
    }

    if (this.props.type == 'bordered') {
      btnStyles.push(styles.bordered);
    }

    if (this.props.disabled) {
      btnStyles.push(styles.buttonDisabled);
    }

    if (this.props.isLoading && !this.props.disabled) {
      btnStyles.push(styles[`buttonLoading${this.props.theme}`]);
    }

    return btnStyles;
  }

  _getTextStyle() {
    const { type, disabled, textStyle } = this.props;
    let textStyles = [
      styles.text,
      styles[`text${type}`],
      textStyle
    ];

    if (disabled) {
      textStyles.push(styles.textDisabled);
    }

    return textStyles;
  }

  render() {
    const buttonStyle = this._getButtonStyle();
    const TouchableActiveOpacity = this.props.disabled ? 1 : 0.6;
    const textStyle = this._getTextStyle();
    let outputElement;

    let { children, loadingText, isLoading } = this.props;

    // if children is just a text...
    if (typeof children == 'string') {
      outputElement = (<Text style={textStyle}>{children.toUpperCase()}</Text>);
    } else if (typeof children == 'object') {
    // if children is a component or a react element
      outputElement = children;
    }

    if (isLoading && loadingText) {
      outputElement = (<Text style={textStyle}>{loadingText}</Text>);
    }

    return (
      <TouchableOpacity
        activeOpacity={TouchableActiveOpacity}
        style={[...buttonStyle, this.props.style]}
        onPress={this.onPress.bind(this)}>
          {outputElement}
      </TouchableOpacity>
    );
  }
}

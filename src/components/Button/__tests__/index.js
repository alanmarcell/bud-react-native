import 'react-native';
import React from 'react';
import Button from '../';
import renderer from 'react-test-renderer';

test('renders correctly green small', () => {
  const tree = renderer.create(
    <Button
      theme="green"
      type="small"
    >Green Small</Button>
  ).toJSON();
  expect(tree).toMatchSnapshot();
});

test('renders correctly green normal', () => {
  const tree = renderer.create(
    <Button
      theme="green"
      type="normal"
    >Green Normal</Button>
  ).toJSON();
  expect(tree).toMatchSnapshot();
});

test('renders correctly purple small', () => {
  const tree = renderer.create(
    <Button
      theme="purple"
      type="small"
    >Purple Small</Button>
  ).toJSON();
  expect(tree).toMatchSnapshot();
});

test('renders correctly purple normal', () => {
  const tree = renderer.create(
    <Button
      theme="purple"
      type="normal"
    >Purple Normal</Button>
  ).toJSON();
  expect(tree).toMatchSnapshot();
});

test('renders correctly green modal', () => {
  const tree = renderer.create(
    <Button
      theme="green"
      type="modal"
    >Green Modal</Button>
  ).toJSON();
  expect(tree).toMatchSnapshot();
});

test('renders correctly red modal', () => {
  const tree = renderer.create(
    <Button
      theme="red"
      type="modal"
    >Red Modal</Button>
  ).toJSON();
  expect(tree).toMatchSnapshot();
});

test('renders correctly small disabled', () => {
  const tree = renderer.create(
    <Button
      type="small"
      disabled={true}
    >Small Disabled</Button>
  ).toJSON();
  expect(tree).toMatchSnapshot();
});

test('renders correctly small disabled', () => {
  const tree = renderer.create(
    <Button
      type="normal"
      disabled={true}
    >Normal Disabled</Button>
  ).toJSON();
  expect(tree).toMatchSnapshot();
});

test('renders correctly bordered', () => {
  const tree = renderer.create(
    <Button
      type="bordered"
      disabled={true}
    >Bordered</Button>
  ).toJSON();
  expect(tree).toMatchSnapshot();
});


// @flow
import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({

  /* button + text default */
  button: {
    // alignItems: 'center',
    alignSelf: 'center',
    borderRadius: '$Buttons.borderRadius',
    height: 50,
    justifyContent: 'center',
    marginTop: 10,
  },

  buttonFullWidth: {
    alignSelf: 'stretch',
  },

  text: {
    color: '#FFF',
    fontSize: 11,
    fontWeight: 'bold',
    letterSpacing: .5,
    textAlign: 'center',
    marginLeft: 10,
    marginRight: 10,
  },

  /* theme */
  purple: {
    backgroundColor: '$Colors.Purple',
  },

  green: {
    backgroundColor: '$Colors.Green',
  },

  /* size='small | normal | modal | text' */
  small: {
    height: 30,
  },

  textsmall: {
    fontSize: 10,
  },

  normal: {
    height: 50,
  },

  textnormal: {
    fontSize: 12,
  },

  modal: {
    height: 36,
    backgroundColor: '$Colors.Red',
  },

  textmodal: {
    fontSize: 14,
  },

  bordered: {
    height: 50,
    backgroundColor: 'transparent',
    borderWidth: 2,
    borderColor: '$Colors.Purple',
  },

  /* button + text default */
  /* disabled */
  buttonDisabled: {
    backgroundColor: '$Colors.Disabled',
  },

  textDisabled: {
  },

  /* is loading */
  buttonLoadingpurple: {
    backgroundColor: '$Colors.Purple',
  },

  buttonLoadinggreen: {
    backgroundColor: '$Colors.Green',
  },
});

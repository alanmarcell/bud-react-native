import 'react-native';
import React from 'react';
import RoundedIcon from '../';
import renderer from 'react-test-renderer';

test('renders correctly', () => {
  const tree = renderer.create(
    <RoundedIcon />
  ).toJSON();
  expect(tree).toMatchSnapshot();
});

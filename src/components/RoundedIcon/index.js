import React, { Component, PropTypes } from 'react';
import { View, Image } from 'react-native';
import Icon from 'components/Icon';
import R from 'ramda';

import styles from './styles';

export default class RoundedIcon extends Component {
  static propTypes = {
    source: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.func,
      PropTypes.string,
    ]),
    style: React.PropTypes.any,
    object: React.PropTypes.object,
  };

  static defaultProps = {
    source: 'budbuds',
    object: { synced: true },
  };

  renderStatusIcon() {
    const { synced, synced_errors } = this.props.object || {};
    if (synced_errors && synced_errors.length > 0) {
      return (
        <Icon
          name="sync_problem"
          style={styles.statusIcon}
          size={styles._statusIcon.fontSize}
          color={styles._statusSyncProblem.color}
        />
      );
    } else if (synced === false) {
      return (
        <Icon
          name="update"
          style={styles.statusIcon}
          size={styles._statusIcon.fontSize}
          color={styles._statusUpdate.color}
        />
      );
    }
  }

  render() {
    let { style, iconStyle, containerStyle, source } = this.props;
    let icon = null;
    if (typeof source === 'string') {
      iconStyle = R.merge(styles._icon, iconStyle);
      icon = <Icon name={source} size={iconStyle.fontSize} color={iconStyle.color} />;
    } else {
      icon = <Image source={source} />;
    }

    return (
      <View style={[styles.container, containerStyle]}>
        <View style={R.flatten([styles.radius, style])}>
          {icon}
        </View>
        {this.renderStatusIcon()}
      </View>
    );
  }
}

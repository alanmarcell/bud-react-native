// @flow
import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  container: {
    alignSelf: 'center',
    width: 50,
    height: 50,
  },

  thumbDefaultSize: {
    width: 21,
    height: 24
  },

  selectedThumbSize: {
    height: 50,
    width: 50,
  },

  icon: {
    fontSize: 18,
    color: '#95989A'
  },

  radius: {
    flex: 1,
    backgroundColor: '$Colors.LightSilver',
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },

  statusIcon: {
    position: 'relative',
    alignSelf: 'flex-end',
    fontSize: 18,
    marginTop: -18,
    backgroundColor: 'transparent',
  },

  statusSyncProblem: {
    color: 'red',
  },

  statusUpdate: {
    color: 'green',
  }
});

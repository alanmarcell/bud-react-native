// @flow
import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  listView: {
    backgroundColor: '$Colors.background',
  },

  footer: {
    flex: 1,
    height: 75,
    alignSelf: 'baseline'
  },

  row: {},
  'row:first-child': {
    marginTop: 5,
  }
});

import React from 'react';
import R from 'ramda';
import { ActivityIndicator, FlatList } from 'react-native';
import { footer } from '../../components';
import BlankList from '../../components/BlankList';
import Refreshable from '../../utils/components/Refreshable';
import { I18n } from '../../utils/I18n';

const Footer = footer(ActivityIndicator);

const renderList = ProductCard => {
  return class List extends React.PureComponent {
    static defaultProps = {
      products: [],
      onProductPress: () => null,
      blakListProps: {}
    };

    static propTypes = {
      products: React.PropTypes.array.isRequired,
      productsType: React.PropTypes.string.isRequired,
      onProductPress: React.PropTypes.func,
      loadMoreEntries: React.PropTypes.func,
      hasNextPage: React.PropTypes.bool,
      loading: React.PropTypes.bool,
    };


    renderFooter = () => {
      const { loading } = this.props.refreshControl;
      return (<Footer loading={loading} />);
    }

    renderItem = ({ item, index }) => {
      const { products ,onProductPress } = this.props;

      return (
        <ProductCard
          {...item}
          onPress={()=> onProductPress(item.id) }
          listLength={products.listLength}
          // Its a good practice to pass the radix
          // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/parseInt
          listIndex={parseInt(index, 10)}
        />
      );
    }

    renderBody = () => {
      const { products, productsType, icon, shouldSkipBlankList, blakListProps } = this.props;
      
      if (products.length <= 0 && !shouldSkipBlankList) {
        const title = !R.isNil( blakListProps.title) ? blakListProps.title : productsType;
        const message = !R.isNil( blakListProps.message) ? blakListProps.message : productsType;
        return (
          <BlankList
            icon={icon}
            title={I18n.t(`${title}.blank.title`)}
            message={I18n.t(`${message}.blank.message`)}
          />
        );
      } else {
        return null;
      }
    }

    loadMore = () => {
      const { loadMoreEntries, hasNextPage } = this.props;
      const { loading } = this.props || this.props.refreshControl;

      if (!loading && hasNextPage) {
        loadMoreEntries();
      }
    }

    render() {
      const props = this.props;
      const { products, refreshControl } = this.props;
      

      return (<FlatList {...props}
        data={products}
        refresing={refreshControl.loading}
        refreshControl={Refreshable(refreshControl)} 
        keyExtractor={(item, index) => index}
        renderItem={this.renderItem}
        ListFooterComponent={this.renderFooter}
        ListHeaderComponent={this.renderBody}
        onEndReached={this.loadMore}
        />);
    }
  };
};

const productsList = ProductCard => {
  const List = renderList(ProductCard);
  return class ProductsList extends React.PureComponent {
    static defaultProps = {
      products: [],
    };

    static propTypes = {
      products: React.PropTypes.array.isRequired,
      productsType: React.PropTypes.string.isRequired,
      refreshControl: React.PropTypes.object.isRequired,
      onProductPress: React.PropTypes.func,
    };

    render() {
      return (
        <List {...this.props} />
      );
    }
  };
};


export { productsList };
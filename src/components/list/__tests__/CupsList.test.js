import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import CupsList from '../';
import moment from 'moment';

const cups = [
  {id: 'id_cup_1', name: 'Cup BudBuds', startedAt: moment().add(10, 'day').toDate(), endAt:moment().add(15, 'day').toDate() },
  {id: 'id_cup_1', name: 'Cup BudBuds', startedAt: moment().subtract(10, 'day').toDate(), endAt:moment().subtract(5, 'day').toDate() }
];

describe('<CupsList />', () => {

  test.skip('renders correctly', () => {
    const wrapper = renderer.create(<CupsList cups={cups} productType="cups" />).toJSON();
    expect(wrapper).toMatchSnapshot();
  });

  // test('children struct', () => {
  //   const wrapper  = renderer.create(<CupsList plants={plants} />, context).dive(context);
  //   const children = wrapper.find('StaticRenderer');

  //   expect(children.length).toEqual(10);
  //   expect(children.at(0).dive().find('PlantCardItem')).toBeTruthy();
  // });
});

describe('<CupsList /> empty', () => {
  test.skip('renders correctly', () => {
    const wrapper = renderer.create(<CupsList cups={[]} productType="cups" />);
    expect(wrapper.find('BlankList')).toBeTruthy();
    expect(wrapper).toMatchSnapshot();
  });
});

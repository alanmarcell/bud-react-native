import React, { Component } from 'react';
import { Image } from 'react-native';

export default class Rating extends Component {
  render() {
    const { style } = this.props;
    return (
      <Image style={style} source={require('./images/icon-stars.png')} />
    );
  }
}

import { Component } from 'react';
import U from 'utils';
import { I18n } from 'utils/I18n';

export default class StrainSelect extends Component {
  serializeRelation(doc, option) {
    const value = doc[option] || {};
    let result = null;
    if (value.id) {
      result = {
        index  : value.id,
        label  : value.name
      };

      const type = value.__typename.toUpperCase();
      if (type === 'STRAIN') {
        result.breeder = value.breeder;
      } else {
        result.type = type;
      }
    }
    return result;
  }

  unserializeRelation(value) {
    if (value) {
      return { id: value.index, type: value.type };
    }
    return value;
  }

  searchStrain = async (query, current) => {
    return this.searchParent(query, current, 'strains');
  };

  searchParent = async (query, current, type = 'plants') => {
    const { searchParent } = this.props;
    const scope = ['forms', 'searchParent'];
    query = query.trim();
    const results = await searchParent(query, type);

    const extras = [];
    if (!U.isBlank(current) && !U.isBlank(current.index)) {
      extras.push({
        string: I18n.t([...scope, 'remove'], current),
        original: null,
      });
    }

    if (!U.isBlank(query) && type === 'strains') {
      extras.push({
        string: I18n.t([...scope, 'add'], { query }),
        original: {
          index: '__new__',
          label: query,
          // type : type === 'plants' ? 'PLANT' : 'STRAIN',
        }
      });
    }

    results.data = [...extras, ...results.data];
    return results;
  };

  formatStrain(string, strain, forContent = false) {
    if (strain) {
      const title = forContent ? 'breeder' : '**breeder**';
      return `${string}${strain.breeder ?  ` - ${title}: ${strain.breeder}` : ''}`;
    } else {
      return string;
    }
  }
}

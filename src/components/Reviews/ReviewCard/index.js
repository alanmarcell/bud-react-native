import React, { Component, PropTypes } from 'react';
import { View, Text, Image } from 'react-native';
import R from 'ramda';

import ReviewTag      from 'components/Reviews/ReviewTag';
import Stars              from 'components/Stars';

import {
  Moment,
  Touchable,
} from 'utils/components';

import styles from './styles';

const date      = new Date('2017-02-27T10:00:00');

export default class ReviewCard extends Component {
  static defaultProps = {
    type: 'task',
    userName: 'Lorem Ipsum',
    created_at: date,
    /*eslint no-console: 0*/
    onRemove: () => { console.log('Clicked'); },
    // type == task:
    comment: 'Paria sunt igitur. Odium autem et invidiam facile vitabis.',
    // type == review:
    review: [],
    rating: 2,
  };

  static propTypes = {
    type:       PropTypes.oneOf(['task', 'review']).isRequired,
    userName:   PropTypes.string.isRequired,
    created_at: PropTypes.instanceOf(Date),
    onRemove:   PropTypes.func,
    style:      PropTypes.oneOfType([
      PropTypes.shape(),
      PropTypes.number,
    ]),
    // type == task:
    comment:    PropTypes.string,
    // type == review:
    tags:       PropTypes.array,
    rating:     PropTypes.number,
  };

  renderRemoveButton() {
    const { onRemove } = this.props;
    return (
      <Touchable onPress={onRemove} style={styles.removeButtonWrapper} >
        <Image source={require('./images/ico-remove.png')} style={styles.removeButton} />
      </Touchable>
    );
  }

  renderContentByType = (selecteds) => {
    const { type } = this.props;
    if (type === 'task') {
      const { comment } = this.props;
      return (<Text  style={[styles.comment]}>{comment}</Text>);
    }

    if (type === 'review') {
      // TODO: Whey is not beeing used?
      //const { score } = this.props;
      return (
        <View>
          <Stars startSize={12} selecteds={selecteds} style={{marginTop: 10, marginBottom: 5}} />
          <View style={{flexDirection: 'row', marginTop: 5}}>
            { this.renderTags() }
          </View>
        </View>
      );
    }
  }

  renderTags() {
    const { review, consumption } = this.props;
    const compactedTags = R.reduce(
      (acc, { tags }) => [ ...acc,  ...(tags || []) ],
    [...(consumption||[])], review);

    if (compactedTags.length <= 0) return null;

    // 5 viewable tags, rest under `+X` transparent tag
    const groupedTags = R.splitEvery(3, compactedTags);

    return (
      <View style={{flexDirection: 'row'}}>
        {
          R.head(groupedTags).map((tag, index) => {
            return (
              <ReviewTag
                key={`${tag}.${index}`}
                label={tag}
              />
            );
          })
         }
        { groupedTags[1] ? <ReviewTag label="+" count={groupedTags[1].length} /> : null}
      </View>
    );
  }

  render() {
    const { score, user: { name }, created_at } = this.props;
    const roundedScore = score.toFixed(2);
    return (
      <View style={styles.cardContainer}>
        <View style={styles.iconWrapper}>
          <Image source={require('./images/ico-user.png')} style={{width: 30, height: 30}} />
        </View>
        <View style={styles.commentWrapper}>
          <Text  style={[styles.userName]}>{name}</Text>
          <Moment
            date={created_at}
            action="format"
            args={['L']}
            containerStyle={styles.momentContainerStyle}
            style={styles.momentText} />
          { this.renderContentByType(roundedScore) }
        </View>
        <View>
          <Text style={styles.score}>{roundedScore}</Text>
        </View>
        { /*this.renderRemoveButton() */ }
      </View>
    );
  }
}

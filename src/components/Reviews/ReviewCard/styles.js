import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  cardContainer: {
    backgroundColor: '#FFFFFF',
    marginHorizontal: 10,
    marginBottom: 5,
    flexDirection: 'row',
    // alignItems: 'center',
    // justifyContent: 'center',
    // $outline: 1,
    paddingTop: 10,
    paddingBottom: 15,
    paddingRight: 10,
  },
  iconWrapper: {
    // flexDirection: 'column',
    // justifyContent: 'flex-start',
    alignItems: 'center',
    flex: 1,
    // $outline: 1,
  },
  commentWrapper: {
    flex: 6,
    // $output: 1,
  },
  momentContainerStyle: {
    justifyContent: 'flex-start',
  },
  momentText: {
    alignSelf: 'flex-start',
    fontSize: 10,
    color: '$Colors.Disabled',
  },
  userName: {
    fontWeight: 'bold',
    fontSize: 12,
    color: '$Colors.Gray',
  },

  comment: {
    fontSize: 12,
    color: '$Colors.Gray',
    marginTop: 10,
  },
  score: {
    fontSize: 32,
    justifyContent: 'center',
    alignItems: 'center',
  }

});

import React from 'react';
import 'react-native';
import renderer from 'react-test-renderer';

import { shallow } from 'enzyme';

import { context } from '../../../../__mocks__/stateHelper';

import CommentCard from '../';

const defaultProps = {
  user: {
    name: 'Mario Fofoca',
  },
  comment: 'Paria sunt igitur. Odium autem et invidiam facile vitabis.',
  onRemove: () => {},
};

test.skip('renders correctly comment card', () => {
  const tree = renderer.create(
    <CommentCard {...defaultProps} />
  ).toJSON();
  expect(tree).toMatchSnapshot();
});

test.skip('close button calls onRemove props fn', () => {
  const onRemove = jest.fn();
  const props = { ...defaultProps, onRemove };
  const component = shallow(<CommentCard {...props} />, context);
  const touchable = component.find('Touchable');
  touchable.simulate('press');
  expect(onRemove).toHaveBeenCalled();
});

import React, { Component, PropTypes } from 'react';
import { View, Text } from 'react-native';
import _ from 'lodash';
import chroma from 'chroma-js';
import SeededShuffle from 'seededshuffle';

import { I18n } from 'utils/I18n';

import styles from './styles';
import { TagsDictionary } from '../ReviewEdit/ReviewDictionary';

// http://www.graphviz.org/doc/info/colors.html
const pallet = SeededShuffle.shuffle(
  chroma
  .scale(chroma.brewer.Spectral)
  .domain([0, TagsDictionary.length])
  .colors(TagsDictionary.length)
, Math.PI);

const colors = _.fromPairs(_.map(_.sortBy(TagsDictionary, ['length']), (tag, index) => [tag, pallet[index]]));

export default class ReviewTag extends Component {
  static defaultProps = {
    count: 1,
  }

  static propTypes = {
    label: PropTypes.string.isRequired,
    color: PropTypes.string,
    count: PropTypes.number,
  }

  render() {
    let { color, label, count } = this.props;

    let bgColor = color ? color : ((label === '+') ? 'transparent' : colors[label]);
    let bgClass = bgColor ? { borderColor: bgColor, backgroundColor: bgColor } : null;
    let textClass = null;

    if (bgColor === 'transparent') {
      let textColor = '#95989A';
      bgClass   = { borderWidth: 2, borderColor: textColor };
      textClass = { color: textColor, fontWeight:'bold' };
    } else {
      textClass = { color: chroma(bgColor).luminance() > 0.4 ? chroma('#000') : chroma('#fff') };
    }

    if (label !== '+') {
      label = I18n.t(['reviews', 'tags', label]);
      label = _.truncate(label, { length: 15 });
    }

    const content = `${label}${count && (count > 1 || label === '+') ? ` ${count}` : ''}`;

    return (
      <View style={[styles.tagWrapper, bgClass]}>
        <Text style={[styles.label, textClass]}>{content}</Text>
      </View>
    );
  }
}

// @flow
import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  tagWrapper: {
    paddingTop: 2,
    paddingBottom: 0,
    paddingHorizontal: 4,
    borderRadius: 3,
    marginRight: 3,
    marginBottom: 3,
    borderWidth: 2,
  },
  label: {
    color: '#FFFFFF',
    fontSize: 11,
  }
});

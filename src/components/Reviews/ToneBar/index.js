import React, { Component } from 'react';
import { View } from 'react-native';

import EStyleSheet from 'react-native-extended-stylesheet';
import LinearGradient from 'react-native-linear-gradient';
import { domain, colors, gradient } from 'components/BudForms/Factories/ToneFactory';
import _ from 'lodash';

export default class ToneBar extends Component {
  static defaultProps = {
    value: 80,
  };

  static propTypes = {
    value: React.PropTypes.number.isRequired,
  };

  renderValue() {
    let { value } = this.props;
    const color = colors[value - 1];

    return (
      <View style={[styles.color, { backgroundColor: color }]} />
    );
  }

  renderColorBar() {
    const { value } = this.props;
    const start = colors.slice(0, value);
    const end   = _.range(value + 1, domain.max).map(() => '#CCCCCC');

    return (
      <LinearGradient
        { ...gradient }
        style={styles.colorsBar}
        colors={[...start, ...end]}
      />
    );
  }

  render() {
    return(
      <View style={styles.container}>
        {this.renderValue()}
        {this.renderColorBar()}
      </View>
    );
  }
}

const styles = EStyleSheet.create({
  $barHeight: 20,

  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    height: '$barHeight',
  },

  colorsBar: {
    flex: 1,
    height: '$barHeight',
  },

  color: {
    width: '$barHeight',
    height: '$barHeight',
    marginRight: 7,
    borderRadius: 5,
  },
});

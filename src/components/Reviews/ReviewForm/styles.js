// @flow
import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  card: {
    backgroundColor: '$Colors.cardsBackground',
    padding: 10,
    marginBottom: 10,
  },

  cardLabel: {
    flex: 1,
    marginBottom: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },

  cardLabelText: {
    color: '$Colors.Gray',
    fontSize: 16,
    marginLeft: 2,
  },

  cardIcon: {
    color: '$Colors.Gray',
    fontSize: 16,
  },

  cardDescript: {
    marginTop: 5,
    color: '$Colors.Gray',
  },

  tagLabel: {
    fontSize: 12,
    marginLeft: 3,
    marginBottom: 15,
  },

  tagLabelChecked: {
    color: '$Colors.Green',
    fontSize: 12,
    marginLeft: 3,
    marginBottom: 15,
  },

  tagIcon: {
    '@media android': {
      marginTop: 3,
    },
    color: '$Colors.Silver',
    fontSize: 14,
    alignSelf: 'center',
  },

  tagIconChecked: {
    '@media android': {
      marginTop: 3,
    },
    color: '$Colors.Green',
    fontSize: 14,
  },

  resumeTitle: {
    fontSize: 20,
    color: '#CCCCCC',
    fontWeight: '100',
  },

  resumeFeatureItemWrapper: {
    marginTop: 15,
  },

  resumedFeatureItem: {
    paddingVertical: 12,
    paddingHorizontal: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  silverBorderTop: {
    borderTopWidth: 1,
    borderColor: '#EAEAEB',
  },

  silverBorderBottom: {
    borderBottomWidth: 1,
    borderColor: '#EAEAEB',
  },

  resumedFeatureLabel: {
    // color: '$Colors.grayStar', TODO: duplicated why?
    fontSize: 10,
    fontWeight: 'bold',
    color: '#95989A',
  },

  resumedFeatureTotalLabel: {
    color: '$Colors.Gray'
  },
  resumedFeatureTotalValue: {
    fontSize: 14,
    marginRight: 5,
  }
});


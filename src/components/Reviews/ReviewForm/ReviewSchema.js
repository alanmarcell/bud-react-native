import { SimpleSchema, OptionSchema } from 'components/BudForms/Schemas';
import ReviewDictionary from '../ReviewEdit/ReviewDictionary';
import R from 'ramda';

const FeaturesSchema = R.map(([featureName, feature]) => {
  // default
  let schema = {
    [`${featureName}`]: {
      type: Object,
      required: false,
    },
    [`${featureName}.score`]: {
      type: Number,
      defaultValue: 1,
      required: false,
    }
  };

  // color: adds `tone`
  if (feature.tone)
    schema[`${featureName}.tone`] = {
      type: Number,
      required: false
    };

  // when tags available
  if (feature.tags) {
    schema = {
      ...schema,
      [`${featureName}.tags`]: {
        type: Array,
        required: false,
      },
      [`${featureName}.tags.$`]: {
        type: String,
        allowedValues: feature.tags,
        required: false,
      }
    };
  }
  return schema;
}, R.toPairs(ReviewDictionary));

const getCategorySchema = props => {
  return R.length(props.categoriesToVote || []) < 2
    ? {}
    : {
      category: {
        type: OptionSchema,
        required: true
      },
    };
};

const getReviewSchema = props => {
  return new SimpleSchema(R.mergeAll(
    FeaturesSchema.concat(getCategorySchema(props))
  ));
};

export {
  getReviewSchema
};

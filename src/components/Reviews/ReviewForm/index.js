import React from 'react';
import { Text, View } from 'react-native';
import U from 'utils';
import Icon from 'components/Icon';
import { I18n } from 'utils/I18n';
import R from 'ramda';
import BudForms from 'components/BudForms';
import { I18nField } from 'components/BudForms/Fields';
import { AutoCheckboxFactory, ScoreFactory, ToneFactory } from 'components/BudForms/Factories';
import { ObjectFactory } from 'utils/Form/Factories';
import { CheckboxOption } from 'utils/Form/Factories/CheckboxFactory';
import Stars from 'components/Stars';

import { getReviewSchema } from './ReviewSchema';
import ReviewDictionary from '../ReviewEdit/ReviewDictionary';

import { SelectFactory } from 'utils/Form/Factories';
import styles from './styles';

const getOption = obj => ({
  key: obj.id,
  label: obj.name
});

const categoriesToOptions = R.map(getOption);

class BudCategory extends React.PureComponent {

  categoryOptions = () => {
    return categoriesToOptions(this.props.categoriesToVote);
  }

  render() {
    const { categoriesToVote } = this.props;

    if (R.isEmpty(categoriesToVote)) {
      return null;
    }

    const props = R.length(categoriesToVote) === 1
      ? {
        initLabel: categoriesToVote[0].name,
        initValue: categoriesToVote[0]
      }
      : {};

    return (
      <View style={{ flex: 1, marginBottom: 30 }}>
        <I18nField
          fieldName="category"
          i18nScope="forms.category"
          options={this.categoryOptions()}
          type={SelectFactory}
          {...props}
        />
      </View>
    );
  }
}

class CardFactory extends ObjectFactory {
  static defaultProps = {
    ...ObjectFactory.defaultProps,
    description: false,
  };

  static propTypes = {
    ...ObjectFactory.propTypes,
    description: React.PropTypes.bool,
  };

  renderLabel() {
    const { fieldName, description } = this.props;
    const scope = ['reviews', 'ratings'];
    return (
      <View key={fieldName}>
        <View style={styles.cardLabel}>
          <Icon name="tint" style={styles._cardIcon} />
          <Text style={styles.cardLabelText}>
            {I18n.T([...scope, fieldName, 'label'])}
          </Text>
        </View>
        {!description ? null : (
          <Text style={styles.cardDescript}>
            {I18n.T([...scope, fieldName, 'description'])}
          </Text>
        )}
      </View>
    );
  }
}

class TagOption extends CheckboxOption {
  renderLabel() {
    const { label, checked } = this.props;
    const style = checked ? styles.tagLabelChecked : styles.tagLabel;
    return (
      <Text style={style}>{label}</Text>
    );
  }

  renderIcon(icon, _style, checked) {
    let style = checked ? styles._tagIconChecked : styles._tagIcon;
    return <Icon name={icon} style={style} />;
  }
}

export default class ReviewForm extends React.PureComponent {

  static defaultProps = {
    userReviews: {}
  }

  static propTypes = {
    review: React.PropTypes.object,
    submitText: React.PropTypes.string.isRequired,
  };

  componentWillMount() {
    const defaultRatings = U.reduce((ratings, [key, feature]) => {
      if (feature.title) return ratings;

      return { ...ratings, [key]: { score: 1 } };
    }, {}, U.toPairs(U.filter(
      ({ hideScore }) => !hideScore, ReviewDictionary)));
    this.setState({ ...defaultRatings });
  }

  renderTag(props) {
    return <TagOption {...props} />;
  }

  renderFeatures() {
    const isCup = U.path(['bud', 'edition', 'id'], this.props);


    return U.map(([key, feature]) => {

      if(isCup && !feature.cup){
        return null;
      }

      if (feature.title)
        return (
          <Text key={key} style={{ fontSize: 20, marginBottom: 15, marginTop: 15 }}>
            {I18n.t(['reviews', 'titles', feature.title])}
          </Text>
        );

      return (
        <I18nField key={key} fieldName={key} description={feature.description} style={styles.card} type={CardFactory}>
          {!feature.hideScore && <I18nField fieldName="score" type={ScoreFactory} />}
          {this.renderColorFeature(feature)}
          {!isCup && this.renderTagFeature(key, feature)}
        </I18nField>
      );
    }, U.toPairs(ReviewDictionary));
  }

  renderTagFeature(key, { tags }) {
    const scope = ['reviews', 'tags'];
    return !tags ? null : (
      <I18nField
        fieldName="tags"
        i18nScope={scope}
        label={false}
        type={AutoCheckboxFactory}
        underline={false}
        renderOption={this.renderTag}
      />
    );
  }

  renderColorFeature({ tone, cup }) {
    return tone && !cup ? (<I18nField fieldName="tone" type={ToneFactory} />) : null;
  }

  renderRatingResume() {
    const total = this.calculateAverageRating();
    const formatedTotal = total.toPrecision(2);
    return (
      <View style={styles.card}>
        <Text style={styles.resumeTitle}>RESUMOS</Text>
        <View style={styles.resumeFeatureItemWrapper}>

          {U.map(([feature, { score }]) => {
            return (
              <View key={feature} style={[styles.resumedFeatureItem, styles.silverBorderTop]}>
                <Text style={styles.resumedFeatureLabel}>{I18n.T(`reviews.ratings.${feature}.label`)}</Text>
                <Stars selecteds={score} fieldName={feature} startSize={12} theme="gray" onPress={() => { }} />
              </View>
            );
          }, U.toPairs(
            U.filter(({ score }) => !!score, this.state)))}

          <View style={[styles.resumedFeatureItem, styles.silverBorderTop, styles.silverBorderBottom]}>
            <Text style={[styles.resumedFeatureLabel, styles.resumedFeatureTotalLabel]}>TOTAL</Text>
            <View style={{ flexDirection: 'row' }}>
              <Text style={[styles.resumedFeatureLabel, styles.resumedFeatureTotalLabel, styles.resumedFeatureTotalValue]}>{formatedTotal}</Text>
              <Stars selecteds={Math.floor(total)} fieldName={'total'} startSize={15} theme="yellow" onPress={() => { }} />
            </View>
          </View>

        </View>
      </View>
    );
  }


  calculateAverageRating() {
    const scoreCalc = (input) => {
      const baseMax = 7.7625;
      const baseMin = -2.8125;
      const limMax = 10;
      const limMin = 1;

      return ((limMax - limMin) * (input - baseMin) / (baseMax - baseMin)) + limMin;
    };

    const deltaCalc = {
      look: function (num) { return { score: num * 1 }; },
      color: function (num) { return { score: num * 1 }; },
      touch: function (num) { return { score: num * 1 }; },
      smell: function (num) { return { score: num * 1 }; },
      flavor: function (num) { return { score: num * 1 }; },
      high: function (num) { return { score: num * 1.5 }; },
      merriment: function (num) { return { score: num * 1.4 }; },
      euphoria: function (num) { return { score: num * 1.1 }; },
      relaxation: function (num) { return { score: num * 1.1 }; },
      criativity: function (num) { return { score: num * 1.4 }; },
      throat_soreness: function (num) { return { score: num * -1.3 }; },
      headache: function (num) { return { score: num * -1.3 }; },
      anxiety: function (num) { return { score: num * -1 }; },
      fatigue: function (num) { return { score: num * -1 }; },
      nausea: function (num) { return { score: num * -1.2 }; },
      medicinal: function (num) { return { score: num * 1.5 }; }
    };

    if (!this.state) return 0;

    const scores = U.pipe(
      U.toPairs,
      U.map(([, { score }]) => score),
    )(U.mapObjIndexed((num, key) => deltaCalc[key](num.score),
      U.filter(({ score }) => !!score, this.state)));


    if (U.isEmpty(scores)) return 0;

    const totalScore = U.sum(scores);
    const ratingLength = Object.keys(scores).length;

    return scoreCalc(totalScore / ratingLength);
  }

  _handleSubmit = ({ ...values }) => {
    const { saveReview } = this.props;

    const { categoriesToVote } = this.props;

    const category = R.isNil(values.category) && R.length(categoriesToVote) === 1
      ? getOption(categoriesToVote[0])
      : values.category;

    saveReview({
      ...values,
      category
    });
  };

  render() {
    const pickedDoc = this.state;
    const formProps = {
      doc: pickedDoc,
      schema: getReviewSchema(this.props),
      onChange: ({ ...values }) => this.setState({ ...values }),
      submitText: this.props.submitText,
      onSubmit: this._handleSubmit,
    };
    return (
      <BudForms {...formProps}>
        <BudCategory {...this.props} />
        {this.renderFeatures()}
        {/* {this.renderRatingResume()} */}
      </BudForms>
    );
  }
}

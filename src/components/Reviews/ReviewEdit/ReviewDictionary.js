import U from 'utils';

// FEATURES ID's
//
// {
//   look            : 'e092768d-0bdd-4ff1-94c1-a7fa57b7547c',
//   color           : '7100640b-28f6-499e-b841-39856b76470a',
//   touch           : 'c2ca8fee-b416-4c2c-a02b-ea31acac9ba6',
//   smell           : '2ed12abd-9a2b-4d1f-877b-741d8085dbbc',
//   flavor          : '2d1cbc58-5908-4884-a14a-079f8e025bb8',
//   high            : '91069b3c-43c0-4c3e-9dfb-ab407c290889',
//   effect          : '35c0795f-3885-4457-91f2-4c771a68757e',
//   merriment       : 'f1c1caff-857c-4717-a14e-9e7ebe988154',
//   euphoria        : 'f7d2e75a-374a-4a86-bb78-91791efd14eb',
//   relaxation      : 'e624b1d8-aa93-44d2-bb37-e841125af5e6',
//   criativity      : '3e12691c-893d-4959-affb-7f5345ccebb9',
//   throat_soreness : 'ac18a12c-e9e4-4060-8b59-4a0b43d4a69d',
//   headache        : '87b29093-0a18-4238-b889-d986f1fcef6f',
//   anxiety         : '0835d9a9-081f-4a4f-829c-82389e0b74dd',
//   fatigue         : '074ed171-27b9-4969-abf0-e70ca552cc92',
//   nausea          : '6f9eeb0e-4d55-44f6-bb55-9549d7d6deb2',
//   medicinal       : 'ab0b96b5-ac9e-4f50-a574-0131d5c6d5e2'
// }



const ReviewDictionary = {
  consumption: {
    id: '40a9860a-d05d-11e7-8fab-cec278b6b50a',
    hideScore: true,
    tags: [
      'smoke',
      'vaporizer',
      'edibles',
      'drink',
      'pills',
      'spray',
      'dye',
    ]
  },
  'look': {
    'id': 'e092768d-0bdd-4ff1-94c1-a7fa57b7547c',
    'description': true,
    cup: true,
    'tags': [
      'brillante',
      'mate',
      'basto',
      'fino',
      'limpio',
      'sucio',
      'compacto',
      'suelto',
      'viejo',
      'fresco',
      'seco'
    ]
  },
  'color': {
    'id': '7100640b-28f6-499e-b841-39856b76470a',
    'description': true,
    'tone': true
  },
  'touch': {
    'id': 'c2ca8fee-b416-4c2c-a02b-ea31acac9ba6',
    'description': true,
    'tags': [
      'duro',
      'blando',
      'pastoso',
      'pegajoso',
      'seco',
      'crujiente',
      'polvoriento',
      'denso',
      'pesado',
      'ligero'
    ]
  },
  'dry_cure': {
    'id': '7e5d33c0-bba2-4da1-b4fc-f1458acca7e3',
    cup: true
  },
  'smell': {
    'id': '2ed12abd-9a2b-4d1f-877b-741d8085dbbc',
    'description': true,
    cup: true,
    'tags': [
      'acido',
      'dulce',
      'profundo',
      'ligero',
      'sabroso',
      'picante',
      'orina',
      'heces',
      'moho',
      'tierra',
      'fruta_fresca',
      'fruta_madura',
      'fruta_podrida',
      'madera',
      'incienso',
      'flores',
      'queso',
      'pimenton',
      'vino',
      'carne',
      'sangre_fresca',
      'sudor'
    ]
  },
  'flavor': {
    'id': '2d1cbc58-5908-4884-a14a-079f8e025bb8',
    'description': true,
    cup: true,
    'tags': [
      'acido',
      'dulce',
      'profundo',
      'ligero',
      'sabroso',
      'picante',
      'orina',
      'heces',
      'moho',
      'tierra',
      'fruta_fresca',
      'fruta_madura',
      'fruta_podrida',
      'madera',
      'incienso',
      'flores',
      'queso',
      'pimenton',
      'vino',
      'carne',
      'sangre_fresca',
      'sudor'
    ]
  },
  'high': {
    'id': '91069b3c-43c0-4c3e-9dfb-ab407c290889',
    cup: true
  },
  'effect': {
    'title': 'effects'
  },
  'merriment': {
    'id': 'f1c1caff-857c-4717-a14e-9e7ebe988154',
  },
  'euphoria': {
    'id': 'f7d2e75a-374a-4a86-bb78-91791efd14eb',
  },
  'relaxation': {
    'id': 'e624b1d8-aa93-44d2-bb37-e841125af5e6',
  },
  'criativity': {
    'id': '3e12691c-893d-4959-affb-7f5345ccebb9',
  },
  'throat_soreness': {
    'id': 'ac18a12c-e9e4-4060-8b59-4a0b43d4a69d',
  },
  'headache': {
    'id': '87b29093-0a18-4238-b889-d986f1fcef6f',
  },
  'anxiety': {
    'id': '0835d9a9-081f-4a4f-829c-82389e0b74dd',
  },
  'fatigue': {
    'id': '074ed171-27b9-4969-abf0-e70ca552cc92',
  },
  'nausea': {
    'id': '6f9eeb0e-4d55-44f6-bb55-9549d7d6deb2',
  },
  'medicinal': {
    'id': 'ab0b96b5-ac9e-4f50-a574-0131d5c6d5e2',
    'description': true,
    'tags': [
      'alzheimer',
      'anti_inflamatorio',
      'propiedades_antitumorales',
      'ansiedad',
      'malestar_de_artritis',
      'enfermedades_autoinmunes',
      'cancer',
      'efectos_carcinogenos',
      'colico',
      'obstaculo',
      'creatividad',
      'enfermedad_de_crohn',
      'reducir_el_consumo_de_alcohol',
      'depresion',
      'sindrome_de_dravet',
      'eliminar_pesadillas',
      'epilepsia',
      'euforia',
      'glaucoma',
      'felicidad',
      'dolor_de_cabeza',
      'hepatitis_c',
      'aumenta_la_efectividad_del_tratamiento',
      'enfermedades_inflamatorias_intestinales',
      'insomnio',
      'concentracion_mental',
      'metabolismo_hacia_abajo',
      'metabolismo',
      'esclerosis_multiple',
      'relajante_muscular',
      'tension_muscular_y_espasmo',
      'nauseas_y_vomitos',
      'alivio_del_dolor',
      'parkinson',
      'proteger_el_cerebro_de_conmociones_cerebrales_y_trauma',
      'protege_el_cerebro_despues_de_un_derrame_cerebral',
      'regular_la_produccion_de_insulina',
      'estimular_el_apetito',
      'estres',
      'sintomas_del_lupus',
      'veteranos_que_sufren_de_p_t_s_d'
    ]
  },
};


export function serializeReview({ category, ...values
}) {
  const ratings = [
    ...U.pipe(
      U.toPairs,
      U.map(([key, { ...data
      }]) => {
        return {
          featureId: ReviewDictionary[key].id,
          ...data
        };
      }),
    )(U.filter((a) => a.score, values))
  ];

  let tags = {};
  U.pipe(
    U.filter((a) => !a.score), 
    U.keys,
    U.map(a => tags[a] = U.path([a, 'tags'], values))
  )(values);

  return {
    categoryId: U.path(['key'], category),
    ratings,
    ...tags,
  };
}

export const TagsDictionary = U.flatten(U.map(([, {
  tags
}]) => tags || [], U.toPairs(ReviewDictionary)));

export default ReviewDictionary;

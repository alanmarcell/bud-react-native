import {
  serializeReview
} from './ReviewDictionary';
import U from 'utils';

test('searilize correctly', () => {
  const values = {
    anxiety: {
      score: 1
    },
    criativity: {
      score: 10
    },
    consumption: {
      tags: ['edibles', 'spray']
    },    
    consumption2: {
      tags: ['smoke', 'drink']
    }
  };

  const serialized = serializeReview(values);

  Object.keys( U.filter((valuesWithScore) => valuesWithScore.score, values)).map((key, index) => {    
    expect(serialized.ratings[index].score).toBe(values[key].score);
  });

  expect(...serialized.consumption).toBe(...values.consumption.tags);
  expect(...serialized.consumption2).toBe(...values.consumption2.tags);
});

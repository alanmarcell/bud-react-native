import React from 'react';
import { ActivityIndicator, InteractionManager, ScrollView } from 'react-native';
import U from 'utils';

import { BudHeader } from 'components';
import ReviewForm from 'components/Reviews/ReviewForm';

import withCategoriesToVote from 'controllers/helpers/withCategoriesToVote';

class ReviewEdit extends React.PureComponent {
  static propTypes = {
    bud       : React.PropTypes.object,
    review    : React.PropTypes.object,
    loading   : React.PropTypes.bool,
    saveReview: React.PropTypes.func.isRequired,
    submitText: React.PropTypes.string.isRequired,
  };

  state = {
    showForm: false,
  }

  _handleSubmit = ({review, ...values}) => {
    const { saveReview } = this.props;
    saveReview({
      ...values,
      review,
    });
  };

  getUserReviews() {
    const {
      authedUserId,
      bud: { reviews: { edges: reviews } }
    } = this.props;

    // TODO: improve validation
    const reviewedByMe = U.find((review) => review.node.reviewedBy.id === authedUserId, reviews);
    if (!reviewedByMe) return {};

    const { node: { ratings } } = reviewedByMe;
    return ratings;
  }

  userReviews() {
    const ratings = this.getUserReviews();
    const pickedDoc = U.pipe(
      U.map(({...feature}) => {
        const { feature: { name } } = feature;
        let schema = { score: feature.score };
        if (feature.tone) schema.tone = feature.tone;
        if (feature.tags) schema.tags = feature.tags;
        return {
          [`${name}`]: { ...schema }
        };
      }),
      U.mergeAll
    )(ratings);
    return pickedDoc;
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.setState({ showForm: true });
    });
  }

  render() {
    const { loading } = this.props;
    const { showForm } = this.state;

    if (loading) {
      return <ActivityIndicator />;
    }

    return (
      <ScrollView>
        <BudHeader {...this.props} />
        { showForm ? <ReviewForm {...this.props} /> : <ActivityIndicator /> }
      </ScrollView>
    );
  }
}

export default withCategoriesToVote(ReviewEdit);

// @flow
import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  container: {
    marginHorizontal: 15,
    marginBottom: 5,
    marginTop: 20,
    padding: 15,
    paddingBottom: 15,
    backgroundColor: '#FFFFFF',
  },
  title: {
    color: '$Colors.Disabled',
    fontWeight: 'bold',
    marginBottom: 10,
    // textTransform: 'uppercase',
  },
});


import React, { Component } from 'react';
import {
  ScrollView,
  View,
  Text,
} from 'react-native';
import R from 'ramda';
import { I18n } from 'utils/I18n';

import FeatureCard from 'components/Reviews/FeatureCard';
import UserReviewCard from 'components/Reviews/ReviewCard';
import ReviewTag from 'components/Reviews/ReviewTag';

import ReviewDictionary from 'components/Reviews/ReviewEdit/ReviewDictionary';

import styles from './styles';

export default class ReviewDetails extends Component {
  static propTypes = {
    review: React.PropTypes.object,
  }

  render() {
    const { review } = this.props;
    const {
      reviewedBy,
      score,
      ratings,
      createdAt,
      id,
      consumption
    } = review;

    return (
      <ScrollView>
        <View style={{marginTop: 20}}>
          <UserReviewCard
            key={`review.${id}`}
            type="review"
            user={reviewedBy}
            score={score}
            consumption={consumption}
            created_at={new Date(createdAt)}
            review={ratings} />
        </View>
        { this.renderConsumption() }
        { this.renderReviewAndRatings() }
      </ScrollView>
    );
  }

  renderReviewAndRatings() {
    return (
      <View>
        <View>
          { this.renderFeaturesCards() }
        </View>
      </View>
    );
  }

  renderFeaturesCards() {
    const { review } = this.props;
    const { ratings } = review;
    const ReviewDictionaryKeys = Object.keys(ReviewDictionary);

    let output = [];

    R.map((rating) => {
      const index = ReviewDictionaryKeys.indexOf(rating.feature.name);
      output[index] = rating;
    }, ratings);

    // clean undefined items
    const cleanRatings = R.filter(f => !!f, output);

    return R.map(rating => {
      const { tags, tone, score, feature: { name } } = rating;
      const type = tone ? 'slider' : 'tag';
      const title = I18n.t(['reviews', 'ratings', name, 'label']);
      return (
        <FeatureCard
          key={`${name}.${score}`}
          single
          fieldName={name}
          title={title}
          type={type}
          rating={score}
          tags={tags}
          tone={tone}
        />);
    }, cleanRatings);
  }

  renderConsumption() {
    const { review } = this.props;
    const { consumption } = review;
    
    if(R.isNil(consumption)){
      return null;
    }

    const title = I18n.T(['reviews', 'ratings', 'consumption', 'label']);
    return (
      <View style={styles.container}>
        <Text style={styles.title}>{title}</Text>
        <View style={{flexDirection: 'row', flexWrap: 'wrap' }}>
          {
            R.map((tag, index) => (
              <ReviewTag
                key={`${tag}.${index}`}
                label={tag}
                count={1}
              />
            ), consumption)
          }
        </View>
      </View>
    );
  }
}

import React, { Component } from 'react';
import { View, Text } from 'react-native';
import R from 'ramda';

import ToneBar   from 'components/Reviews/ToneBar';
import ReviewTag from 'components/Reviews/ReviewTag';
import Stars     from 'components/Stars';

import styles from './styles';

export default class FeatureCard extends Component {
  static propTypes = {
    type: React.PropTypes.oneOf(['tag', 'slider']).isRequired,
    single: React.PropTypes.bool,
    title: React.PropTypes.string,
    rating: React.PropTypes.number,
    tone: React.PropTypes.number,
    tags: React.PropTypes.array,
  }

  renderType = () => {
    const { type } = this.props;
    return this[`${type}Render`]();
  }

  tagRender = () => {
    const { tags, single } = this.props;
    if (R.isEmpty(tags) || R.isNil(tags)) return null;

    // 5 viewable tags, rest under `+X` transparent tag
    const groupedTags = R.splitEvery(3, tags);

    return (
      <View style={styles.contentWrapper}>
        <View style={{flexDirection: 'row', flexWrap: 'wrap' }}>
          { single &&
            R.map((tag, index) => (
              <ReviewTag
                key={`${tag}.${index}`}
                label={tag}
                count={1}
              />
            ), tags)
          }
          {
            !single &&
            R.head(groupedTags).map((tag, index) => {
              return (
                <ReviewTag
                  key={`${tag.label}.${index}`}
                  label={tag.tag}
                  count={tag.count}
                />
              );
            })
          }
          { !single && groupedTags[1] ? <ReviewTag label="+" count={groupedTags[1].length} /> : null}
        </View>
      </View>
    );
  }

  sliderRender = () => {
    return (
      <View style={styles.contentWrapper}>
        <ToneBar value={this.props.tone} />
      </View>
    );
  }

  render() {
    const { title, rating, fieldName } = this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.title}>{title.toUpperCase()}</Text>
        <View style={styles.rating}>
          <Stars selecteds={rating} startSize={12} fieldName={fieldName} theme="gray" />
        </View>
        { this.renderType() }
      </View>
    );
  }
}

// @flow
import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  container: {
    marginHorizontal: 15,
    marginBottom: 5,
    padding: 15,
    paddingBottom: 15,
    backgroundColor: '#FFFFFF',
  },
  title: {
    color: '$Colors.Disabled',
    fontWeight: 'bold',
    width:'35%'
    // textTransform: 'uppercase',
  },
  rating: {
    position: 'absolute',
    top: 15,
    right: 15,
  },
  contentWrapper: {
    marginTop: 15,
  }

});

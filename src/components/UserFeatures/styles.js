import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  container: {
    paddingHorizontal: 20,
  },

  card: {
    marginTop: 50,
    paddingHorizontal: 20,
    paddingVertical: 20,
    backgroundColor: '#FFFFFF',
  },

  lockerIconContainer: {
    width: 60,
    height: 60,
    marginTop: -40,
    marginBottom: 2,
  },

  lockerIcon: {
    fontSize: 25,
  },

  lockerIconDropshadow: {
    borderRadius: 60,
    backgroundColor: '#FFFFFF',
    '@media ios': {
      shadowColor: '$Colors.Gray',
      shadowOpacity: 0.1,
      shadowRadius: 2,
      shadowOffset: {
        height: -2,
        width: 0,
      },
    },
    '@media android': {
      // TODO: add dropshow for android
    },
  },

  buttonWrapperInlineChildren: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  buttonText: {
    fontWeight: 'bold',
    color: '$Colors.Purple',
    fontSize: 11,
    marginRight:5,
  },

  buttonIcon: {
    color: '$Colors.Purple',
    fontSize: 11,
  },
});


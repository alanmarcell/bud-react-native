import React, { Component } from 'react';
import { ScrollView, View, Text } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

import Button       from 'components/Button';
import RoundedIcon  from 'components/RoundedIcon';
import Icon         from 'components/Icon';
import MarkdownBox  from 'components/MarkdownBox';
import { I18n } from 'utils/I18n';

import styles from './styles';

export default class UserFeaturesRegister extends Component {
  static propTypes = {
    onRegister: React.PropTypes.func.isRequired,
  };

  _onContinue = () => this.props.onRegister();

  render() {
    const { contentKey } = this.props;

    return (
      <ScrollView style={styles.container}>
        <View style={styles.card}>
          <RoundedIcon source="budbuds"
            iconStyle={styles._lockerIcon}
            containerStyle={styles.lockerIconContainer}
            style={styles.lockerIconDropshadow}
          />
          <MarkdownBox styles={markdownStyle}>{I18n.t(contentKey)}</MarkdownBox>
        </View>
        <Button onPress={this._onContinue} theme="purple" type="bordered" style={styles.buttonWrapperInlineChildren}>
          <Text style={styles.buttonText}>{I18n.T('screens.pre-register.continue_button')}</Text>
          <Icon name="arrow-right" style={styles.buttonIcon} />
        </Button>
      </ScrollView>
    );
  }
}

const markdownStyle = EStyleSheet.create({
  listItemBullet: {
    fontSize: 15,
    lineHeight: 15,
    marginTop: -2,
    color: '$Colors.grayStar',
  },
  listItemText: {
    fontSize: 13,
    lineHeight: 13,
    color: '$Colors.grayStar',
  },
  heading2: {
    textDecorationLine: 'underline',
    fontSize: 18,
    alignSelf: 'flex-start',
    marginTop: 10,
    marginBottom: 10,
  },
  heading1: {
    fontSize: 22,
    color: '$Colors.Purple',
  },
  view: {
    marginBottom: 20
  }
});

import React, { Component, createElement } from 'react';
import { Text } from 'react-native';
import Markdown from 'react-native-simple-markdown';

import styles from './styles';

export default class MarkdownBox extends Component {
  static propTypes = {
    onOpenLink: React.PropTypes.func,
    styles: React.PropTypes.object,
  };

  _handleLink = (target) => {
    if (this.props.onOpenLink) {
      this.props.onOpenLink(target);
    }
  }

  _rules = () => {
    return({
      link: {
        react: (node, output, state) => {
          state.withinText = true;
          return createElement(Text, {
            style: styles.link,
            key: state.key,
            onPress: () => this._handleLink(node.target)
          }, output(node.content, state));
        },
      }
    });
  };

  render() {
    return (
      <Markdown rules={this._rules()} styles={{ ...styles, ...this.props.styles}}>
        {this.props.children}
      </Markdown>
    );
  }
}

import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  text: {
  },

  // Markdown
  heading: {
    alignSelf: 'center',
  },

  heading1: {
    fontWeight: 'bold',
    fontSize: 14,
    color: '$Colors.Purple',
  },

  paragraph: {
    color: '$Colors.Disabled',
    marginTop: 10,
    marginBottom: 10,
  },

  link: {
    fontWeight: 'bold',
    color: '$Colors.Purple',
  },

  hr: {
    height: 1,
    backgroundColor: '$Colors.LightSilver',
    marginBottom: 5,
    marginTop: 5,
  },

  strong: {
    // fontWeight: 'bold',
    color: '$Colors.Purple',
  },
});

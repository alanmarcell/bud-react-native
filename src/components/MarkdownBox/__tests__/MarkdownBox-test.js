import React from 'react';
import { shallow } from 'enzyme';

import Target from '../';

test('renders correctly', () => {
  const onOpenLink = jest.fn();
  const props = {
    children: '#title\n\n[link](target_link)',
    onOpenLink,
  };
  const wrapper = shallow(<Target {...props} />).dive();
  expect(wrapper).toMatchSnapshot();

  const link = wrapper.findWhere((node) => node.prop('onPress') instanceof Function);
  link.simulate('press');
  expect(onOpenLink).toHaveBeenCalledWith('target_link');
});

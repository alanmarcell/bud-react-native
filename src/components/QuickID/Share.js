import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Touchable } from 'utils/components';
import QRCode from 'react-native-qrcode-svg';
import { takeSnapshot } from 'react-native-view-shot';
import { router } from 'config';

import Icon from 'components/Icon';
import styles from './styles';
import withShare from '../../controllers/helpers/withShare';

class QuickIDShare extends Component {
  static propTypes = {
    label: React.PropTypes.string.isRequired,
    qrcode: React.PropTypes.string.isRequired,
    onShare: React.PropTypes.func,
  }

  async qrCodeImage() {
    if (!this._base64) {
      __DEV__ && console.log('Snapshot...'); // eslint-disable-line no-console
      this._base64 = await takeSnapshot(this.refs.qrCode, {
        result: 'data-uri',
      });
    }
    return this._base64;
  }

  getOptions = async () => {
    const { label } = this.props;

    return {
      title: 'Budbuds',
      message: label,
      url: await this.qrCodeImage(),
    };
  }

  render() {
    const { label, qrcode } = this.props;
    const qrcodeUrl = router.build('qrCode', { qrCode: qrcode }, true);

    return (
      <View style={styles.shareContainer}>
        <View ref="qrCode" collapsable={false} style={styles.qrcode}>
          <QRCode value={qrcodeUrl} size={styles._qrcode.width} />
        </View>
        <View style={styles.shareFooter}>
          <View style={styles.shareLinkContainer}>
            <Text style={styles.shareLink}>{label}</Text>
          </View>
          <Touchable
            style={styles.shareIconContainer}
            onPress={this.props.handleShare(this.getOptions)}>
            <Icon name="share-alt" style={styles._shareIcon} />
          </Touchable>
        </View>
      </View>
    );
  }
}

export default withShare(QuickIDShare);

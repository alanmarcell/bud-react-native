import React, { Component } from 'react';
import { Text } from 'react-native';

import Icon from 'components/Icon';
import { Touchable } from 'utils/components';
import styles from './styles';

export default class QuickIDCard extends Component {
  static defaultProps = {
    link: '[mark, replace this]',
  };

  static propTypes = {
    link: React.PropTypes.string.isRequired,
    onPress: React.PropTypes.func,
  }

  render() {
    const { link, onPress } = this.props;
    const share = !!onPress;
    const linkTest = link.replace(/^https?:\/\//, '');

    return (
      <Touchable disabled={!share} style={styles.cardContainer} onPress={onPress}>
        <Icon name="globe" style={styles._cardIcon} />
        <Text style={styles.cardLink}>{linkTest}</Text>
        <Icon visible={share} name="share-alt" style={styles._cardIcon} />
      </Touchable>
    );
  }
}

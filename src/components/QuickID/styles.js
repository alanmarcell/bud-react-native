// @flow
import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  cardContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '$Colors.LightSilver',
    alignItems: 'stretch',
    paddingVertical: 15,
    marginVertical: 10,
  },

  cardLink: {
    flex: 1,
    color: '$Colors.Silver',
  },

  cardIcon: {
    marginHorizontal: 10,
    color: '$Colors.Silver',
    fontSize: 16,
  },

  shareContainer: {
    flex: 1,
    paddingTop: 50,
    paddingHorizontal: 30,
    alignItems: 'center',
    backgroundColor: '$Colors.cardsBackground',
  },

  qrcode: {
    padding: 10,
    backgroundColor: '$Colors.cardsBackground',
    borderColor: '$Colors.LightSilver',
    borderWidth: 1,
    width: '80% - 20',
    alignItems: 'center',
  },

  shareFooter: {
    marginTop: 20,
    flexDirection: 'row',
    width: '80%',
  },

  shareLinkContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    borderColor: '$Colors.LightSilver',
    borderWidth: 1,
    borderTopLeftRadius: 3,
    borderBottomLeftRadius: 3,
    paddingLeft: 10,
  },

  shareLink: {
    color: '$Colors.Green',
    fontSize: 12,
  },

  _shareIcon: {
    color: 'white',
    fontSize: 14,
  },

  shareIconContainer: {
    backgroundColor: '$Colors.Green',
    borderTopRightRadius: 3,
    borderBottomRightRadius: 3,
    padding: 10,
    alignSelf: 'flex-end',
  }
});

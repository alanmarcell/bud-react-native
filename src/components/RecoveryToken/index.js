import React, { Component } from 'react';
import { ScrollView, View, Text } from 'react-native';

import Button       from 'components/Button';
import RoundedIcon  from 'components/RoundedIcon';
import Icon         from 'components/Icon';
import MarkdownBox  from 'components/MarkdownBox';
import { I18n } from 'utils/I18n';

import styles from './styles';

export default class RecoveryToken extends Component {
  static propTypes = {
    onNext: React.PropTypes.func.isRequired,
    session: React.PropTypes.object.isRequired,
  };

  render() {
    const { onNext, session: { user } } = this.props;

    return (
      <ScrollView style={styles.container}>
        <View style={styles.card}>
          <RoundedIcon source="lock"
            iconStyle={styles._lockerIcon}
            containerStyle={styles.lockerIconContainer}
            style={styles.lockerIconDropshadow}
          />
          <MarkdownBox onOpenLink={this._handleLink}>{I18n.t('screens.recovery-token.text')}</MarkdownBox>
          <View style={styles.tokenContainer}>
            <Text style={styles.tokenText} selectable={true}>{user.recoveryToken}</Text>
          </View>
        </View>
        <Button onPress={onNext} theme="purple" type="bordered" style={styles.buttonWrapperInlineChildren}>
          <Text style={styles.buttonText}>{I18n.T('screens.recovery-token.continue_button')}</Text>
          <Icon name="arrow-right" style={styles.buttonIcon} />
        </Button>
      </ScrollView>
    );
  }
}

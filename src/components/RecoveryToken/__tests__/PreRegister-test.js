import React from 'react';
import { shallow } from 'enzyme';

import Target from '../';
import { context } from '../../../__mocks__/stateHelper';

test.skip('renders correctly', () => {
  const props = {
    onRegister: () => {},
    onTerms   : () => {},
    onPollicy : () => {},
  };
  const wrapper = shallow(<Target {...props} /> , context);
  expect(wrapper).toMatchSnapshot();
});

import React from 'react';
import { View } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

const styles =  EStyleSheet.create({
  listView: {
    backgroundColor: '$Colors.background',
  },

  footer: {
    flex: 1,
    height: 75,
    alignSelf: 'flex-end'
  },

  row: {},
  'row:first-child': {
    marginTop: 5,
  }
});

const footer = ActivityIndicator => {
  return class Footer extends React.PureComponent {

    static defaultProps = {
      loading: true,
    };

    static propTypes = {
      loading: React.PropTypes.bool
    };

    render = () => {
      const { loading } = this.props;
      return (
        <View style={styles.footer}>
          {loading ? <ActivityIndicator /> : null}
        </View>
      );
    };
  };
};


export { footer };
export default footer;
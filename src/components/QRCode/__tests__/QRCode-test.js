import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';

import QRCode from '../';

import { router } from 'config';

test.skip('renders correctly', () => {
  const component = shallow(<QRCode path="p/test" />);
  const shouldUrl = router.build('qrCode', { qrCode: 'test'});

  expect(component.find('QRCode').prop('value')).toEqual(shouldUrl);
  expect(component).toMatchSnapshot();
});

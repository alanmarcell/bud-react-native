import React from 'react';
import { View } from 'react-native';

import { router } from 'config';

import QRCodeWrapper from 'react-native-qrcode';

import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  container: {
    padding    : 5,
    borderWidth: 1,
    borderColor: '$Colors.LightSilver',
  },
});

export default class QRCode extends React.Component {
  static defaultProps = {
    size: 40,
  };

  static propTypes = {
    path : React.PropTypes.string,
    size : React.PropTypes.number,
    style: React.PropTypes.any,
  }

  render() {
    const { baseUrl, path, size, style, ...props } = this.props;
    const url = router.build('qrCode', { qrCode: path });

    return (
      <View style={[styles.container, style]}>
        <QRCodeWrapper
          value={url}
          size={size}
          bgColor='black'
          {...props}
        />
      </View>
    );
  }
}

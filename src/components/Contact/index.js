import React, { Component } from 'react';
import { View } from 'react-native';
import { I18n } from 'utils/I18n';

import { BudForms, SimpleSchema } from 'components/BudForms';
import Button from 'components/Button';

import { I18nField, NameField, EmailField, } from 'components/BudForms/Fields';
import { InputFactory } from 'utils/Form/Factories';
import { NameSchema, EmailSchema } from 'components/BudForms/Schemas';

const ContactSchema = new SimpleSchema({
  name: {
    ...NameSchema,
    required: false
  },
  email: {
    ...EmailSchema,
    required: false,
  },
  subject: String,
  message: {
    type: String,
    required: true,
  }
});

export default class Contact extends Component {
  static propTypes = {
    onSendContact: React.PropTypes.func.isRequired,
  };

  renderFooter = () => {
    const { onSendContact } = this.props;
    return (
      <View>
        <Button theme="purple" type="normal" onPress={onSendContact}>
          {I18n.t('screens.contact.submit')}
        </Button>
      </View>
    );
  };

  render() {
    const { onSendContact } = this.props;
    const formProps = {
      ref          : 'form',
      schema       : ContactSchema,
      onSubmit     : onSendContact,
      submitText   : I18n.t('screens.contact.submit'),
    };

    return (
      <BudForms {...formProps} >
        <NameField />
        <EmailField />
        <I18nField
          fieldName="subject"
          type={InputFactory}
          maxLength={100}
        />
        <I18nField
          fieldName="message"
          type={InputFactory}
          multiline={true}
          editable={true}
          maxLength={400}
          style={{height: 100}}
        />
      </BudForms>
    );
  }
}

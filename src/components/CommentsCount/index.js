import React from 'react';
import { View, Text } from 'react-native';

import Icon from 'components/Icon';
import styles from './styles';

export default class CommentsCount extends React.Component {
  static propTypes = {
    count: React.PropTypes.number.isRequired,
    style: React.PropTypes.oneOfType([
      React.PropTypes.shape(),
      React.PropTypes.number,
    ]),
    textStyle: React.PropTypes.oneOfType([
      React.PropTypes.shape(),
      React.PropTypes.number,
    ]),
  }

  render() {
    const { count, style, textStyle } = this.props;

    if (count <= 0) {
      return null;
    }

    return (
      <View style={[styles.container, style]}>
        <Icon name="comment" />
        {count > 0 && (<Text style={[styles.date, textStyle]}>{`${count}`}</Text>)}
      </View>
    );
  }
}

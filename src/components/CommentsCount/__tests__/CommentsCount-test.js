import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';

import CommentsCount from '../';

test('renders correctly', () => {
  const count = 10;
  const component = shallow(<CommentsCount count={count} />);

  expect(component.find('Text').children().text()).toEqual(`${count}`);
  expect(component).toMatchSnapshot();
});

test('renders empty text when count is 0', () => {
  const count     = 0;
  const component = shallow(<CommentsCount count={count} />);

  expect(component.find('Text').length).toEqual(0);
  expect(component).toMatchSnapshot();
});

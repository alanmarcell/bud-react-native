import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  date: {
    paddingLeft: 5,
    paddingRight: 5,
    color: '$Colors.Gray',
  }
});

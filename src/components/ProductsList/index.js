import React, { PureComponent } from 'react';
import { View, FlatList, ActivityIndicator } from 'react-native';
import { I18n } from 'utils/I18n';
import U from 'utils';

import styles from './styles';
import Refreshable from 'utils/components/Refreshable';
import CupsCardItem from 'controllers/Cups/components/CupsCardItem';
import BudCardItem  from 'controllers/Buds/components/BudCardItem';
import BlankList     from 'components/BlankList';

export default class ProductsList extends PureComponent {
  static defaultProps = {
    products: [],
  };

  products = this.props[this.props.productsType];

  static propTypes = {
    productsType: React.PropTypes.string.isRequired,
  }

  onProductPress = (productId) => {
    if (this.props.onProductPress) {
      this.props.onProductPress(productId);
    }
  };

  renderItem = ({ item, index }) => {
    let listLength = this.products.length;
    let Component = CupsCardItem;

    if (this.props.productsType === 'buds') {
      Component = BudCardItem;
    }

    return (
      <Component
        {...item}
        subtitle={`${item.city} - ${item.country}`}
        onPress={() => this.onProductPress(item.id)}
        listLength={listLength}
        listIndex={parseInt(index)}
      />
    );
  };

  renderFooter = () => {
    const { loading } = this.props;
    return (
      <View style={styles.footer}>
        {loading ? <ActivityIndicator /> : null}
      </View>
    );
  };

  renderHeader = () => {
    const {productsType} = this.props;
    if (this.products.length <= 0) {
      return (
        <BlankList
          icon="trophy"
          title={I18n.t(`${productsType}.blank.title`)}
          message={I18n.t(`${productsType}.blank.message`)}
        />
      );
    } else {
      return null;
    }
  };

  loadMore = () => {
    const { loading, loadMoreEntries, hasNextPage } = this.props;
    if (!loading && hasNextPage) {
      loadMoreEntries();
    }
  }

  getProducts(){
    const { productsType } = this.props;
    return this.props[productsType];
  }

  render() {
    const { loading, refetch } = this.props;
    return (
      <FlatList
        data={this.products}
        refreshing={loading}
        refreshControl={Refreshable({ loading, refetch })}
        keyExtractor={(item, index) => index}
        renderItem={this.renderItem}
        ListHeaderComponent={this.renderHeader}
        ListFooterComponent={this.renderFooter}
        onEndReached={this.loadMore}
      />
    );
  }
}

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity } from 'react-native';
import Moment from 'utils/components/Moment';
import Image from 'components/Image';

import { I18n } from 'utils/I18n';

import styles from './styles';

export default class GaleryPreview extends Component {
  static propTypes = {
    files: PropTypes.array.isRequired,
    openGalery: PropTypes.func
  }

  renderAsset = ({ url }, style) => {
    const source = { uri: url };
    const props = { source , style };

    return <Image {...props} />;
  }

  render() {
    const { files, openGalery } = this.props;

    const viewAll = files.length > 5;
    const preview = files.slice(0, 5);

    return (
      <View style={styles.container}>
        {preview.map((image) => (
          <TouchableOpacity onPress={() => openGalery(image.id)} key={image.id}>
            {this.renderAsset(image, styles.miniatures)}
            <Moment
              date={image.createdAt}
              action="format"
              args={['L']}
              containerStyle={{ marginRight: 5, alignSelf: 'center' }}
              style={{ fontSize: 10 }}
          />
          </TouchableOpacity>
        ))}
        { viewAll &&
          <TouchableOpacity onPress={() => openGalery(preview[0].id)} style={styles.viewAllContainer}>
            <Text style={styles.viewAll}>{I18n.t('components.galery.viewAll')}</Text>
          </TouchableOpacity>
        }
      </View>
    );
  }
}

import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 10,
    marginLeft: 15
  },

  viewAll: {
    fontSize: 24,
    color: '$Colors.primaryInvert',
    alignSelf: 'center',
  },

  viewAllContainer: {
    width: 100,
    height: 100,
    backgroundColor: '$Colors.GrayLowOpacity',
    marginRight: 5,
    marginBottom: 5,
  },

  miniatures: {
    backgroundColor: '$Colors.GrayLowOpacity',
    width: 100,
    height: 100,
    marginRight: 5,
    marginBottom: 5
  }
});




import React, { Component } from 'react';
import { ActivityIndicator, Text, View, Image, Modal } from 'react-native';
import { I18n } from 'utils/I18n';

import styles from './styles';

import { Animated, Easing } from 'react-native';
import R from 'ramda';

export default class ModalLoading extends Component {
  static defaultProps = {
    loadType : 'spinner',
    modal: true,
    text : null,
    show : true,
  };

  componentWillMount() {
    this.rotateValue = new Animated.Value(0);
  }

  componentDidUpdate() {
    const { loadType } = this.props;
    if (loadType === 'spinner') {
      this.runAnimation();
    }
  }

  componentDidMount() {
    const { loadType } = this.props;
    if (loadType === 'spinner') {
      this.runAnimation();
    }
  }

  runAnimation() {
    this.rotateValue.setValue(0);
    Animated.timing(this.rotateValue, {
      toValue: 100,
      duration: 2000,
      easing: Easing.linear
    }).start(({ finished }) => {
      if (finished) {
        this.runAnimation();
      }
    });
  }

  renderSpinner() {
    let interpolatedRotateAnimation = this.rotateValue.interpolate({
      inputRange: [0, 100],
      outputRange: ['0deg', '360deg'],
    });
    const transform = [{rotate: interpolatedRotateAnimation}];

    let { text } = this.props;
    if (!R.isNil(text)) {
      text = <Text style={styles.text}>{I18n.T(text)}</Text>;
    }

    return (
      <Image style={styles.background} source={require('./images/background.png')}>
        <Image style={styles.logo} source={require('./images/logo.png')} />
        <Image source={require('./images/mini_logo.png')} />
        <Animated.Image style={[styles.spinner, { transform }]} source={require('./images/arrow.png')} />
        {text}
      </Image>
    );
  }

  render() {
    const { loadType, modal, show } = this.props;
    let content = null;

    if (loadType === 'spinner') {
      content = this.renderSpinner();
    } else if (loadType === 'process') {
      content = (
        <View style={[styles.block, styles.process]}>
          <ActivityIndicator />
        </View>
      );
    } else if (__DEV__) {
      content = (
        <View style={styles.block}>
          <Text style={styles.blockText}>Modal block</Text>
        </View>
      );
    }

    if (!modal) {
      return show ? content : null;
    } else {
      return (
        <Modal
          animationType="fade"
          transparent={loadType === 'transparent'}
          visible={show}
          onRequestClose={() => {}}
        >
          {content}
        </Modal>
      );
    }
  }
}

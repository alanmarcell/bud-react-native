import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  background: {
    flex: 1,
    width: null,
    height: null,
    alignItems: 'center',
  },

  logo: {
    marginTop: 114,
    marginBottom: 90,
  },

  spinner: {
    marginTop: -49,
    marginBottom: 9,
    backgroundColor: 'rgba(0,0,0,0)',
  },

  text: {
    color: '$Colors.Loading',
    fontSize: 12,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: 'rgba(0,0,0,0)',
  },

  process: {
    backgroundColor: 'rgba(255, 255, 255, 0.4)',
  },

  block: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'rgba(52, 52, 52, 0.7)',
    justifyContent: 'center',
  },

  blockText: {
    alignSelf: 'center',
    color: '#FFF',
    fontWeight: 'bold',
  },
});

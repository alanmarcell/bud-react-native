import 'react-native';
import React from 'react';
import ModalLoading from '../';
import { shallow } from 'enzyme';

import { context } from '../../../__mocks__/stateHelper';

describe('ModalLoading', () => {
  test('renders correctly block modal', () => {
    const wrapper = shallow(<ModalLoading show />, context);
    expect(wrapper).toMatchSnapshot();
  });

  test('renders correctly modal text', () => {
    const wrapper = shallow(<ModalLoading show="loading" />, context);
    expect(wrapper).toMatchSnapshot();
  });

  test('render correctly modal after update props', () => {
    let wrapper = shallow(<ModalLoading show />, context);
    wrapper.setProps({ show: 'loading'});
    expect(wrapper).toMatchSnapshot();
  });
});

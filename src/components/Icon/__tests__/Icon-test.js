import 'react-native';
import React from 'react';
import Icon from '../';
import renderer from 'react-test-renderer';

test('renders FontAwesome icon', () => {
  const tree = renderer.create(
    <Icon name="budbuds" />
  ).toJSON();
  expect(tree).toMatchSnapshot();
});

test('renders Material icon', () => {
  const tree = renderer.create(
    <Icon name="bud" />
  ).toJSON();
  expect(tree).toMatchSnapshot();
});

import React, { Component } from 'react';
import { View } from 'react-native';
import R from 'ramda';

import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from '../../font/BudBuds/selection.json';

import styles from './styles';

const IconComponent = createIconSetFromIcoMoon(icoMoonConfig);

export default class Icon extends Component {
  static propTypes = {
    name          : React.PropTypes.string.isRequired,
    containerStyle: React.PropTypes.any,
    style         : React.PropTypes.oneOfType([
      React.PropTypes.object,
      React.PropTypes.number,
      React.PropTypes.array,
    ]),
    rounded       : React.PropTypes.bool,
    roundedWhite  : React.PropTypes.bool,
    visible       : React.PropTypes.bool,
  };

  static defaultProps = {
    roundedWhite: false,
    style: undefined,
    visible: true,
  };

  render() {
    if (!this.props.visible) {
      return null;
    }

    let {
      containerStyle,
      rounded,
      roundedWhite,
      style,
      ...iconProps
    } = this.props;

    // TODO: is necessary?
    const roundedClass = rounded ? styles.roundedContainer : null;
    const roundedWhiteClass = roundedWhite ? styles.roundedWhiteContainer : null;

    if (R.is(Object, style)) {
      const { color, fontSize: size } = style;
      iconProps = { color, size, ...iconProps };
    }

    iconProps.style = style;

    return (
      <View style={[containerStyle, roundedClass, roundedWhiteClass]}>
        <IconComponent {...iconProps} />
      </View>
    );
  }
}

import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  roundedContainer: {
    backgroundColor: '$Colors.LightSilver',
    height: 50,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '$Thumbs.borderRadius',
  },
  roundedWhiteContainer: {
    backgroundColor: '#FFFFFF',
  },
});

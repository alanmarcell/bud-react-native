// @flow
import EStyleSheet from 'react-native-extended-stylesheet';

const logo = EStyleSheet.create({
  container: {
    margin: 5,
    // alignSelf: 'center',
    // width: 100,
    // height: 300,
  },
  logo: {
    width: 50,
    height: 50,
    borderRadius: 25
  }
});

const icon = EStyleSheet.create({
  container: {
    margin: 5,
    alignSelf: 'center',
    width: 50,
    height: 50
  },

  icon: {
    fontSize: 18,
    color: '#95989A'
  },

  radius: {
    flex: 1,
    backgroundColor: '$Colors.LightSilver',
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const styles = {
  logo,
  icon
};

export default styles;

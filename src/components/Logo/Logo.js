import React from 'react';
import defaultStyles from './styles';
import { isNil } from 'ramda';
import Icon from 'components/Icon';
import Image from 'components/Image';
import { View } from 'react-native';

class Logo extends React.PureComponent {
  static defaultProps = {
    styles: defaultStyles
  }

  render() {
    const { image } = this.props;

    if (isNil(image)) {
      const styles = this.props.styles.icon || defaultStyles.icon;

      return (
        <View style={styles.container}>
          <View style={styles.radius}>
            <Icon name='trophy' size={styles.icon.fontSize} color={styles.icon.color} />
          </View>
        </View>
      );
    }
    else {
      const styles = this.props.styles.logo || defaultStyles.logo;
      const source = { ...image, uri: image.url || image.uri };
      const useNative = !!source.uri;

      return (
        <View style={styles.container}>
          <Image useNative={useNative} source={source} style={styles.logo} />
        </View>
      );
    }
  }
}

export default Logo;

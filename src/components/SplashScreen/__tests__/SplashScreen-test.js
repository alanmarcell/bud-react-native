import 'react-native';
import React from 'react';
import SplashScreen from '../';
import renderer from 'react-test-renderer';

test('renders correctly', () => {
  const tree = renderer.create(
    <SplashScreen />
  ).toJSON();
  expect(tree).toMatchSnapshot();
});

import React, { Component } from 'react';
import { View, Image } from 'react-native';

import styles from './styles';

export default class SplashScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image
          source={require('./images/ico-launch-screen.png')}
        />
      </View>
    );
  }
}

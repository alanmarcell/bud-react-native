import React, { PureComponent, PropTypes } from 'react';
import { View, Text } from 'react-native';
import moment from 'moment';

import { cardItem, itemName } from './index';
import { stylesHelpers } from 'themes';

class EditionName extends PureComponent {

  static propTypes = {
    name: PropTypes.string.isRequired,
    startAt: PropTypes.string.isRequired,
    endAt: PropTypes.string.isRequired,
  };

  render() {
    const { name, startAt, endAt } = this.props;    
    const subtitle= `${moment(startAt).format('L')} - ${moment(endAt).format('L')}`;      

    return (
      <View style={[stylesHelpers.middleColumn, stylesHelpers.alignCenterLeft]}>
        <Text style={itemName.name}>{name}</Text>
        <Text style={itemName.subtitle}>{subtitle}</Text>
      </View>
    );
  }
}

const EditionCard = cardItem(EditionName);

export {EditionCard};
export default EditionCard;

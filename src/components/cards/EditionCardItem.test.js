import 'react-native';
import React from 'react';

import CupsCardItem from '../';
import renderer from 'react-test-renderer';
import moment from 'moment';

const cup =  {
  id: 'id_cup_1',
  name: 'Cup BudBuds',
  startedAt: moment().add(10, 'day').toDate(),
  endAt:moment().add(15, 'day').toDate() 
};

test.skip('renders correctly', () => {
  const tree = renderer.create( <CupsCardItem
    onPress={() => {}}
    {...cup}
  />).toJSON();
  expect(tree).toMatchSnapshot();
});

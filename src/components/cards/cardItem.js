import React, { PureComponent, PropTypes } from 'react';

import SelectableCard from 'components/SelectableCard';
import Logo from 'components/Logo';

import {  logoStyles } from './styles';

const cardItem = ItemName =>
  class CardItem extends PureComponent { 
    static defaultProps = {
      ...SelectableCard.defaultProps,
      ...ItemName.defaultProps,
    };

    static propTypes = {
      ...SelectableCard.propTypes,
      ...ItemName.propTypes,
      logo: PropTypes.object
    };

    render() {
      const { logo } = this.props;
      
      return (
        <SelectableCard {...this.props}>
          <Logo image={logo} styles={logoStyles} />
          <ItemName {...this.props} />         
        </SelectableCard>
      );
    }
  };

export { cardItem };
export default cardItem;

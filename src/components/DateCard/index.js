import React, { Component, PropTypes } from 'react';
import { View, Text } from 'react-native';

import styles from './styles';

export default class DateCard extends Component {
  static defaultProps = {
    date: 'Fevereiro 7, 2017',
  }

  static propTypes = {
    date: PropTypes.string,
  }

  render() {
    return (
      <View style={[this.props.style, styles.container]}>
        <Text style={[styles.date, this.props.style]}>{this.props.date.toUpperCase()}</Text>
      </View>
    );
  }
}

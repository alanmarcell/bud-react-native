// @flow
import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  container: {
    alignItems: 'center',
    padding: 10,
  },
  date: {
    color: '#95989A',
    fontSize: 10,
  },
});

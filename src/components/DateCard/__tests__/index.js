import 'react-native';
import React from 'react';
import DateCard from '../';
import renderer from 'react-test-renderer';

test('renders correctly', () => {
  const style = { backgroundColor: 10 };
  const tree = renderer.create(
    <DateCard style={style} date="Fevereiro 7, 2017" />
  ).toJSON();
  expect(tree).toMatchSnapshot();
});

import React, { Component } from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';

import EStyleSheet from 'react-native-extended-stylesheet';
import Icon from 'components/Icon';
import { Touchable } from 'utils/components';

export default class Stars extends Component {
  static defaultProps = {
    selecteds: 1,
    number   : 10,
    startSize: 30,
    theme    : 'yellow'
  };

  static propTypes = {
    onPress  : PropTypes.func,
    selecteds: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    number   : PropTypes.number,
    startSize: PropTypes.number,
    theme    : PropTypes.oneOf(['yellow', 'gray']),
  }

  renderFiveStars() {
    const { selecteds, startSize, onPress, fieldName } = this.props;

    let _fieldName = !fieldName ? '_none_' : fieldName;

    const stars = medicalColorResolver(_fieldName.replace(/\.score/, ''), Math.round(selecteds));

    return stars.color.map((item, index) => {
      const activeStar = item.indexOf('off') !== -1 ? 'star-o' : 'star';
      return (
        <Touchable key={`star_${(index + 1)}`} active={!!onPress} onPress={onPress ? () => onPress(index + 1) : null}>
          <Icon name={activeStar} size={startSize} style={styles[`_${item}`]} />
        </Touchable>
      );
    }).slice(0,10);
  }

  render() {
    const { style } = this.props;
    return (
      <View style={[styles.container, style]}>
        {this.renderFiveStars()}
      </View>
    );
  }
}

const colorStars = (selecteds, inverted = false) => {
  const colors_off = ['m_red_off', 'm_red_off', 'm_red_off', 'm_yellow_off', 'm_yellow_off', 'm_yellow_off', 'm_yellow_off', 'm_green_off', 'm_green_off', 'm_green_off'];
  const colors = ['m_red', 'm_red', 'm_red', 'm_yellow', 'm_yellow', 'm_yellow', 'm_yellow', 'm_green', 'm_green', 'm_green'];
  return inverted
    && [...colors.reverse().slice(0, selecteds), ...colors_off.reverse().slice(-(10-selecteds))]
    || [...colors.slice(0, selecteds), ...colors_off.slice(-(10-selecteds))];
};

const medicalColorResolver = (fieldName, selecteds) => {
  switch (fieldName.trim()) {
    case 'color':
      return { delta: 1.0, color: colorStars(selecteds) };
    case 'criativity':
      return { delta: 1.4, color: colorStars(selecteds) };
    case 'effect':
      return { delta: 1.0, color: colorStars(selecteds) };
    case 'euphoria':
      return { delta: 1.1, color: colorStars(selecteds) };
    case 'flavor':
      return { delta: 1.0, color: colorStars(selecteds) };
    case 'high':
      return { delta: 1.5, color: colorStars(selecteds) };
    case 'look':
      return { delta: 1.0, color: colorStars(selecteds) };
    case 'medicinal':
      return { delta: 1.5, color: colorStars(selecteds) };
    case 'merriment':
      return { delta: 1.4, color: colorStars(selecteds) };
    case 'relaxation':
      return { delta: 1.1, color: colorStars(selecteds) };
    case 'smell':
      return { delta: 1.0, color: colorStars(selecteds) };
    case 'touch':
      return { delta: 1.0, color: colorStars(selecteds) };

    case 'anxiety':
      return { delta: 1.0, color: colorStars(selecteds, true) };
    case 'fatigue':
      return { delta: 1.0, color: colorStars(selecteds, true) };
    case 'headache':
      return { delta: 1.3, color: colorStars(selecteds, true) };
    case 'nausea':
      return { delta: 1.2, color: colorStars(selecteds, true) };
    case 'throat_soreness':
      return { delta: 1.3, color: colorStars(selecteds, true) };

    default:
      return { delta: 1.0, color: colorStars(selecteds) };
  }

};

const starStyle = {
  paddingHorizontal: 4,
};

const styles = EStyleSheet.create({
  container: {
    flexDirection: 'row',
    // alignSelf: 'center',
  },

  yellow: {
    ...starStyle,
    color: '$Colors.yellowStar',
  },

  m_red_off: {
    ...starStyle,
    color: '$Colors.medicalRedOff',
  },

  m_red: {
    ...starStyle,
    color: '$Colors.medicalRed',
  },

  m_yellow: {
    ...starStyle,
    color: '$Colors.medicalYellow',
  },

  m_yellow_off: {
    ...starStyle,
    color: '$Colors.medicalYellowOff',
  },

  m_green: {
    ...starStyle,
    color: '$Colors.medicalGreen',
  },

  m_green_off: {
    ...starStyle,
    color: '$Colors.medicalGreenOff',
  },

  gray: {
    ...starStyle,
    color: '$Colors.grayStar',
  },

  unselected: {
    ...starStyle,
    color: '$Colors.unSelectedStar'
  },
});

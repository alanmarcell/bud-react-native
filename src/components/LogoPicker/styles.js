// @flow
import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  container: {
    alignSelf: 'center',
    // width: 100,
    // height: 100,
  }
});

const logo = EStyleSheet.create({
  container: {
    alignSelf: 'center',
    // width: 100,
    // height: 100,
  },
  logo: {
    width: 100,
    height: 100,
    borderRadius: 50,
  }
});

const icon = EStyleSheet.create({
  container: {
    alignSelf: 'center',
    width: 100,
    height: 100,
    marginRight: 10,
    marginLeft: 10
  },

  icon: {
    fontSize: 18,
    color: '#95989A'
  },

  radius: {
    flex: 1,
    backgroundColor: '$Colors.LightSilver',
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const logoStyles = {
  logo,
  icon
};

export default styles;

export {
  styles,
  logoStyles
};

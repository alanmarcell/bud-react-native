import React from 'react';
import PropTypes from 'prop-types';
import Logo from '../Logo';
import { View, NativeModules } from 'react-native';
import Button from 'components/Button';
import { styles, logoStyles } from './styles';
import { I18n } from 'utils/I18n';
import uuid from 'uuid';
import { prop } from 'ramda';

const ImageCropPicker = NativeModules.ImageCropPicker;

class LogoPicker extends React.PureComponent {
  static propTypes = {
    logo: PropTypes.object
  }

  static contextTypes = {
    doc: PropTypes.object,
    onChange: PropTypes.func.isRequired,
    errorMessages: PropTypes.object,
    form: PropTypes.any.isRequired,
    parentFieldName: PropTypes.string
  }

  constructor(args) {
    super(args);

    this.state = {
      loading: false
    };
  }

  openPicker = () => {
    this.setState({ loading: true });

    ImageCropPicker.openPicker({
      includeBase64: true,
      cropping: true,
      width: 640,
      height: 640,
    })
      .then(this.onSelectImage)
      .finally(() => {
        this.setState({ loading: false });
      });
  }

  getFieldName() {
    if (this.context.parentFieldName) {
      if (this.props.fieldName) {
        return `${this.context.parentFieldName}.${this.props.fieldName}`;
      } else {
        return this.context.parentFieldName;
      }
    } else {
      return this.props.fieldName;
    }
  }

  getValue() {
    const doc = this.context.doc || {};
    return prop(this.getFieldName(), doc);
  }

  onSelectImage = async (image) => {
    const newImage = {
      uri: `data:${image.mime};base64,${image.data}`,
      data: image.data,
      mimetype: image.mime,
      size: image.size,
      fileName: image.filename || `photo_${uuid()}`,
      type: 'PHOTO',
      id: uuid(),
    };

    this.context.onChange(this.getFieldName(), newImage);
  }

  render() {
    const image = this.getValue();
    return (
      <View style={styles.container}>
        <Logo
          styles={logoStyles}
          image={image}
        />
        <Button
          type="bordered"
          style={styles.addBtn}
          textStyle={{ color: '#3C484A' }}
          onPress={this.openPicker}>
          {I18n.t('forms.logo.addButton')}
        </Button>
      </View>
    );
  }
}

export default LogoPicker;

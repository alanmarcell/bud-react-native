import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  container: {
    marginTop: -3,
    justifyContent: 'center',
    // marginRight: -15,
  },

  info: {
    backgroundColor: 'white',
    padding: 20,
  },

  infoIcon: {
    fontSize: 30,
    color: 'black',
  },

  infoTitle: {
    color: 'green',
    fontSize: 24,
    marginBottom: 20,
  },

  content: {
    justifyContent: 'center',
    marginBottom: 20,
  },

  cancelButton: {
    width: '100%',
  }

});


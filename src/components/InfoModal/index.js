import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, View } from 'react-native';
import Modal from 'react-native-animated-modal';

import styles from './styles';

import Button from 'components/Button';
import Icon from 'components/Icon';

export default class InfoModal extends Component {
  static propTypes = {
    title: PropTypes.string,
    closeText: PropTypes.string,
  }

  static defaultProps = {
    title: 'Budbuds',
    closeText: 'Close',
  }

  state = {
    isModalVisible: false,
  }

  _showModal = () => {
    this.setState({ isModalVisible: true }) ;
  }

  _hideModal = () => {
    this.setState({ isModalVisible: false }) ;
  }

  render() {
    const { isModalVisible } = this.state;
    const { title, closeText, children, hideInfoModal } = this.props;

    if(hideInfoModal){
      return null;
    }

    return (
      <View style={styles.container}>
        <Icon
          onPress={this._showModal}
          name="info_outline"
          style={styles.infoIcon}
        />

        <Modal
          isVisible={isModalVisible}
          onRequestClose={this._hideModal}
        >
          <View style={styles.backdrop} onStartShouldSetResponder={this._hideModal}>
            <View style={styles.info} >
              <Text style={styles.infoTitle} >{title}</Text>

              <View style={styles.content} >
                {children}
              </View>

              <Button style={styles.cancelButton} type="modal" theme="green" onPress={this._hideModal}>
                {closeText}
              </Button>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}


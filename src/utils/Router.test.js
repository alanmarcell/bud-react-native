import { router } from 'config';

describe('Router', () => {
  test('match with a qrcode route', () => {
    let result = router.process('https://budbuds.us/q/qrcode');
    expect(result).toHaveProperty('params', { qrCode: 'qrcode' });

    result = router.process('https://budbuds.us/q/id;qrcode');
    expect(result).toHaveProperty('params', { qrCode: 'qrcode' });
  });

  test('Remove space in the end', () => {
    let result = router.process('https://budbuds.us/q/qrCode ');
    expect(result).toHaveProperty('params', { qrCode: 'qrCode' });
  });

  // TODO: Other hosts are working, Should it not work?
  test.skip('return error if is invalid host', () => {
    const result = router.process('https://example.com/q/qrcode');
    expect(result).toHaveProperty('error', 'invalid_host');
  });

  test('match with quickIds', () => {
    const result = router.process('https://budbuds.us/daniel/p/plantId');
    expect(result).toHaveProperty('params', {
      userNick: 'daniel',
      prodNick: 'plantId',
    });
  });

  test('generate a route', () => {
    const url = router.build('budQuickID', { userNick: 'sam', prodNick: 'bud' });
    expect(url).toEqual('budbuds.us/sam/b/bud');
  });

  test('support a pre process', () => {
    const result = router.process('https://bbu.uy/qrcode');
    expect(result).toHaveProperty('params', { qrCode: 'qrcode' });
  });
});

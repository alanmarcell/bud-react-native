import { Alert } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import Raven from 'raven-js';
import Config from 'config';
import { I18n } from 'utils/I18n';

require('raven-js/plugins/react-native')(Raven);

const ReleaseVersion = `${DeviceInfo.getVersion()}-${DeviceInfo.getBuildNumber()}`;

Raven
  .config(Config.RavenUrl, { release: ReleaseVersion })
  .install();

Raven.setTagsContext({
  environment: Config.env,
});

export default {
  isGraphQlError(error) {
    return (error.graphQLErrors || []).length > 0;
  },

  formatError(error) {
    let messages = [], title = 'Error';

    if (this.isGraphQlError(error)) {
      title    = I18n.t('error.api.title');
      messages = error.graphQLErrors.map((msg) => {
        return I18n.t(['error', 'api', msg.message]);
      });
    } else {
      messages = [error.message];
    }
    return [title, messages[0]];
  },

  async logException(error, extra) {
    if (this.isGraphQlError(error)) {
      __DEV__ && console.warn(error);
      await new Promise((resolve, reject) => {
        try {
          const [title, message] = this.formatError(error);
          const buttons = [ { text: 'OK', onPress: () => resolve() } ];
          Alert.alert(title, message, buttons, { cancelable: false });
        } catch (error) {
          reject(error);
        }
      });
    } else {
      __DEV__ || Raven.captureException(error, {
        extra: extra,
        contexts: {
          api: {
            codename: Config.api.codename,
            baseUrl: Config.api.baseURL
          },
          os: {
            name: DeviceInfo.getSystemName(),
            version: DeviceInfo.getSystemVersion()
          }
        }
      });
      __DEV__ && window.console && console.error && console.error(error);
    }
    return { error };
  }
};

import R from 'ramda';

function validToMerge(value) {
  return R.is(Object, value) && !R.is(Date, value) && !R.is(Error, value);
}

export default {
  ...R,
  isBlank(...args) {
    return this.isEmpty(...args) || this.isNil(...args);
  },

  deepMerge(a, b, ...tail) {
    let head = b;
    if (validToMerge(a) && validToMerge(b)) {
      head = this.mergeWith(this.deepMerge.bind(this), a, b);
    }
    if (!this.isEmpty(tail)) {
      return this.deepMerge(head, ...tail);
    }
    return head;
  },

  deepClone(value) {
    if (!this.isNil(value) && this.is(Function, value.map)) {
      return value.map((item) => this.deepClone(item));
    }

    if (value instanceof Date) {
      return new Date(value);
    }

    if (this.is(Object, value)) {
      return this.mapObjIndexed((v) => this.deepClone(v), value);
    }

    return this.clone(value);
  },

  lazyCall(time, fn) {
    let executed = false, result;
    const fnClojure = (options = {}) => {
      if (!executed) {
        clearTimeout(id); // if necessary
        executed = true;
        result = fn(options);
      }
      return result;
    };

    const id = setTimeout(fnClojure, time);
    return {
      cancel: () => clearTimeout(id),
      flush : () => fnClojure({ flush: true }),
    };
  },

  camelize(str = '') {
    return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, (match, index) => {
      if (+match === 0) return ''; // or if (/\s+/.test(match)) for white spaces
      return index == 0 ? match.toUpperCase() : match.toLowerCase();
    });
  },

  sortScore(buds) {
    return buds.sort((a, b) => {
      if (!a.score) {
        return 0;
      }

      if (a.score > b.score) {
        return -1;
      }

      if (a.score < b.score) {
        return 1;
      }

      return 0;
    });
  }
};

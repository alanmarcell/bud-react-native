// import { extractObjectFromQRCode, isQRCode } from '../qrcode';

describe('utils qrcode', () => {
  const uuid    = '85d03224d087eba8d284818386b4c1899fba877b';
  const invalid = 'ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ';

  it.skip('extractObjectFromQRCode', () => {
    // expect(extractObjectFromQRCode(`https://budbuds.us/q/${uuid}`)).toEqual(uuid);
    // expect(extractObjectFromQRCode('https://budbuds.us/q/plant'  )).toEqual('plant');
    // expect(extractObjectFromQRCode('https://budbuds.us/q/'       )).toEqual(null);
    // expect(extractObjectFromQRCode('https://budbuds.us/q'        )).toEqual(null);
    // expect(extractObjectFromQRCode(null                          )).toEqual(null);

    // // deprecated short url
    // expect(extractObjectFromQRCode(`http://bbu.uy/${uuid}`)).toEqual(uuid);
    // expect(extractObjectFromQRCode('http://bbu.uy/plant'  )).toEqual('plant');
    // expect(extractObjectFromQRCode('http://bbu.uy/'       )).toEqual(null);
    // expect(extractObjectFromQRCode('http://bbu.uy'        )).toEqual(null);
  });

  it.skip('isQRCode', () => {
    // expect(isQRCode(uuid                          )).toBeTruthy();
    // expect(isQRCode(`https://budbuds.us/q/${uuid}`)).toBeTruthy();
    // expect(isQRCode('https://budbuds.us/q/plant'  )).toBeTruthy();
    // expect(isQRCode('https://budbuds.us/q/'       )).toBeFalsy();
    // expect(isQRCode('https://budbuds.us/q'        )).toBeFalsy();
    // expect(isQRCode(null                          )).toBeFalsy();
    // expect(isQRCode(invalid                       )).toBeFalsy();

    // // deprecated short url
    // expect(isQRCode(`http://bbu.uy/${uuid}`)).toBeTruthy();
    // expect(isQRCode('http://bbu.uy/plant'  )).toBeTruthy();
    // expect(isQRCode('http://bbu.uy/'       )).toBeFalsy();
    // expect(isQRCode('http://bbu.uy'        )).toBeFalsy();
  });

});

import R from 'ramda';

const byScore = R.descend(R.prop('score'));

const orderByScore = buds => R.isNil(buds)
  ? buds
  : R.sort(byScore, buds);

export default orderByScore;

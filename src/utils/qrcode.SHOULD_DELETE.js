// @flow
import Config from 'config';
import URL from 'url-parse';

export function extractObjectFromQRCode(data) {
  if (!data) { return null; }

  const { slashes, pathname } = new URL(data);
  let qrcode = slashes ? pathname.substring(1) : pathname;
  qrcode = qrcode.replace(/^q\/?/, '');

  return qrcode === '' ? null : qrcode;
}

// https://regex101.com/r/S4lwZd/2
const urls       = Config.qrcode.urls.map((url) => new RegExp(`^https?:\/\/${url}\/.{1,}$`, 'mg'));
const qrcodeCode = /^[0-9a-f]{1,}$/g;

export function isQRCode(data) {
  return data && (
    !!data.match(qrcodeCode) ||
    !!urls.find((urlRegex) => !!data.match(urlRegex))
  );
}

import React, { Component } from 'react';
import { View } from 'react-native';

import styles from './styles';

export default class Underline extends Component {
  static defaultProps = {
    error: false,
  };

  static propTypes = {
    error: React.PropTypes.bool
  };

  render() {
    const style = styles[this.props.error ? 'underlineError' : 'underline'];
    return <View style={style} />;
  }
}

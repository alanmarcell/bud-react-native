import React from 'react';
import { View } from 'react-native';

import { Form as SimpleReactForm } from 'simple-react-form';
export { Field, ArrayComponent } from 'simple-react-form';
export { SimpleSchema } from './SimpleSchema';

export class Form extends SimpleReactForm {
  static propTypes = {
    ...SimpleReactForm.propTypes,
    afterSubmit: React.PropTypes.func,
    containerStyle: React.PropTypes.oneOfType([
      React.PropTypes.number,
      React.PropTypes.object,
      React.PropTypes.array,
    ]),
  };

  setErrorsWithContext (context) {
    context.invalidKeys = context.validationErrors;
    return super.setErrorsWithContext(context);
  }

  render() {
    return (
      <View style={this.props.containerStyle}>
        {super.render()}
      </View>
    );
  }

  submit () {
    if (this.props.afterSubmit) {
      const data = this.props.commitOnlyChanges ? this.state.changes : this.state.doc;
      return this.props.afterSubmit(data, () => super.submit());
    }
    return super.submit();
  }
}

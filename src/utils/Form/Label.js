import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet, ViewPropTypes } from 'react-native';

export default class Label extends Component {
  static propTypes = {
    containerStyle: ViewPropTypes.style,
    labelStyle: Text.propTypes.style,
    children: PropTypes.any,
    fontFamily: PropTypes.string,
  };

  render() {
    const {
      containerStyle,
      labelStyle,
      children,
      fontFamily,
      ...attributes
    } = this.props;

    return (
      <View
        style={[styles.container, containerStyle && containerStyle]}
        {...attributes}
      >
        <Text
          style={[
            styles.label,
            labelStyle && labelStyle,
            fontFamily && { fontFamily },
          ]}
        >
          {children}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  label: {
    color: '#95989A',
    fontSize: 10,
    marginLeft: 0,
    marginRight: 0,
    marginTop: 15,
    marginBottom: 1,
    fontWeight: 'bold',
  },
});

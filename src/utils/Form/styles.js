
import EStyleSheet from 'react-native-extended-stylesheet';
export default EStyleSheet.create({
  formContainer: {
    flex: 1,
  },

  fieldsContainer: {
    flex: 1,
    justifyContent: 'flex-start',
  },

  fieldsContainerPasswd: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },

  field: {
    flex: 1,
    paddingTop: 8,
    paddingBottom: 8,
  },

  errorBox: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  errorStyle: {
    fontSize: 12,
    color: '#B92D00',
    marginTop: 8,
  },

  errorIcon: {
    fontSize: 14,
    marginTop: 8,
    marginRight: 5,
    color: '#C23227',
  },

  underline: {
    flex: 1,
    height: 1,
    backgroundColor: '#CCCCCC',
  },

  underlineError: {
    flex: 1,
    height: 1,
    backgroundColor: '#B92D00',
  },

  fieldText: {
    fontSize: 14,
    marginTop: 5,
    marginBottom: 10,
  },

  fieldTextDate: {
    color: '#000',
    fontSize: 16,
  },

  // Input
  inputContainer: {
    marginLeft: 0,
    marginRight: 0,
    borderBottomWidth: 0,
  },

  inputStyle: {
    flex: 1,
    color: '#000',
    paddingHorizontal: 0,
    fontSize: 16,
    '@media ios': {
      height: 36,
    },
    '@media android': {
      height: 37,
      paddingVertical: 0,
      includeFontPadding: false,
      textAlignVertical: 'center',
    },
  },

  iconPasswd: {
    paddingLeft: 5,
    paddingRight: 5
  },

  // Radios and checkboxs
  multipleContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },

  multipleOptionContainer: {
    flex: 1,
    flexDirection: 'row',
    paddingRight: 2,
  },

  multipleOption: {
    flexDirection: 'row',
  },

  multipleOptionIcon: {
    color: '$Colors.Silver',
    fontSize: 20,
  },

  multipleOptionIconChecked: {
    color: '$Colors.Green',
    fontSize: 20,
  },

  multipleOptionLabel: {
    marginLeft: 3,
  },

  multipleOptionLabelContainer: {
    flexGrow: 1,
    width: 0
  },

  infoDate: {
    fontSize: 12,
    marginLeft: 5,
    padding: 2,
    color: '$Colors.Gray',
    backgroundColor: '$Colors.LightSilver',
  },

  changeStage: {
    marginLeft: 14
  }
});

import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Icon from 'components/Icon';

import styles from './styles';

export default class ErrorMessage extends Component {
  static propTypes = {
    message: React.PropTypes.string.isRequired,
    style  : React.PropTypes.any,
  };

  render() {
    const { message, style } = this.props;
    return(
      <View key="errorBox" style={styles.errorBox}>
        <Text style={[styles.errorStyle, style]}>
          {message}
        </Text>
        <Icon
          name="times-circle"
          size={styles._errorIcon.fontSize}
          color={styles._errorIcon.color}
          style={styles.errorIcon}
        />
      </View>
    );
  }
}

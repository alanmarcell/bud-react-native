import React, { PureComponent } from 'react';
import { View, Text } from 'react-native';
import { FieldType } from 'simple-react-form';

import R from 'utils';

import Underline    from '../Underline';
import ErrorMessage from '../ErrorMessage';
import Label        from '../Label';

import styles from '../styles';

export default class BaseFactory extends PureComponent {
  static defaultProps = {
    ...FieldType.defaultProps,
    underline: true,
    form: {},
    errorStyle: null,
  };

  static propTypes = {
    ...FieldType.propTypes,
    containerStyle: React.PropTypes.oneOfType([
      React.PropTypes.number,
      React.PropTypes.object,
    ]),
    errorStyle: React.PropTypes.any,
    underline: React.PropTypes.bool
  };

  constructor(props) {
    super(props);
    this.state = {
      value: props.value
    };
  }

  componentWillReceiveProps(nextProps) {
    const { value } = nextProps;
    if (value !== this.state.value) {
      this.setState({ value });
    }
  }

  lazyChange = (value) => {
    if (this.lazy) {
      this.lazy.cancel();
    }
    
    const { onChange } = this.props;
    if (typeof onChange == 'function') {
      this.lazy = R.lazyCall(300, () => {
        return this.setState({ value }, onChange(value));
      });
    }
    else{
      this.setState({ value });
    }
  };

  renderContainer() {
    const field = this.renderField();
    const { containerStyle } = this.props;
    return (
      <View style={containerStyle}>
        {this.renderError(field)}
      </View>
    );
  }

  renderField() {
    const { fieldName } = this.props;
    return (
      <View key={fieldName}>
        <Text>{`Render for ${fieldName} not implemented yet`}</Text>
      </View>
    );
  }

  renderUnderline(errorMessage) {
    const { underline } = this.props;
    return underline ? <Underline error={!!errorMessage} key="underline" /> : null;
  }

  renderError(field) {
    const { errorMessage, errorStyle, fieldName } = this.props;
    const children = [field, this.renderUnderline(errorMessage)];
    if (errorMessage) {
      children.push(
        <ErrorMessage
          key={`error_${fieldName}`}
          message={errorMessage}
          style={errorStyle}
        />
      );
    }
    return children;
  }

  renderLabel(label = null) {
    const { label: propLabel, fieldName } = this.props;
    if (label === null) {
      label = propLabel;
    }

    if (label !== false) {
      return <Label>{label || fieldName}</Label>;
    }
    return null;
  }

  renderContent(onPress, content, extraStyles = []) {
    return (
      <Text style={[styles.field, ...extraStyles]} onPress={onPress}>
        {content}
      </Text>
    );
  }

  render() {
    return this.renderContainer();
  }
}

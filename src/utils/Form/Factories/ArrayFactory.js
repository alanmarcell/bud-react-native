import React from 'react';
import { View } from 'react-native';
import { ArrayComponent } from 'simple-react-form';
import ArrayContextItem from 'simple-react-form/lib/Array/ArrayContextItem';
import R from 'ramda';
import isArray from 'lodash/isArray';
import without from 'lodash/without';

import ErrorMessage from '../ErrorMessage';
import Button from 'components/Button';
import Icon from 'components/Icon';

import EStyleSheet from 'react-native-extended-stylesheet';

class ArrayFactoryContextItem extends ArrayContextItem {
  render() {
    return (
      <View>
        {this.props.children}
      </View>
    );
  }
}

export default class ArrayFactory extends ArrayComponent {
  static propTypes = {
    ...ArrayComponent.propTypes,
    errorStyle: React.PropTypes.oneOfType([
      React.PropTypes.number,
      React.PropTypes.object,
    ]),
  };

  shouldComponentUpdate(nextProps) {
    if (R.isNil(this._items)) {
      return true; // No new or removed items
    }

    return this._items.length === nextProps.value.length;
  }

  addItem(itemValue = {}) {
    var newArray = this.props.value;
    if (isArray(newArray)) {
      newArray.push(itemValue);
    } else {
      newArray = [itemValue];
    }

    this._items = newArray;
    this.props.onChange(newArray);
  }

  removeItem(index) {
    const value = this.props.value || [];
    var newArray = without(value, value[index]);

    this._items = newArray;
    this.props.onChange(newArray);
  }

  renderChildrenItem({ index, children }) {
    return (
      <View style={styles.fieldsBox} key={`${this.props.fieldName}.${index}`}>
        {this.renderChildrenItemWithContext({ index, children })}
        {this.props.showRemoveButton
          ? <View style={styles.removeButton}>
            <Icon name="remove" color="#95989A" onPress={() => this.removeItem(index)} />
          </View>
          : null}
      </View>
    );
  }

  renderChildrenItemWithContext({ index, children }) {
    return (
      <ArrayFactoryContextItem index={index} fieldName={this.props.fieldName}>
        {children}
      </ArrayFactoryContextItem>
    );
  }

  renderAddButton() {
    const { showAddButton, addLabel } = this.props;
    if (!showAddButton) return null;
    return (
      <Button
        type="bordered"
        style={styles.borderedButton}
        textStyle={{ color: '#3C484A' }}
        onPress={() => this.addItem()}>
        {addLabel}
      </Button>);
  }

  renderErrorMessage() {
    const { errorMessage, errorStyle } = this.props;
    if (!errorMessage) return null;
    return (
      <ErrorMessage message={errorMessage} style={errorStyle} />
    );
  }

  render() {
    return (
      <View>
        {this.renderErrorMessage()}
        {this.renderChildren()}
        {this.renderAddButton()}
      </View>
    );
  }
}

const styles = EStyleSheet.create({
  fieldsBox: {
    marginTop: 10,
    paddingBottom: 5,
    paddingHorizontal: 10,
    backgroundColor: '#FFFFFF',
  },
  title: {
    color: '$Colors.Disabled',
    fontSize: 10,
    fontWeight: 'bold',
    marginLeft: 10,
  },
  borderedButton: {
    marginTop: 20,
    borderColor: '#3C484A',
    height: 40,
  },
  removeButton: {
    position: 'absolute',
    top: 10,
    right: 10,
  }
});

import React from 'react';
import { Text, View, Slider } from 'react-native';
import R from 'ramda';

import styles from '../styles';
import BaseFactory from './BaseFactory';

export default class SliderFactory extends BaseFactory {
  static defaultProps = {
    ...BaseFactory.defaultProps,
    underline: false,
    value: 0,
  };

  static propTypes = {
    ...BaseFactory.propTypes,
    value      : React.PropTypes.number.isRequired,
    renderValue: React.PropTypes.func,
    style      : React.PropTypes.any,
  };

  inputProps() {
    const { schema, fieldName, value, passProps } = this.props;

    // Extract min and max value from SimpleSchema
    // The props have a preference ouver the schema
    if (schema && (!passProps.minimumValue || !passProps.maximumValue)) {
      const defs = schema.getDefinition(fieldName, ['type']);

      if (!R.isEmpty(defs) && !R.isNil(defs)) {
        const { type: [{ min: minimumValue, max: maximumValue }]} = defs;
        if (R.isNil(passProps.minimumValue) && !R.isNil(minimumValue)) {
          passProps.minimumValue = minimumValue;
        }
        if (R.isNil(passProps.maximumValue) && !R.isNil(maximumValue)) {
          passProps.maximumValue = maximumValue;
        }
      }
    }

    return {
      value,
      ...passProps,
      onValueChange: this.handleChange,
      onSlidingComplete: this.lazyChange,
    };
  }

  handleChange = (value) => {
    if (value !== this.state.value) {
      this.setState({ value });
    }
  }

  renderValue() {
    const { renderValue } = this.props;
    const { value } = this.state;
    let content = value;

    if (typeof renderValue === 'function') {
      content = renderValue(value, this.props);
    }

    if (typeof content !== 'object') {
      content = (<Text style={styles.fieldText}>{content}</Text>);
    }

    return content;
  }

  renderSlider() {
    return (<Slider {...this.inputProps() } />);
  }

  renderField() {
    const { fieldName, style } = this.props;
    return (
      <View key={fieldName} style={[styles.fieldsContainer, style]}>
        {this.renderLabel()}
        {this.renderValue()}
        {this.renderSlider()}
      </View>
    );
  }
}

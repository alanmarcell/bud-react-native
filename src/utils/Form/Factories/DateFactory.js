import React from 'react';
import { View } from 'react-native';

import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';

import styles from '../styles';
import BaseFactory from './BaseFactory';

export default class DateFactory extends BaseFactory {
  static defaultProps = {
    ...BaseFactory.defaultProps,
    value      : new Date(),
    dateFormat : 'L',
    form       : {},
    placeholder: 'Select me!',
  };

  static propTypes = {
    ...BaseFactory.propTypes,
    value: React.PropTypes.oneOfType([
      React.PropTypes.instanceOf(Date),
      React.PropTypes.string,
    ]),
    placeholder: React.PropTypes.string,
  };

  state = {
    ...this.state,
    isDateTimePickerVisible: false,
  };

  _showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true });
  };

  _hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
  };

  _handleDatePicked = (date) => {
    this.lazyChange(date);
    this._hideDateTimePicker();
  };

  dateProps() {
    let { value } = this.state;
    value = value ? moment(value) : value;
    const { dateFormat, passProps, placeholder } = this.props;
    const _dateFormat = passProps.dateFormat || dateFormat;
    return {
      content: value ? value.format(_dateFormat) : placeholder,
      props: {
        date     : value ? value.toDate() : new Date(),
        isVisible: this.state.isDateTimePickerVisible,
        ...passProps,
        onConfirm: this._handleDatePicked,
        onCancel : this._hideDateTimePicker,
      }
    };
  }

  renderField() {
    const { fieldName } = this.props;
    const { props, content } = this.dateProps();
    return (
      <View key={`date_${fieldName}`} style={{ flex: 1 }}>
        {this.renderLabel()}
        {this.renderContent(this._showDateTimePicker, content, [styles.fieldTextDate])}
        <DateTimePicker {...props} />
      </View>
    );
  }
}

import React from 'react';
import { Text } from 'react-native';
import { shallow } from 'enzyme';
import BaseFactory from '../BaseFactory';
import { FieldType } from 'simple-react-form';

describe('BaseFactory', () => {
  const defaultProps = {
    fieldName: 'baseFieldTest',
    onChange: () => {},
    form: {},
  };

  test('have a FieldType propTypes', () => {
    expect(BaseFactory.propTypes).toMatchObject(FieldType.propTypes);
  });

  test.skip('no call render direct', () => {
    console.warn = jest.fn();
    shallow(<BaseFactory {...defaultProps} />);
    expect(console.warn).toHaveBeenCalledWith('renderField not implemented');
  });

  describe('with extend', () => {
    class Field extends BaseFactory {
      renderField() {
        return <Text key="field" {...this.props.passProps} />;
      }
    }

    test('render a container', () => {
      const props = {
        ...defaultProps,
        containerStyle: {
          backgroundColor: 'black',
        }
      };
      const wrapper = shallow(<Field {...props} />);
      expect(wrapper).toMatchSnapshot();
    });

    test('render error message if was passed', () => {
      const props = {
        ...defaultProps,
        errorMessage: 'Error message',
        errorStyle  : {
          color: 'yellow'
        }
      };
      const wrapper = shallow(<Field {...props} />);
      expect(wrapper).toMatchSnapshot();
    });
  });
});

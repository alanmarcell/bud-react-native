import React from 'react';
import { shallow } from 'enzyme';

import DateTimePicker from 'react-native-modal-datetime-picker';
import DateFactory from '../DateFactory';
import moment from 'moment';

describe('DateFactory', () => {
  const defaultProps = {
    fieldName: 'dateExample',
    onChange: () => {},
  };

  test.skip('renders base field correctly field', () => {
    const date  = new Date();
    const props = {
      ...defaultProps,
      value: date,
    };

    const wrapper = shallow(<DateFactory {...props} />);
    const picker  = wrapper.find(DateTimePicker);
    const label   = wrapper.find('Label');

    expect(picker).toHaveProp('date', date);
    expect(label.children().text()).toEqual('dateExample');
  });

  test.skip('forwarding props to main component', () => {
    const props = {
      ...defaultProps,
      label: 'dateExampleCustom',
      passProps: {
        cancelTextIOS : 'myCancel',
        confirmTextIOS: 'myConfirm',
      }
    };

    const wrapper = shallow(<DateFactory {...props} />);
    const picker = wrapper.find(DateTimePicker);
    const label = wrapper.find('Label');

    expect(picker).toHaveProp('date');
    expect(picker).toHaveProp('cancelTextIOS', 'myCancel');
    expect(picker).toHaveProp('confirmTextIOS', 'myConfirm');
    expect(label.children().text()).toEqual('dateExampleCustom');
  });

  test.skip('render value and support custom format', () => {
    const date  = new Date();
    const props = {
      ...defaultProps,
      value       : date,
      dateFormat  : 'LL',
    };

    const wrapper = shallow(<DateFactory {...props} />);
    const text    = wrapper.find('Text').at(0).childAt(0);
    expect(text.text()).toEqual(moment(date).format('LL'));
  });

  test.skip('raise onChange if DateTimePicker changed', () => {
    const date = new Date();
    const onChange = jest.fn();
    const props = {
      ...defaultProps,
      onChange,
      value: moment(date).subtract(2, 'days').toDate(),
    };

    const wrapper = shallow(<DateFactory {...props} />);
    const picker = wrapper.find(DateTimePicker);
    picker.simulate('confirm', date);
    expect(onChange).toHaveBeenCalledWith(date);
  });

  test.skip('show erros if have one', () => {
    const props = {
      ...defaultProps,
      errorMessage: 'Error message',
    };
    const wrapper = shallow(<DateFactory {...props} />);
    const text = wrapper.find('Text').at(1).childAt(0);
    expect(text.text()).toEqual(props.errorMessage);
  });
});

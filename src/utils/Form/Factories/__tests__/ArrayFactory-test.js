import React from 'react';
import renderer from 'react-test-renderer';
import { Form, Field, SimpleSchema } from '../../';

import { ArrayFactory, InputFactory } from '../';

const friendy = new SimpleSchema({
  name: String
});

const Schema = new SimpleSchema({
  friendies: [friendy]
});

it('should render correctly', () => {
  const tree = renderer.create(
    <Form schema={Schema}>
      <Field fieldName='friendies' type={ArrayFactory}>
        <Field fieldName="name" type={InputFactory} />
      </Field>
    </Form>
  ).toJSON();
  expect(tree).toMatchSnapshot();
});

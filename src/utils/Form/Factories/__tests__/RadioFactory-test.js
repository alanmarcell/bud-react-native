import React from 'react';
import { shallow } from 'enzyme';

import RadioFactory from '../RadioFactory';

describe('RadioFactory', () => {
  const defaultProps = {
    fieldName: 'radio',
    onChange : () => {},
    value    : 'value1',
    options  : [
      'value1',
      { label: 'option2', value: 'value2' },
    ],
    underline: true,
  };

  test('renders correctly field', () => {
    const props = {
      ...defaultProps,
    };

    const wrapper = shallow(<RadioFactory {...props} />);
    const inputs  = wrapper.find('RadioOption');
    const label   = wrapper.find('Label');

    expect(label.children().text()).toEqual('radio');

    expect(inputs.length).toEqual(2);
    expect(inputs.at(0)).toHaveProp('checked', true);
    expect(inputs.at(1)).toHaveProp('checked', false);

    expect(wrapper).toMatchSnapshot();
  });

  test('renders correctly radio field', () => {
    const props = {
      ...defaultProps,
      label: 'radioCustom',
      value: 'value2',
    };

    const wrapper = shallow(<RadioFactory {...props} />);
    const inputs  = wrapper.find('RadioOption');
    const label   = wrapper.find('Label');

    expect(label.children().text()).toEqual('radioCustom');

    expect(inputs.length).toEqual(2);
    expect(inputs.at(0)).toHaveProp('checked', false);
    expect(inputs.at(1)).toHaveProp('checked', true);

    expect(wrapper).toMatchSnapshot();
  });

  test('renders radio without label', () => {
    const props = {
      ...defaultProps,
      label: false,
      value: 'value2',
    };

    const wrapper = shallow(<RadioFactory {...props} />);
    const label   = wrapper.find('Label');

    expect(label.length).toEqual(0);
  });

  test.skip('raise onChange if RadioOption pressed', () => {
    const onChange = jest.fn();
    const props = {
      ...defaultProps,
      onChange,
    };

    const wrapper = shallow(<RadioFactory {...props} />);
    const label   = wrapper.find('Label');

    expect(label.children().text()).toEqual('radio');

    // trigger onPress
    wrapper.find('RadioOption').at(1).simulate('press');

    const inputs   = wrapper.find('RadioOption');
    const newValue = 'value2';
    expect(onChange).toHaveBeenCalledWith(newValue);
    expect(wrapper.state().value).toEqual(newValue);

    expect(inputs.length).toEqual(2);
    expect(inputs.at(0)).toHaveProp('checked', false);
    expect(inputs.at(1)).toHaveProp('checked', true);
    expect(wrapper).toMatchSnapshot();
  });

  test.skip('show erros if have one', () => {
    const props = {
      ...defaultProps,
      errorMessage: 'Error message',
    };
    const wrapper = shallow(<RadioFactory {...props} />);
    const text = wrapper.find('Text').childAt(0);
    expect(text.text()).toEqual(props.errorMessage);
  });
});

import React from 'react';
import { View, Text } from 'react-native';
import { shallow } from 'enzyme';

import SliderFactory from '../SliderFactory';
import { SimpleSchema } from '../../';

describe('SliderFactory', () => {
  const defaultProps = {
    fieldName: 'sliderExample',
    onChange: () => {},
  };

  test('renders correctly field', () => {
    const props = {
      ...defaultProps,
      value: 10,
    };

    const wrapper = shallow(<SliderFactory {...props} />);
    const slider  = wrapper.find('Slider');
    const label   = wrapper.find('Label');

    expect(slider).toHaveProp('value', 10);
    expect(label.children().text()).toEqual('sliderExample');
  });

  test('renders correctly slider field', () => {
    const props = {
      ...defaultProps,
      label: 'sliderCustom',
      passProps: {
        step: 1,
        minimumValue: 0,
        maximumValue: 10,
      }
    };

    const wrapper = shallow(<SliderFactory {...props} />);
    const slider = wrapper.find('Slider');
    const label = wrapper.find('Label');

    expect(slider).toHaveProp('value', 0);
    expect(slider).toHaveProp('step', 1);
    expect(slider).toHaveProp('minimumValue', 0);
    expect(slider).toHaveProp('maximumValue', 10);
    expect(label.children().text()).toEqual('sliderCustom');
  });

  test('extract min and max from schame', () => {
    const schema = new SimpleSchema({
      field: {
        type: Number,
        min: 10,
      }
    });

    const props = {
      ...defaultProps,
      schema,
      fieldName: 'field',
      passProps: {
        step: 1,
        maximumValue: 20,
      }
    };

    const wrapper = shallow(<SliderFactory {...props} />);
    const slider = wrapper.find('Slider');

    expect(slider).toHaveProp('value', 0);
    expect(slider).toHaveProp('step', 1);
    expect(slider).toHaveProp('minimumValue', 10);
    expect(slider).toHaveProp('maximumValue', 20);
  });

  test('support custom renderValue', () => {
    const props = {
      ...defaultProps,
      value       : 5,
      renderValue : (value, _props) => `${value}º`,
    };

    const wrapper   = shallow(<SliderFactory {...props} />);
    const valueView = wrapper.containsMatchingElement(<Text>5º</Text>);

    expect(valueView).toBeTruthy();
  });

  test('support custom renderValue using View', () => {
    const renderValue = (value, _props) => (
      <View>
        <Text>{`${value} ppm`}</Text>
      </View>
    );
    const props = {
      ...defaultProps,
      value : 11,
      renderValue,
    };

    const wrapper   = shallow(<SliderFactory {...props} />);
    const valueView = wrapper.contains(renderValue(11));

    expect(valueView).toBeTruthy();
  });

  test.skip('raise onChange if Slider changed', () => {
    const onChange = jest.fn();
    const props = {
      ...defaultProps,
      onChange,
    };

    const wrapper = shallow(<SliderFactory {...props} />);
    const slider = wrapper.find('Slider');
    slider.simulate('valueChange', 20);
    slider.simulate('slidingComplete');
    expect(onChange).toHaveBeenCalledWith(20);
  });

  test.skip('show erros if have one', () => {
    const props = {
      ...defaultProps,
      errorMessage: 'Error message',
    };
    const wrapper = shallow(<SliderFactory {...props} />);
    const text = wrapper.find('Text').at(1).childAt(0);
    expect(text.text()).toEqual(props.errorMessage);
  });
});

import React from 'react';
import { shallow } from 'enzyme';

import CheckboxFactory from '../CheckboxFactory';

describe('CheckboxFactory', () => {
  const defaultProps = {
    fieldName: 'checkbox',
    onChange : () => {},
    value    : ['value1'],
    options  : [
      'value1',
      { label: 'option2', value: 'value2' },
    ],
  };

  test('renders correctly field', () => {
    const props = {
      ...defaultProps,
    };

    const wrapper = shallow(<CheckboxFactory {...props} />);
    const inputs  = wrapper.find('CheckboxOption');
    const label   = wrapper.find('Label');

    expect(label.children().text()).toEqual('checkbox');

    expect(inputs.length).toEqual(2);
    expect(inputs.at(0)).toHaveProp('checked', true);
    expect(inputs.at(1)).toHaveProp('checked', false);

    expect(wrapper).toMatchSnapshot();
  });

  test('renders correctly checkbox field', () => {
    const props = {
      ...defaultProps,
      label: 'checkboxCustom',
      value: ['value1', 'value2'],
    };

    const wrapper = shallow(<CheckboxFactory {...props} />);
    const inputs  = wrapper.find('CheckboxOption');
    const label   = wrapper.find('Label');

    expect(label.children().text()).toEqual('checkboxCustom');

    expect(inputs.length).toEqual(2);
    expect(inputs.at(0)).toHaveProp('checked', true);
    expect(inputs.at(1)).toHaveProp('checked', true);

    expect(wrapper).toMatchSnapshot();
  });

  test.skip('raise onChange if CheckboxOption pressed', () => {
    const onChange = jest.fn();
    const props = {
      ...defaultProps,
      onChange,
    };

    const wrapper = shallow(<CheckboxFactory {...props} />);

    // trigger onPress
    wrapper.find('CheckboxOption').at(1).simulate('press');

    const inputs   = wrapper.find('CheckboxOption');
    const newValue = ['value1', 'value2'];
    expect(onChange).toHaveBeenCalledWith(newValue);

    expect(wrapper.state().value).toEqual(newValue);

    expect(inputs.length).toEqual(2);
    expect(inputs.at(0)).toHaveProp('checked', true);
    expect(inputs.at(1)).toHaveProp('checked', true);
    expect(wrapper).toMatchSnapshot();
  });

  test.skip('show erros if have one', () => {
    const props = {
      ...defaultProps,
      errorMessage: 'Error message',
    };
    const wrapper = shallow(<CheckboxFactory {...props} />);
    const text = wrapper.find('Text').childAt(0);
    expect(text.text()).toEqual(props.errorMessage);
  });
});

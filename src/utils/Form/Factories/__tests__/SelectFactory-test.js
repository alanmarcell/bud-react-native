import React from 'react';
import { shallow } from 'enzyme';

import ModalPicker from 'react-native-modal-picker';
import SelectFactory from '../SelectFactory';

describe('SelectFactory', () => {
  const defaultProps = {
    fieldName: 'selectExample',
    onChange: () => {},
  };

  test.skip('renders base field correctly field', () => {
    const props = {
      ...defaultProps,
      value: { label: 'Item1' },
      options : [
        { label: 'Item1' }
      ],
    };

    const wrapper = shallow(<SelectFactory {...props} />);
    const select  = wrapper.find(ModalPicker);
    const label   = wrapper.find('Label');

    expect(select).toHaveProp('data', props.options);
    expect(label.children().text()).toEqual('selectExample');

    const text = select.find('Text').at(0).childAt(0);
    expect(text.text()).toEqual(props.value.label);
  });

  test.skip('render a default initValue', () => {
    const wrapper = shallow(<SelectFactory {...defaultProps} />);
    const select  = wrapper.find(ModalPicker);
    const text = select.find('Text').at(0).childAt(0);
    expect(text.text()).toEqual(ModalPicker.defaultProps.initValue);
  });

  test.skip('render a custom initValue', () => {
    const props = {
      ...defaultProps,
      initValue: 'customInitValue'
    };

    const wrapper = shallow(<SelectFactory {...props} />);
    const select  = wrapper.find(ModalPicker);
    const text = select.find('Text').at(0).childAt(0);
    expect(text.text()).toEqual('customInitValue');
  });

  test.skip('forwarding props to main component', () => {
    const props = {
      ...defaultProps,
      label: 'selectExampleCustom',
      passProps: {
        cancelText: 'myCancel',
      }
    };

    const wrapper = shallow(<SelectFactory {...props} />);
    const select = wrapper.find(ModalPicker);
    const label = wrapper.find('Label');

    expect(select).toHaveProp('cancelText', 'myCancel');
    expect(label.children().text()).toEqual('selectExampleCustom');
  });

  test.skip('raise onChange if ModalPicker changed', () => {
    const onChange = jest.fn();
    const props = {
      ...defaultProps,
      onChange,
    };

    const wrapper = shallow(<SelectFactory {...props} />);
    const select  = wrapper.find(ModalPicker);
    select.simulate('change', { key: 1 });
    expect(onChange).toHaveBeenCalledWith({ key: 1 });
  });

  test.skip('raise press in selected field', () => {
    const onChange = jest.fn();
    const props = {
      ...defaultProps,
      onChange,
      options: [
        { key: 1, label: 'item 1' },
        { key: 2, label: 'item 2' },
      ]
    };

    const wrapper = shallow(<SelectFactory {...props} />);
    const select  = wrapper.find(ModalPicker).dive();
    const field   = select.find('TouchableOpacity').at(1);
    field.simulate('press');
    expect(onChange).toHaveBeenCalledWith(props.options[1]);
  });

  test.skip('show erros if have one', () => {
    const props = {
      ...defaultProps,
      errorMessage: 'Error message',
    };
    const wrapper = shallow(<SelectFactory {...props} />);
    const text = wrapper.find('Text').at(1).childAt(0);
    expect(text.text()).toEqual(props.errorMessage);
  });
});

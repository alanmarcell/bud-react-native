import React from 'react';
import { shallow } from 'enzyme';

import InputFactory from '../InputFactory';

describe('InputFactory', () => {
  const defaultProps = {
    fieldName: 'inputExample',
    onChange: () => {},
  };

  test.skip('renders correctly string field', () => {
    const props = {
      ...defaultProps,
      value: 'foo@bar.com',
    };

    const wrapper = shallow(<InputFactory {...props} />);
    const input = wrapper.find('TextField');
    expect(input).toHaveProp('value', 'foo@bar.com');
    expect(input).toHaveProp('label', 'inputExample');
  });

  test.skip('renders correctly password field', () => {
    const props = {
      ...defaultProps,
      label: 'pass',
      passProps: {
        secureTextEntry: true,
        autoCapitalize: 'none',
        autoCorrect: false,
      }
    };

    const wrapper = shallow(<InputFactory {...props} />);
    const input = wrapper.find('TextField');
    expect(input).toHaveProp('secureTextEntry', true);
    expect(input).toHaveProp('value', '');
    expect(input).toHaveProp('label', 'pass');
    expect(input).toHaveProp('autoCapitalize', 'none');
    expect(input).toHaveProp('autoCorrect', false);
  });

  test.skip('raise onChange if TextField changed', () => {
    const onChange = jest.fn();
    const props = {
      ...defaultProps,
      onChange,
    };

    const wrapper = shallow(<InputFactory {...props} />);
    const input = wrapper.find('TextField');
    input.simulate('changeText', 'foo@bar.com');
    expect(onChange.mock.calls[0]).toEqual(['foo@bar.com']);
  });

  test.skip('show erros if have one', () => {
    const props = {
      ...defaultProps,
      errorMessage: 'Error message',
    };
    const wrapper = shallow(<InputFactory {...props} />);
    const text = wrapper.find('Text').childAt(0);
    expect(text.text()).toEqual(props.errorMessage);
  });
});

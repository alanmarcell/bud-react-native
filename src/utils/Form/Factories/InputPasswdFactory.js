import React from 'react';
import { View, TextInput } from 'react-native';
import Icon from 'components/Icon';
import R from 'utils';

import styles from '../styles';
import BaseFactory from './BaseFactory';

export default class InputPasswdFactory extends BaseFactory {
  static defaultProps = {
    ...BaseFactory.defaultProps,
    number: false,
    maxLength: 20,
  };

  state = {
    passViewIconName: 'eye',
    passwdValue: ''
  }

  static propTypes = {
    ...BaseFactory.propTypes,
    value: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number,
    ]),
    inputStyle: React.PropTypes.any,
    number: React.PropTypes.bool,
    maxLength: React.PropTypes.number
  };

  inputProps() {
    let { value } = this.state;
    let { inputStyle, passProps, number, maxLength } = this.props;
    let inputProps = {
      containerStyle: styles.inputContainer,
      underlineColorAndroid: 'transparent',
      style: [styles.inputStyle, inputStyle],
      maxLength: maxLength,
      ...passProps,
    };

    if (number) {
      inputProps = {
        keyboardType: 'numeric',
        ...inputProps,
      };
    }

    if (!R.isNil(value)) {
      inputProps.value = value.toString();
    }

    return inputProps;
  }

  handlePasswdView = () => {
    this.setState(({passViewIconName}) => ({
      passViewIconName: passViewIconName === 'eye' ? 'eye-blocked' : 'eye'
    }));
  }

  handleChangeText = (text) => {
    this.setState({ passwdValue: text });
    this.lazyChange(text);
  }

  getPasswdSelection = () => ({
    start: this.state.passwdValue.length,
    end: this.state.passwdValue.length
  })

  renderInput() {
    let passInputProps = Object.assign({}, this.inputProps());
    delete passInputProps['secureTextEntry'];
    delete passInputProps['value'];

    return (
      <View style={styles.fieldsContainerPasswd}>
        <TextInput {...passInputProps}
          value={this.state.passwdValue}
          selection={this.getPasswdSelection()}
          onChangeText={this.handleChangeText}
          secureTextEntry={this.state.passViewIconName === 'eye'}
        />
        <Icon onPress={this.handlePasswdView}
          name={this.state.passViewIconName}
          style={styles.iconPasswd}
          size={18}
        />
      </View>
    );
  }

  renderField() {
    const { fieldName } = this.props;
    return (
      <View key={`input_${fieldName}`} style={styles.fieldsContainer}>
        {this.renderLabel()}
        {this.renderInput()}
      </View>
    );
  }
}

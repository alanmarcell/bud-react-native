import React from 'react';
import { Text, TextInput, View, TouchableOpacity } from 'react-native';
import { Platform, Keyboard } from 'react-native';
import { FlatList, ActivityIndicator } from 'react-native';
import Modal from 'react-native-animated-modal';
import MarkdownBox from 'components/MarkdownBox';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import StatusBarSizeIOS from 'react-native-status-bar-size';
import { debounce } from 'lodash/function';

import R from 'ramda';
import fuzzy from 'fuzzy';

import { I18n } from 'utils/I18n';

import Icon from 'components/Icon';
import Button from 'components/Button';
import BaseFactory from './BaseFactory';

import styles from './SearchSelectFactory/styles';

const SEARCH_RESULT_INIT = { hasNextPage: false };

class SearchFactory extends BaseFactory {
  static defaultProps = {
    ...BaseFactory.defaultProps,
    options: [],
    placeholder: 'Select me!',
    allDataOnEmptySearch: true,
    autoFocus: false,
    showSearchOnInit: false
  };

  static propTypes = {
    ...BaseFactory.propTypes,
    value: React.PropTypes.object,
    placeholder: React.PropTypes.string,
    formatText: React.PropTypes.func,
    searchFn: React.PropTypes.func,
    options: React.PropTypes.oneOfType([
      React.PropTypes.arrayOf(
        React.PropTypes.shape({
          label: React.PropTypes.string,
          index: React.PropTypes.any
        })
      ),
      React.PropTypes.func,
    ]),
    allDataOnEmptySearch: React.PropTypes.bool,
    autoFocus: React.PropTypes.bool,
    actionEmpty: React.PropTypes.func,
    actionEmptyText: React.PropTypes.string,
  };
  constructor(props) {
    super(props);
    this.changes = this.props.form.state.changes;
  }
  state = {
    query: '',
    loading: false,
    loadingMore: false,
    showSearch: false,
    searchResults: SEARCH_RESULT_INIT,
  };

  componentWillMount() {
    if(this.props.showSearchOnInit)
      this._showSearch();
  }

  _handlerSelect = (rowData) => {
    const { onChange } = this.props;
    Keyboard.dismiss();
    onChange(rowData);
    this._hideSearch();
  }

  _handlerBack = () => {
    Keyboard.dismiss();
    this._hideSearch();
  };

  _hideSearch = () => {
    this.setState({ showSearch: false });
  }

  _showSearch = () => {
    this.setState({ showSearch: true });
    this._runQuery();
  };

  _runQuery = async (loadMore = false) => {
    const { query } = this.state;
    this.setState({ [loadMore ? 'loadingMore' : 'loading']: true });
    this.setState({
      loading: false,
      loadingMore: false,
      searchResults: await this._find(query, loadMore),
    });
  };

  _loadMore = () => {
    const { searchResults } = this.state;
    this._runQuery(searchResults);
  }

  _handleChangeText = (query) => {
    this.setState({ query });
    if (this.debounce) {
      this.debounce.cancel();
    }

    this.debounce = debounce(async () => {
      if (query === this.state.query) {
        this._runQuery();
      }
    }, 500)();
  };

  _cleanInput = () => {
    if (this.state.query !== '') {
      this.setState({
        query: '',
        loading: false,
        searchResults: SEARCH_RESULT_INIT
      });
      this._handleChangeText('');
    }
  };

  _find(query, loadMore) {
    const { searchFn, value } = this.props;
    const { searchResults } = this.state;

    if (searchFn) {
      return loadMore ? searchResults.loadMoreEntries() : searchFn(query, value);
    }

    let { options } = this.props;
    options = R.is(Function, options) ? options() : options;

    // Fuzzy find by query query
    let result = this._internalSearch(query, options);
    if (R.isEmpty(query)) {
      result = R.tail(result);
    }

    return {
      hasNextPage: false,
      data: R.pipe(
        R.reverse,
        R.uniqWith((a, b) => a.score === Infinity && b.score === Infinity),
        R.sort(R.descend(R.prop('score'))),
      )(result)
    };
  }

  _internalSearch = R.memoize((query, list) => {
    let { allDataOnEmptySearch } = this.props;
    if (query === '') {
      return !allDataOnEmptySearch ? [] : R.map((el) => ({
        string: el.label,
        original: el,
      }), list);
    }
    return fuzzy.filter(query, list, {
      pre: '**',
      post: '**',
      extract: (el) => {
        if (el.index === '__new__') {
          return query;
        } else {
          return el.label;
        }
      },
    });
  });

  renderClean() {
    return (
      <TouchableOpacity
        onPress={this._cleanInput}
      >
        <Icon
          style={[styles.icons, styles.cleanIcon]}
          size={styles._icons.fontSize}
          name="times-circle"
        />
      </TouchableOpacity>
    );
  }

  renderInput() {
    const { autoFocus } = this.props;
    const { query, showSearch } = this.state;
    return (
      <View style={styles.searchContainer}>
        <Icon style={[styles.icons, styles.searchIcon]} size={styles._icons.fontSize} name="search" />
        <TextInput
          autoFocus={autoFocus && showSearch}
          value={query}
          style={styles.query}
          placeholder="Search..."
          onChangeText={this._handleChangeText}
        />
        {this.renderClean()}
      </View>
    );
  }

  renderItem = ({ item }) => {
    const { formatText } = this.props;
    let { string, original } = item;

    // Format text
    string = string.replace(/\*{4,}/g, '');
    string = R.is(Function, formatText) ? formatText(string, original) : string;

    return (
      <TouchableOpacity
        onPress={_ => this._handlerSelect(original)}
        style={styles.itemContainer}
      >
        <MarkdownBox>{string}</MarkdownBox>
      </TouchableOpacity>
    );
  };

  renderActivity(loading) {
    return !loading ? null : <ActivityIndicator style={styles.activity} />;
  }

  renderHeader = () => {
    const { loading, loadingMore } = this.state;
    return this.renderActivity(!loadingMore && loading);
  };

  renderFooter = () => {
    const { loadingMore } = this.state;
    return this.renderActivity(loadingMore);
  };

  renderResult() {
    const { loading, loadingMore, showSearch, searchResults } = this.state;
    const { data, hasNextPage } = searchResults;

    if (showSearch) {
      return (
        <FlatList
          data={data}
          refreshing={loading || loadingMore}
          keyExtractor={(item, index) => index}
          style={styles.resultsContainer}
          renderItem={this.renderItem}
          onEndReached={hasNextPage ? this._loadMore : () => { }}
          ListHeaderComponent={this.renderHeader}
          ListFooterComponent={this.renderFooter}
          keyboardDismissMode="interactive"
          keyboardShouldPersistTaps="always"
        />
      );
    }
  }

  renderEmpty() {
    const { actionEmpty, actionEmptyText } = this.props;
    const handleActionEmpty = () => {
      this._hideSearch();

      return actionEmpty();
    };

    return (
      <View style={styles.resultsContainer}>
        <Text style={styles.emptyText}>{I18n.t('forms.searchParent.empty')}</Text>

        {!R.isNil(actionEmpty) &&
          <Button style={styles.buttonAction} type="bordered" onPress={handleActionEmpty}>
            <Text style={styles.buttonActionText}>{`+ ${actionEmptyText}`}</Text>
          </Button>
        }
      </View>
    );
  }

  renderBack() {
    return (
      <TouchableOpacity
        onPress={this._hideSearch}
        style={styles.backContainer}
      >
        <Icon
          style={[styles.icons, styles.backIcon]}
          size={styles._icons.fontSize}
          name="arrow-left"
        />
      </TouchableOpacity>
    );
  }

  renderModal(label) {
    const { showSearch, searchResults } = this.state;
    const { data } = searchResults;
    const modalProps = {
      isVisible: showSearch
    };
    return (
      <Modal
        style={styles.modal}
        {...modalProps}
      >
        <View style={[styles.modalContainer, { borderTopWidth: StatusBarSizeIOS.currentHeight }]}>
          <View style={styles.search}>
            <View style={styles.titleContainer}>
              <View style={styles.title}>
                <Text style={styles.titleText}>{label}</Text>
              </View>
              {this.renderBack()}
            </View>
            {this.renderInput()}
          </View>
          {R.isEmpty(data) && this.renderEmpty()}
          {!R.isEmpty(data) && this.renderResult()}
        </View>
        {Platform.OS === 'ios' ? <KeyboardSpacer /> : null}
      </Modal>
    );
  }
  renderField() {
    const { label, fieldName } = this.props;

    return (
      <View key={`select_${this.name}`} style={{ flex: 1 }}>
        {this.renderModal(label || fieldName)}
      </View>
    );
  }
}

export default SearchFactory;


import React from 'react';
import { View, TextInput } from 'react-native';

import R from 'utils';

import styles from '../styles';
import BaseFactory from './BaseFactory';

export default class InputFactory extends BaseFactory {
  static defaultProps = {
    ...BaseFactory.defaultProps,
    number: false,
    maxLength: 20,
  };

  static propTypes = {
    ...BaseFactory.propTypes,
    value: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number,
    ]),
    inputStyle: React.PropTypes.any,
    number: React.PropTypes.bool,
    maxLength: React.PropTypes.number
  };

  inputProps() {
    let { value } = this.state;
    let { inputStyle, passProps, number, maxLength } = this.props;
    let inputProps = {
      containerStyle: styles.inputContainer,
      underlineColorAndroid: 'transparent',
      style: [styles.inputStyle, inputStyle],
      maxLength: maxLength,
      ...passProps,
    };

    if (number) {
      inputProps = {
        keyboardType: 'numeric',
        autoCapitalize: 'none',
        ...inputProps,
      };
    }

    if (!R.isNil(value)) {
      inputProps.value = value.toString();
    }

    //if a higher component pass 'onChangeText', then setState first.
    inputProps.hasOwnProperty('onChangeText') && this.lazyChange(value);
    return inputProps;
  }

  handleChangeText = (text) => {
    this.setState({ value: text });
    this.lazyChange(text);
  }

  renderInput() {
    return <TextInput
      onChangeText={this.handleChangeText}
      {...this.inputProps()}
    />;
  }

  renderField() {
    const { fieldName } = this.props;
    return (
      <View key={`input_${fieldName}`} style={styles.fieldsContainer}>
        {this.renderLabel()}
        {this.renderInput()}
      </View>
    );
  }
}

import React from 'react';
import { View } from 'react-native';
import ModalPicker from 'react-native-modal-picker';

import styles from '../styles';
import BaseFactory from './BaseFactory';

export default class SelectFactory extends BaseFactory {
  static defaultProps = {
    ...BaseFactory.defaultProps,
    options: [],
    placeholder: ModalPicker.defaultProps.initValue,
  };

  static propTypes = {
    ...BaseFactory.propTypes,
    value: React.PropTypes.shape({
      key     : React.PropTypes.any,
      label   : React.PropTypes.string,
    }),
    initValue: React.PropTypes.string,
    placeholder: React.PropTypes.string,
    options  : React.PropTypes.oneOfType([
      React.PropTypes.array,
      React.PropTypes.func,
    ]),
  };

  pickerProps() {
    const { value } = this.state;

    const {
      options: data,
      placeholder,
      initValue,
      passProps,
    } = this.props;
    const {onChangeCb} = passProps;
    const { label: content } = value || { label: passProps.initLabel || initValue || placeholder };
    // If have callback execute it after lazyChange
    const lazyWithCb = (args)=> {
      if(args.key)
        this.lazyChange(args);
      onChangeCb(args);
    };
    const onChange = onChangeCb ? lazyWithCb : this.lazyChange;
    return {
      content,
      props: {
        data: typeof data === 'function' ? data() : data,
        onChange,
        ...passProps,
      }
    };
  }

  renderField() {
    const { fieldName } = this.props;
    const { props, content } = this.pickerProps();
    
    return (
      <View key={`select_${fieldName}`} style={styles.fieldsContainer}>
        {this.renderLabel()}
        <ModalPicker { ...props }>
          { this.renderContent(null, content, [])  }
        </ModalPicker>
      </View>
    );
  }
}

import React from 'react';
import { Text, View } from 'react-native';

import BaseFactory from './BaseFactory';

import Icon      from 'components/Icon';
import Touchable from 'utils/components/Touchable';

import styles from '../styles';

export class RadioOption extends React.PureComponent {
  static defaultProps = {
    checked: false,
    underline: true,
    label: false,
  };

  static propTypes = {
    label  : React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.bool,
    ]),
    value  : React.PropTypes.any,
    checked: React.PropTypes.bool,
    onPress: React.PropTypes.func,
  };

  renderIcon() {
    const { checked } = this.props;
    let icon, style;
    if (checked) {
      icon = 'radio_button_checked';
      style = styles._multipleOptionIconChecked;
    } else {
      icon = 'radio_button_unchecked';
      style = styles._multipleOptionIcon;
    }

    return <Icon
      name={icon}
      color={style.color}
      size={style.fontSize}
      style={style}
    />;
  }

  renderLabel() {
    const { label, children } = this.props;
    return !label ? children : <Text style={styles.multipleOptionLabel}>{label}</Text>;
  }

  renderContainer() {
    return (
      <View style={styles.multipleOption}>
        {this.renderIcon()}
        {this.renderLabel()}
      </View>
    );
  }

  render() {
    const { label, value, onPress } = this.props;
    let touchableStyle = styles.multipleOptionContainer;

    if (!label) {
      touchableStyle = null;
    }

    return (
      <Touchable
        style={touchableStyle}
        onPress={onPress ? () => onPress(value) : null }
      >
        {this.renderContainer()}
      </Touchable>
    );
  }
}

export default class RadioFactory extends BaseFactory {
  static defaultProps = {
    ...BaseFactory.defaultProps,
    options: [],
  };

  static propTypes = {
    ...BaseFactory.propTypes,
    value    : React.PropTypes.any,
    options  : React.PropTypes.arrayOf(
      React.PropTypes.oneOfType([
        React.PropTypes.string,
        React.PropTypes.shape({
          label: React.PropTypes.string,
          value: React.PropTypes.any
        })
      ])
    ),
  };

  renderOption = (option, index) => {
    if (typeof option === 'string') {
      option = { label: option, value: option };
    }

    if (!option || !option.label || !option.value) { return null; }
    const { value } = this.state;

    const checked = value === option.value;

    const underline = this.props.underline;

    let props = {
      ...option,
      underline,
      key: index,
      checked,
      index,
      onPress: () => this.lazyChange(option.value),
    };

    return (<RadioOption {...props} />);
  }

  renderField() {
    const { fieldName, options } = this.props;

    return (
      <View key={`radio_${fieldName}`} style={styles.fieldsContainer}>
        {this.renderLabel()}
        <View style={[styles.field, styles.multipleContainer]}>
          {options.map(this.renderOption)}
        </View>
      </View>
    );
  }
}

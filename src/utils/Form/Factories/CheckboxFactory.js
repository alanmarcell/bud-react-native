import React from 'react';
import { Text, View } from 'react-native';
import R from 'ramda';

import BaseFactory from './BaseFactory';
import Icon      from 'components/Icon';
import Touchable from 'utils/components/Touchable';

import styles from '../styles';

export class CheckboxOption extends React.PureComponent {
  static defaultProps = {
    checked: false,
  };

  static propTypes = {
    label  : React.PropTypes.string.isRequired,
    value  : React.PropTypes.any.isRequired,
    checked: React.PropTypes.bool,
    onPress: React.PropTypes.func,
  };

  renderLabel() {
    const { label } = this.props;
    return (
      <Text style={styles.multipleOptionLabel}>{label}</Text>
    );
  }

  renderIcon(icon, style, _checked) {
    return <Icon name={icon} style={style} />;
  }

  render() {
    const { value, checked, onPress } = this.props;
    let icon, style;
    if (checked) {
      icon = 'check-square-o';
      style = styles._multipleOptionIconChecked;
    } else {
      icon = 'square-o';
      style = styles._multipleOptionIcon;
    }

    return (
      <Touchable
        style={styles.multipleOptionContainer}
        onPress={() => onPress(value)}
      >
        {this.renderIcon(icon, style, checked)}
        <View style={styles.multipleOptionLabelContainer}>
          {this.renderLabel()}
        </View>
      </Touchable>
    );
  }
}

export default class CheckboxFactory extends BaseFactory {
  static defaultProps = {
    ...BaseFactory.defaultProps,
    form   : {},
    options: [],
    value  : [],
    split  : 3,
  };

  static propTypes = {
    ...BaseFactory.propTypes,
    value  : React.PropTypes.array,
    split  : React.PropTypes.number,
    renderOption  : React.PropTypes.func,
    options  : React.PropTypes.arrayOf(
      React.PropTypes.oneOfType([
        React.PropTypes.string,
        React.PropTypes.shape({
          label: React.PropTypes.string,
          value: React.PropTypes.any
        })
      ])
    ),
  };

  optionHandle = (optionValue) => {
    const { value } = this.state;

    const checked = !!(value || []).find((v) => v === optionValue);
    let newValue = (value || []);
    if (checked) {
      newValue = newValue.filter((v) => v !== optionValue);
    } else {
      newValue = [...newValue, optionValue];
    }

    if (value !== newValue) {
      this.lazyChange(newValue);
    }
  }

  makeOption = (option, index) => {
    const key = `option_${index}`;

    // Only a space
    if (typeof option === 'number') {
      return <View key={key} style={styles.multipleOptionContainer} />;
    }

    if (typeof option === 'string') {
      option = { label: option, value: option };
    }

    if (!option || !option.label || !option.value) { return null; }
    const { value } = this.state;

    const checked = !!(value || []).find((v) => v === option.value);

    return this.renderOption({
      ...option,
      key,
      checked,
      index,
      onPress: () => this.optionHandle(option.value),
    });
  }

  renderOption(props) {
    const { renderOption } = this.props;
    if (renderOption) {
      return renderOption(props);
    }
    return (<CheckboxOption {...props} />);
  }

  renderOptions() {
    let { options, split } = this.props;
    const mapIndexed = R.addIndex(R.map);

    const required = split - (options.length % split);
    options = [...options, ...R.range(0, required)];

    return R.pipe(
      R.splitEvery(split),
      mapIndexed((line, index) => (
        <View key={`options_line_${index}`} style={styles.multipleContainer}>
          {mapIndexed(this.makeOption, line)}
        </View>
      )),
      R.flatten
    )(options);
  }

  renderField() {
    const { fieldName } = this.props;
    return (
      <View key={fieldName} style={styles.fieldsContainer}>
        {this.renderLabel()}
        <View style={[styles.field]}>
          {this.renderOptions()}
        </View>
      </View>
    );
  }
}

import React from 'react';
import { View } from 'react-native';
import { ObjectComponent } from 'simple-react-form';

import Label  from '../Label';
import ErrorMessage from '../ErrorMessage';

export default class ObjectFactory extends ObjectComponent {
  static propTypes = {
    ...ObjectComponent.propTypes,
    style: React.PropTypes.any,
    errorStyle: React.PropTypes.oneOfType([
      React.PropTypes.number,
      React.PropTypes.object,
    ]),
  };

  renderErrorMessage() {
    const { errorMessage, errorStyle } = this.props;
    return !errorMessage ? null : (
      <ErrorMessage message={errorMessage} style={errorStyle} />
    );
  }

  renderLabel() {
    const { fieldName, label } = this.props;
    return (<Label>{label || fieldName}</Label>);
  }

  render() {
    const { style } = this.props;
    return (
      <View style={style}>
        { this.renderLabel() }
        { this.renderErrorMessage() }
        { this.getChildrenComponents() }
      </View>
    );
  }
}

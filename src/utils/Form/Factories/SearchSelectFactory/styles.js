import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  activity: {
    marginBottom: 5,
    marginTop: 5,
  },

  fieldText: {
    fontSize: 16,
  },

  search: {
    backgroundColor: '$Colors.Purple',
  },

  modal: {
    margin: 0,
  },

  modalContainer: {
    flex: 1,
    borderTopColor: '$Colors.DarkPurple',
  },

  // Navigation
  $navHeight: 40,

  titleContainer: {
    height: '$navHeight',
    alignItems: 'center',
  },

  backContainer: {
    flex: 1,
    height: '$navHeight',
    marginTop: '$navHeight * -1',
    justifyContent: 'center',
    alignSelf: 'flex-start',
    zIndex: 10,
  },

  backIcon: {
    color: 'white',
    marginLeft: 25,
  },

  title: {
    flex: 1,
    height: '$navHeight',
    justifyContent: 'center',
  },

  titleText: {
    color: '$Colors.navbarTitle',
    fontWeight: 'bold',
    fontSize: 13,
    letterSpacing: 1,
    textAlign: 'center',
  },

  searchContainer: {
    height: 40,
    marginHorizontal: 15,
    marginVertical: 10,
    flexDirection: 'row',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },

  query: {
    height: 40,
    flex: 8,
    color: '$Colors.Silver',
  },

  icons: {
    margin: 10,
    textAlign: 'center',
    fontSize: 18,
    justifyContent: 'center',
    color: '$Colors.Silver',
  },

  searchIcon: {
  },

  cleanIcon: {
  },

  resultsContainer: {
    flex: 1,
    padding: 15,
    backgroundColor: '#F1F1F1',
  },

  scrollViewContent: {
    '@media android': {
      paddingBottom: 30,
    },
    '@media ios': {
      paddingBottom: 15,
    }
  },

  itemContainer: {
    paddingVertical: 5,
    paddingHorizontal: 20,
    backgroundColor: 'white',
    marginBottom: 2,
  },

  item: {
    fontSize: 16,
    color: '#3C484A',
  },

  buttonAction: {
    borderColor: '$Colors.GrayLowOpacity'
  },

  emptyText: {
    color: '$Colors.GrayLowOpacity',
    fontSize: 24,
    alignSelf: 'center',
    marginTop: '30%',
    marginBottom: 5
  },

  buttonActionText: {
    color: '$Colors.GrayLowOpacity',
    alignSelf: 'center',
    fontSize: 17,
  }
});

import React from 'react';
import { View } from 'react-native';
import R from 'ramda';
import SearchFactory from '../SearchFactory';
import styles from './styles';

// TODO: 'SEARCH_RESULT_INIT' is assigned a value but never used
// Is it right?
// const SEARCH_RESULT_INIT = { hasNextPage: false };

class SearchSelectFactory extends SearchFactory {

  renderField() {
    const { formatText, label, value, placeholder, fieldName } = this.props;
    let { label: content } = value || {};
    if (content && R.is(Function, formatText)) {
      content = formatText(content, value, true);
    } else if (!content) {
      content = placeholder;
    }

    return (
      <View key={`select_${this.name}`} style={{ flex: 1 }}>
        {this.renderLabel()}
        {this.renderContent(this._showSearch, content, [styles.fieldText])}
        {this.renderModal(label || fieldName)}
      </View>
    );
  }
}

export default SearchSelectFactory;


import React from 'react';
import { shallow } from 'enzyme';

import ModalPicker from 'react-native-modal-picker';
import SearchSelectFactory from '../';

describe('SearchSelectFactory', () => {
  const defaultProps = {
    fieldName: 'selectExample',
    onChange: () => {},
  };

  test.skip('renders base field correctly field', () => {
    const props = {
      ...defaultProps,
      value: { key: 'item1', label: 'Item1' },
      options : [
        { key: 'item1', label: 'Item1' },
        { key: 'item2', label: 'Item2' }
      ],
    };

    const wrapper = shallow(<SearchSelectFactory {...props} />);
    const label  = wrapper.find('Label');
    expect(label.children().text()).toEqual('selectExample');
    const text = wrapper.find('Text').at(0).childAt(0);
    expect(text.text()).toEqual('Item1');
  });

  test.skip('render a default initValue', () => {
    const wrapper = shallow(<SearchSelectFactory {...defaultProps} />);
    const text = wrapper.find('Text').at(0).childAt(0);
    expect(text.text()).toEqual(ModalPicker.defaultProps.initValue);
  });

  test.skip('render a custom initValue', () => {
    const props = {
      ...defaultProps,
      initValue: 'customInitValue'
    };

    const wrapper = shallow(<SearchSelectFactory {...props} />);
    const text = wrapper.find('Text').at(0).childAt(0);
    expect(text.text()).toEqual('customInitValue');
  });

  test.skip('show erros if have one', () => {
    const props = {
      ...defaultProps,
      errorMessage: 'Error message',
    };
    const wrapper = shallow(<SearchSelectFactory {...props} />);
    const text = wrapper.find('Text').at(2).childAt(0);
    expect(text.text()).toEqual(props.errorMessage);
  });
});

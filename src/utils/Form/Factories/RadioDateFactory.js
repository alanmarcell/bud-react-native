import React from 'react';
import { Text, View } from 'react-native';

import BaseFactory from './BaseFactory';

import Icon      from 'components/Icon';
import Touchable from 'utils/components/Touchable';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';

import styles from '../styles';

export class RadioDateOption extends React.PureComponent {
  static defaultProps = {
    checked: false,
    underline: true,
    label: false,
  };

  static propTypes = {
    label  : React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.bool,
    ]),
    value  : React.PropTypes.any,
    checked: React.PropTypes.bool,
    onPress: React.PropTypes.func,
  };

  state = {
    ...this.state,
    isDateTimePickerVisible: false,
    dateChangedStage: this.props.date || new Date()
  };

  renderIcon() {
    const { checked } = this.props;
    let icon, style;
    if (checked) {
      icon = 'radio_button_checked';
      style = styles._multipleOptionIconChecked;
    } else {
      icon = 'radio_button_unchecked';
      style = styles._multipleOptionIcon;
    }

    return <Icon
      name={icon}
      color={style.color}
      size={style.fontSize}
      style={style}
    />;
  }

  renderLabel() {
    const { label, children } = this.props;
    return !label ? children : <Text style={styles.multipleOptionLabel}>{label}</Text>;
  }

  renderDate() {
    let { checked } = this.props;
    let dateFormated = moment(this.state.dateChangedStage).format('L');
    return checked && <Text onPress={this._showDateTimePicker} style={styles.infoDate}>{dateFormated}</Text>;
  }

  _showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true });
  };

  _hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
  };

  _handleDatePicked = (date) => {
    let { handleOutsideState, doc } = this.props.form;
    doc.changedStageAt = date;
    handleOutsideState(doc);

    this.setState({
      dateChangedStage: date
    });

    this._hideDateTimePicker();
  };

  renderDatePicker() {
    let props = {
      date     : this.state.dateChangedStage,
      isVisible: this.state.isDateTimePickerVisible,
      onConfirm: this._handleDatePicked,
      onCancel : this._hideDateTimePicker,
    };
    return <DateTimePicker {...props}/>;
  }

  renderContainer() {
    return (
      <View>
        <View style={styles.multipleOption}>
          {this.renderIcon()}
          {this.renderLabel()}
        </View>
        <View style={styles.changeStage}>
          {this.renderDate()}
          {this.renderDatePicker()}
        </View>
      </View>
    );
  }

  render() {
    const { label, value, onPress } = this.props;
    let touchableStyle = styles.multipleOptionContainer;

    if (!label) {
      touchableStyle = null;
    }

    return (
      <Touchable
        style={touchableStyle}
        onPress={onPress ? () => onPress(value) : null }
      >
        {this.renderContainer()}
      </Touchable>
    );
  }
}

export default class RadioFactory extends BaseFactory {
  static defaultProps = {
    ...BaseFactory.defaultProps,
    options: [],
  };

  static propTypes = {
    ...BaseFactory.propTypes,
    value: React.PropTypes.any,
    options: React.PropTypes.arrayOf(
      React.PropTypes.oneOfType([
        React.PropTypes.string,
        React.PropTypes.shape({
          label: React.PropTypes.string,
          value: React.PropTypes.any
        })
      ])
    ),
  };

  renderOption = (option, index) => {
    if (typeof option === 'string') {
      option = { label: option, value: option };
    }

    if (!option || !option.label || !option.value) { return null; }
    const { value } = this.state;

    const checked = value === option.value;

    const underline = this.props.underline;

    let props = {
      ...option,
      underline,
      key: index,
      checked,
      index,
      form: this.props.form.props,
      onPress: () => this.lazyChange(option.value),
    };

    return (<RadioDateOption {...props} />);
  }

  renderField() {
    const { fieldName, options, form } = this.props;

    let opt = options.map(x => Object.assign(x, { 
      date: form.state.doc.changedStageAt
    }));

    return (
      <View key={`radio_${fieldName}`} style={styles.fieldsContainer}>
        {this.renderLabel()}
        <View style={[styles.field, styles.multipleContainer]}>
          {opt.map(this.renderOption)}
        </View>
      </View>
    );
  }
}

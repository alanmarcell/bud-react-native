import React from 'react';
import { shallow } from 'enzyme';

import { Form as SimpleReactForm } from 'simple-react-form';
import { Form, SimpleSchema } from '../';
import { Field, InputFactory } from '../Factories';

const Schema = new SimpleSchema({
  email: String,
});

describe('Form', () => {
  test('have a SimpleReactForm propTypes', () => {
    expect(Form.propTypes).toMatchObject(SimpleReactForm.propTypes);
  });

  test('render a container', () => {
    const props = {
      schema: Schema,
      containerStyle: {
        backgroundColor: 'black',
      }
    };
    const wrapper = shallow(
      <Form {...props}>
        <Field fieldName="email" type={InputFactory} />
      </Form>
    );
    expect(wrapper).toMatchSnapshot();
  });
});

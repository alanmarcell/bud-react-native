import { default as OSimpleSchema } from '@clayne/simple-schema';

export class SimpleSchema extends OSimpleSchema {
  getAllowedValuesForKey(key) {
    // For array fields, `allowedValues` is on the array item definition
    if (this.allowsKey(`${key}.$`)) {
      key = `${key}.$`;
    }

    const defs = this.getDefinition(key, ['type']);

    return defs && defs.type[0].allowedValues;
  }

  extend(...args) {
    super.extend(...args);
    return this;
  }

  static maybe = (type) => {
    return {
      type,
      required: false,
    };
  }
}

SimpleSchema.extendOptions({
  srf: Object
});

import React from 'react';
import { View, Text, } from 'react-native';

import R from 'ramda';
import moment from 'moment';

const styles = {
  container: {
    justifyContent: 'center',
    flexDirection : 'column',
  },
  text: {
    alignSelf: 'flex-end',
  },
};

export default function(props) {
  const { date, action = 'fromNow', args = [], style, containerStyle } = props;

  const momentInstance = moment(R.is(Date, date) ? date : (new Date(date)));
  return momentInstance.isValid() && (
    <View style={[styles.container, containerStyle]}>
      <Text
        accessible={true}
        allowFontScaling={true}
        ellipsizeMode="tail"
        style={[styles.text, style]}
      >
        {momentInstance[action](...args)}
      </Text>
    </View>
  );
}

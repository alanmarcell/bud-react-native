import 'react-native';
import React from 'react';
import Moment from '../';
import { Text } from 'react-native';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';

import moment from 'moment';

test('renders correctly', () => {
  const date = new Date();
  const tree = renderer.create(<Moment date={date} /> ).toJSON();
  expect(tree).toMatchSnapshot();
});

test('renders date fromNow', () => {
  const date = new Date();
  const component = shallow(<Moment date={date} />);
  expect(component.find(Text).children().text()).toEqual(moment(date).fromNow());
});

test('renders date using format', () => {
  const date = new Date();
  const component = shallow(<Moment date={date} action="format" args={['L']} />);
  expect(component.find(Text).children().text()).toEqual(moment(date).format('L'));
});

test('renders date using format without args', () => {
  const date = new Date();
  const component = shallow(<Moment date={date} action="format" />);
  expect(component.find(Text).children().text()).toEqual(moment(date).format());
});

test('renders null component when date is invalid', () => {
  const date = 'invalid';
  const component = shallow(<Moment date={date} />);
  expect(component.type()).toEqual(null);
});

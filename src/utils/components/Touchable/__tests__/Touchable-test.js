import 'react-native';
import React from 'react';
import Touchable from '../';
import renderer from 'react-test-renderer';

test('renders correctly', () => {
  const tree = renderer.create(<Touchable onPress={() => {}}>{'content'}</Touchable>).toJSON();
  expect(tree).toMatchSnapshot();
});

test('renders correctly without onPress', () => {
  const tree = renderer.create(<Touchable>{'content'}</Touchable>).toJSON();
  expect(tree).toMatchSnapshot();
});

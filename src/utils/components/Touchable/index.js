import React from 'react';
import {
  Animated,
  TouchableOpacity,
} from 'react-native';

export default class Touchable extends React.Component {
  static defaultProps = {
    disabled: false,
  };

  static propTypes = {
    disabled: React.PropTypes.bool,
    style   : React.PropTypes.any,
    onPress : React.PropTypes.func,
    children: React.PropTypes.any,
  }

  _onPress = (evt) => {
    if (!this.props.disabled) {
      this.props.onPress(evt);
    }
  }

  render() {
    const { children, ...props } = this.props;

    let type  = Animated.View;
    let child = children;

    if (typeof props.onPress === 'function' && !props.disabled) {
      type = TouchableOpacity;
      props.onPress = this._onPress;
    }

    return React.createElement(type, props, child);
  }
}

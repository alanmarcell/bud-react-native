import React from 'react';
import { RefreshControl } from 'react-native';

export default ({ loading, refetch }) => (
  <RefreshControl
    refreshing={loading}
    onRefresh={refetch}
    tintColor="#A7E1C7"
    title="Loading..."
    titleColor="#A7E1C7"
    colors={['#7AD3AB', '#24B574', '#360968']}
    progressBackgroundColor="#A7E1C7"
    progressViewOffset={0}
  />
);



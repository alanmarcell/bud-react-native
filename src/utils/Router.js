import createRouter from 'router5';
import URL from 'url-parse';

export default class Router {
  constructor(options) {
    this.options = options;
  }

  get hosts() {
    const { hostname, alias } = this.options;
    return [hostname, ...alias];
  }

  get router5() {
    if (!this._router5) {
      const { routes } = this.options;
      this._router5 = createRouter(routes, {
        trailingSlash: true,
      });
    }
    return this._router5;
  }

  build(route, params, full = false) {
    const { hostname } = this.options;
    const path = this.router5.buildPath(route, params);
    return `${full ? 'https://' : ''}${hostname}${path}`;
  }

  process(uri) {
    if(uri){
      uri = uri.trim();
    }

    const { preprocess } = this.options;

    // Pre process
    if (preprocess) {
      uri = preprocess(uri);
    }

    const url = new URL(uri, '/');

    const result = this.router5.matchPath(url.pathname);
    return result || { error: 'invalid' + url.pathname };
  }
}

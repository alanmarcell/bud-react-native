import RNFS from 'react-native-fs';
import { Platform } from 'react-native';

function readFile(filename) {
  __DEV__ && console.log('reading file', filename); // eslint-disable-line no-console
  if (Platform.OS === 'android') {
    return RNFS.readFileAssets(filename);
  } else {
    return RNFS.readFile(RNFS.MainBundlePath + '/' + filename);
  }
}

async function readJson(filename) {
  const content = await readFile(filename + '.json');
  __DEV__ && console.log('parsing file', filename);  // eslint-disable-line no-console
  return JSON.parse(content);
}

export {
  readFile,
  readJson
};


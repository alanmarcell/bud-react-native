import R from 'ramda';
import I18n from 'react-native-i18n';
import { Globalize } from 'react-native-globalize';

const deviceLocale = I18n.locale;
export { deviceLocale };

switch (true) {
  case /^en/.test(deviceLocale):
    require('moment/locale/en-gb');
    break;
  case /^pt/.test(deviceLocale):
    require('moment/locale/pt-br');
    break;
  case /^es/.test(deviceLocale):
    require('moment/locale/es');
    break;
  default:
    require('moment/locale/en-gb');
}

I18n.fallbacks = true;
I18n.translations = require('../translations');
if (!__DEV__) {
  I18n.defaultLocale = 'en';
}

// Globalize to support numbers
const globalize = new Globalize(deviceLocale, null, { fallback: true });
export { globalize };

export function parseNumber(value) {

  if (R.is(Number, value)) {
    return value;
  }

  return Number(value.replace(/\,/g, '.'));
}

export function formatNumber(value, options = {}) {
  options = { minimumFractionDigits: 2, ...options };
  return globalize.globalize.formatNumber(value, options);
}


let NOT_FOUND = [];

const addNotFound = scope => {
  const _scope = scope.toString();
  if(!R.contains(_scope, NOT_FOUND)){
    NOT_FOUND = NOT_FOUND.concat(_scope);
    __DEV__ && console.log('>> Not found translations: ', NOT_FOUND); // eslint-disable-line no-console
  }
};

const removeNotFound = scope => {
  delete NOT_FOUND[scope];
};

I18n.t = function (scope, ...args) {
  let result = this.translate(scope, ...args);
  if (R.is(Object, result)) {
    if(!R.isNil(result._default)){
      return result._default;
    }

    addNotFound(scope);
    return `[i18n] Missing translation for ${scope}`;
  } else {
    if (R.startsWith('[missing "', result)) {
      addNotFound(scope);
    } else {
      removeNotFound(scope);
    }

    return result;
  }
};

I18n.T = function (...args) {
  return this.t(...args).toUpperCase();
};

export { I18n };

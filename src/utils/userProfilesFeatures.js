const featuresByProfile = {
  GROWER: {
    tasks: true,
    mixes: true,
    plants: true,
    buds: true,
    quickId: true
  },
  CONSUMER: {
    tasks: false,
    mixes: false,
    plants: false,
    buds: true,
    quickId: true
  },
  GARDENER: {
    tasks: true,
    mixes: true,
    plants: true,
    buds: true,
    quickId: true
  },
  CLUB: {
    tasks: true,
    mixes: true,
    plants: true,
    buds: true,
    quickId: true
  }
};

module.exports = featuresByProfile;


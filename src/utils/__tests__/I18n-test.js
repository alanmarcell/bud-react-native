import { I18n, parseNumber } from '../I18n';

// I18n configure
I18n.fallbacks = true;
I18n.translations = {
  en: {
    label: 'english msg',
    notSet: 'english',
    subscope: {
      _default: 'default value',
      oneKey: 'oneValue',
    },
    object: {
      key: 'value',
    },
  },
  pt: {
    label: 'português msg',
  },
};

describe('I18n', () => {
  test('add T fn to translate and form with upper case', () => {
    const down  = I18n.t('label');
    const upper = I18n.T('label');
    expect(upper).toEqual(down.toUpperCase());
  });

  test.skip('add support to _default clouse', () => {
    const text = I18n.t('subscope');
    expect(text).toEqual('default value');
  });

  test.skip('missing translate for find scope but is a object', () => {
    const missing = I18n.t('object');
    expect(missing).toEqual('[i18n] Missing translation for object');
  });
});

describe('Parse Number', () => {
  test('with comma or dot', () => {
    const numWithComa = '1,100';
    const numWithDot = '1.100';
    
    const comaParsed = parseNumber(numWithComa);
    const dotParsed = parseNumber(numWithDot);
    
    expect(comaParsed).toEqual(1.1);
    expect(dotParsed).toEqual(1.1);
  });
});

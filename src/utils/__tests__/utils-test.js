import U from '../';

test('test if value as a blank (empty or null)', () => {
  // Blanks
  expect(U.isBlank(null)).toBeTruthy();
  expect(U.isBlank()).toBeTruthy();
  expect(U.isBlank(undefined)).toBeTruthy();
  expect(U.isBlank('')).toBeTruthy();
  expect(U.isBlank({})).toBeTruthy();
  expect(U.isBlank([])).toBeTruthy();

  // Not blanks
  expect(U.isBlank(1)).toBeFalsy();
  expect(U.isBlank([1])).toBeFalsy();
  expect(U.isBlank(' hello ')).toBeFalsy();
});

test('deep merge', () => {
  const a = { b: { c: 1, f: 3 }};
  const d = { b: { c: 2 }, e: 3};
  const r = U.deepMerge(a, d, { g: 4});
  expect(r.b.c).toEqual(2);
  expect(r.b.f).toEqual(3);
  expect(r.e).toEqual(3);
  expect(r.g).toEqual(4);
  expect(r).toMatchSnapshot();
});

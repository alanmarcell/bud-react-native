import Router from 'utils/Router';
import NativeModules from 'NativeModules';
import URL from 'url-parse';
import { CODENAME, API_URL_DEBUG, API_URL_RELEASE } from 'babel-dotenv';

const Config = {
  env: __DEV__ ? 'dev' : 'prod',

  router: new Router({
    hostname: 'budbuds.us',
    alias: ['bbu.uy'],
    preprocess: (url) => {
      // https://regex101.com/r/4EjUqj/1/
      const fixrgOld = /^https?:\/\/bbu\.uy\/([a-zA-Z\d]*)$/;
      url = url.replace(fixrgOld, 'https://bbu.uy/q/$1');

      // https://regex101.com/r/knH8d2/1
      const fixrgWrong = /^https?:\/\/budbuds\.us\/q\/[a-zA-Z\d]*\;([a-zA-Z\d]*)$/;
      url = url.replace(fixrgWrong, 'https://budbuds.us/q/$1');

      if (url.indexOf('/') === -1) {
        url = `https://budbuds.us/q/${url}`;
      }

      return url;
    },
    routes: [
      { name: 'budQuickID'  , path: '/:userNick/b/:prodNick'   },
      { name: 'plantQuickID', path: '/:userNick/p/:prodNick' },
      { name: 'cupQuickID', path: '/:userNick/c/:prodNick' },
      { name: 'qrCode'      , path: '/q/:qrCode' },
      { name: 'qrCodeCustom', path: '/:qrCode' },
    ]
  }),

  RavenUrl: 'https://7afaf92ee6264a989cc4668fcb7cfd7b@sentry.io/144290',

  ui: {
    DEFAULT_LIST_SIZE: 20,
  },

  // Api general options
  api: {
    timeout_max: 10000,
    codename: CODENAME,
    get baseURL() {
      const url = __DEV__ ? API_URL_DEBUG : API_URL_RELEASE;
      if (url === 'auto') {
        const bundleServer = new URL(NativeModules.SourceCode.scriptURL);
        return `http://${bundleServer.hostname}:3000/graphql`;
      } else {
        return url;
      }
    }
  },
};

export default Config;

const router = Config.router;
export { router };


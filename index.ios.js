import { AppRegistry } from 'react-native';

global.__IOS__     = true;
global.__ANDROID__ = false;

import App from './src/main';
// import App from './scripts/lab/fs';
AppRegistry.registerComponent('BudBuds', () => App);
